#--职称申报方式
INSERT INTO `cf_human_resource`.`t_dictionary` (`id`, `type`, `dickey`, `value`, `memo`, `sort`, `create_date`, `create_user`, `update_date`, `update_user`)
VALUES ('fde5ea246cf0482c8ff358c76bdce0b0', 'RankApplyType', '1001', '认定', '职称申报方式', 1, NULL, NULL, NULL, NULL);
INSERT INTO `cf_human_resource`.`t_dictionary` (`id`, `type`, `dickey`, `value`, `memo`, `sort`, `create_date`, `create_user`, `update_date`, `update_user`)
VALUES ('fde5ea246cf0482c8ff358c76bdce0b5', 'RankApplyType', '1002', '评审', '职称申报方式', 2, NULL, NULL, NULL, NULL);

#-- 认定/申报等级 高级工程师、中级工程师、助理工程师、技术员
INSERT INTO `cf_human_resource`.`t_dictionary` (`id`, `type`, `dickey`, `value`, `memo`, `sort`, `create_date`, `create_user`, `update_date`, `update_user`)
VALUES ('fde5ea246cf0482c8ff358c76bdce0c0', 'RankLevel', '2001', '高级工程师', '认定/申报等级', 4, NULL, NULL, NULL, NULL);
INSERT INTO `cf_human_resource`.`t_dictionary` (`id`, `type`, `dickey`, `value`, `memo`, `sort`, `create_date`, `create_user`, `update_date`, `update_user`)
VALUES ('fde5ea246cf0482c8ff358c76bdce0c3', 'RankLevel', '2002', '中级工程师', '认定/申报等级', 3, NULL, NULL, NULL, NULL);
INSERT INTO `cf_human_resource`.`t_dictionary` (`id`, `type`, `dickey`, `value`, `memo`, `sort`, `create_date`, `create_user`, `update_date`, `update_user`)
VALUES ('fde5ea246cf0482c8ff358c76bdce0c4', 'RankLevel', '2003', '助理工程师', '认定/申报等级', 2, NULL, NULL, NULL, NULL);
INSERT INTO `cf_human_resource`.`t_dictionary` (`id`, `type`, `dickey`, `value`, `memo`, `sort`, `create_date`, `create_user`, `update_date`, `update_user`)
VALUES ('fde5ea246cf0482c8ff358c76bdce0c5', 'RankLevel', '2004', '技术员', '认定/申报等级', 1, NULL, NULL, NULL, NULL);

#--网站
INSERT INTO `cf_human_resource`.`t_dictionary` (`id`, `type`, `dickey`, `value`, `memo`, `sort`, `create_date`, `create_user`, `update_date`, `update_user`)
VALUES ('aaaaea246cf0482c8ff358c76bdce0c1', 'WebSite', '0001', '企业网', '网站', 1, NULL, NULL, NULL, NULL);
INSERT INTO `cf_human_resource`.`t_dictionary` (`id`, `type`, `dickey`, `value`, `memo`, `sort`, `create_date`, `create_user`, `update_date`, `update_user`)
VALUES ('aaaaea246cf0482c8ff358c76bdce0c2', 'WebSite', '0002', '人才网', '网站', 2, NULL, NULL, NULL, NULL);
INSERT INTO `cf_human_resource`.`t_dictionary` (`id`, `type`, `dickey`, `value`, `memo`, `sort`, `create_date`, `create_user`, `update_date`, `update_user`)
VALUES ('aaaaea246cf0482c8ff358c76bdce0c3', 'WebSite', '0003', '网站三', '网站', 3, NULL, NULL, NULL, NULL);


