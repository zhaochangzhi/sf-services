#--行业
INSERT INTO `cf_human_resource`.`t_rank_major` (`id`, `parent_id`, `name`, `level`, `sort`)
VALUES ('abc5ea246cf0482c8ff358c76bdce0a0', NULL, '建设行业', 1, 1);
INSERT INTO `cf_human_resource`.`t_rank_major` (`id`, `parent_id`, `name`, `level`, `sort`)
VALUES ('abc5ea246cf0482c8ff358c76bdce0a1', NULL, '交通行业', 1, 2);
INSERT INTO `cf_human_resource`.`t_rank_major` (`id`, `parent_id`, `name`, `level`, `sort`)
VALUES ('abc5ea246cf0482c8ff358c76bdce0a2', NULL, '水利行业', 1, 3);
INSERT INTO `cf_human_resource`.`t_rank_major` (`id`, `parent_id`, `name`, `level`, `sort`)
VALUES ('abc5ea246cf0482c8ff358c76bdce0a3', NULL, '生态环境行业', 1, 4);
INSERT INTO `cf_human_resource`.`t_rank_major` (`id`, `parent_id`, `name`, `level`, `sort`)
VALUES ('abc5ea246cf0482c8ff358c76bdce0a4', NULL, '其它', 1, 5);

#--专业
INSERT INTO `cf_human_resource`.`t_rank_major` (`id`, `parent_id`, `name`, `level`, `sort`)
VALUES
#-- 建设行业
('aaa5ea246cf0482c8ff358c76bdce001', 'abc5ea246cf0482c8ff358c76bdce0a0', '建筑学', 2, 1),
('aaa5ea246cf0482c8ff358c76bdce002', 'abc5ea246cf0482c8ff358c76bdce0a0', '城市规划', 2, 2),
('aaa5ea246cf0482c8ff358c76bdce003', 'abc5ea246cf0482c8ff358c76bdce0a0', '建筑装饰', 2, 3),
('aaa5ea246cf0482c8ff358c76bdce004', 'abc5ea246cf0482c8ff358c76bdce0a0', '城市景观与风景园林', 2, 4),
('aaa5ea246cf0482c8ff358c76bdce005', 'abc5ea246cf0482c8ff358c76bdce0a0', '结构工程', 2, 5),
('aaa5ea246cf0482c8ff358c76bdce006', 'abc5ea246cf0482c8ff358c76bdce0a0', '道路桥隧', 2, 6),
('aaa5ea246cf0482c8ff358c76bdce007', 'abc5ea246cf0482c8ff358c76bdce0a0', '工程地质', 2, 7),
('aaa5ea246cf0482c8ff358c76bdce008', 'abc5ea246cf0482c8ff358c76bdce0a0', '给排水', 2, 8),
('aaa5ea246cf0482c8ff358c76bdce009', 'abc5ea246cf0482c8ff358c76bdce0a0', '采暖通风', 2, 9),
('aaa5ea246cf0482c8ff358c76bdce010', 'abc5ea246cf0482c8ff358c76bdce0a0', '建筑电气', 2, 10),
('aaa5ea246cf0482c8ff358c76bdce011', 'abc5ea246cf0482c8ff358c76bdce0a0', '建筑机械', 2, 11),
('aaa5ea246cf0482c8ff358c76bdce012', 'abc5ea246cf0482c8ff358c76bdce0a0', '工程概预算', 2, 12),
('aaa5ea246cf0482c8ff358c76bdce013', 'abc5ea246cf0482c8ff358c76bdce0a0', '建筑施工', 2, 13),
('aaa5ea246cf0482c8ff358c76bdce014', 'abc5ea246cf0482c8ff358c76bdce0a0', '建筑设备安装', 2, 14),
('aaa5ea246cf0482c8ff358c76bdce015', 'abc5ea246cf0482c8ff358c76bdce0a0', '煤气热力', 2, 15),
('aaa5ea246cf0482c8ff358c76bdce016', 'abc5ea246cf0482c8ff358c76bdce0a0', '建设管理', 2, 16),
#--交通行业
('aaa5ea246cf0482c8ff358c76bdce017', 'abc5ea246cf0482c8ff358c76bdce0a1', '道路与桥梁工程', 2, 17),
('aaa5ea246cf0482c8ff358c76bdce018', 'abc5ea246cf0482c8ff358c76bdce0a1', '汽车运用工程', 2, 18),
('aaa5ea246cf0482c8ff358c76bdce019', 'abc5ea246cf0482c8ff358c76bdce0a1', '筑养路机械', 2, 19),
('aaa5ea246cf0482c8ff358c76bdce020', 'abc5ea246cf0482c8ff358c76bdce0a1', '港口与航道工程', 2, 20),
('aaa5ea246cf0482c8ff358c76bdce021', 'abc5ea246cf0482c8ff358c76bdce0a1', '船舶运用工程', 2, 21),
('aaa5ea246cf0482c8ff358c76bdce022', 'abc5ea246cf0482c8ff358c76bdce0a1', '交通工程', 2, 22),
('aaa5ea246cf0482c8ff358c76bdce023', 'abc5ea246cf0482c8ff358c76bdce0a1', '地方铁路工程以及轨道交通', 2, 23),
#--水利行业
('aaa5ea246cf0482c8ff358c76bdce024', 'abc5ea246cf0482c8ff358c76bdce0a2', '水利水电工程', 2, 24),
('aaa5ea246cf0482c8ff358c76bdce025', 'abc5ea246cf0482c8ff358c76bdce0a2', '水文与水资源工程', 2, 25),
('aaa5ea246cf0482c8ff358c76bdce026', 'abc5ea246cf0482c8ff358c76bdce0a2', '河流泥沙及治河工程', 2, 26),
('aaa5ea246cf0482c8ff358c76bdce027', 'abc5ea246cf0482c8ff358c76bdce0a2', '农业水利工程', 2, 27),
('aaa5ea246cf0482c8ff358c76bdce028', 'abc5ea246cf0482c8ff358c76bdce0a2', '水土保持与荒漠化防治', 2, 28),
('aaa5ea246cf0482c8ff358c76bdce029', 'abc5ea246cf0482c8ff358c76bdce0a2', '水文地质与工程测绘', 2, 29),
('aaa5ea246cf0482c8ff358c76bdce030', 'abc5ea246cf0482c8ff358c76bdce0a2', '水利自动化与信息化', 2, 30),
#--生态环境行业
('aaa5ea246cf0482c8ff358c76bdce031', 'abc5ea246cf0482c8ff358c76bdce0a3', '生态环境管理', 2, 31),
('aaa5ea246cf0482c8ff358c76bdce032', 'abc5ea246cf0482c8ff358c76bdce0a3', '生态环境科研', 2, 32),
('aaa5ea246cf0482c8ff358c76bdce033', 'abc5ea246cf0482c8ff358c76bdce0a3', '生态环境监测', 2, 33),
('aaa5ea246cf0482c8ff358c76bdce034', 'abc5ea246cf0482c8ff358c76bdce0a3', '生态环境工程', 2, 34),
#--其它
('aaa5ea246cf0482c8ff358c76bdce035', 'abc5ea246cf0482c8ff358c76bdce0a4', '矿业', 2, 35),
('aaa5ea246cf0482c8ff358c76bdce036', 'abc5ea246cf0482c8ff358c76bdce0a4', '冶金', 2, 36),
('aaa5ea246cf0482c8ff358c76bdce037', 'abc5ea246cf0482c8ff358c76bdce0a4', '纺织', 2, 37),
('aaa5ea246cf0482c8ff358c76bdce038', 'abc5ea246cf0482c8ff358c76bdce0a4', '机械', 2, 38),
('aaa5ea246cf0482c8ff358c76bdce039', 'abc5ea246cf0482c8ff358c76bdce0a4', '电子', 2, 39),
('aaa5ea246cf0482c8ff358c76bdce040', 'abc5ea246cf0482c8ff358c76bdce0a4', '电气', 2, 40),
('aaa5ea246cf0482c8ff358c76bdce041', 'abc5ea246cf0482c8ff358c76bdce0a4', '化工', 2, 41),
('aaa5ea246cf0482c8ff358c76bdce042', 'abc5ea246cf0482c8ff358c76bdce0a4', '材料', 2, 42);







