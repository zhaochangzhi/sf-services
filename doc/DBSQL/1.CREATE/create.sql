CREATE TABLE `cf_human_resource`.`t_rank_apply`  (
                                            `id` varchar(32) NOT NULL COMMENT '主键',
                                            `rank_code` varchar(50) NULL COMMENT '职称码',
                                            `rank_apply_type` varchar(20) NULL COMMENT '职称申报方式',
                                            `apply_user_id` varchar(255) NULL COMMENT '申请人id',
                                            `apply_user_account` varchar(50) NULL COMMENT '申请人用户名(系统)',
                                            `apply_user_name` varchar(20) NULL COMMENT '申请人姓名',
                                            `apply_user_sex` varchar(1) NULL COMMENT '性别 1男2女',
                                            `apply_user_id_number` varchar(20) NULL COMMENT '申请人身份证号',
                                            `apply_user_birthday` varchar(20) NULL COMMENT '申请人出生日期',
                                            `apply_user_phone` varchar(50) NULL COMMENT '申请人手机号',
                                            `apply_user_education` varchar(20) NULL COMMENT '申请人学历',
                                            `apply_user_education2` varchar(20) NULL COMMENT '申请人第二学历',
                                            `apply_user_education3` varchar(20) NULL COMMENT '申请人第三学历',
                                            `apply_user_company_full_name` varchar(200) NULL COMMENT '所属单位全称',
                                            `identify_major` varchar(20) NULL COMMENT '认定专业',
                                            `identify_level` varchar(20) NULL COMMENT '认定等级',
                                            `obtained_credentials` varchar(200) NULL COMMENT '已取得职称证书专业（等级）',
                                            `review_major` varchar(20) NULL COMMENT '申报专业所属行业（专业）',
                                            `review_level` varchar(20) NULL COMMENT '申报等级',
                                            `paper_sum` int NULL COMMENT '论文数量',
                                            `audit_status` tinyint NULL DEFAULT 1 COMMENT '审核状态：1 审核中，2 通过，3 不通过',
                                            `audit_user_id` varchar(255) NULL COMMENT '审核人id',
                                            `audit_user_name` varchar(20) NULL COMMENT '审核人姓名',
                                            `audit_time` datetime(0) NULL COMMENT '审核时间',
                                            `audit_comment` varchar(255) NULL COMMENT '审核备注',
                                            `appointment_start_time` datetime(0) NULL COMMENT '预约办理开始时间',
                                            `appointment_end_time` datetime(0) NULL COMMENT '预约办理结束时间',
                                            `create_user` varchar(255) NULL COMMENT '创建人id',
                                            `create_time` datetime(0) NULL COMMENT '创建时间',
                                            `update_user` varchar(255) NULL COMMENT '更新人id',
                                            `update_time` datetime(0) NULL COMMENT '更新时间',
                                            PRIMARY KEY (`id`)
) COMMENT = '职称申请表';

CREATE TABLE `cf_human_resource`.`t_rank`  (
                                            `id` int NOT NULL COMMENT '主键',
                                            `rank_code` varchar(50) NULL COMMENT '职称码',
                                            `rank_name` varchar(50) NULL COMMENT '职称名称',
                                            `address` varchar(255) NULL COMMENT '办理地址',
                                            `attachment_id` varchar(32) NULL COMMENT '附件id',
                                            `create_user` varchar(255) NULL COMMENT '创建人',
                                            `create_time` datetime(0) NULL COMMENT '创建时间',
                                            `update_user` varchar(255) NULL COMMENT '更新人',
                                            `update_time` datetime(0) NULL COMMENT '更新时间',
                                            PRIMARY KEY (`id`)
) COMMENT = '职称表';

ALTER TABLE `cf_human_resource`.`t_rank`
    MODIFY COLUMN `id` int NOT NULL AUTO_INCREMENT COMMENT '主键' FIRST;

CREATE TABLE `cf_human_resource`.`t_rank_file`  (
                                            `id` varchar(32) NOT NULL,
                                            `rank_code` varchar(50) NOT NULL COMMENT 't_rank.rank_code',
                                            `attachment_id` varchar(32) NOT NULL COMMENT 't_file.file_id',
                                            PRIMARY KEY (`id`)
) COMMENT = '职称文件关联表';


CREATE TABLE `cf_human_resource`.`t_rank_apply_file`  (
                                            `id` varchar(32) NOT NULL COMMENT '主键',
                                            `rank_apply_id` varchar(32) NULL COMMENT '职称申请id',
                                            `file_id` varchar(32) NULL COMMENT '文件id',
                                            PRIMARY KEY (`id`)
) COMMENT = '职称申请文件关联表';

CREATE TABLE `cf_human_resource`.`t_rank_major`  (
                                            `id` varchar(32) NOT NULL,
                                            `parent_id` varchar(32) NULL COMMENT '父级id',
                                            `name` varchar(50) NOT NULL COMMENT '名称',
                                            `level` tinyint NOT NULL COMMENT '级别',
                                            `sort` tinyint NULL COMMENT '排序',
                                            PRIMARY KEY (`id`)
) COMMENT = '行业专业表';

CREATE TABLE `cf_human_resource`.`t_web_section`  (
                                            `id` varchar(32) NOT NULL,
                                            `web_site_key` varchar(10) NOT NULL COMMENT '所属网站字典key',
                                            `name` varchar(10) NOT NULL COMMENT '栏目名称',
                                            `create_user` varchar(32) NULL,
                                            `create_time` datetime(0) NULL,
                                            `update_user` varchar(32) NULL,
                                            `update_time` datetime(0) NULL,
                                            `deleted` tinyint NULL DEFAULT 0 COMMENT '逻辑删除位，默认0可用，1删除',
                                            PRIMARY KEY (`id`)
) COMMENT = '网站栏目表';

CREATE TABLE `cf_human_resource`.`t_job_fair`  (
                                            `id` varchar(32) NOT NULL,
                                            `title` varchar(255) NULL COMMENT '标题',
                                            `sponsor` varchar(255) NULL COMMENT '主办方',
                                            `start_time` datetime(0) NULL COMMENT '开始时间',
                                            `end_time` datetime(0) NULL COMMENT '结束时间',
                                            `address` varchar(255) NULL COMMENT '举办地址',
                                            `contacts` varchar(20) NULL COMMENT '联系人',
                                            `contacts_phone` varchar(50) NULL COMMENT '联系方式',
                                            `image_id` varchar(50) NULL COMMENT '推广图片id',
                                            `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态：0 不可用，1可用',
                                            `create_user` varchar(32) NULL COMMENT '创建人',
                                            `create_time` datetime(0) NULL COMMENT '创建时间',
                                            `update_user` varchar(32) NULL COMMENT '更新人',
                                            `update_time` datetime(0) NULL COMMENT '更新时间',
                                            `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '删除状态，默认0 不删除，1 已删除',
                                            PRIMARY KEY (`id`)
) COMMENT = '招聘会表';




