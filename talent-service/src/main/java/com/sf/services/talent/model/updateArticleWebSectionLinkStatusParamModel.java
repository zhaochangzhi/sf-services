package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "栏目关联-更新文章关联栏目标识")
@Data
public class updateArticleWebSectionLinkStatusParamModel {

    @ApiModelProperty(value = "文章id", example = "0080223c5da54529938064447e70addf")
    private String articleId;

    @ApiModelProperty(value = "关联栏目标识（0：未开启，1：已开启）", example = "1")
    private Integer webSectionLinkStatus;



}