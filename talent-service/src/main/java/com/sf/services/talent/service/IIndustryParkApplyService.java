package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.IndustryParkApplyAddParamModel;
import com.sf.services.talent.model.IndustryParkApplyAuditParamModel;
import com.sf.services.talent.model.IndustryParkApplyListParamModel;
import com.sf.services.talent.model.vo.IndustryParkApplyVO;

/**
 * @Description: 课程
 * @author: guchangliang
 * @date: 2021/8/13
 */
public interface IIndustryParkApplyService {

    /**
     * 获取列表
     * @param params 参数
     * @return 列表
     */
    PageInfo<IndustryParkApplyVO> getList(IndustryParkApplyListParamModel params);


    /**
     * 获取详情
     * @param id 参数
     * @return 列表
     */
    BaseResultModel get(String id);


    /**
     * 申请入园
     * @param params 参数
     * @param userId
     * @return
     */
    BaseResultModel add(IndustryParkApplyAddParamModel params
            , String userId);

    /**
     * 申请入园-审核
     * @param params 参数
     * @param userId
     * @return
     */
    BaseResultModel audit(IndustryParkApplyAuditParamModel params
            , String userId);

    /**
     * 查询最后一条数据
     * @param createUser 参数
     * @return 列表
     */
    BaseResultModel getLast(String createUser);
}

