package com.sf.services.talent.controller;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.annotations.CurrentUser;
import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.dto.UserDto;
import com.sf.services.talent.model.vo.EmploymentPositionVO;
import com.sf.services.talent.service.IEnterprisePositionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Slf4j
@Api(value = "企业中心-职位管理", tags = "企业中心-职位管理")
@RestController
@RequestMapping("/enterprisePosition")
public class EnterprisePositionController {
	
	@Resource
	private IEnterprisePositionService enterprisePositionService;

	@ApiOperation(value = "企业中心-职位管理列表", notes = "企业中心-职位管理列表")
	@PostMapping("/list")
	@IgnoreSecurity
	public BaseResultModel list(@RequestBody EmploymentPositionParamModel params) {


		PageInfo<EmploymentPositionVO> pageInfo = enterprisePositionService.getList(params);
		return BaseResultModel.success(pageInfo);
	}

	@ApiOperation(notes = "企业中心-职位管理-创建职位", value="企业中心-职位管理-创建职位")
	@PostMapping("/add")
	public BaseResultModel add(@RequestBody EmploymentPositionAddParamModel param
			, @CurrentUser UserDto currentUser) {

		return enterprisePositionService.add(param, currentUser.getUserId());
	}

	@ApiOperation(notes = "企业中心-职位管理-更新职位", value="企业中心-职位管理-更新职位")
	@PostMapping("/update")
	public BaseResultModel update(@RequestBody EmploymentPositionUpdateParamModel param
			, @CurrentUser UserDto currentUser) {

		return enterprisePositionService.update(param, currentUser.getUserId());
	}

	@ApiOperation(notes = "企业中心-职位管理-审核", value="企业中心-职位管理-审核")
	@PostMapping("/audit")
	public BaseResultModel audit(@RequestBody EmploymentPositionAuditParamModel param
			, @CurrentUser UserDto currentUser) {

		return enterprisePositionService.audit(param, currentUser.getUserId());
	}

	@ApiOperation(notes = "企业中心-职位管理-批量审核", value="企业中心-职位管理-批量审核")
	@PostMapping("/batchAudit")
	public BaseResultModel batchAudit(@RequestBody EmploymentPositionBatchAuditParamModel param
			, @CurrentUser UserDto currentUser) {

		return enterprisePositionService.batchAudit(param, currentUser.getUserId());
	}

	@ApiOperation(notes = "企业中心-职位管理-刪除职位", value="企业中心-职位管理-删除职位")
	@PostMapping("/delete")
	public BaseResultModel delete(@RequestBody EmploymentPositionDeleteParamModel param
			, @CurrentUser UserDto currentUser) {

		return enterprisePositionService.delete(param, currentUser.getUserId());
	}

	@ApiOperation(notes = "企业中心-职位管理-批量刪除职位", value="企业中心-职位管理-批量删除职位")
	@PostMapping("/batchDelete")
	public BaseResultModel batchDelete(@RequestBody IdsParamModel param
			, @CurrentUser UserDto currentUser) {

		return enterprisePositionService.batchDelete(param, currentUser.getUserId());
	}

	@ApiOperation(value = "企业中心-职位管理-职位详情", notes = "企业中心-职位管理-职位详情")
	@PostMapping("/detail")
	@IgnoreSecurity
	public BaseResultModel detail(@RequestBody EmploymentPositionDetailParamModel params) {


		EmploymentPositionVO employmentPositionVO = enterprisePositionService.getDetail(params);
		return BaseResultModel.success(employmentPositionVO);
	}

}

