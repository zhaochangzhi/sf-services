package com.sf.services.talent.mapper;

import com.sf.services.talent.model.ChatOnlineFriendsListParamModel;
import com.sf.services.talent.model.po.ChatOnlineFriends;
import com.sf.services.talent.model.vo.ChatOnlineFriendsVO;

import java.util.List;
import java.util.Map;

public interface ChatOnlineFriendsMapper {
    int deleteByPrimaryKey(String id);

    int insert(ChatOnlineFriends record);

    int insertSelective(ChatOnlineFriends record);

    ChatOnlineFriends selectByPrimaryKey(String id);

    int selectPageListCount(Map<String, Object> searchMap);

    List<ChatOnlineFriends> selectPageList(Map<String, Object> searchMap);

    int updateByPrimaryKeySelective(ChatOnlineFriends record);

    int updateByPrimaryKey(ChatOnlineFriends record);

    //查询列表
    List<ChatOnlineFriendsVO> selectList(ChatOnlineFriendsListParamModel record);

}