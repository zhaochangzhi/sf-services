package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 *
 * @author guchangliang
 * @date 2021/9/22
 */
@ApiModel(value = "JobUserAddParamModel", description = "招聘会-个人用户报名参会")
@Data
public class JobUserAddParamModel {

    @ApiModelProperty(value = "主键id（t_job_fair表id）")
    private String JobFairId;

    @ApiModelProperty(value = "会员memberId（t_member表member_id）")
    private String memberId;

}
