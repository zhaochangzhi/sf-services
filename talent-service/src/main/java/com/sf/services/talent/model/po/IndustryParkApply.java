package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

@Data
public class IndustryParkApply {
    private String id;

    private String enterpriseName;

    private String legalRepresentativeName;

    private Date registTime;

    private String registAddress;

    private String registCapital;

    private String actualCapital;

    private String contactName;

    private String contactMobile;

    private Integer enterpriseStatus;

    private String enterpriseIntroduction;

    private Integer servicePersonTimes;

    private String existingCustomers;

    private String existingCustomersIndustry;

    private String lastyearBusinessIncome;

    private String lastyearBusinessPayduty;

    private Integer lastyearPersonTimes;

    private String joinReasons;

    private String joinBusiness;

    private Integer isGetLwpqjyxkz;

    private Integer isGetRlzyfwxkz;

    private String customerResources;

    private String expectBusiness;

    private String expectYearIncome;

    private String expectYearPayduty;

    private Integer expectYearPersonTimes;

    private String applyWorkArea;

    private Integer isNeedHandleLwpqjyxkz;

    private Integer isNeedHandleRlzyfwxkz;

    private String workAddress;

    private String workMobile;

    private Integer auditStatus;

    private String auditReason;

    private Date auditTime;

    private String auditUser;

    private Integer sort;

    private String remark;

    private Date createTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;

}