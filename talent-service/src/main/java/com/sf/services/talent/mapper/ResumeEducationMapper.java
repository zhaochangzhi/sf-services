package com.sf.services.talent.mapper;

import java.util.List;

import com.sf.services.talent.model.po.ResumeEducation;

public interface ResumeEducationMapper {
    int deleteByPrimaryKey(String id);

    int insert(ResumeEducation record);

    int insertSelective(ResumeEducation record);

    List<ResumeEducation> selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ResumeEducation record);

    int updateByPrimaryKey(ResumeEducation record);
    
    int insertBatch(List<ResumeEducation> esumeEducationList);
}