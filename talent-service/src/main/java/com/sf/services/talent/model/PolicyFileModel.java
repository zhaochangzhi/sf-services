package com.sf.services.talent.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PolicyFileModel extends PageParamModel{
	@ApiModelProperty(value = "主键id", example = "49f36f829ce6449294ad1f19e2a6aa59")
    private String id;
	@ApiModelProperty(value = "政策", example = "创业担保贷款")
    private String policy;
	@ApiModelProperty(value = "街道", example = "A街道")
    private String streetDistrict;
	@ApiModelProperty(value = "文件名字", example = "文件描述")
    private String fileName;
	@ApiModelProperty(value = "文件id", example = "49f36f829ce6449294ad1f19e2a6aa59")
    private String fileId;

}