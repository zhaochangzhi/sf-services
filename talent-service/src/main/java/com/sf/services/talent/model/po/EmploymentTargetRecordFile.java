package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

@Data
public class EmploymentTargetRecordFile {
    private String id;

    private String employmentTargetId;

    private String employmentTargetRecordId;

    private String streetKey;

    private String streetName;

    private String fileId;

    private String fileUrl;

    private String remark;

    private Date creatTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;


}