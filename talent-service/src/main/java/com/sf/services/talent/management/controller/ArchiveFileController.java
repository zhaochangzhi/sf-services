package com.sf.services.talent.management.controller;

import com.sf.services.talent.annotations.CurrentUser;
import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.mapper.ArchiveFileMapper;
import com.sf.services.talent.model.ArchiveFileParamModel;
import com.sf.services.talent.model.ArchiveFileUploadeParamModel;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.GetOneParamModel;
import com.sf.services.talent.model.dto.UserDto;
import com.sf.services.talent.service.IArchiveFileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author guchangliang
 * @date 2021/8/9
 */
@Api(value = "管理后台-档案文件", tags = "管理后台-档案文件")
@RestController(value = "ArchiveFileController")
@RequestMapping("/manage/archiveFile")
public class ArchiveFileController {

    @Resource
    private IArchiveFileService archiveFileService;
    @Resource
    private ArchiveFileMapper archiveFileMapper;

    @IgnoreSecurity
    @ApiOperation(value = "档案文件列表", notes = "档案文件列表")
    @PostMapping("/list")
    public BaseResultModel list(@RequestBody ArchiveFileParamModel params) {

        return archiveFileService.list(params);
    }

    @ApiOperation(value = "删除档案文件", notes = "删除档案文件")
    @PostMapping("/delete")
    public BaseResultModel deleteAttachment(@RequestBody GetOneParamModel params, @CurrentUser UserDto currentUser) {
        if (StringUtils.isBlank(params.getId())) {
            return BaseResultModel.badParam("id不能为空");
        }

        return archiveFileService.delete(params.getId(), currentUser.getUserId());
    }

    @ApiOperation(value = "上传档案文件", notes = "上传档案文件")
    @PostMapping("/upload")
    public BaseResultModel upload(@RequestBody ArchiveFileUploadeParamModel params, @CurrentUser UserDto currentUser) {

        if (StringUtils.isBlank(params.getFileId())) {
            return BaseResultModel.badParam("请选择上传文件");
        }

        return BaseResultModel.success(archiveFileService.uploadFile(params, currentUser.getUserId()));
    }


}
