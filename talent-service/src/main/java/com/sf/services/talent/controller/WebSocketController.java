package com.sf.services.talent.controller;

import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.service.IChatOnlineService;
import com.sf.services.talent.util.RedisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author guchangliang
 * @date 2021/8/19
 */
@Slf4j
@Api(value = "webSocket", tags = "webSocket")
@RestController
@RequestMapping("/webSocket")
public class WebSocketController {
	
	@Autowired
	private RedisUtils redisUtils;

	@Autowired
	private WebSocketServer webSocketServer;
	@Autowired
	private IChatOnlineService chatOnlineService;

	@Value("${domain.backURL}")
	private String domainBackURL;//后端地址

//	消息规则：
//	用户以json格式字符串进行接收消息
//	例：{"messagesTypeId":"1","fromMemberId":"05f955b79bbc4711a6824720aebff4e7","toMemberId":"0759720918c24b398bd08a220f622f4b","messages":"聊天内容","sendTime":"2021-09-10 08:58:56"}
//	用户以json格式字符串进行发送消息
//	例：{"toMemberId":"1", "messages":"hello", "jobFairId":"0b569354c2294ff0862f25e342fb823e"}
//
//
//	字段含义：
//	messagesTypeId（消息类型id（1：普通文本，2：通知消息，3：上线通知，4：下线通知））
//	fromMemberId（发送者会员id（t_member表id））
//	toMemberId（接收者会员id（t_member表id））
//	messages（消息内容）
//	sendTime（消息发送时间）
//	jobFairId（招聘会id）
//
//
//	消息类型id详解：
//			1：普通文本
//	用户之间聊天内容，直接显示在聊天框中。
//	{"messagesTypeId":"1","fromMemberId":"05f955b79bbc4711a6824720aebff4e7","toMemberId":"0759720918c24b398bd08a220f622f4b","messages":"用户聊天内容","sendTime":"2021-09-10 08:58:56"}
//
//2：通知消息
//	系统提示等。可以以灰色小字提醒用户。例如：对方非在线状态。
//	{"messagesTypeId":"2","fromMemberId":"0","toMemberId":"0759720918c24b398bd08a220f622f4b","messages":"对方非在线状态","sendTime":"2021-09-10 08:58:56"}
//
//3：上线通知
//	收到此消息后，将用户聊天列表中的用户状态更新为在线。fromMemberId值为上线用户memberId
//	{"messagesTypeId":"3","fromMemberId":"05f955b79bbc4711a6824720aebff4e7","toMemberId":"0759720918c24b398bd08a220f622f4b","messages":"","sendTime":"2021-09-10 08:58:56"}
//
//4：下线通知
//	收到此消息后，将用户聊天列表中的用户状态更新为下线。fromMemberId值为下线用户memberId
//	{"messagesTypeId":"4","fromMemberId":"05f955b79bbc4711a6824720aebff4e7","toMemberId":"0759720918c24b398bd08a220f622f4b","messages":"","sendTime":"2021-09-10 08:58:56"}

	@IgnoreSecurity
	@ApiOperation(notes = "webSocket首页", value="webSocket首页")
	@GetMapping(value = "/index")
	public Object webSocketIndex(HttpServletRequest request, ModelAndView mv)  {
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("serverURL", domainBackURL);

		System.out.println(domainBackURL);

		mv.addAllObjects(map);
		mv.setViewName("websocket/index");
		return mv;
	}

	@IgnoreSecurity
	@ApiOperation(notes = "是否在线", value="是否在线")
	@GetMapping(value = "/isOnline")
	public BaseResultModel isOnline(String memberId) {

		boolean isOnline = webSocketServer.isOnline(memberId);
		return BaseResultModel.success(isOnline);
	}


}
