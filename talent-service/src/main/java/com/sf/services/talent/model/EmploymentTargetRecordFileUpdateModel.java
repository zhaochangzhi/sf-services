package com.sf.services.talent.model;

import lombok.Data;

@Data
public class EmploymentTargetRecordFileUpdateModel {

    private String fileId;

}