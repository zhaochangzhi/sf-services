package com.sf.services.talent.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PolicyCategoryModel extends PageParamModel{
	
	@ApiModelProperty(value = "主键id", example = "49f36f829ce6449294ad1f19e2a6aa59")
    private String id;

	@ApiModelProperty(value = "街道", example = "盖州街道")
    private String streeDistrict;

	@ApiModelProperty(value = "政策", example = "创业带头人社会保险补贴")
    private String policy;

	@ApiModelProperty(value = "是否开发", example = "Y是N 否")
    private String isOpen;
	
	@ApiModelProperty(value = "要闻", example = "一堆字")
    private String focusNews;
	
	@ApiModelProperty(value = "用户类型", example = "1个人2 企业")
    private String userType;


}