package com.sf.services.talent.service;

import java.util.List;

import com.sf.services.talent.model.AreaParam;
import com.sf.services.talent.model.po.Area;

public interface IAreaService {
	
	/**
	 * @Title: getArea 
	 * @Description: 获取地区
	 * @param areaParam
	 * @return
	 * @return: List<Area>
	 */
	List<Area> getArea(AreaParam areaParam);
}

