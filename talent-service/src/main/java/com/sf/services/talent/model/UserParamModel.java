package com.sf.services.talent.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "用户", description = "用户")
public class UserParamModel extends PageParamModel{
	
	@ApiModelProperty(value = "主键id", example = "49f36f829ce6449294ad1f19e2a6aa59")
	private String id;

	@ApiModelProperty(value = "用户名", example = "张三")
    private String userName;

	@ApiModelProperty(value = "性别", example = "1 男 2 女")
    private String sex;

	@ApiModelProperty(value = "生日", example = "2021-10-10")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date birthday;

	@ApiModelProperty(value = "学历", example = "本科")
    private String education;

	@ApiModelProperty(value = "工作经验", example = "一年")
    private String workExperience;

	@ApiModelProperty(value = "电话", example = "13111111111")
    private String phone;
	
	@ApiModelProperty(value = "身份证好", example = "411022111105258899")
    private String identity;

	@ApiModelProperty(value = "地址", example = "沈阳市大东区街道")
    private String address;

	@ApiModelProperty(value = "图片id", example = "49f36f829ce6449294ad1f19e2a6aa59")
    private String imageId;

	@ApiModelProperty(value = "会员id", example = "49f36f829ce6449294ad1f19e2a6aa59")
    private String memberId;
	@ApiModelProperty(value = "邮箱", example = "136@136.com")
	private String email;
	
	@ApiModelProperty(value = "政治面貌", example = "数据字典")
	private String political;
	
	@ApiModelProperty(value = "个人照片", example = "49f36f829ce6449294ad1f19e2a6aa59")
	private String userPhoto;
    
	@ApiModelProperty(value = "用户权限", example = "admin")
	private String role;
	
	@ApiModelProperty(value = "用户类型", example = "0")
	private String type;
	
	private String loginName;
	
	private String password;
	
	@ApiModelProperty(value = "街道key")
    private String streetKey;

    @ApiModelProperty(value = "街道名称")
    private String streetName;
    
    private String isSystem;
}

