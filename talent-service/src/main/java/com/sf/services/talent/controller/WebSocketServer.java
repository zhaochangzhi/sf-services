package com.sf.services.talent.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sf.services.talent.model.vo.ChatOnlineMessagesVO;
import com.sf.services.talent.service.IChatOnlineService;
import com.sf.services.talent.util.OtherUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author guchangliang
 * @date 2021/8/26
 */
@Slf4j
@ServerEndpoint("/webSocketServer/{memberId}")
@Component
public class WebSocketServer {

	//引入service层需要在WebSocketConfig中注入
	public static IChatOnlineService chatOnlineService;

	/**静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。*/
	private static int onlineCount = 0;
	/**concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。*/
	private static ConcurrentHashMap<String, WebSocketServer> webSocketMap = new ConcurrentHashMap<>();
	/**与某个客户端的连接会话，需要通过它来给客户端发送数据*/
	private Session session;
	/**接收memberId*/
	private String memberId="";

	/**
	 * 连接建立成功调用的方法*/
	@OnOpen
	public void onOpen(Session session, @PathParam("memberId") String memberId) {
		this.session = session;
		this.memberId = memberId;
		if(webSocketMap.containsKey(memberId)){
			webSocketMap.remove(memberId);
			webSocketMap.put(memberId,this);
			//加入set中
		}else{
			webSocketMap.put(memberId,this);
			//加入set中
			addOnlineCount();
			//在线数加1
		}

		log.info("用户连接:"+memberId+",当前在线人数为:" + getOnlineCount());

		//向所有在线用户，推送其上线通知
		sendMessageToAllMember(getMessagesJsonString("3", memberId, "", memberId+ "上线了", null));//消息类型id（1：普通文本，2：通知消息，3：上线通知，4：下线通知）

	}

	/**
	 * 连接关闭调用的方法
	 */
	@OnClose
	public void onClose()  {
		if(webSocketMap.containsKey(memberId)){
			webSocketMap.remove(memberId);
			//从set中删除
			subOnlineCount();
		}

		log.info("用户退出:"+memberId+",当前在线人数为:" + getOnlineCount());

		//向所有在线用户，推送其下线通知
		sendMessageToAllMember(getMessagesJsonString("4", memberId, "", memberId+ "下线了", null));//消息类型id（1：普通文本，2：通知消息，3：上线通知，4：下线通知）
	}

	/**
	 * 收到客户端消息后调用的方法
	 *
	 * @param message 客户端发送过来的消息*/
	@OnMessage
	public void onMessage(String message, Session session) {
		if("ping".equals(message)){
			return;
		}

		log.info("FromMemberId:"+memberId+",报文:"+message);

		//可以群发消息
		//消息保存到数据库、redis
		if(StringUtils.isNotBlank(message)){
			try {
				//解析发送的报文
				JSONObject jsonObject = JSON.parseObject(message);
				//追加发送人(防止串改)
				String toMemberId = jsonObject.getString("toMemberId");
				String messages = jsonObject.getString("messages");
				String jobFairId = jsonObject.getString("jobFairId");

				//JsonString格式消息
				String messagesJsonString = getMessagesJsonString("1", this.memberId, toMemberId, messages, jobFairId);
				//传送给对应toMemberId用户的websocket
				if(StringUtils.isNotBlank(toMemberId) && webSocketMap.containsKey(toMemberId)){
					webSocketMap.get(toMemberId).sendMessage(messagesJsonString);
				}else{
					log.error("请求的memberId:"+toMemberId+"不在该服务器上");
					//添加消息记录	status    接收状态（0：未接收，1：已接收）
					chatOnlineService.add(messagesJsonString, 0);
				}

			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}

	/**
	 *
	 * @param session
	 * @param error
	 */
	@OnError
	public void onError(Session session, Throwable error) {
		log.error("用户错误:"+this.memberId+",原因:"+error.getMessage());
		error.printStackTrace();
	}
	/**
	 * 实现服务器主动推送
	 */
	public void sendMessage(String message)  {
		try {
			this.session.getBasicRemote().sendText(message);
			//添加消息记录	status    接收状态（0：未接收，1：已接收）
			chatOnlineService.add(message, 0);
		} catch (IOException e) {
			e.printStackTrace();
			log.error("用户:"+memberId+",网络异常!!!!!!");
		}
	}

	/**
	 * 向所有在线会员发送消息
	 */
	public void sendMessageToAllMember(String message) {
		//群体发消息只记录一次。且默认消息状态为已接收
		//添加消息记录	status    接收状态（0：未接收，1：已接收）
		chatOnlineService.add(message, 1);

		webSocketMap.forEach((key, value) -> {
			try {
				value.session.getBasicRemote().sendText(message.replace("toMemberId\":\"\"", "toMemberId\":\""+key+"\""));
			} catch (IOException e) {
				e.printStackTrace();
				log.error("用户:"+key+",网络异常!!!!!!");
			}
		});
	}


	/**
	 * 发送自定义消息
	 * */
	public static void sendInfo(String message,@PathParam("memberId") String memberId) throws IOException {
		log.info("发送消息到:"+memberId+"，报文:"+message);
		if(StringUtils.isNotBlank(memberId)&&webSocketMap.containsKey(memberId)){
			webSocketMap.get(memberId).sendMessage(message);
		}else{
			log.info("用户"+memberId+",不在线！");
		}
	}

	/**
	 * 是否在线
	 * */
	public static boolean isOnline(String memberId) {
		if(StringUtils.isNotBlank(memberId) && webSocketMap.containsKey(memberId)){
			log.info("用户"+memberId+",在线！");
			return true;
		}else{
			log.info("用户"+memberId+",不在线！");
			return false;
		}
	}

	public static synchronized int getOnlineCount() {
		return onlineCount;
	}

	public static synchronized void addOnlineCount() {
		WebSocketServer.onlineCount++;
	}

	public static synchronized void subOnlineCount() {
		WebSocketServer.onlineCount--;
	}

	//获取JsonString格式消息
	public String getMessagesJsonString(String messagesTypeId, String fromMemberId, String toMemberId, String Messages, String jobFairId){
		ChatOnlineMessagesVO chatOnlineMessagesVO = new ChatOnlineMessagesVO();
		chatOnlineMessagesVO.setMessagesTypeId(messagesTypeId);//消息类型id（1：普通文本，2：通知消息，3：上线通知，4：下线通知）
		chatOnlineMessagesVO.setFromMemberId(fromMemberId);//发送者会员id（t_member表id）
		chatOnlineMessagesVO.setToMemberId(toMemberId);//接收者会员id（t_member表id）
		chatOnlineMessagesVO.setMessages(Messages);//消息内容
		chatOnlineMessagesVO.setJobFairId(jobFairId);//招聘会id（t_job_fair表id）
		chatOnlineMessagesVO.setSendTime(OtherUtil.returntime());//发送时间

		//返回JsonString格式消息
		return JSON.toJSONString(chatOnlineMessagesVO);
	}
}