package com.sf.services.talent.model.po;

import lombok.Data;

/**
 * RankFile
 *
 * @author zhaochangzhi
 * @date 2021/7/19
 */
@Data
public class RankFile {

    private String id;

    private String rankCode;

    private String attachmentId;

    private String rankApplyTypeKey;
    private String rankApplyType;
}
