package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author guchangliang
 * @date 2021/8/4
 */
@Data
@ApiModel(value = "网站栏目", description = "网站栏目实体")
public class WebSectionParamModel extends PageParamModel {

    @ApiModelProperty(value = "所属网站字典key（t_dictionary表type=Website数据dickey）", example = "1")
    private String webSiteKey;

    @ApiModelProperty(value = "栏目名称", example = "首页-中间图片")
    private String name;

    @ApiModelProperty(value = "逻辑删除位，默认0可用，1删除", example = "0")
    private Integer deleted;
}
