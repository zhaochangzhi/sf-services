package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

@Data
public class EnterprisePositionResume {
    private String id;

    private String enterprisePositionId;

    private String resumeId;

    private String memberId;

    private String jobFairId;

    private String remark;

    private Date creatTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;

}