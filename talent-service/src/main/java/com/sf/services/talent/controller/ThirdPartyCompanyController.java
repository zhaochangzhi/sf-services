package com.sf.services.talent.controller;

import com.alibaba.fastjson.JSON;
import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.vo.ThirdPartyCompanyVO;
import com.sf.services.talent.util.OKhttpUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Api(value = "第三方公司", tags = "第三方公司")
@RestController
@RequestMapping("/thirdPartyCompany")
public class ThirdPartyCompanyController {

	@Autowired
	OKhttpUtil oKhttpUtil;

	@IgnoreSecurity
	@ApiOperation(value = "第三方公司列表", notes = "第三方公司列表")
	@GetMapping("/list")
	public BaseResultModel list() {
		//第三方列表
		List<ThirdPartyCompanyVO> resultList = new ArrayList<ThirdPartyCompanyVO>();

		//查询
		//人才认定公示	https://sfqrc.xuantuyun.com/api/ac/sfpersonnelmanage/noLoginApplyPass/setNoLoginApplyPass
		resultList.addAll(getThirdPartyCompany("https://sfqrc.xuantuyun.com", "/api/ac/sfpersonnelmanage/noLoginApplyPass/setNoLoginApplyPass", "人才认定公示"));

		//租房公式	https://sfqrc.xuantuyun.com/api/ac/sfpersonnelmanage/noLoginApplyPass/setNoLoginRentalPass
		resultList.addAll(getThirdPartyCompany("https://sfqrc.xuantuyun.com", "/api/ac/sfpersonnelmanage/noLoginApplyPass/setNoLoginRentalPass", "租房公示"));

		//购房公式	https://sfqrc.xuantuyun.com/api/ac/sfpersonnelmanage/noLoginApplyPass/setNoLoginHousePass
		resultList.addAll(getThirdPartyCompany("https://sfqrc.xuantuyun.com", "/api/ac/sfpersonnelmanage/noLoginApplyPass/setNoLoginHousePass", "购房公示"));

		//公寓公式	https://sfqrc.xuantuyun.com/api/ac/sfpersonnelmanage/noLoginApplyPass/setNoLoginTalentPass
		resultList.addAll(getThirdPartyCompany("https://sfqrc.xuantuyun.com", "/api/ac/sfpersonnelmanage/noLoginApplyPass/setNoLoginTalentPass", "公寓公示"));

		//安家公式	https://sfqrc.xuantuyun.com/api/ac/sfpersonnelmanage/noLoginApplyPass/setNoLoginSettlePass
		resultList.addAll(getThirdPartyCompany("https://sfqrc.xuantuyun.com", "/api/ac/sfpersonnelmanage/noLoginApplyPass/setNoLoginSettlePass", "安家公示"));

		//配偶就业公式	https://sfqrc.xuantuyun.com/api/ac/sfpersonnelmanage/noLoginApplyPass/setNoLoginSpousePass
		resultList.addAll(getThirdPartyCompany("https://sfqrc.xuantuyun.com", "/api/ac/sfpersonnelmanage/noLoginApplyPass/setNoLoginSpousePass", "配偶就业公示"));

		//子女入学公	https://sfqrc.xuantuyun.com/api/ac/sfpersonnelmanage/noLoginApplyPass/setNoLoginChildrenPass
		resultList.addAll(getThirdPartyCompany("https://sfqrc.xuantuyun.com", "/api/ac/sfpersonnelmanage/noLoginApplyPass/setNoLoginChildrenPass", "子女入学公示"));

		//人才纳税公式	https://sfqrc.xuantuyun.com/api/ac/sfpersonnelmanage/noLoginApplyPass/setNoLoginTaxSubsidyPass
		resultList.addAll(getThirdPartyCompany("https://sfqrc.xuantuyun.com", "/api/ac/sfpersonnelmanage/noLoginApplyPass/setNoLoginTaxSubsidyPass", "人才纳税公示"));


		return BaseResultModel.success(resultList);
	}

	@IgnoreSecurity
	@ApiOperation(value = "第三方公司列表临时", notes = "第三方公司列表临时")
	@GetMapping("/listTemp")
	public BaseResultModel getThirdPartyCompanyTemp()
	{
		//入参
		Map<String, String> paramsMap = new HashMap<String, String>();
		//header
		Map<String, String> headersMap = new HashMap<String, String>();
		//调用接口
		Map<String, Object> result = oKhttpUtil.httpGet("http://39.103.138.125", "/api/thirdPartyCompany/list", paramsMap, headersMap);
		List<ThirdPartyCompanyVO> resultData = (List<ThirdPartyCompanyVO>)result.get("data");
		return BaseResultModel.success(resultData);

	}

	//获取第三方公司信息列表
	public List<ThirdPartyCompanyVO> getThirdPartyCompany(String host, String url, String thirdPartyType){
		List<ThirdPartyCompanyVO> thirdPartyCompanyList = new ArrayList<ThirdPartyCompanyVO>();
		//入参
		Map<String, String> paramsMap = new HashMap<String, String>();
		//header
		Map<String, String> headersMap = new HashMap<String, String>();
		//调用接口
		Map<String, Object> result = oKhttpUtil.httpGet(host, url, paramsMap, headersMap);
		System.out.println(JSON.toJSONString(result));

		//提取结果
		if(result != null && result.get("data") != null){
			List<Map<String, Object>> dataList = (List<Map<String, Object>>)result.get("data");
			if(dataList != null && dataList.size() >= 1){
				for (int i = 0; i < dataList.size(); i++) {
					ThirdPartyCompanyVO thirdPartyCompanyVO = new ThirdPartyCompanyVO();
					thirdPartyCompanyVO.setThirdPartyType(thirdPartyType);
					if(dataList.get(i).get("name") != null){
						thirdPartyCompanyVO.setName(dataList.get(i).get("name").toString());
					}
					if(dataList.get(i).get("company") != null){
						thirdPartyCompanyVO.setCompany(dataList.get(i).get("company").toString());
					}
					thirdPartyCompanyVO.setCount(dataList.size());
					thirdPartyCompanyList.add(thirdPartyCompanyVO);
				}
			}
		}

		return thirdPartyCompanyList;
	}


}

