package com.sf.services.talent.mapper;

import com.sf.services.talent.model.po.ArticleFile;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ArticleFileMapper {
    int deleteByPrimaryKey(String id);

    int insert(ArticleFile record);

    int insertSelective(ArticleFile record);

    ArticleFile selectByPrimaryKey(String id);

    int selectPageListCount(Map<String, Object> searchMap);

    List<ArticleFile> selectPageList(Map<String, Object> searchMap);

    int updateByPrimaryKeySelective(ArticleFile record);

    int updateByPrimaryKey(ArticleFile record);

    //查询列表
    List<ArticleFile> selectList(@Param("articleId") String articleId);

    //软删除-通过articleId
    int deleteByArticleId(@Param("articleId") String articleId);

    //查询文件id列表
    List<String> selectFileIdList(@Param("articleId") String articleId);
}