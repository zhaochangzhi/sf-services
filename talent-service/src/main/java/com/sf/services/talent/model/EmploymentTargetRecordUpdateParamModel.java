package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 就业指标记录添加实体
 *
 * @author guchangliang
 * @date 2021/7/13
 */
@Data
@ApiModel(value = "EmploymentTargetRecordAddParamModel", description = "就业指标记录添加实体")
public class EmploymentTargetRecordUpdateParamModel {

    @ApiModelProperty(value = "指标记录id（t_employment_target_record表id）", example = "6b466adbbb864c13854cec7daf20a1a5")
    private String id;

    @ApiModelProperty(value = "设定值（当复数状态=2时，存入字符串例：123+321）", example = "70")
    private String target;

    @ApiModelProperty(value = "设定值1（当复数状态=2时，该字段拆解target第一个数字）", example = "70")
    private String targetOne;

    @ApiModelProperty(value = "设定值2（当复数状态=2时，该字段拆解target第二个数字）", example = "70")
    private String targetTwo;

}
