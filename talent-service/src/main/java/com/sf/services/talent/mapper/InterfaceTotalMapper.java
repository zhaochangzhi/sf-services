package com.sf.services.talent.mapper;

import org.apache.ibatis.annotations.Param;

import com.sf.services.talent.model.po.InterfaceTotal;

public interface InterfaceTotalMapper {
    int deleteByPrimaryKey(String id);

    int insert(InterfaceTotal record);

    int insertSelective(InterfaceTotal record);

    InterfaceTotal selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(InterfaceTotal record);

    int updateByPrimaryKey(InterfaceTotal record);
    
    int updateTotal(@Param("url") String url);
}