package com.sf.services.talent.mapper;

import com.sf.services.talent.model.po.EmploymentTargetRecord;
import com.sf.services.talent.model.vo.EmploymentTargetRecordVO;
import com.sf.services.talent.model.vo.EmploymentTargetVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EmploymentTargetRecordMapper {
    int deleteByPrimaryKey(String id);

    int insert(EmploymentTargetRecord record);

    int insertSelective(EmploymentTargetRecord record);

    EmploymentTargetRecord selectByPrimaryKey(String id);

    int selectPageListCount(Map<String, Object> searchMap);

    List<EmploymentTargetRecord> selectPageList(Map<String, Object> searchMap);

    int updateByPrimaryKeySelective(EmploymentTargetRecord record);

    int updateByPrimaryKey(EmploymentTargetRecord record);

    //根据targetId查询列表
    List<EmploymentTargetRecord> selectListByTargetId(@Param("targetId") String targetId);

    //根据targetYear/targetMonth/streetKey查询上报列表
    List<EmploymentTargetRecordVO> getReportList(@Param("targetYear") Integer targetYear, @Param("targetMonth") Integer targetMonth, @Param("streetKey") String streetKey);
}