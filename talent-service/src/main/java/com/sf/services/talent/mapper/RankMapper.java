package com.sf.services.talent.mapper;

import com.sf.services.talent.model.PageParamModel;
import com.sf.services.talent.model.dto.RankDto;
import com.sf.services.talent.model.dto.RankFileDto;
import com.sf.services.talent.model.po.Rank;
import com.sf.services.talent.model.po.RankFile;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * RankMapper
 *
 * @author zhaochangzhi
 * @date 2021/7/10
 */
@Mapper
public interface RankMapper {

    /**
     * 查询列表
     *
     * @param params 参数
     * @return 列表数据
     */
    List<RankDto> selectList(PageParamModel params);

    /**
     * 查询列表(包含附件信息)
     *
     * @param params 参数
     * @return 列表数据
     */
    List<RankFileDto> selectListWithAttachment(PageParamModel params);

    /**
     * 根据id查询详情
     *
     * @param id id
     * @return 详情
     */
    RankDto selectOneById(@Param("id") Integer id);

    /**
     * 查询最大code
     *
     * @return code
     */
    Integer selectLastCode();

    /**
     * 新增
     *
     * @param rank 职称
     * @return 结果
     */
    Integer insertOne(Rank rank);

    /**
     * 根据id删除
     *
     * @param id id
     * @return 结果
     */
    Integer deleteOneById(@Param("id") Integer id);

    /**
     * 修改
     *
     * @param rank 职称
     * @return 结果
     */
    Integer updateOneById(Rank rank);

    /**
     * 查询一个职称文件
     *
     * @param id 关联文件id
     * @return 结果
     */
    RankFile selectOneRankFile(@Param("id") String id);

    /**
     * 新增职称文件
     *
     * @param rankFileList 参数
     * @return 结果
     */
    Integer insertBatchRankFile(@Param("rankFileList") List<RankFile> rankFileList);

    /**
     * 删除职称文件
     *
     * @param id 职称文件id
     * @return 结果
     */
    Integer deleteOneRankFile(@Param("id") String id);
}
