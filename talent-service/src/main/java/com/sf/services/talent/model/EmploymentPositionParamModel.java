package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 企业中心-职位管理-职位列表
 * @author guchangliang
 * @date 2021/7/24
 */
@Data
@ApiModel(description = "企业中心-职位管理-职位列表")
public class EmploymentPositionParamModel extends PageParamModel {

    @ApiModelProperty(value = "主键id", example = "691bd23ba5174f7c8229f6e9b1168776")
    private String id;

    @ApiModelProperty(value = "企业账号id（t_enterprise_user表id）", example = "28b20fbf454f4558ab4c507255906072")
    private String enterpriseUserId;

    @ApiModelProperty(value = "会员id（t_member表id）", example = "ae2d6aae9fa4443fa86a23cd3a0a9f68")
    private String memberId;

    @ApiModelProperty(value = "审核状态：1 审核中，2 通过，3 不通过", example = "2")
    private Integer auditStatus;

    @ApiModelProperty(value = "企业名称", example = "xxx有限公司")
    private String enterpriseName;
    @ApiModelProperty(value = "信用代码", example = "xxx")
    private String enterpriseLcreditCode;

    @ApiModelProperty(value = "是否为热点职位（0：否，1：是）", example = "0")
    private Integer isHot;


}
