package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WebSectionVO {

    @ApiModelProperty(value = "主键id（t_web_section表id）")
    private String id;

    @ApiModelProperty(value = "所属网站字典key（t_dictionary表type=Website数据key）")
    private String webSiteKey;

    @ApiModelProperty(value = "所属栏目名称（t_web_section表name）")
    private String name;

    @ApiModelProperty(value = "显示栏目名称（t_web_section表display_name）")
    private String displayName;

    @ApiModelProperty(value = "网站名称")
    private String webSiteName;




}