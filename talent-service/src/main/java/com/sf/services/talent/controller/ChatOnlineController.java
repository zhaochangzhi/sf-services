package com.sf.services.talent.controller;

import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.*;
import com.sf.services.talent.service.IChatOnlineService;
import com.sf.services.talent.util.RedisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author guchangliang
 * @date 2021/9/10
 */
@Slf4j
@Api(value = "在线聊天", tags = "在线聊天")
@RestController
@RequestMapping("/chatOnline")
public class ChatOnlineController {
	
	@Autowired
	private RedisUtils redisUtils;

	@Autowired
	private WebSocketServer webSocketServer;
	@Autowired
	private IChatOnlineService chatOnlineService;

	@IgnoreSecurity
	@ApiOperation(notes = "是否在线", value="是否在线")
	@PostMapping("/isOnline")
	public BaseResultModel isOnline(String memberId) {

		boolean isOnline = webSocketServer.isOnline(memberId);
		return BaseResultModel.success(isOnline);
	}

	@IgnoreSecurity
	@ApiOperation(notes = "查询联系人列表（包含每个好友/陌生人（联系过）在线状态）", value="查询联系人列表（包含每个好友/陌生人（联系过）在线状态）")
	@PostMapping("/friends/list")
	public BaseResultModel friendsList(@RequestBody ChatOnlineFriendsListParamModel param) {

		return BaseResultModel.success(chatOnlineService.friendsList(param));
	}

	@IgnoreSecurity
	@ApiOperation(notes = "添加好友/联系人", value="添加好友/联系人")
	@PostMapping("/friends/add")
	public BaseResultModel friendsAdd(@RequestBody ChatOnlineFriendsAddParamModel param) {

		return chatOnlineService.friendsAdd(param);
	}

	//查询未读聊天记录列表
	@IgnoreSecurity
	@ApiOperation(notes = "查询未读聊天记录列表", value="查询未读聊天记录列表")
	@PostMapping("/messages/notReadList")
	public BaseResultModel messagesNotReadList(@RequestBody ChatOnlineMessageNotReadListParamModel param) {

		return chatOnlineService.messagesNotReadList(param);
	}

	//查询未读聊天记录条数
	@IgnoreSecurity
	@ApiOperation(notes = "查询未读聊天记录条数", value="查询未读聊天记录条数")
	@PostMapping("/messages/notReadCount")
	public BaseResultModel notReadCount(@RequestBody ChatOnlineMessageNotReadListParamModel param) {

		return chatOnlineService.notReadCount(param);
	}

	//查询聊天记录（数据库查询）
	@IgnoreSecurity
	@ApiOperation(notes = "查询聊天记录列表", value="查询聊天记录列表")
	@PostMapping("/messages/List")
	public BaseResultModel messagesList(@RequestBody ChatOnlineMessageListParamModel param) {

		return chatOnlineService.messagesList(param);
	}

	@IgnoreSecurity
	@ApiOperation(notes = "更新聊天记录为已读", value="更新聊天记录为已读")
	@PostMapping("/messages/updateMessageRead")
	public BaseResultModel messagesUpdateMessageRead(@RequestBody ChatOnlineMessageNotReadListParamModel param) {

		return chatOnlineService.messagesUpdateMessageRead(param);
	}

	
}
