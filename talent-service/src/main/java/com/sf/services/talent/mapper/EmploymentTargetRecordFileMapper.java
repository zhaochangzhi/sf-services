package com.sf.services.talent.mapper;

import com.sf.services.talent.model.po.EmploymentTargetRecordFile;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EmploymentTargetRecordFileMapper {
    int deleteByPrimaryKey(String id);

    int insert(EmploymentTargetRecordFile record);

    int insertSelective(EmploymentTargetRecordFile record);

    EmploymentTargetRecordFile selectByPrimaryKey(String id);

    int selectPageListCount(Map<String, Object> searchMap);

    List<EmploymentTargetRecordFile> selectPageList(Map<String, Object> searchMap);

    int updateByPrimaryKeySelective(EmploymentTargetRecordFile record);

    int updateByPrimaryKey(EmploymentTargetRecordFile record);

    //根据employment_target_record_id软删除
    int updateDeleteByEmploymentTargetRecordId(@Param("employmentTargetRecordId") String employmentTargetRecordId);

    //根据employment_target_record_id查询列表
    List<EmploymentTargetRecordFile> selectList(@Param("employmentTargetRecordId") String employmentTargetRecordId);
}