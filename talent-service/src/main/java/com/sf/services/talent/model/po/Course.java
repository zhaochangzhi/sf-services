package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

@Data
public class Course {
    private String id;

    private String courseName;

    private String courseTypeId;

    private String courseTypeName;

    private Date startTime;

    private String startAddress;

    private Integer auditStatus;

    private String auditReason;

    private Date auditTime;

    private String auditUser;

    private Integer sort;

    private String remark;

    private Date creatTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;

}