package com.sf.services.talent.annotations;

import java.lang.annotation.*;

/**
 * 当前登录用户使用注解
 *
 * @author zhaochangzhi
 * @date 2021/7/9
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CurrentUser {
}