package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.mapper.FileMapper;
import com.sf.services.talent.mapper.RankMapper;
import com.sf.services.talent.model.RankFileParamModel;
import com.sf.services.talent.model.RankParamModel;
import com.sf.services.talent.model.dto.RankDto;
import com.sf.services.talent.model.dto.RankFileDto;
import com.sf.services.talent.model.po.Rank;
import com.sf.services.talent.model.po.RankFile;
import com.sf.services.talent.model.vo.RankFileVO;
import com.sf.services.talent.model.vo.RankVO;
import com.sf.services.talent.service.IRankService;
import com.sf.services.talent.util.UUIDUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * RankService
 *
 * @author zhaochangzhi
 * @date 2021/7/10
 */
@Service
public class RankService implements IRankService {

    private final FileMapper fileMapper;

    private final RankMapper rankMapper;

    @Autowired
    public RankService(RankMapper rankMapper, FileMapper fileMapper) {
        this.rankMapper = rankMapper;
        this.fileMapper = fileMapper;
    }

    @Override
    public PageInfo<RankDto> getList(RankParamModel params) {

        List<RankVO> rankList = new ArrayList<>();
        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<RankDto> list = rankMapper.selectList(params);

//        if (!list.isEmpty()) {
//            list.stream().forEach(rank -> {
//                RankVO rankVO = new RankVO();
//                BeanUtils.copyProperties(rank, rankVO);
//                rankList.add(rankVO);
//            });
//        }
        return new PageInfo<>(list);
    }

    @Override
    public PageInfo<RankFileVO> getListWithAttachment(RankFileParamModel params) {

        List<RankFileVO> rankFileList = new ArrayList<>();
        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<RankFileDto> list = rankMapper.selectListWithAttachment(params);
        PageInfo pageInfo = new PageInfo<>(list);
        if (!list.isEmpty()) {
            list.stream().forEach(rankFile -> {
                RankFileVO rankFileVO = new RankFileVO();
                BeanUtils.copyProperties(rankFile, rankFileVO);
                rankFileList.add(rankFileVO);
            });
        }
        pageInfo.setList(rankFileList);
        return pageInfo;
    }

    @Override
    public RankVO getOneById(Integer id) {
        RankVO rankVO = new RankVO();
        RankDto rank = rankMapper.selectOneById(id);
        if (Objects.nonNull(rank)) {
            BeanUtils.copyProperties(rank, rankVO);
        }
        return rankVO;
    }

    @Override
    public Integer addOne(RankDto params) {
        Rank rank = new Rank();
        BeanUtils.copyProperties(params, rank);
        Integer lastRankCode = rankMapper.selectLastCode();
        rank.setRankCode((lastRankCode == null ? 1000 : lastRankCode + 1) + "");
        return rankMapper.insertOne(rank);
    }

    @Override
    public Integer deleteOneById(Integer id) {
        //TODO 删除职称下关联的所有文件 及 文件表中的关联文件数据
        return rankMapper.deleteOneById(id);
    }

    @Override
    public Integer updateOneById(RankDto params) {
        Rank rank = new Rank();
        BeanUtils.copyProperties(params, rank);
        return rankMapper.updateOneById(rank);
    }

    @Override
    public Integer uploadRankFile(RankFileDto rankFileDto) {
        List<RankFile> rankFileList = new ArrayList<>();
        //TODO 跟库里已有文件对比去重

        Arrays.asList(rankFileDto.getFileId()).stream().forEach(rf -> {
            RankFile rankFile = new RankFile();
            rankFile.setId(UUIDUtil.getUUid());
            rankFile.setRankCode(rankFileDto.getRankCode());
            rankFile.setAttachmentId(rf);
            rankFile.setRankApplyTypeKey(rankFileDto.getRankApplyTypeKey());
            rankFile.setRankApplyType(rankFileDto.getRankApplyType());
            rankFileList.add(rankFile);
        });
        return rankMapper.insertBatchRankFile(rankFileList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer deleteOneRankFile(String id) {
        Integer flag = 0;
        RankFile rankFile = rankMapper.selectOneRankFile(id);
        if (null != rankFile) {
            rankMapper.deleteOneRankFile(id);
            if (null != fileMapper.selectByPrimaryKey(rankFile.getAttachmentId())) {
                fileMapper.deleteByPrimaryKey(rankFile.getAttachmentId());
            }
            flag = 1;
        }
        return flag;
    }
}
