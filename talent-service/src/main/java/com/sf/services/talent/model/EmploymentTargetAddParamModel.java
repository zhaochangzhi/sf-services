package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 就业指标设定实体
 *
 * @author guchangliang
 * @date 2021/7/13
 */
@Data
@ApiModel(value = "EmploymentTargetAddParamModel", description = "就业指标添加实体")
public class EmploymentTargetAddParamModel {

    @ApiModelProperty(value = "指标类型id（t_employment_target_type表id）", example = "835a3e5fb9c842c594c2e7372bd60d21")
    private String typeId;

    @ApiModelProperty(value = "指标形式状态（1：百分比；2：固定数值）", example = "1")
    private Integer formStatus;

    @ApiModelProperty(value = "运算符号状态（1：大于等于；2：小于等于；3：等于）", example = "1")
    private Integer symbolStatus;

    @ApiModelProperty(value = "复数状态（1：设定值为一个数值；2：设定值为两个数值）", example = "1")
    private Integer complexStatus;

    @ApiModelProperty(value = "指标年份", example = "2021")
    private Integer targetYear;

    @ApiModelProperty(value = "街道key（t_dictionary表dickey）", example = "Street0001")
    private String streetKey;

    @ApiModelProperty(value = "就业指标记录添加实体", example = "[{\"target\":70,\"targetMonth\":1,\"targetOne\":\"\",\"targetTwo\":\"\"}]")
    private List<EmploymentTargetRecordAddParamModel> employmentTargetRecordList;

}
