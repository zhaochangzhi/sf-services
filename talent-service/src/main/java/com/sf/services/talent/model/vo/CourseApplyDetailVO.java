package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CourseApplyDetailVO {
    private String id;

    private String courseApplyId;

    private String courseId;

    private String applyMemberId;

    private String applyMemberType;

    private String applyName;

    private String applySex;

    private String applyBirthday;

    private String applyIdnumber;

    private String applyMobile;


}