package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class RankApplyOpenTimeVO {

    @ApiModelProperty(value = "开放开始时间", example = "0")
    private Date startTime;

    @ApiModelProperty(value = "开放结束时间", example = "0")
    private Date endTime;

    @ApiModelProperty(value = "开放结束时间", example = "0")
    private String closeContent;

    @ApiModelProperty(value = "是否开放中（0：否，1：是）", example = "0")
    private Integer isOpen;

}