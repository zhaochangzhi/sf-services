package com.sf.services.talent.mapper;

import com.sf.services.talent.model.po.JobFairEnterprisePosition;
import com.sf.services.talent.model.vo.JobFairEmploymentPositionVO;
import com.sf.services.talent.model.vo.JobFairEmploymentVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JobFairEnterprisePositionMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(JobFairEnterprisePosition record);

    JobFairEnterprisePosition selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(JobFairEnterprisePosition record);

    //查询参与招聘企业列表
    List<JobFairEmploymentVO> selectEnterpriseList(@Param("jobFairId") String jobFairId, @Param("auditStatus") Integer auditStatus);

    //查询参与招聘企业详情
    JobFairEmploymentVO selectEnterpriseDetail(@Param("jobFairId") String jobFairId, @Param("enterpriseUserId") String enterpriseUserId, @Param("auditStatus") Integer auditStatus);

    //查询参与招聘企业职位列表
    List<JobFairEmploymentPositionVO> selectEnterprisePositionList(@Param("jobFairId") String jobFairId, @Param("enterpriseUserId") String enterpriseUserId, @Param("auditStatus") Integer auditStatus);

    //查询参与招聘企业职位
    JobFairEnterprisePosition selectEnterprisePosition(@Param("jobFairId") String jobFairId, @Param("enterprisePositionId") String enterprisePositionId);

    //软删除-根据职位id
    int deleteByEnterprisePositionId(@Param("enterprisePositionId") String enterprisePositionId);
}