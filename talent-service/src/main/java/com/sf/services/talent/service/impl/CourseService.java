package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.mapper.*;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.po.*;
import com.sf.services.talent.model.vo.*;
import com.sf.services.talent.service.IActLogService;
import com.sf.services.talent.service.ICourseService;
import com.sf.services.talent.util.ReadExcel;
import com.sf.services.talent.util.UUIDUtil;
import com.sf.services.talent.util.WDWUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description: 课程
 * @author guchangliang
 * @date 2021/8/13
 */

@Slf4j
@Service
public class CourseService implements ICourseService {

    @Resource
    private CourseMapper courseMapper;
    @Resource
    private CourseTypeMapper courseTypeMapper;
    @Resource
    private CourseApplyMapper courseApplyMapper;
    @Resource
    private CourseApplyDetailMapper courseApplyDetailMapper;
    @Resource
    private MemberMapper memberMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    private EnterpriseUserMapper enterpriseUserMapper;
    @Autowired
    private RedissonClient redisson;
    @Resource
    private FileMapper fileMapper;
    @Resource
    private IActLogService actLogService;//日志

    @Override
    public PageInfo<CourseVO> getList(CourseListParamModel params) {

        if(params == null){
            return null;
        }
        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<CourseVO> list = courseMapper.selectList(params);

        return new PageInfo<>(list);
    }

    @Override
    public PageInfo<CourseTypeVO> getTypeList(CourseTypeListParamModel params) {

        if(params == null){
            return null;
        }
        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<CourseTypeVO> list = courseTypeMapper.selectList(params);

        return new PageInfo<>(list);
    }

    @Override
    public BaseResultModel typeAdd(CourseTypeAddParamModel params, String userId) {
        //校验非空-列表
        if(params == null){
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getTypeName())) {
            return BaseResultModel.badParam("课程类型名称不能为空");
        }
//        if (StringUtils.isBlank(params.getPhotoId())) {
//            return BaseResultModel.badParam("课程类型图片id不能为空");
//        }

        String photoUrl = "";
        if(!StringUtils.isBlank(params.getPhotoId())){
            File file = fileMapper.selectByPrimaryKey(params.getPhotoId());
            if(file == null){
                return BaseResultModel.badParam("课程类型图片id不存在");
            }
            photoUrl = file.getFileUrl();
        }


        //新增
        CourseType courseType = new CourseType();
        BeanUtils.copyProperties(params, courseType);
        courseType.setId(UUIDUtil.getUUid());//id
        courseType.setCreatTime(new Date());//创建时间
        courseType.setUpdateTime(courseType.getCreatTime());//更新时间
        courseType.setCreateUser(userId);//创建人

        courseType.setPhotoUrl(photoUrl);//课程类型图片url

        int count = courseTypeMapper.insertSelective(courseType);

        return BaseResultModel.success(count);
    }

    @Override
    public BaseResultModel typeUpdate(CourseTypeUpdateParamModel params, String userId) {
        //校验非空-列表
        if(params == null){
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getId())) {
            return BaseResultModel.badParam("课程id不能为空");
        }
        if (StringUtils.isBlank(params.getTypeName())) {
            return BaseResultModel.badParam("课程类型名称不能为空");
        }
        if (StringUtils.isBlank(params.getPhotoId())) {
            return BaseResultModel.badParam("课程类型图片id不能为空");
        }

        //查询课程类型id
        CourseType courseTypeCheck = courseTypeMapper.selectByPrimaryKey(params.getId());
        if (courseTypeCheck == null) {
            return BaseResultModel.badParam("课程类型id不存在");
        }

        //查询图片
        String photoUrl = "";
        if(!StringUtils.isBlank(params.getPhotoId())){
            File file = fileMapper.selectByPrimaryKey(params.getPhotoId());
            if(file == null){
                return BaseResultModel.badParam("课程类型图片id不存在");
            }
            photoUrl = file.getFileUrl();
        }


        //更新
        CourseType courseType = new CourseType();
        BeanUtils.copyProperties(params, courseType);

        courseType.setUpdateTime(new Date());//更新时间
        courseType.setUpdateUser(userId);//更新人

        courseType.setPhotoUrl(photoUrl);//课程类型图片url

        int count = courseTypeMapper.updateByPrimaryKeySelective(courseType);

        return BaseResultModel.success(count);
    }

    @Override
    public BaseResultModel typeDelete(String id, String updateUser) {

        //校验非空-列表
        if (StringUtils.isBlank(id)) {
            return BaseResultModel.badParam("课程类型id不能为空");
        }

        //校验课程维护了类型不可删除
        CourseApplyListParamModel params = new CourseApplyListParamModel();
        params.setCourseTypeId(id);
        List<CourseApplyVO> courseApplyList = courseApplyMapper.selectList(params);
        if(courseApplyList != null && courseApplyList.size() >= 1){
            return BaseResultModel.badParam("已有课程维护了此类型，请先删除课程");
        }

        //软删除课程
        CourseType courseType = courseTypeMapper.selectByPrimaryKey(id);
        if (courseType == null) {
            return BaseResultModel.badParam("id不存在");
        }
        courseType.setIsdelete(1);
        courseType.setUpdateTime(new Date());//更新时间
        courseType.setUpdateUser(updateUser);//更新人
        int count = courseTypeMapper.updateByPrimaryKeySelective(courseType);

        return BaseResultModel.success(count);
    }

    @Override
    public BaseResultModel add(CourseAddParamModel params, String userId) {
        //校验非空-列表
        if(params == null){
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getCourseName())) {
            return BaseResultModel.badParam("课程名称不能为空");
        }
        if (StringUtils.isBlank(params.getCourseTypeId())) {
            return BaseResultModel.badParam("课程类型id不能为空");
        }
        if (params.getStartTime() == null) {
            return BaseResultModel.badParam("开课时间不能为空");
        }
        if (StringUtils.isBlank(params.getStartAddress())) {
            return BaseResultModel.badParam("开课地点不能为空");
        }

        //查询课程类型id
        CourseType courseType = courseTypeMapper.selectByPrimaryKey(params.getCourseTypeId());
        if (courseType == null) {
            return BaseResultModel.badParam("课程类型id不存在");
        }

        //新增
        Course course = new Course();
        BeanUtils.copyProperties(params, course);
        course.setId(UUIDUtil.getUUid());//id
        course.setCreatTime(new Date());//创建时间
        course.setUpdateTime(course.getCreatTime());//更新时间
        course.setCreateUser(userId);//创建人

        course.setCourseTypeName(courseType.getTypeName());//课程类型名称
        int count = courseMapper.insertSelective(course);

        //保存操作日志
        try {
            actLogService.save(userId, "创建课程", course.getCourseName());
        } catch (Exception e) {
            log.error("插入日志异常:{}",e);
        }

        return BaseResultModel.success(count);
    }

    @Override
    public BaseResultModel update(CourseUpdateParamModel params, String userId) {
        //校验非空-列表
        if(params == null){
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getId())) {
            return BaseResultModel.badParam("课程id不能为空");
        }
        if (StringUtils.isBlank(params.getCourseName())) {
            return BaseResultModel.badParam("课程名称不能为空");
        }
        if (StringUtils.isBlank(params.getCourseTypeId())) {
            return BaseResultModel.badParam("课程类型id不能为空");
        }
        if (params.getStartTime() == null) {
            return BaseResultModel.badParam("开课时间不能为空");
        }
        if (StringUtils.isBlank(params.getStartAddress())) {
            return BaseResultModel.badParam("开课地点不能为空");
        }

        //查询课程类型id
        CourseType courseType = courseTypeMapper.selectByPrimaryKey(params.getCourseTypeId());
        if (courseType == null) {
            return BaseResultModel.badParam("课程类型id不存在");
        }

        //更新
        Course course = new Course();
        BeanUtils.copyProperties(params, course);

        course.setUpdateTime(new Date());//更新时间
        course.setUpdateUser(userId);//更新人

        course.setCourseTypeName(courseType.getTypeName());//课程类型名称

        int count = courseMapper.updateByPrimaryKeySelective(course);

        //保存操作日志
        try {
            actLogService.save(userId, "更新课程", course.getCourseName());
        } catch (Exception e) {
            log.error("插入日志异常:{}",e);
        }

        return BaseResultModel.success(count);
    }



    @Override
    public PageInfo<CourseApplyVO> getCourseApplyList(CourseApplyListParamModel params) {

        if(params == null){
            return null;
        }
        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<CourseApplyVO> list = courseApplyMapper.selectList(params);

        return new PageInfo<>(list);
    }

    @Override
    public BaseResultModel getCourseApply(String id) {

        if(StringUtils.isBlank(id)){
            return BaseResultModel.badParam("id不能为空");
        }

        //查询申请课程记录
        CourseApplyVO courseApplyVO = courseApplyMapper.selectById(id);
        if(courseApplyVO == null){
            return BaseResultModel.badParam("id不存在");
        }

        //查询申请课程详情列表
        List<CourseApplyDetailVO> courseApplyDetailVOList = courseApplyDetailMapper.selectList(id);

        courseApplyVO.setCourseApplyDetailList(courseApplyDetailVOList);

        return BaseResultModel.success(courseApplyVO);
    }

    @Override
    public BaseResultModel courseApplyAdd(CourseApplyAddParamModel params, String userId) {
        //校验非空-列表
        if(params == null){
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getCourseId())) {
            return BaseResultModel.badParam("课程id不能为空");
        }
        if (params.getCurseApplyDetailList() == null || params.getCurseApplyDetailList().size() == 0) {
            return BaseResultModel.badParam("报名人员信息列表不能为空");
        }

        //redisson分布式锁
        String lockKey = "CourseServiceCourseApplyAdd"+userId;
        RLock rlock = redisson.getLock(lockKey);
        try {
            //校验是否并发
            if(rlock.isLocked()){
                return BaseResultModel.badParam("请慢些操作");
            }

            //上锁
            rlock.lock();

            //查询课程id
            Course course = courseMapper.selectByPrimaryKey(params.getCourseId());
            if(course == null){
                return BaseResultModel.badParam("课程id不存在");
            }

            //查询课程类型id
            CourseType courseType = courseTypeMapper.selectByPrimaryKey(course.getCourseTypeId());
            if (courseType == null) {
                return BaseResultModel.badParam("课程类型id不存在");
            }

            if(StringUtils.isBlank(userId)){
                return BaseResultModel.badParam("请重新登陆");
            }
            //查询登陆账号信息
            Member member = memberMapper.selectByPrimaryKey(userId);
            if(member == null ){
                return BaseResultModel.badParam("登陆账号异常");
            }

            String applyMemberName = null;//报名主体名称（个人用户姓名、企业名称、街道名称）
            //判断申请人会员类型（t_member表type）（1：个人用户，2：企业用户，3：街道用户）
            if("1".equals(member.getType())){
                User user = userMapper.selectByMemberId(userId);
                applyMemberName = user.getUserName();//用户姓名
            }else if("2".equals(member.getType())){
                EnterpriseUserVO enterpriseUserVO = enterpriseUserMapper.selectByMemberId(userId);
                applyMemberName = enterpriseUserVO.getEnterpriseName();//企业名称
            }else if("3".equals(member.getType())){
                UserStreetVO userStreetVO = userMapper.getUserStreet(userId);
                applyMemberName = userStreetVO.getStreetName();//街道信息
            }else {
                return BaseResultModel.badParam("仅支持个人用户/企业用户/街道用户");
            }

            //校验当前已有待审核状态数据
            //审核状态：1 审核中，2 通过，3 不通过
            int waitReviewCount = courseApplyMapper.selectCount(userId, 1);
            if (waitReviewCount >= 1) {
                return BaseResultModel.badParam("已申请，请等待审核");
            }

            //新增
            CourseApply courseApply = new CourseApply();
            BeanUtils.copyProperties(params, courseApply);

            courseApply.setId(UUIDUtil.getUUid());//id
            courseApply.setCreatTime(new Date());//创建时间
            courseApply.setUpdateTime(courseApply.getCreatTime());//更新时间
            courseApply.setCreateUser(userId);//创建人

            courseApply.setApplyMemberId(userId);//申请人会员id（t_member表member_id）
            courseApply.setApplyMemberType(member.getType());//申请人会员类型（t_member表type）
            courseApply.setApplyMemberName(applyMemberName);//报名主体名称（个人用户姓名、企业名称、街道名称）
            courseApply.setApplyTime(courseApply.getCreatTime());//报名时间
            courseApply.setAuditStatus(1);//审核状态：1 审核中，2 通过，3 不通过

            int count = courseApplyMapper.insertSelective(courseApply);

            //新增报名用户列表
            List<CourseApplyDetailAddParamModel> curseApplyDetailList = params.getCurseApplyDetailList();
            CourseApplyDetail courseApplyDetail = new CourseApplyDetail();
            for (int i = 0; i < curseApplyDetailList.size(); i++) {
                BeanUtils.copyProperties(curseApplyDetailList.get(i), courseApplyDetail);
                courseApplyDetail.setId(UUIDUtil.getUUid());//id
                courseApplyDetail.setCreatTime(courseApply.getCreatTime());//创建时间
                courseApplyDetail.setUpdateTime(courseApply.getCreatTime());//更新时间
                courseApplyDetail.setCreateUser(userId);//创建人

                courseApplyDetail.setCourseApplyId(courseApply.getId());
                courseApplyDetail.setCourseId(courseApply.getCourseId());
                courseApplyDetail.setApplyMemberId(courseApply.getApplyMemberId());
                courseApplyDetail.setApplyMemberType(courseApply.getApplyMemberType());

                courseApplyDetailMapper.insertSelective(courseApplyDetail);
            }

            //保存操作日志
            try {
                actLogService.save(userId, "报名参加课程", course.getCourseName());
            } catch (Exception e) {
                log.error("插入日志异常:{}",e);
            }

            return BaseResultModel.success(count);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("CourseServiceCourseApplyAdd", e);
            return BaseResultModel.badParam("异常");
        }finally {
            //解锁
            rlock.unlock();
        }
    }

    @Override
    public BaseResultModel courseApplyAudit(CourseApplyAuditParamModel params, String userId) {
        //校验非空-列表
        if(params == null){
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getId())) {
            return BaseResultModel.badParam("id不能为空");
        }
        if (params.getAuditStatus() == null) {
            return BaseResultModel.badParam("审核状态不能为空");
        }
        //审核状态：1 审核中，2 通过，3 不通过
        if (params.getAuditStatus() != 2 && params.getAuditStatus() != 3) {
            return BaseResultModel.badParam("审核状态不正确");
        }
        //校验审核不通过原因
        if(params.getAuditStatus() == 3 ){
            //暂时不校验
        }else {
            //非审核不通过状态，将审核不通过原因置为空
            params.setAuditReason("");
        }

        //校验状态
        CourseApply checkCourseApply = courseApplyMapper.selectByPrimaryKey(params.getId());
        if (checkCourseApply == null) {
            return BaseResultModel.badParam("id不存在");
        }
        if(checkCourseApply.getAuditStatus() != null && (checkCourseApply.getAuditStatus() == 2 || checkCourseApply.getAuditStatus() == 3)){
            return BaseResultModel.badParam("已审核，无需再次审核");
        }

        //更新
        CourseApply courseapply = new CourseApply();
        BeanUtils.copyProperties(params, courseapply);

        courseapply.setAuditTime(new Date());//审核时间
        courseapply.setAuditUser(userId);//审核人

        int count = courseApplyMapper.updateByPrimaryKeySelective(courseapply);

        //保存操作日志
        try {
            Course course = courseMapper.selectByPrimaryKey(checkCourseApply.getCourseId());
            actLogService.save(userId, "课程报名审核", checkCourseApply.getApplyMemberName()+course.getCourseName());
        } catch (Exception e) {
            log.error("插入日志异常:{}",e);
        }

        return BaseResultModel.success(count);
    }

    @Override
    public BaseResultModel getCourseApplyExcelMessage(MultipartFile file) {
        try {
            //判断文件是否为空
            if (file == null) {
                return BaseResultModel.badParam("文件不能为空");
            }

            //验证文件名
            if (StringUtils.isBlank(file.getOriginalFilename()) && file.getSize() == 0 && !WDWUtil.validateExcel(file.getOriginalFilename())) {
                return BaseResultModel.badParam("请选择上传文件");
            }

            //解析excel，获取信息集合。
            ReadExcel readExcel = new ReadExcel();
            List<Map<String, Object>> list = readExcel.getExcelInfo(file, 0, null);

            //校验数据
            if(list == null || list.size() == 0){
                return BaseResultModel.badParam("表头不正确");
            }
            if(list.size() ==1){
                return BaseResultModel.badParam("上传数据不能为空");
            }

            List<CourseApplyDetailAddParamModel> curseApplyDetailList = new ArrayList<CourseApplyDetailAddParamModel>();

            //插入数据
            for (int i = 1; i < list.size(); i++) {
                CourseApplyDetailAddParamModel courseApplyDetailAddParamModel = new CourseApplyDetailAddParamModel();
                courseApplyDetailAddParamModel.setApplyName(list.get(i).get("key0").toString());
                courseApplyDetailAddParamModel.setApplySex(list.get(i).get("key1").toString());
                courseApplyDetailAddParamModel.setApplyBirthday(list.get(i).get("key2").toString());
                courseApplyDetailAddParamModel.setApplyIdnumber(list.get(i).get("key3").toString());
                courseApplyDetailAddParamModel.setApplyMobile(list.get(i).get("key4").toString());
                courseApplyDetailAddParamModel.setApplyMemberName(list.get(i).get("key5").toString());
                curseApplyDetailList.add(courseApplyDetailAddParamModel);
            }

            return BaseResultModel.success(curseApplyDetailList);
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResultModel.badParam("Excel导入失败！");
        }
    }

    @Override
    public BaseResultModel getLast(String createUser) {
        if(createUser == null){
            return BaseResultModel.badParam("申请人id不能为空");
        }

        //查询最后一条申请记录
        CourseApplyVO courseApplyVO = courseApplyMapper.selectLast(createUser);
        if(courseApplyVO == null){
            return BaseResultModel.success(null);
        }

        //查询申请课程详情列表
        List<CourseApplyDetailVO> courseApplyDetailVOList = courseApplyDetailMapper.selectList(courseApplyVO.getId());

        courseApplyVO.setCourseApplyDetailList(courseApplyDetailVOList);

        return BaseResultModel.success(courseApplyVO);
    }

    @Override
    public BaseResultModel delete(String id, String updateUser) {

        //软删除课程
        Course course = courseMapper.selectByPrimaryKey(id);
        if (course == null) {
            return BaseResultModel.badParam("id不存在");
        }
        course.setIsdelete(1);
        course.setUpdateTime(new Date());//更新时间
        course.setUpdateUser(updateUser);//更新人
        int count = courseMapper.updateByPrimaryKeySelective(course);

        //软删除课程申请记录
        courseApplyMapper.updateIsdeleteByCourseId(course.getId(), 1);

        //保存操作日志
        try {
            actLogService.save(updateUser, "删除课程", course.getCourseName());
        } catch (Exception e) {
            log.error("插入日志异常:{}",e);
        }

        return BaseResultModel.success(count);
    }



}
