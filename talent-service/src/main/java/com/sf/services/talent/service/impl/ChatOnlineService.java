package com.sf.services.talent.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.controller.WebSocketServer;
import com.sf.services.talent.mapper.ChatOnlineFriendsMapper;
import com.sf.services.talent.mapper.ChatOnlineMessagesMapper;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.po.ChatOnlineFriends;
import com.sf.services.talent.model.po.ChatOnlineMessages;
import com.sf.services.talent.model.vo.ChatOnlineFriendsVO;
import com.sf.services.talent.model.vo.ChatOnlineMessagesVO;
import com.sf.services.talent.service.IChatOnlineService;
import com.sf.services.talent.util.UUIDUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class ChatOnlineService implements IChatOnlineService {

	@Resource
	private ChatOnlineFriendsMapper chatOnlineFriendsMapper;
	@Resource
	private ChatOnlineMessagesMapper chatOnlineMessagesMapper;

	@Autowired
	private WebSocketServer webSocketServer;

	@Value("${domain.url}")
	String domainUrl;//域名

	@Override
	public BaseResultModel add(String message, Integer status) {

		//json转object
		Object messageObject = JSONObject.parse(message);
		ObjectMapper objectMapper = new ObjectMapper();
		ChatOnlineMessages chatOnlineMessages = objectMapper.convertValue(messageObject, ChatOnlineMessages.class);

		chatOnlineMessages.setId(UUIDUtil.getUUid());
		chatOnlineMessages.setStatus(status);//接收状态（0：未接收，1：已接收）
		if(status != null && status == 1){
			chatOnlineMessages.setReceiveTime(new Date());//接收时间
		}
		chatOnlineMessages.setSendTime(new Date());
		chatOnlineMessages.setCreateTime(new Date());

		//新增
		int count = chatOnlineMessagesMapper.insertSelective(chatOnlineMessages);

		//增加朋友
		//用户聊天，设置用户双方为陌生人
		//messagesTypeId 消息类型id（1：普通文本，2：通知消息，3：上线通知，4：下线通知）
		if("1".equals(chatOnlineMessages.getMessagesTypeId())){
			ChatOnlineFriendsAddParamModel params = new ChatOnlineFriendsAddParamModel();
			params.setFriendTypeId("1");
			params.setMemberId(chatOnlineMessages.getFromMemberId());
			params.setFriendMemberId(chatOnlineMessages.getToMemberId());
			friendsAdd(params);

			//增加互相朋友
			params.setMemberId(chatOnlineMessages.getToMemberId());
			params.setFriendMemberId(chatOnlineMessages.getFromMemberId());
			friendsAdd(params);
		}

		//A给B发消息，则认为B给A的消息已读
		if(!StringUtils.isBlank(chatOnlineMessages.getFromMemberId()) && !StringUtils.isBlank(chatOnlineMessages.getToMemberId())){
			//tatus 接收状态（0：未接收，1：已接收）
			chatOnlineMessagesMapper.updateStatus(chatOnlineMessages.getToMemberId(), chatOnlineMessages.getFromMemberId(), 1);
		}

		return BaseResultModel.success(count);
	}

	@Override
	public PageInfo<ChatOnlineFriendsVO> friendsList(ChatOnlineFriendsListParamModel params) {

		if(StringUtils.isBlank(params.getMemberId())){
			log.error("friendsList-memberId为空");
			return null;
		}
		//分页处理
		PageHelper.startPage(params.getPageNum(), params.getPageSize());
		List<ChatOnlineFriendsVO> list = chatOnlineFriendsMapper.selectList(params);

		//查询好友在线状态
		ChatOnlineMessages lastChat = new ChatOnlineMessages();
		if(list != null && list.size() >= 1){
			for (int i = 0; i < list.size(); i++) {
				list.get(i).setIsOnline(webSocketServer.isOnline(list.get(i).getFriendMemberId()));

				//查询好友消息未读数
				//messagesTypeId 消息类型id（1：普通文本，2：通知消息，3：上线通知，4：下线通知）
				list.get(i).setNotReadCount(chatOnlineMessagesMapper.selectCount(list.get(i).getFriendMemberId(), list.get(i).getMemberId(), 0, "1"));

				//查询最后一条消息时间
				lastChat = chatOnlineMessagesMapper.selectLastChat(list.get(i).getFriendMemberId(), list.get(i).getMemberId());
				if(lastChat != null){
					list.get(i).setLastMessageTime(lastChat.getSendTime());
				}
			}
		}

		return new PageInfo<>(list);
	}

	@Override
	public BaseResultModel friendsAdd(ChatOnlineFriendsAddParamModel params) {
		if(params == null || StringUtils.isBlank(params.getMemberId()) || StringUtils.isBlank(params.getFriendMemberId())){
			log.error("friendsAdd参数不能为空");
			return BaseResultModel.badParam("参数不能为空");
		}
		int count = 0;
		//增加朋友
		try {
			//用户聊天，设置用户双方为陌生人
			//messagesTypeId 消息类型id（1：普通文本，2：通知消息，3：上线通知，4：下线通知）
			ChatOnlineFriends chatOnlineFriends = new ChatOnlineFriends();
			chatOnlineFriends.setId(UUIDUtil.getUUid());
			chatOnlineFriends.setFriendTypeId(params.getFriendTypeId());//朋友类型id（1：陌生人（聊过天），2：好友）
			chatOnlineFriends.setMemberId(params.getMemberId());
			chatOnlineFriends.setFriendMemberId(params.getFriendMemberId());
			chatOnlineFriends.setCreateTime(new Date());
			count = chatOnlineFriendsMapper.insertSelective(chatOnlineFriends);
		} catch (DuplicateKeyException e) {
			//捕获唯一约束异常，不做处理
		}

		return BaseResultModel.success(count);
	}

	@Override
	public BaseResultModel messagesNotReadList(ChatOnlineMessageNotReadListParamModel params) {

		String fromMemberId = params.getFriendMemberId();//发送消息人memberId
		String toMemberId = params.getMemberId();//接收消息人memberId

		//校验本人memberId非空
		if(StringUtils.isBlank(toMemberId)){
			return BaseResultModel.badParam("本人memberId不能为空");
		}

		//查询未读消息list
		//status 接收状态（0：未接收，1：已接收）
		int limitNumber = 1000;//查询数据条数
		List<ChatOnlineMessagesVO> messagesNotReadList = chatOnlineMessagesMapper.selectList(fromMemberId, toMemberId, 0, "1", limitNumber);

		//将未读更新为已读	tatus 接收状态（0：未接收，1：已接收）
		chatOnlineMessagesMapper.updateStatus(fromMemberId, toMemberId, 1);

		return BaseResultModel.success(messagesNotReadList);
	}

	@Override
	public BaseResultModel notReadCount(ChatOnlineMessageNotReadListParamModel params) {

		String fromMemberId = params.getFriendMemberId();//发送消息人memberId
		String toMemberId = params.getMemberId();//接收消息人memberId

		//校验本人memberId非空
		if(StringUtils.isBlank(toMemberId)){
			return BaseResultModel.badParam("本人memberId不能为空");
		}

		//查询未读消息list
		//status 接收状态（0：未接收，1：已接收）
		//messagesTypeId 消息类型id（1：普通文本，2：通知消息，3：上线通知，4：下线通知）
		int notReadCount = chatOnlineMessagesMapper.selectCount(fromMemberId, toMemberId, 0, "1");

		return BaseResultModel.success(notReadCount);
	}

	@Override
	public BaseResultModel messagesList(ChatOnlineMessageListParamModel params) {

		String fromMemberId = params.getFriendMemberId();//发送消息人memberId
		String toMemberId = params.getMemberId();//接收消息人memberId

		//校验本人memberId非空
		if(StringUtils.isBlank(toMemberId)){
			return BaseResultModel.badParam("本人memberId不能为空");
		}

		//查询未读消息list
		//status 接收状态（0：未接收，1：已接收）
		int limitNumber = 1000;//查询数据条数
		List<ChatOnlineMessagesVO> messagesList = chatOnlineMessagesMapper.selectBothList(fromMemberId, toMemberId, null, "1", limitNumber);

		//将未读更新为已读	tatus 接收状态（0：未接收，1：已接收）
		chatOnlineMessagesMapper.updateStatus(fromMemberId, toMemberId, 1);

		return BaseResultModel.success(messagesList);
	}


	@Override
	public BaseResultModel updateStatus(String fromMemberId, String toMemberId, Integer status) {

		//将未读更新为已读	tatus 接收状态（0：未接收，1：已接收）
		int count = chatOnlineMessagesMapper.updateStatus(fromMemberId, toMemberId, 1);

		return BaseResultModel.success(count);
	}

	@Override
	public BaseResultModel messagesUpdateMessageRead(ChatOnlineMessageNotReadListParamModel params) {

		//将未读更新为已读	tatus 接收状态（0：未接收，1：已接收）
		int count = chatOnlineMessagesMapper.updateStatus(params.getFriendMemberId(), params.getMemberId(), 1);

		return BaseResultModel.success(count);
	}
}

