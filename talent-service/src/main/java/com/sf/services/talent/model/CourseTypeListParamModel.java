package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 课程类型
 * @author guchangliang
 * @date 2021/8/13
 */
@Data
@ApiModel(description = "课程类型列表")
public class CourseTypeListParamModel extends PageParamModel {


    @ApiModelProperty(value = "课程类型名称", example = "xxx课程")
    private String typeName;



}
