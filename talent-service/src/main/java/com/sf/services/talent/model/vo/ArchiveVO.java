package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@ApiModel(value = "档案实体")
@Data
public class ArchiveVO extends PageVO{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2950798619255119386L;
	@ApiModelProperty(value = "主键id")
	private String id;
	@ApiModelProperty(value = "档案号")
	private String archiveNumber;
	@ApiModelProperty(value = "档案所属")
    private String ownerName;
	@ApiModelProperty(value = "档案身份证id")
    private String ownerId;
	@ApiModelProperty(value = "地址")
    private String storePlace;
	
}
