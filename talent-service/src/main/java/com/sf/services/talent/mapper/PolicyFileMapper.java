package com.sf.services.talent.mapper;

import java.util.List;

import com.sf.services.talent.model.po.PolicyFile;

public interface PolicyFileMapper {
	
    int deleteByPrimaryKey(String id);

    int insert(PolicyFile record);

    int insertSelective(PolicyFile record);

    PolicyFile selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PolicyFile record);

    int updateByPrimaryKey(PolicyFile record);
    
    List<PolicyFile> selectAll(PolicyFile record);
}