package com.sf.services.talent.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * 结果返回值信息
 *
 * @author zhaochangzhi
 * @date 2021/7/7
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseResultModel<T> {

    private Integer code = HttpStatus.INTERNAL_SERVER_ERROR.value();

    private String msg;

    private T data;

    public static <T> BaseResultModel<T> success(T data) {
        return new BaseResultModel(HttpStatus.OK.value(), "成功", data);
    }

    public static <T> BaseResultModel<T> failure(String message) {
        return new BaseResultModel(HttpStatus.INTERNAL_SERVER_ERROR.value(), message, null);
    }

    public static <T> BaseResultModel<T> failure(Integer code, String message) {
        return new BaseResultModel(code, message, null);
    }

    public static <T> BaseResultModel<T> noData() {
        return new BaseResultModel(HttpStatus.NO_CONTENT.value(), "成功", null);
    }

    public static <T> BaseResultModel<T> badParam(String msg) {
        return new BaseResultModel(HttpStatus.BAD_REQUEST.value(), msg, null);
    }

    public static <T> BaseResultModel<T> noAuth() {
        return new BaseResultModel(HttpStatus.UNAUTHORIZED.value(), "需要登录", null);
    }

    public static <T> BaseResultModel<T> forbidden(String msg) {
        return new BaseResultModel(HttpStatus.FORBIDDEN.value(), msg, null);
    }

    @JsonIgnore
    public boolean isNoData() {
        return this.code == HttpStatus.NO_CONTENT.value();
    }

    @JsonIgnore
    public boolean isSuccess() {
        return this.code == HttpStatus.OK.value();
    }

    @JsonIgnore
    public boolean isBadParam() {
        return this.code == HttpStatus.BAD_REQUEST.value();
    }
}
