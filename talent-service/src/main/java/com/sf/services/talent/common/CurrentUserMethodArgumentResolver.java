package com.sf.services.talent.common;

import com.sf.services.talent.annotations.CurrentUser;
import com.sf.services.talent.common.constants.Constants;
import com.sf.services.talent.exceptions.AuthException;
import com.sf.services.talent.model.dto.UserDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.Objects;

/**
 * currentUser 注解解析器
 *
 * @author zhaochangzhi
 * @date 2021/7/9
 */
@Log4j2
public class CurrentUserMethodArgumentResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().isAssignableFrom(UserDto.class)
                && parameter.hasParameterAnnotation(CurrentUser.class);
    }

    @Override
    public UserDto resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest, WebDataBinderFactory binderFactory) {
        Object currentUserObj = webRequest.getAttribute(Constants.CURRENT_USER, RequestAttributes.SCOPE_REQUEST);
        if (Objects.nonNull(currentUserObj)) {
           return (UserDto) currentUserObj;
        } else {
            throw new AuthException();
        }
    }
}