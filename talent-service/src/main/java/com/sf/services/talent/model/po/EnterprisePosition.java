package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

@Data
public class EnterprisePosition {
    private String id;

    private String enterpriseUserId;

    private String enterprisePositionName;

    private String tradeId;
    private String tradeName;

    private String positionId;
    private String positionName;

    private Integer workAddressProvinceId;

    private Integer workAddressCityId;

    private Integer workAddressDistrictId;

    private String workAddressProvince;

    private String workAddressCity;

    private String workAddressDistrict;

    private String salaryRequirementKey;

    private String salaryRequirement;

    private String salaryMin;

    private String salaryMax;

    private Integer negotiationStatus;

    private String workExpKey;

    private String workExp;

    private String educationKey;

    private String education;

    private String positionDescribe;

    private Integer recruitNum;

    private String welfareKey;

    private String welfare;

    private Integer resumeNum;

    private Integer browseNum;

    private Integer collectionNum;

    private Integer isHot;

    private Integer auditStatus;
    private String auditReason;
    private Date auditTime;
    private String auditUser;

    private Integer sort;

    private String remark;

    private Date creatTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;

}