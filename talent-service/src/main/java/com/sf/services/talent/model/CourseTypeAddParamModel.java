package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 课程类型
 * @author guchangliang
 * @date 2021/8/13
 */
@Data
@ApiModel(description = "课程类型创建")
public class CourseTypeAddParamModel {


    @ApiModelProperty(value = "课程类型名称", example = "xxx课程")
    private String typeName;

    @ApiModelProperty(value = "课程类型图片id", example = "00c497b8e1464ae4be3f890e05fa3f7d")
    private String photoId;



}
