package com.sf.services.talent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 添加课程
 * @author guchangliang
 * @date 2021/8/13
 */
@Data
@ApiModel(description = "添加课程")
public class CourseUpdateParamModel {

    @ApiModelProperty(value = "主键id", example = "0e123d79c2fa468ba901426ab21b3fcf")
    private String id;

    @ApiModelProperty(value = "课程名称", example = "xxx课程")
    private String courseName;

    @ApiModelProperty(value = "课程类型id（t_course_type表id）", example = "ae2d6aae9fa4443fa86a23cd3a0a9f68")
    private String courseTypeId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "开课时间", example = "2021-08-13")
    private Date startTime;

    @ApiModelProperty(value = "开课地点）", example = "开课地点xxx")
    private String startAddress;

}
