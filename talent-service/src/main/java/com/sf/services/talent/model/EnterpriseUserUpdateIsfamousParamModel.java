package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "更新企业用户是否为名企", description = "更新企业用户是否为名企")
public class EnterpriseUserUpdateIsfamousParamModel {


	@ApiModelProperty(value = "主键id", example = "12a53ef7c0af4a52ac9f0e8c1001a0ad")
	private String id;

	@ApiModelProperty(value = "是否为著名企业（0：否，1：是）", example = "1")
	private Integer isfamous;


}

