package com.sf.services.talent.util;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * IP处理工具
 *
 * @author zhao
 * @date 2005/6/30
 */
public class IpAddressUtils {

	private final static String UNKNOWN_STRING = "unknown";

	private final static String SELF_IP_STRING = "127.0.0.1";

	private final static String DEFAULT_IP_STRING = "0:0:0:0:0:0:0:1";

	/**
	 * 获取客户端IP地址
	 *
	 * @param request 请求
	 * @return
	 */
	public static String getIpAddress(HttpServletRequest request) {

//		System.out.println("request.getHeader(\"x-forwarded-for\")------------------:"+request.getHeader("x-forwarded-for"));
//		System.out.println("request.getHeader(\"Proxy-Client-IP\")------------------:"+request.getHeader("Proxy-Client-IP"));
//		System.out.println("request.getHeader(\"WL-Proxy-Client-IP\")------------------:"+request.getHeader("WL-Proxy-Client-IP"));
//		System.out.println("request.getHeader(\"HTTP_CLIENT_IP\")------------------:"+request.getHeader("HTTP_CLIENT_IP"));
//		System.out.println("request.getHeader(\"HTTP_X_FORWARDED_FOR\")------------------:"+request.getHeader("HTTP_X_FORWARDED_FOR"));
//		System.out.println("request.getHeader(\"X-Real-IP\")------------------:"+request.getHeader("X-Real-IP"));
//		System.out.println("request.getHeader(\"X-Forwarded-For\")------------------:"+request.getHeader("X-Forwarded-For"));

		String ip = request.getHeader("x-forwarded-for");
		if (StringUtils.isBlank(ip) || UNKNOWN_STRING.equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (StringUtils.isBlank(ip) || UNKNOWN_STRING.equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (StringUtils.isBlank(ip) || UNKNOWN_STRING.equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (StringUtils.isBlank(ip) || UNKNOWN_STRING.equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (StringUtils.isBlank(ip) || UNKNOWN_STRING.equalsIgnoreCase(ip)) {
			ip = request.getHeader("X-Real-IP");
		}
		if (StringUtils.isBlank(ip) || UNKNOWN_STRING.equalsIgnoreCase(ip)) {
			ip = request.getHeader("X-Forwarded-For");
		}

		if (StringUtils.isBlank(ip) || UNKNOWN_STRING.equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
			if (SELF_IP_STRING.equals(ip)) {
				//根据网卡取本机配置的IP
				try {
					InetAddress iNet = InetAddress.getLocalHost();
					ip = iNet.getHostAddress();
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}

			}
		}
		// 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
		int ipsLength = 15;
		if (ip != null && ip.length() > ipsLength) {
			String reg = ",";
			if (ip.indexOf(reg) > 0) {
				ip = ip.substring(0, ip.indexOf(reg));
			}
		}
		if (DEFAULT_IP_STRING.equals(ip)) {
			ip = SELF_IP_STRING;
		}
		return ip;
	}
}