package com.sf.services.talent.mapper;

import com.sf.services.talent.model.ArticleParamModel;
import com.sf.services.talent.model.po.Article;
import com.sf.services.talent.model.vo.ArticleVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ArticleMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(Article record);

    Article selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Article record);

    //查询列表
    List<ArticleVO> selectList(ArticleParamModel record);

    //查询网站栏目名称（t_web_section表name）
    String selectWebSectionName(@Param("id") String id);

    /**
     * 增加文章xx数量
     * @param id    职位表id（t_article表id）
     * @param browseNum 增加的浏览数量
     * @return
     */
    int addNum(@Param("id") String id, @Param("browseNum") Integer browseNum);

    Article selectById(String id);
}