package com.sf.services.talent;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 启动类
 *
 * @author zhaochangzhi
 * @date 2021/7/7
 */
@ComponentScan("com.sf.services.talent.*")
@MapperScan("com.sf.services.talent.mapper")
@SpringBootApplication
public class TalentServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(TalentServiceApplication.class, args);
    }

}
