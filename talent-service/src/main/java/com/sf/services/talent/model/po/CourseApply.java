package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

@Data
public class CourseApply {
    private String id;

    private String courseId;

    private String applyMemberId;

    private String applyMemberType;

    private String applyMemberName;

    private Date applyTime;

    private Integer auditStatus;

    private String auditReason;

    private Date auditTime;

    private String auditUser;

    private Integer sort;

    private String remark;

    private Date creatTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;


}