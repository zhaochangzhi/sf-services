package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: guchangliang
 * @date: 2021年8月19日 下午9:43:50
 */
@Data
@ApiModel(value = "忘记密码-重置密码", description = "忘记密码-重置密码")
public class RetrievePasswordParamModel {

	@ApiModelProperty(value = "用户名", example = "用户名xxx")
	private String userName;

	@ApiModelProperty(value = "新密码", example = "新密码")
	private String newPassWord;

	@ApiModelProperty(value = "验证码", example = "验证码")
	private String verificationCode;

}

