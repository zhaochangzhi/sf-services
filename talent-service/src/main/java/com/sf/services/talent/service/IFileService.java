package com.sf.services.talent.service;

import com.sf.services.talent.model.po.File;

/**
 * @ClassName: IFileService 
 * @Description: 文件接口
 * @author: heyang
 * @date: 2021年7月12日 下午10:07:17
 */
public interface IFileService {
	
	/**
	 * @Title: upload  
	 * @Description: 文件上传接口
	 * @param record
	 * @return
	 * @return: String
	 */
	 String upload(File record);
	 
	 /**
	  * @Title: download 
	  * @Description: 文件下载接口
	  * @param fileId
	  * @return: File
	  */
	 File download(String fileId);
}

