package com.sf.services.talent.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * EmploymentTarget
 * 就业指标
 * @author guchangliang
 * @date 2021/7/13
 */
@ApiModel(value = "企业中心-职位实体")
@Data
public class EmploymentPositionVO extends PageVO{

    @ApiModelProperty(value = "主键id")
    private String id;

    @ApiModelProperty(value = "企业账号id（t_enterprise_user表id）")
    private String enterpriseUserId;

    @ApiModelProperty(value = "企业职位名称")
    private String enterprisePositionName;


    @ApiModelProperty(value = "行业一级id（z_trade表id）")
    private String tradeOneId;
    @ApiModelProperty(value = "行业一级名称")
    private String tradeOneName;
    @ApiModelProperty(value = "行业二级id（z_trade表id）")
    private String tradeTwoId;
    @ApiModelProperty(value = "行业二级名称")
    private String tradeTwoName;


    @ApiModelProperty(value = "职位一级id（z_position表id）")
    private String positionOneId;
    @ApiModelProperty(value = "职位一级名称")
    private String positionOneName;
    @ApiModelProperty(value = "职位二级id（z_position表id）")
    private String positionTwoId;
    @ApiModelProperty(value = "职位二级名称")
    private String positionTwoName;
    @ApiModelProperty(value = "职位三级id（z_position表id）")
    private String positionThreeId;
    @ApiModelProperty(value = "职位三级名称")
    private String positionThreeName;

    @ApiModelProperty(value = "工作地点省id（t_area表id")
    private Integer workAddressProvinceId;
    @ApiModelProperty(value = "工作地点市id（t_area表id）")
    private Integer workAddressCityId;
    @ApiModelProperty(value = "工作地点区id（t_area表id）")
    private Integer workAddressDistrictId;

    @ApiModelProperty(value = "工作地点省")
    private String workAddressProvince;
    @ApiModelProperty(value = "工作地点市")
    private String workAddressCity;
    @ApiModelProperty(value = "工作地点区")
    private String workAddressDistrict;

    @ApiModelProperty(value = "薪资要求key（t_dictionary表type=SalaryRequirement数据dickey）")
    private String salaryRequirementKey;
    @ApiModelProperty(value = "薪资要求")
    private String salaryRequirement;
    @ApiModelProperty(value = "薪资-最小值")
    private String salaryMin;
    @ApiModelProperty(value = "薪资-最大值")
    private String salaryMax;

    @ApiModelProperty(value = "面议状态（0：非面议；1：面议）")
    private Integer negotiationStatus;

    @ApiModelProperty(value = "工作经验key（t_dictionary表type=WorkExp数据dickey）")
    private String workExpKey;
    @ApiModelProperty(value = "工作经验")
    private String workExp;

    @ApiModelProperty(value = "学历要求key（t_dictionary表type=Education数据dickey")
    private String educationKey;
    @ApiModelProperty(value = "学历要求")
    private String education;

    @ApiModelProperty(value = "职位描述")
    private String positionDescribe;

    @ApiModelProperty(value = "招聘人数")
    private Integer recruitNum;

    @ApiModelProperty(value = "福利待遇key（逗号分隔）（t_dictionary表type=WorkExp数据dickey）")
    private String welfareKey;
    @ApiModelProperty(value = "福利待遇（逗号分隔）")
    private String welfare;

    @ApiModelProperty(value = "收到简历数量")
    private Integer resumeNum;

    @ApiModelProperty(value = "浏览数量")
    private Integer browseNum;

    @ApiModelProperty(value = "收藏数量")
    private Integer collectionNum;

    @ApiModelProperty(value = "创建时间")
    private Date creatTime;

    @JsonFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;


    @ApiModelProperty(value = "审核状态：1 审核中，2 通过，3 不通过")
    private Integer auditStatus;
    @ApiModelProperty(value = "审核不通过原因")
    private String auditReason;
    @ApiModelProperty(value = "审核时间")
    private Date auditTime;
    @ApiModelProperty(value = "审核人")
    private String auditUser;

    @ApiModelProperty(value = "企业名称")
    private String enterpriseName;
    @ApiModelProperty(value = "统一社会信用代码")
    private String enterpriseLcreditCode;


    @ApiModelProperty(value = "是否已申请（0：否，1：是）")
    private Integer isApply = 0;
    @ApiModelProperty(value = "是否已收藏（0：否，1：是）")
    private Integer isCollection = 0;

    @ApiModelProperty(value = "是否为热点职位（0：否，1：是）")
    private Integer isHot;



}