package com.sf.services.talent.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sf.services.talent.exceptions.TalentException;
import com.sf.services.talent.model.ArchiveServiceAppointmentModel;
import com.sf.services.talent.service.IArchiveServiceAppointmentService;
import com.sf.services.talent.util.ResultUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(value = "档案预约", tags = "档案预约")
@RestController
@RequestMapping("/archiveServiceAppointment")
public class ArchiveServiceAppointmentController {
	
	@Autowired
	private IArchiveServiceAppointmentService archiveServiceAppointmentService;
	
	
	/**
	 * @Title: list 
	 * @Description: 档案预约列表
	 * @param archiveServiceAppointmentModel
	 * @return
	 * @return: ResultUtil
	 */
	@ApiOperation(notes = "档案预约列表", value="档案预约列表")
	@ApiImplicitParam(name = "params", value = "档案模型", required = false, dataType = "ArchiveServiceAppointmentModel")
	@PostMapping("/list")
	public ResultUtil list(@RequestBody ArchiveServiceAppointmentModel archiveServiceAppointmentModel) {
		try {
			return ResultUtil.success(archiveServiceAppointmentService.getPageList(archiveServiceAppointmentModel));
		} catch (Exception e) {
			log.error("档案预约列表异常:{}",e);
			return ResultUtil.error(401, "档案预约列表异常");
		}
		
	}
	
	/**
	 * @Title: appointment 
	 * @Description: 档案预约
	 * @param archiveServiceAppointmentModel
	 * @return
	 * @return: ResultUtil
	 */
	@ApiOperation(notes = "档案预约", value="档案预约")
	@ApiImplicitParam(name = "params", value = "主键id", required = false, dataType = "ArchiveServiceAppointmentModel")
	@PostMapping("/appointment")
	public ResultUtil appointment(@RequestBody ArchiveServiceAppointmentModel archiveServiceAppointmentModel) {
		try {
			return ResultUtil.success(archiveServiceAppointmentService.appointment(archiveServiceAppointmentModel));
		}catch (Exception e) {
			log.error("档案预约异常:{}",e);
			return ResultUtil.error(401, "档案预约异常");
		}
		
	}
	/**
	 * 档案插入
	 * @param archiveVO
	 * @return
	 */
	@ApiOperation(notes = "档案审核", value="档案审核")
	@ApiImplicitParam(name = "params", value = "档案模型", required = false, dataType = "ArchiveServiceAppointmentModel")
	@PostMapping("/audit")
	public ResultUtil audit(@RequestBody ArchiveServiceAppointmentModel archiveServiceAppointmentModel) {
		try {
			archiveServiceAppointmentService.audit(archiveServiceAppointmentModel);
			return ResultUtil.success();
		}catch(TalentException te){
			return ResultUtil.error(te.getCode(), te.getMessage());
		}catch (Exception e) {
			log.error("档案审核异常:{}",e);
			return ResultUtil.error(401, "档案审核异常");
		}
		
	}
	
	/**
	 * @Title: getNewestDetail 
	 * @Description: 获取最新一条预约
	 * @param archiveVO
	 * @return
	 * @return: ResultUtil
	 */
	@ApiOperation(notes = "获取最新一条预约", value="获取最新一条预约")
	@ApiImplicitParam(name = "params", value = "档案模型", required = false, dataType = "ArchiveServiceAppointmentModel")
	@PostMapping("/getNewestDetail")
	public ResultUtil getNewestDetail(@RequestBody ArchiveServiceAppointmentModel archiveServiceAppointmentModel) {
		try {
			return ResultUtil.success(archiveServiceAppointmentService.getNewestDetail());
		}catch(TalentException te){
			return ResultUtil.error(te.getCode(), te.getMessage());
		}catch (Exception e) {
			log.error("获取最新一条预约异常:{}",e);
			return ResultUtil.error(401, "获取最新一条预约异常");
		}
		
	}
}

