package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName: AreaParam 
 * @Description: 地区参数
 * @author: heyang
 * @date: 2021年7月21日 下午9:43:50
 */
@Data
@ApiModel(value = "地区", description = "地区")
public class AreaParam {
	
	@ApiModelProperty(value = "父id", example = "10")
	private Short pid;
	@ApiModelProperty(value = "级别 1.省 2.市", example = "1")
    private Integer level;
}

