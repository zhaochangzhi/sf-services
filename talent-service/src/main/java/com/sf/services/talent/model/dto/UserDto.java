package com.sf.services.talent.model.dto;

import lombok.Data;

/**
 * UserDto 用户实体
 *
 * @author zhaochangzhi
 * @date 2020/7/9
 */
@Data
public class UserDto {

    private String userId;//t_member表id

    private String type;
}
