package com.sf.services.talent.mapper;

import com.sf.services.talent.model.po.JobFairUser;
import com.sf.services.talent.model.vo.JobFairUserVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JobFairUserMapper {
    int deleteByPrimaryKey(String id);

    int insert(JobFairUser record);

    int insertSelective(JobFairUser record);

    JobFairUser selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(JobFairUser record);

    int updateByPrimaryKey(JobFairUser record);

    JobFairUser selectByJobFairIdAndUserId(@Param("jobFairId") String jobFairId, @Param("memberId") String memberId);

    //查询列表
    List<JobFairUserVO> selectList(@Param("jobFairId") String jobFairId, @Param("auditStatus") Integer auditStatus);
}