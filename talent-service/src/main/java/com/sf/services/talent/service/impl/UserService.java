package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.config.ThreadLocalCache;
import com.sf.services.talent.mapper.DictionaryMapper;
import com.sf.services.talent.mapper.FileMapper;
import com.sf.services.talent.mapper.MemberMapper;
import com.sf.services.talent.mapper.UserMapper;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.UserParamModel;
import com.sf.services.talent.model.po.File;
import com.sf.services.talent.model.po.User;
import com.sf.services.talent.model.vo.UserStreetVO;
import com.sf.services.talent.service.IActLogService;
import com.sf.services.talent.service.IUserService;
import com.sf.services.talent.util.UUIDUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @ClassName: UserService 
 * @Description: 用户接口实现
 * @author: heyang
 * @date: 2021年7月11日 下午11:20:10
 */
@Service
public class UserService implements IUserService{

	@Resource
	private UserMapper userMapper;
	@Resource
	private FileMapper fileMapper;
	@Resource
	private DictionaryMapper dictionaryMapper;
	@Resource
	private MemberMapper memberMapper;
	
	@Autowired
	private IActLogService actLogService;
	@Override
	public User getUser() {
		String memberId = ThreadLocalCache.getUser();
		User user = userMapper.selectByMemberId(memberId);
		if(user!=null) {
			if(StringUtils.isNoneBlank(user.getImageId())) {
				File file = fileMapper.selectByPrimaryKey(user.getImageId());
				user.setImageUrl(file.getFileUrl());
			}
			if(StringUtils.isNoneBlank(user.getRole())) {
				user.setRoles(user.getRole().split(","));
			}
		}
		
		return user;
	}
	@Override
	public int update(UserParamModel userParamModel) {
		User user = new User();
		BeanUtils.copyProperties(userParamModel, user);
		if("3".equals(userParamModel.getType())) {
			UserStreetVO userStreetVO = new UserStreetVO();
			userStreetVO.setStreetKey(userParamModel.getStreetKey());
			userStreetVO.setStreetName(userParamModel.getStreetName());
			userStreetVO.setMemberId(userParamModel.getMemberId());
			userMapper.updateUserStreet(userStreetVO);
		}
		actLogService.save(ThreadLocalCache.getUser(), "编辑用户", userParamModel.getUserName());
		return userMapper.updateByPrimaryKeySelective(user);
	}

	@Override
	public UserStreetVO getUserStreet(String memberId) {
		return userMapper.getUserStreet(memberId);
	}
	@Override
	public PageInfo<User> list(UserParamModel record) {
		PageHelper.startPage(record.getPageNum(), record.getPageSize());
		User user = new User();
		BeanUtils.copyProperties(record, user);
		List<User> list = userMapper.selectList(user);
		list.forEach(u -> {
			if (StringUtils.isNoneBlank(u.getRole())) {
				String[] roles = u.getRole().split(",");
				u.setRoles(roles);
				List<String> rolesDesc = new ArrayList<>();
				for (String s : roles) {
					rolesDesc.add(dictionaryMapper.selectOneByTypeAndDickey("UserRole", s).getValue());
				}
				u.setRolesDesc(rolesDesc);
			}
			// 街道用户
			if("3".equals(u.getType())) {
				UserStreetVO userStreetVO = userMapper.getUserStreet(u.getMemberId());
				u.setStreetKey(userStreetVO.getStreetKey());
				u.setStreetName(userStreetVO.getStreetName());
			}
		});
		return new PageInfo<User>(list);
	}
	@Override
	public User getUserDetail(String id) {
		User user = userMapper.selectByPrimaryKey(id);
		if(StringUtils.isNoneBlank(user.getImageId())) {
			File file = fileMapper.selectByPrimaryKey(user.getImageId());
			user.setImageUrl(file.getFileUrl());
		}
		if(StringUtils.isNoneBlank(user.getRole())) {
			String[] roles = user.getRole().split(",");
			user.setRoles(roles);
			List<String> rolesDesc = new ArrayList<>();
			for(String s:roles){
				rolesDesc.add(dictionaryMapper.selectOneByTypeAndDickey("UserRole", s).getValue());
			}
			user.setRolesDesc(rolesDesc);
			
		}
		UserStreetVO userStreetVO = userMapper.getUserStreet(user.getMemberId());
		if(userStreetVO!=null) {
			user.setStreetKey(userStreetVO.getStreetKey());
			user.setStreetName(userStreetVO.getStreetName());
			user.setType("3");
		}
		return user;
	}
	
	@Override
	public void registUser(UserParamModel record) {
		User user = new User();
		BeanUtils.copyProperties(record, user);
		userMapper.updateByMemberIdSelective(user);
		// 街道用户添加街道表
		if("3".equals(record.getType())) {
			UserStreetVO userStreetVO = new UserStreetVO();
			userStreetVO.setId(UUIDUtil.getUUid());
			userStreetVO.setStreetKey(record.getStreetKey());
			userStreetVO.setStreetName(record.getStreetName());
			userStreetVO.setMemberId(record.getMemberId());
			userMapper.insertUserStreet(userStreetVO);
		}
		
	}
	
	@Override
	@Transactional
	public int delUser(String id) {
		memberMapper.deleteByPrimaryKey(id);
		try {
			User user =userMapper.selectByPrimaryKey(id);
			actLogService.save(ThreadLocalCache.getUser(), "删除个人用户", user.getUserName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userMapper.deleteByPrimaryKey(id);
	}

	@Override
	public BaseResultModel batchDelete(String ids, String updateUser) {

		int count = 0;
		List<String> idList = Arrays.asList(ids.split(","));
		for (int i = 0; i < idList.size(); i++) {
			count = count + delUser(idList.get(i));
		}

		return BaseResultModel.success(count);
	}
}

