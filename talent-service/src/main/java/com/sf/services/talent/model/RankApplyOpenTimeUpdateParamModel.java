package com.sf.services.talent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class RankApplyOpenTimeUpdateParamModel {

    @ApiModelProperty(value = "职称申报开放开始时间", example = "2021-08-01 00:00:00")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    @ApiModelProperty(value = "职称申报开放结束时间", example = "2021-08-31 23:59:59")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @ApiModelProperty(value = "职称申报关闭提示内容", example = "职称申报关闭提示内容")
    private String closeContent;

}