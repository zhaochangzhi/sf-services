package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class CourseApplyVO {
    private String id;

    private String courseId;

    private String applyMemberId;

    private String applyMemberType;
    private String applyMemberTypeName;

    private String startTime;
    private String startAddress;

    private String applyMemberName;

    private Date applyTime;

    private Integer auditStatus;

    private String auditReason;

    private Date auditTime;

    private String auditUser;

    private String courseTypeName;

    private String courseName;

    @ApiModelProperty(value = "申请记录详情列表")
    private List<CourseApplyDetailVO> courseApplyDetailList;

}