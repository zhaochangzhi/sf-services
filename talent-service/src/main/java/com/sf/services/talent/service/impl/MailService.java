package com.sf.services.talent.service.impl;

import com.alibaba.fastjson.JSON;
import com.sf.services.talent.exceptions.AuthException;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.CheckEmailVerificationCodeParamModel;
import com.sf.services.talent.model.SendEmailVerificationCodeParamModel;
import com.sf.services.talent.model.SendMailParamModel;
import com.sf.services.talent.service.IMailService;
import com.sf.services.talent.util.JwtUtil;
import com.sf.services.talent.util.OtherUtil;
import com.sf.services.talent.util.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.Charsets;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import java.util.Map;

@Slf4j
@Service
public class MailService implements IMailService {

	protected static Logger interfaceLog = LoggerFactory.getLogger("interfaceLog");//interfaceLog

	@Autowired
	private MailSender mailSender;

	@Value("${spring.mail.username}")
	private String username;//邮箱账号
	@Value("${spring.mail.platform}")
	private String platform;//邮箱平台

	@Autowired
	private RedisUtils redisUtils;
	@Autowired
	private RedissonClient redisson;

	@Override
	public BaseResultModel send(SendMailParamModel params) {
		BaseResultModel baseResultModel = new BaseResultModel();
		String timeBegin = OtherUtil.returntime();//接口调用开始时间
		// 发送
		try {
			//校验非空-列表
			if(params == null){
				return baseResultModel.badParam("参数不能为空");
			}
			if (StringUtils.isBlank(params.getMail())) {
				return baseResultModel.badParam("邮箱地址不能为空");
			}
			if (StringUtils.isBlank(params.getTitle())) {
				return baseResultModel.badParam("邮箱title不能为空");
			}
			if (StringUtils.isBlank(params.getContent())) {
				return baseResultModel.badParam("邮箱内容不能为空");
			}

			// new 一个简单邮件消息对象
			SimpleMailMessage message = new SimpleMailMessage();
			// 和配置文件中的的username相同，相当于发送方
			message.setFrom(username+"@"+platform);
			// 收件人邮箱
			message.setTo(params.getMail());
			// 标题
			message.setSubject(params.getTitle());
			// 正文
			message.setText(params.getContent());

			System.out.println(JSON.toJSONString(message));

			//发送
			mailSender.send(message);

			return BaseResultModel.success(1);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("send", e);
			return BaseResultModel.badParam("发送失败");
		} finally {
			String timeEnd = OtherUtil.returntime();
			long second = OtherUtil.subTimeSecond(timeBegin, timeEnd);
			interfaceLog.info("MailService-send---:"+"params:"+ JSON.toJSONString(params) +",result:"+JSON.toJSONString(baseResultModel)+",second="+second);
		}
	}


	@Override
	public BaseResultModel sendEmailVerificationCode(SendEmailVerificationCodeParamModel params) {
		BaseResultModel baseResultModel = new BaseResultModel();
		String timeBegin = OtherUtil.returntime();//接口调用开始时间
		//校验非空-列表
		if(params == null){
			return baseResultModel.badParam("参数不能为空");
		}
		if (StringUtils.isBlank(params.getUserName())) {
			return baseResultModel.badParam("用户名不能为空");
		}
		if (StringUtils.isBlank(params.getEmail())) {
			return baseResultModel.badParam("邮箱能为空");
		}

		//redisson分布式锁
		String lockKey = "MailServicesendEmailVerificationCode"+params.getUserName();
		RLock rlock = redisson.getLock(lockKey);
		try {
			//校验是否并发
			if(rlock.isLocked()){
				return baseResultModel.badParam("正在发送");
			}

			//上锁
			rlock.lock();

			//redis发送验证码次数限制（每分钟每个账号发送一次）
			String verificationCodeTimesKey = "userNameVerificationCodeTimes"+params.getUserName();
			Long Expire = redisUtils.getExpire(verificationCodeTimesKey);
			if(Expire > 0){
				return baseResultModel.badParam("每分钟只能发送一次，请"+Expire+"秒后重试");
			}else if (Expire == 0){
				//返回0代表永久有效，这种情况需要重置时间（也可以利用此方法针对指定账号禁用其发送验证码）
				//重置时间 60秒
				redisUtils.expire(verificationCodeTimesKey, 60);
			}

			//生成token
			String id = params.getUserName();//JWTid(用户名)
			String subject = String.valueOf((int)((Math.random() * 9 + 1) * 100000));//验证码6位随机数字
			long ttlMillis = 60*30;//JWT生效时间（判断验证码是否已过期）（单位秒）
			String token = JwtUtil.createJWT(id, subject, ttlMillis);

			//token存入reis
			String redisKey = "userNameVerificationCode"+params.getUserName();
			String redisValue = token;
			long time = 60*60*2;//redis生效时间（判断验证码是否已生成）（单位秒）
			boolean isSetRedis = redisUtils.setObjectTime(redisKey, redisValue, time);
			if(!isSetRedis){
				return baseResultModel.badParam("验证码生成失败");
			}

			//发送邮件
			SendMailParamModel param = new SendMailParamModel();
			param.setMail(params.getEmail());
			param.setTitle("沈抚就业和人才服务中心|验证码");
			param.setContent("【沈抚就业和人才服务中心】您正在使用沈抚就业和人才服务中心服务，验证码为"+subject+"，请在30分钟内输入。");
			baseResultModel = send(param);
			if(baseResultModel != null && baseResultModel.getCode() != 200){
				return baseResultModel;
			}

			//存入redis发送验证码次数限制（每分钟每个账号发送一次）
			String verificationCodeTimesValye = params.getUserName();
			long verificationCodeTime = 60;//redis生效时间（判断验证码是否已生成）（单位秒）
			redisUtils.setObjectTime(verificationCodeTimesKey,verificationCodeTimesValye, verificationCodeTime);

			return baseResultModel.success(1);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("send", e);
			return baseResultModel.badParam("异常");
		}finally {
			//解锁
			rlock.unlock();

			String timeEnd = OtherUtil.returntime();
			long second = OtherUtil.subTimeSecond(timeBegin, timeEnd);
			interfaceLog.info("MailService-sendEmailVerificationCode---:"+"params:"+ JSON.toJSONString(params) +",result:"+JSON.toJSONString(baseResultModel)+",second="+second);
		}
	}

	@Override
	public BaseResultModel checkEmailVerificationCode(CheckEmailVerificationCodeParamModel params) {
		//校验非空-列表
		if(params == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if (StringUtils.isBlank(params.getUserName())) {
			return BaseResultModel.badParam("用户名不能为空");
		}
		if (StringUtils.isBlank(params.getVerificationCode())) {
			return BaseResultModel.badParam("验证码不能为空");
		}

		//根据用户名取出token(包含验证码信息)
		String redisKey = "userNameVerificationCode"+params.getUserName();
		Object tokenObject = redisUtils.getObject(redisKey);
		//校验redis是否已生成验证码
		if(tokenObject == null){
			return BaseResultModel.badParam("请发送验证码");
		}
		String token = String.valueOf(tokenObject);//token

		//解析token
		String[] jwtInfo = token.split("\\.");
		if (jwtInfo.length != 3) {
			throw new AuthException(403, "token异常");
		}
		String payLoad = jwtInfo[1];
		byte[] bytes = Base64.decodeBase64(payLoad);
		String jsonData = new String(bytes, Charsets.UTF_8);

		Map<String,Object> jsonMap = (Map<String,Object>) JSON.parse(jsonData);
		String id = jsonMap.get("jti").toString();//账号
		String subject = jsonMap.get("sub").toString();//验证码
		String strartTime = jsonMap.get("iat").toString();//生成时间
		String expireTime = jsonMap.get("exp").toString();//过期时间

		//校验token失效时间
		long nowMillis = System.currentTimeMillis();
		if(Long.parseLong(expireTime) >= nowMillis){
			return BaseResultModel.badParam("验证码已失效");
		}

		//校验正确性
		if(!params.getVerificationCode().equals(subject)){
			return BaseResultModel.badParam("验证码不正确");
		}

		//删除redis验证码
		redisUtils.delKey(redisKey);

		return BaseResultModel.success(1);
	}

}

