package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * 分页参数实体
 *
 * @author zhaochangzhi
 * @date 2021/7/10
 */
@Data
@ApiModel(value = "JobParamModel", description = "分页参数实体")
public class PageParamModel {

    private Integer pageNum = 1;

    private Integer pageSize = 10;
}
