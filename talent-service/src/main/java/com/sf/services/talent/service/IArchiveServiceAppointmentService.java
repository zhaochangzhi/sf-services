package com.sf.services.talent.service;

import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.ArchiveServiceAppointmentModel;
import com.sf.services.talent.model.po.ArchiveServiceAppointment;

/**
 * @ClassName: IArchiveServiceAppointment 
 * @Description: 档案预约
 * @author: heyang
 * @date: 2021年7月11日 下午5:44:34
 */
public interface IArchiveServiceAppointmentService {
	
	/**
	 * @Title: appointment 
	 * @Description: 预约
	 * @param ArchiveServiceAppointmentModel
	 * @return
	 * @return: int
	 */
	int appointment(ArchiveServiceAppointmentModel archiveServiceAppointment);
	
	/**
	 * @Title: getPageList 
	 * @Description: 获取分页列表
	 * @return
	 * @return: List<ArchiveServiceAppointment>
	 */
	PageInfo<ArchiveServiceAppointment> getPageList(ArchiveServiceAppointmentModel archiveServiceAppointment);
	
	/**
	 * @Title: audit 
	 * @Description: 审核
	 * @param ArchiveServiceAppointmentModel
	 * @return
	 * @return: int
	 */
	int audit(ArchiveServiceAppointmentModel archiveServiceAppointment);
	
	/**
	 * @Title: getNewestDetail 
	 * @Description: 获取最新一次详情
	 * @return
	 * @return: Map<String, Object>
	 */
	Map<String, Object> getNewestDetail();
}

