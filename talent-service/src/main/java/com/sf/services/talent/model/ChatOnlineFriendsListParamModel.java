package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "在线聊天-好友列表")
@Data
public class ChatOnlineFriendsListParamModel extends PageParamModel{

    @ApiModelProperty(value = "本人memberId", example = "0e123d79c2fa468ba901426ab21b3fcf")
    private String memberId;

    @ApiModelProperty(value = "朋友类型id（1：陌生人（聊过天），2：好友）", example = "1")
    private String friendTypeId;

}