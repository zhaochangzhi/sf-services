package com.sf.services.talent.controller;

import com.alibaba.fastjson.JSON;
import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.po.Dictionary;
import com.sf.services.talent.model.po.InterfaceTotal;
import com.sf.services.talent.service.impl.TotalService;
import com.sf.services.talent.util.IpAddressUtils;
import com.sf.services.talent.util.RedisUtils;
import com.sf.services.talent.util.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @ClassName: TotalController 
 * @Description: 统计controller
 * @author: heyang
 * @date: 2021年8月12日 下午9:26:15
 */
@Slf4j
@Api(value = "统计", tags = "统计")
@RestController
@RequestMapping("/total")
public class TotalController {
	
	@Autowired
	private TotalService totalService;
	@Autowired
	private RedisUtils redisUtils;
	
	
	@ApiOperation(notes = "统计页面", value="统计页面")
	@PostMapping("/home")
	public ResultUtil list(@RequestBody Map<String, Object> param) {
		try {
			Map<String, Object> res = new HashMap<>();
			res.put("overallStatistics", totalService.overallStatistics());
			res.put("overallArticle", totalService.overallArticle());
			res.put("publishArticle", totalService.publishArticle());
			res.put("overallUser", totalService.overallUser());
			res.put("overallInterface", totalService.overallInterface());
			return ResultUtil.success(res);
		} catch (Exception e) {
			log.error("统计页面异常:{}",e);
			return ResultUtil.error(401, "统计页面异常");
		}
		
	}
	
	
	
	@ApiOperation(notes = "用户注册量", value="用户注册量")
	@PostMapping("/userRegister")
	public ResultUtil userRegister(@RequestBody Map<String, Object> param) {
		try {
			return ResultUtil.success(totalService.userRegister());
		} catch (Exception e) {
			log.error("统计页面异常:{}",e);
			return ResultUtil.error(401, "统计页面异常");
		}
		
	}
	@ApiOperation(notes = "网站注册量", value="网站注册量")
	@PostMapping("/webRegister")
	@IgnoreSecurity
	public ResultUtil webRegister(@RequestBody Map<String, Object> param,HttpServletRequest request) {
		try {
			InterfaceTotal interfaceTotal = new InterfaceTotal();
			interfaceTotal.setName(param.get("name")+"");
			interfaceTotal.setUrl(param.get("url")+"");
			interfaceTotal.setTotal(1);
			totalService.saveInterface(interfaceTotal);
			//String localAddr=IpAddressUtils.getIpAddress(request);
			String localAddr=param.get("localAddr")+"";
			if(!redisUtils.hasKey(localAddr)){
				Dictionary dictionary = totalService.getSumMin();
				if(dictionary!=null) {
					String min = dictionary.getValue();
					int m = Integer.valueOf(min);
					redisUtils.setObjectTime(localAddr, "1", 60*m);
				}else {
					redisUtils.setObjectTime(localAddr, "1", 60*3600*2);
				}
				if("1".equals(param.get("url")+"")) {
					//两小时特殊处理标记
					interfaceTotal.setUrl("101");
					interfaceTotal.setName("就业和人才服务网");
				}else if("2".equals(param.get("url")+"")) {
					//两小时特殊处理标记
					interfaceTotal.setUrl("102");
					interfaceTotal.setName("沈抚人力资源服务产业园");
				}else if("3".equals(param.get("url")+"")) {
					//两小时特殊处理标记
					interfaceTotal.setUrl("103");
					interfaceTotal.setName("沈抚人力资源产业发展集团");
				}
				
				totalService.saveInterface(interfaceTotal);
			}
			return ResultUtil.success();
		} catch (Exception e) {
			log.error("统计页面异常:{}",e);
			return ResultUtil.error(401, "统计页面异常");
		}
		
	}
	
	@ApiOperation(notes = "网站点击率", value="网站点击率")
	@PostMapping("/webSite")
	public ResultUtil webSite(@RequestBody Map<String, Object> param) {
		try {
			return ResultUtil.success(totalService.webRegister());
		} catch (Exception e) {
			log.error("统计页面异常:{}",e);
			return ResultUtil.error(401, "统计页面异常");
		}
		
	}

	@IgnoreSecurity
	@ApiOperation(notes = " 就业人才网当天统计", value=" 就业人才网当天统计")
	@PostMapping("/webNowDayTotal")
	public ResultUtil webNowDayTotal(@RequestBody Map<String, Object> param) {
		try {
			return ResultUtil.success(totalService.webNowDayTotal(param.get("url")+""));
		} catch (Exception e) {
			log.error("统计页面异常:{}",e);
			return ResultUtil.error(401, "统计页面异常");
		}
		
	}

	@IgnoreSecurity
	@ApiOperation(notes = "就业人才网总统计", value="就业人才网总统计")
	@PostMapping("/webAllTotal")
	public ResultUtil webAllTotal(@RequestBody Map<String, Object> param) {
		try {
			return ResultUtil.success(totalService.webAllTotal(param.get("url")+""));
		} catch (Exception e) {
			log.error("统计页面异常:{}",e);
			return ResultUtil.error(401, "统计页面异常");
		}
		
	}
	
	@IgnoreSecurity
	@ApiOperation(notes = " 就业人才网按两小时当天统计", value=" 就业人才网按两小时当天统计")
	@PostMapping("/webNowDayTotalTwohours")
	public ResultUtil webNowDayTotalTwohours(@RequestBody Map<String, Object> param) {
		try {
			return ResultUtil.success(totalService.webNowDayTotalTwohours(param.get("url")+""));
		} catch (Exception e) {
			log.error("统计页面异常:{}",e);
			return ResultUtil.error(401, "统计页面异常");
		}
		
	}

	@IgnoreSecurity
	@ApiOperation(notes = " 就业人才网按两小时总统计", value=" 就业人才网按两小时总统计")
	@PostMapping("/webAllTotalTwohours")
	public ResultUtil webAllTotalTwohours(@RequestBody Map<String, Object> param) {
		try {
			return ResultUtil.success(totalService.webAllTotalTwohours(param.get("url")+""));
		} catch (Exception e) {
			log.error("统计页面异常:{}",e);
			return ResultUtil.error(401, "统计页面异常");
		}
		
	}
	
	@IgnoreSecurity
	@ApiOperation(notes = " 就业人才网按两小时总统计", value=" 就业人才网按两小时总统计")
	@PostMapping("/webAll")
	public ResultUtil webAll(@RequestBody Map<String, Object> param) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<Object> forEntity = restTemplate.getForEntity("https://openapi.baidu.com/rest/2.0/tongji/report/getData?access_token=121.dfd590f40d8c693b09d89f6a95c6f5af.YnZXieroJpM2sHW1d47Ygl0rsBDq2rGxbOg8UrA.JujEwQ&site_id=17284290&method=trend/time/a&gran=day&start_date="+param.get("startDate")+""
					+ "&end_date="+param.get("endDate")+"&metrics=pv_count,visitor_count", Object.class);
			return ResultUtil.success(forEntity.getBody());
		} catch (Exception e) {
			log.error("统计页面异常:{}",e);
			return ResultUtil.error(401, "统计页面异常");
		}
		
	}
	
	@IgnoreSecurity
	@ApiOperation(notes = "更改分钟变量", value=" 就业人才网按两小时总统计")
	@PostMapping("/updateSumMin")
	public ResultUtil updateSumMin(@RequestBody Map<String, Integer> param) {
		return ResultUtil.success();
		
	}
	
	@IgnoreSecurity
	@ApiOperation(notes = " 就业人才网按两小时总统计", value=" 就业人才网按两小时总统计")
	@PostMapping("/webTotalList")
	public ResultUtil webTotalList(@RequestBody Map<String, Object> param) {
		try {
			return ResultUtil.success(totalService.webTotalList(param));
		} catch (Exception e) {
			log.error("统计页面异常:{}",e);
			return ResultUtil.error(401, "统计页面异常");
		}
		
	}
	
	@IgnoreSecurity
	@ApiOperation(value = "导出Excel", notes = "导出Excel")
	@RequestMapping(value="/exportList",method = { RequestMethod.GET })
	public void exportList(HttpServletRequest request, HttpServletResponse response) {

		totalService.exportList(request, response);
	}
}

