package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 课程申请记录列表
 * @author guchangliang
 * @date 2021/8/13
 */
@Data
@ApiModel(description = "课程申请记录列表")
public class CourseApplyListParamModel extends PageParamModel {

    @ApiModelProperty(value = "课程id（t_course表id）", example = "ae2d6aae9fa4443fa86a23cd3a0a9f68")
    private String courseId;

    @ApiModelProperty(value = "审核状态：1 审核中，2 通过，3 不通过", example = "2")
    private Integer auditStatus;

    @ApiModelProperty(value = "课程名称", example = "xxx课程")
    private String courseName;

    @ApiModelProperty(value = "课程类型id（t_course_type表id）", example = "ae2d6aae9fa4443fa86a23cd3a0a9f68")
    private String courseTypeId;


}
