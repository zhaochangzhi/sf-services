package com.sf.services.talent.mapper;

import java.util.List;

import com.sf.services.talent.model.ActLogModel;
import com.sf.services.talent.model.po.ActLog;
import com.sf.services.talent.model.vo.ActLogVO;

public interface ActLogMapper {
	
	ActLog selectByPrimaryKey(String id);
	
	List<ActLogVO> selectAll(ActLogModel log);
	
	int deleteByPrimaryKey(String[] ids);
	
	int insertSelective(ActLog log);
	
	int updateByPrimaryKeySelective(ActLog log);
}

