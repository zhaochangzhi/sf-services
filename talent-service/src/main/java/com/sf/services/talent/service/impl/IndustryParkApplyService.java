package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.mapper.EnterpriseUserMapper;
import com.sf.services.talent.mapper.IndustryParkApplyMapper;
import com.sf.services.talent.mapper.MemberMapper;
import com.sf.services.talent.mapper.UserMapper;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.IndustryParkApplyAddParamModel;
import com.sf.services.talent.model.IndustryParkApplyAuditParamModel;
import com.sf.services.talent.model.IndustryParkApplyListParamModel;
import com.sf.services.talent.model.po.EnterprisePosition;
import com.sf.services.talent.model.po.IndustryParkApply;
import com.sf.services.talent.model.vo.EnterpriseUserVO;
import com.sf.services.talent.model.vo.IndustryParkApplyVO;
import com.sf.services.talent.service.IActLogService;
import com.sf.services.talent.service.IIndustryParkApplyService;
import com.sf.services.talent.util.UUIDUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Description: 课程
 * @author guchangliang
 * @date 2021/8/13
 */
@Slf4j
@Service
public class IndustryParkApplyService implements IIndustryParkApplyService {

    @Resource
    private IndustryParkApplyMapper industryParkApplyMapper;
    @Resource
    private MemberMapper memberMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    private EnterpriseUserMapper enterpriseUserMapper;
    @Autowired
    private RedissonClient redisson;
    @Resource
    private IActLogService actLogService;//日志


    @Override
    public PageInfo<IndustryParkApplyVO> getList(IndustryParkApplyListParamModel params) {

        if(params == null){
            return null;
        }
        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<IndustryParkApplyVO> list = industryParkApplyMapper.selectList(params);

        if(list != null && list.size() >= 1){
            for (int i = 0; i < list.size(); i++) {
                EnterpriseUserVO enterpriseUserVO = enterpriseUserMapper.selectByMemberId(list.get(i).getCreateUser());
                list.get(i).setEnterpriseUser(enterpriseUserVO);
            }
        }

        return new PageInfo<>(list);
    }

    @Override
    public BaseResultModel get(String id) {

        if(StringUtils.isBlank(id)){
            return BaseResultModel.badParam("id不能为空");
        }

        //查询申请课程记录
        IndustryParkApply industryParkApply = industryParkApplyMapper.selectByPrimaryKey(id);
        if(industryParkApply == null){
            return BaseResultModel.badParam("id不存在");
        }
        IndustryParkApplyVO industryParkApplyVO = new IndustryParkApplyVO();
        BeanUtils.copyProperties(industryParkApply, industryParkApplyVO);

        return BaseResultModel.success(industryParkApplyVO);
    }


    @Override
    public BaseResultModel add(IndustryParkApplyAddParamModel params, String userId) {
        //校验非空-列表
        if(params == null){
            return BaseResultModel.badParam("参数不能为空");
        }
        if(StringUtils.isBlank(userId)){
            return BaseResultModel.badParam("请重新登陆");
        }

        //redisson分布式锁
        String lockKey = "IndustryParkApplyServiceAdd"+userId;
        RLock rlock = redisson.getLock(lockKey);
        try {
            //上锁
            rlock.lock();

            //校验当前已有待审核状态数据
            //审核状态：1 审核中，2 通过，3 不通过
            int waitReviewCount = industryParkApplyMapper.selectCount(userId, 1);
            if (waitReviewCount >= 1) {
                return BaseResultModel.badParam("已申请，请等待审核");
            }

            //校验当前已审核通过状态数据
            //审核状态：1 审核中，2 通过，3 不通过
            int passedCount = industryParkApplyMapper.selectCount(userId, 2);
            if (passedCount >= 1) {
                return BaseResultModel.badParam("已通过，无需再次申请");
            }

            //新增
            IndustryParkApply industryParkApply = new IndustryParkApply();
            BeanUtils.copyProperties(params, industryParkApply);

            industryParkApply.setId(UUIDUtil.getUUid());//id
            industryParkApply.setCreateTime(new Date());//创建时间
            industryParkApply.setUpdateTime(industryParkApply.getCreateTime());//更新时间
            industryParkApply.setCreateUser(userId);//创建人

            industryParkApply.setAuditStatus(1);//审核状态：1 审核中，2 通过，3 不通过

            int count = industryParkApplyMapper.insertSelective(industryParkApply);

            //保存操作日志
            try {
                actLogService.save(userId, "入园申请", null);
            } catch (Exception e) {
                log.error("插入日志异常:{}",e);
            }

            return BaseResultModel.success(count);
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResultModel.badParam("异常");
        }finally {
            //解锁
            rlock.unlock();
        }



    }

    @Override
    public BaseResultModel audit(IndustryParkApplyAuditParamModel params, String userId) {
        //校验非空-列表
        if(params == null){
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getId())) {
            return BaseResultModel.badParam("id不能为空");
        }
        if (params.getAuditStatus() == null) {
            return BaseResultModel.badParam("审核状态不能为空");
        }
        //审核状态：1 审核中，2 通过，3 不通过
        if (params.getAuditStatus() != 2 && params.getAuditStatus() != 3) {
            return BaseResultModel.badParam("审核状态不正确");
        }
        //校验审核不通过原因
        if(params.getAuditStatus() == 3 ){
            //暂时不校验
        }else {
            //非审核不通过状态，将审核不通过原因置为空
            params.setAuditReason("");
        }

        //校验状态
        IndustryParkApply checkIndustryParkApply = industryParkApplyMapper.selectByPrimaryKey(params.getId());
        if (checkIndustryParkApply == null) {
            return BaseResultModel.badParam("id不存在");
        }
        if(checkIndustryParkApply.getAuditStatus() != null && (checkIndustryParkApply.getAuditStatus() == 2 || checkIndustryParkApply.getAuditStatus() == 3)){
            return BaseResultModel.badParam("已审核，无需再次审核");
        }

        //更新
        IndustryParkApply industryParkApply = new IndustryParkApply();
        BeanUtils.copyProperties(params, industryParkApply);

        industryParkApply.setAuditTime(new Date());//审核时间
        industryParkApply.setAuditUser(userId);//审核人

        int count = industryParkApplyMapper.updateByPrimaryKeySelective(industryParkApply);

        //保存操作日志
        try {
            actLogService.save(userId, "入园审核", checkIndustryParkApply.getEnterpriseName());
        } catch (Exception e) {
            log.error("插入日志异常:{}",e);
        }

        return BaseResultModel.success(count);
    }

    @Override
    public BaseResultModel getLast(String createUser) {

        if(createUser == null){
            return BaseResultModel.badParam("申请人id不能为空");
        }

        //查询倒叙列表
        IndustryParkApplyVO industryParkApplyVO = industryParkApplyMapper.selectLast(createUser, null);

        return BaseResultModel.success(industryParkApplyVO);
    }

}
