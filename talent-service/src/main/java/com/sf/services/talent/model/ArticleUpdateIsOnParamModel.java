package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author guchangliang
 * @date 2021/8/18
 */
@ApiModel(value = "文章管理-上下架")
@Data
public class ArticleUpdateIsOnParamModel {

    @ApiModelProperty(value = "主键id", example = "6f92b90f6cb24ee2beadb8a57d0a0eb1")
    private String id;

    @ApiModelProperty(value = "是否已发布（0：否，1：是）", example = "1")
    private Integer isOn;



}