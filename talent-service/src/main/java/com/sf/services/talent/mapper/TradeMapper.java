package com.sf.services.talent.mapper;

import com.sf.services.talent.model.po.Trade;

import java.util.List;
import java.util.Map;

public interface TradeMapper {
    int deleteByPrimaryKey(String id);

    int insert(Trade record);

    int insertSelective(Trade record);

    Trade selectByPrimaryKey(String id);

    int selectPageListCount(Map<String, Object> searchMap);

    List<Trade> selectPageList(Map<String, Object> searchMap);

    int updateByPrimaryKeySelective(Trade record);

    int updateByPrimaryKey(Trade record);

    /**
     * @Title: selectAll
     * @Description: 查询全部
     * @param record
     * @return
     * @return: List<Trade>
     */
    List<Trade> selectAll(Trade record);
}