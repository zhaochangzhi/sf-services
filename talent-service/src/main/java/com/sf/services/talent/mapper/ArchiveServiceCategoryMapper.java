package com.sf.services.talent.mapper;

import java.util.List;
import java.util.Map;

import com.sf.services.talent.model.po.ArchiveServiceCategory;

public interface ArchiveServiceCategoryMapper {
	
    int deleteByPrimaryKey(String id);

    int insert(ArchiveServiceCategory record);

    int insertSelective(ArchiveServiceCategory record);

    ArchiveServiceCategory selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ArchiveServiceCategory record);

    int updateByPrimaryKey(ArchiveServiceCategory record);
    
    List<ArchiveServiceCategory> list(ArchiveServiceCategory record);
    
    List<Map<String, Object>> getCategoryList();
    
    List<Map<String, Object>> getCategoryChirlderList(String name);
}