package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 *
 * @author guchangliang
 * @date 2021/8/3
 */
@ApiModel(value = "JobFairEnterpriseAddParamModel", description = "招聘会-企业报名参会-审核")
@Data
public class JobFairEnterpriseAuditParamModel {

    @ApiModelProperty(value = "招聘会id（t_job_fair表id）", example = "56b2f8f7e6d1494481050904aeeb75db")
    private String JobFairId;

    @ApiModelProperty(value = "职位id列表（t_enterprise_position表id）")
    private List<String> enterprisePositionIdList;

    @ApiModelProperty(value = "审核状态：1 审核中，2 通过，3 不通过", example = "2")
    private Integer auditStatus;
    @ApiModelProperty(value = "审核不通过原因", example = "审核不通过原因xxx")
    private String auditReason;



}
