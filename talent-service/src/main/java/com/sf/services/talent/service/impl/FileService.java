package com.sf.services.talent.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sf.services.talent.mapper.FileMapper;
import com.sf.services.talent.model.po.File;
import com.sf.services.talent.service.IFileService;
import com.sf.services.talent.util.FillUserInfoUtil;
import com.sf.services.talent.util.UUIDUtil;


@Service
public class FileService implements IFileService{

	@Autowired
	private FileMapper fileMapper;
	
	@Override
	public String upload(File record) {
		String fileId = UUIDUtil.getUUid();
		record.setFileId(fileId);
		FillUserInfoUtil.fillCreateUserInfo(record);
		fileMapper.insertSelective(record);
		return fileId;
	}

	@Override
	public File download(String fileId) {
		
		return fileMapper.selectByPrimaryKey(fileId);
	}
}

