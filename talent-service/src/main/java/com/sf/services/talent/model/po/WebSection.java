package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

/**
 * WebSection 网站栏目
 *
 * @author zhaochangzhi
 * @date 2021/7/20
 */
@Data
public class WebSection {

    private String id;

    private String webSiteKey;

    private String name;
    private String displayName;

    private Integer deleted;

    private String createUser;

    private Date createTime;

    private String updateUser;

    private Date updateTime;

    private String webSiteName;
}
