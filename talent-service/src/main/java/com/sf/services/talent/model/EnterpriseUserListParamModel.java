package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "企业用户列表", description = "企业用户列表")
public class EnterpriseUserListParamModel extends PageParamModel{

    @ApiModelProperty(value = "企业全称", example = "xxx科技有限公司")
    private String enterpriseName;

    @ApiModelProperty(value = "行业一级id（z_trade表id）（精确到一级数据）", example = "0bbb377381f94cfab6b712aaad51bd52")
    private String tradeOneId;
    @ApiModelProperty(value = "行业二级id（z_trade表id）（精确到二级数据）", example = "5d304ff8d7d94589bb18465db0fbc787")
    private String tradeTwoId;

    @ApiModelProperty(value = "企业性质key（字典t_dictionary表type=EnterpriseNature数据dickey）", example = "1")
    private String enterpriseNatureKey;

    @ApiModelProperty(value = "企业规模key（字典t_dictionary表type=EnterpriseScale数据diekey）", example = "1")
    private String enterpriseScaleKey;

    @ApiModelProperty(value = "是否为著名企业（0：否，1：是）", example = "1")
    private Integer isfamous;

}

