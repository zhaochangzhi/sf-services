package com.sf.services.talent.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * JobFairVO
 *
 * @author zhaochangzhi
 * @date 2021/7/23
 */
@ApiModel(value = "JobFairVO", description = "招聘会实体")
@Data
public class JobFairVO {

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "招聘会类型标识（1：现场招聘，2：网络招聘会）")
    private Integer typeStatus;

    @ApiModelProperty(value = "开始状态：0：即将开始，1：正在进行，2：已结束")
    private Integer onStatus;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "主办方")
    private String sponsor;

    @ApiModelProperty(value = "主举办地址")
    private String address;

    @JsonFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "开始时间")
    private Date startTime;

    @JsonFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "结束时间")
    private Date endTime;

    @ApiModelProperty(value = "联系人")
    private String contacts;

    @ApiModelProperty(value = "联系方式")
    private String contactsPhone;

    @ApiModelProperty(value = "推广图片id")
    private String imageId;

    @ApiModelProperty(value = "推广图片地址")
    private String imageUrl;

    @ApiModelProperty(value = "交通路线")
    private String routes;
    @ApiModelProperty(value = "招聘会介绍")
    private String introduce;
    @ApiModelProperty(value = "媒体宣传")
    private String media;
    @ApiModelProperty(value = "展位设置方案")
    private String booth;
    @ApiModelProperty(value = "参与办法")
    private String joinMethod;

    @ApiModelProperty(value = "是否为悬浮banner")
    private Integer isArticleFloatBanner;
    @ApiModelProperty(value = "悬浮banner图片id")
    private String articleFloatBannerPhotoId;
    @ApiModelProperty(value = "悬浮banner图片url")
    private String articleFloatBannerPhotoUrl;

}
