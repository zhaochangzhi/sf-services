package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "在线聊天-未读消息列表")
@Data
public class ChatOnlineMessageListParamModel {

    @ApiModelProperty(value = "本人memberId", example = "0e123d79c2fa468ba901426ab21b3fcf")
    private String memberId;

    @ApiModelProperty(value = "朋友memberId（非必填）", example = "0759720918c24b398bd08a220f622f4b")
    private String friendMemberId;
    

}