package com.sf.services.talent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class IndustryParkApplyAddParamModel {

    @ApiModelProperty(value = "企业名称", example = "企业名称")
    private String enterpriseName;

    @ApiModelProperty(value = "法定代表人", example = "法定代表人")
    private String legalRepresentativeName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "注册时间", example = "2021-08-01")
    private Date registTime;

    @ApiModelProperty(value = "注册地址", example = "注册地址")
    private String registAddress;

    @ApiModelProperty(value = "注册资本（万元整）", example = "100")
    private String registCapital;

    @ApiModelProperty(value = "实缴资金（万元整）", example = "100")
    private String actualCapital;

    @ApiModelProperty(value = "联系人", example = "联系人")
    private String contactName;

    @ApiModelProperty(value = "联系人电话", example = "联系人电话")
    private String contactMobile;

    @ApiModelProperty(value = "企业状态（1：存量企业，2：新成立公司）", example = "1")
    private Integer enterpriseStatus;

    @ApiModelProperty(value = "发展历程、主营业务及经营情况", example = "发展历程、主营业务及经营情况")
    private String enterpriseIntroduction;

    @ApiModelProperty(value = "共服务人次", example = "100")
    private Integer servicePersonTimes;

    @ApiModelProperty(value = "现有客户包括", example = "现有客户包括")
    private String existingCustomers;

    @ApiModelProperty(value = "现有客户涉及行业", example = "现有客户涉及行业")
    private String existingCustomersIndustry;

    @ApiModelProperty(value = "去年全年营业收入（万元）", example = "100")
    private String lastyearBusinessIncome;

    @ApiModelProperty(value = "去年全年纳税（万元）", example = "100")
    private String lastyearBusinessPayduty;

    @ApiModelProperty(value = "去年全年服务人次", example = "100")
    private Integer lastyearPersonTimes;

    @ApiModelProperty(value = "入驻产业园原因", example = "入驻产业园原因")
    private String joinReasons;

    @ApiModelProperty(value = "入驻产业园后预计开展的业务", example = "入驻产业园后预计开展的业务")
    private String joinBusiness;

    @ApiModelProperty(value = "是否取得劳务派遣经营许可证（0：否，1：是）", example = "1")
    private Integer isGetLwpqjyxkz;

    @ApiModelProperty(value = "是否取得人力资源服务许可证（0：否，1：是）", example = "1")
    private Integer isGetRlzyfwxkz;

    @ApiModelProperty(value = "客户资源、预签单客户", example = "客户资源、预签单客户")
    private String customerResources;

    @ApiModelProperty(value = "预开展业务", example = "预开展业务")
    private String expectBusiness;

    @ApiModelProperty(value = "预计全年营业收入（万元）", example = "100")
    private String expectYearIncome;

    @ApiModelProperty(value = "预计全年纳税（万元）", example = "100")
    private String expectYearPayduty;

    @ApiModelProperty(value = "预计服务人次", example = "100")
    private Integer expectYearPersonTimes;

    @ApiModelProperty(value = "申请办公面积（平方米）", example = "100")
    private String applyWorkArea;

    @ApiModelProperty(value = "是否需要办理劳务派遣经营许可证（0：否，1：是", example = "1")
    private Integer isNeedHandleLwpqjyxkz;

    @ApiModelProperty(value = "是否需要办理许人力资源服务可证（0：否，1：是）", example = "1")
    private Integer isNeedHandleRlzyfwxkz;


}