package com.sf.services.talent.mapper;

import com.sf.services.talent.model.po.SyrcCompanyJob;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * JobMapper
 *
 * @author zhaochangzhi
 * @date 2021/7/7
 */
@Mapper
public interface JobMapper {
    /**
     * 查询列表
     *
     * @return 列表数据
     */
    List<SyrcCompanyJob> selectList();

    /**
     * 根据id查询详情
     *
     * @param id id
     * @return 详情
     */
    SyrcCompanyJob selectById(@Param("id") Integer id);

    /**
     * 查询数量
     *
     * @return 数量
     */
    Integer selectCount();


}
