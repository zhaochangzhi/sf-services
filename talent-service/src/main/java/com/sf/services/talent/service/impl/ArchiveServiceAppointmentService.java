package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.config.ThreadLocalCache;
import com.sf.services.talent.mapper.ArchiveServiceAppointmentMapper;
import com.sf.services.talent.model.ArchiveServiceAppointmentModel;
import com.sf.services.talent.model.po.ArchiveServiceAppointment;
import com.sf.services.talent.service.IActLogService;
import com.sf.services.talent.service.IArchiveServiceAppointmentService;
import com.sf.services.talent.util.FillUserInfoUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: ArchiveServiceAppointmentService 
 * @Description: 预约档案接口实现
 * @author: heyang
 * @date: 2021年7月11日 下午6:08:39
 */
@Service
public class ArchiveServiceAppointmentService implements IArchiveServiceAppointmentService {
	
	@Autowired
	private ArchiveServiceAppointmentMapper archiveServiceAppointmentMapper;
	
	@Autowired
	private IActLogService actLogService;
	
	@Override
	public int appointment(ArchiveServiceAppointmentModel record) {
		String memberId = ThreadLocalCache.getUser();
		ArchiveServiceAppointment appoinment = new ArchiveServiceAppointment();
		BeanUtils.copyProperties(record, appoinment);
		appoinment.setMemberId(memberId);
		appoinment.setAppointmentTime(new Date());
		FillUserInfoUtil.fillCreateUserInfo(appoinment);
		actLogService.save(memberId, "档案服务预约", "");
		return archiveServiceAppointmentMapper.insert(appoinment);
	}
	@Override
	public PageInfo<ArchiveServiceAppointment> getPageList(ArchiveServiceAppointmentModel record) {
		PageHelper.startPage(record.getPageNum(), record.getPageSize());
		ArchiveServiceAppointment appoinment = new ArchiveServiceAppointment();
		BeanUtils.copyProperties(record, appoinment);
		List<ArchiveServiceAppointment> list =archiveServiceAppointmentMapper.selectAll(appoinment);
		return new PageInfo<>(list);
	}
	@Override
	public int audit(ArchiveServiceAppointmentModel record) {
		String memberId = ThreadLocalCache.getUser();
		ArchiveServiceAppointment appoinment = new ArchiveServiceAppointment();
		BeanUtils.copyProperties(record, appoinment);
		FillUserInfoUtil.fillUpdateUserInfo(appoinment);
		appoinment.setAuditMemberId(memberId);
		appoinment.setAuditTime(new Date());
		ArchiveServiceAppointment res = archiveServiceAppointmentMapper.selectByPrimaryKey(record.getId());
		actLogService.save(memberId, "审核档案服务预约", res.getApplicantName());
		return archiveServiceAppointmentMapper.updateByPrimaryKeySelective(appoinment);
	}
	@Override
	public Map<String, Object> getNewestDetail() {
		String memberId = ThreadLocalCache.getUser();
		return archiveServiceAppointmentMapper.getNewestDetail(memberId);
	}
}

