package com.sf.services.talent.mapper;

import com.sf.services.talent.model.po.ChatOnlineMessages;
import com.sf.services.talent.model.vo.ChatOnlineMessagesVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ChatOnlineMessagesMapper {
    int deleteByPrimaryKey(String id);

    int insert(ChatOnlineMessages record);

    int insertSelective(ChatOnlineMessages record);

    ChatOnlineMessages selectByPrimaryKey(String id);

    int selectPageListCount(Map<String, Object> searchMap);

    List<ChatOnlineMessages> selectPageList(Map<String, Object> searchMap);

    int updateByPrimaryKeySelective(ChatOnlineMessages record);

    int updateByPrimaryKey(ChatOnlineMessages record);

    //查询消息list
    List<ChatOnlineMessagesVO> selectList(@Param("fromMemberId") String fromMemberId, @Param("toMemberId") String toMemberId, @Param("status") Integer status, @Param("messagesTypeId") String messagesTypeId, @Param("limitNumber") Integer limitNumber);

    //查询双方消息list
    List<ChatOnlineMessagesVO> selectBothList(@Param("fromMemberId") String fromMemberId, @Param("toMemberId") String toMemberId, @Param("status") Integer status, @Param("messagesTypeId") String messagesTypeId, @Param("limitNumber") Integer limitNumber);

    //查询数据条数
    int selectCount(@Param("fromMemberId") String fromMemberId, @Param("toMemberId") String toMemberId, @Param("status") Integer status, @Param("messagesTypeId") String messagesTypeId);

    //根据fromMemberId,toMemberId更新status 接收状态（0：未接收，1：已接收）
    int updateStatus(@Param("fromMemberId") String fromMemberId, @Param("toMemberId") String toMemberId, @Param("status") Integer status);

    //查询最后一条聊天记录
    ChatOnlineMessages selectLastChat(@Param("fromMemberId") String fromMemberId, @Param("toMemberId") String toMemberId);


}