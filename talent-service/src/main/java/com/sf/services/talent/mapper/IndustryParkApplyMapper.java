package com.sf.services.talent.mapper;

import com.sf.services.talent.model.IndustryParkApplyListParamModel;
import com.sf.services.talent.model.po.IndustryParkApply;
import com.sf.services.talent.model.vo.IndustryParkApplyVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IndustryParkApplyMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(IndustryParkApply record);

    IndustryParkApply selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(IndustryParkApply record);

    //查询列表
    List<IndustryParkApplyVO> selectList(IndustryParkApplyListParamModel params);

    Integer selectCount(@Param("createUser") String createUser, @Param("auditStatus") Integer auditStatus);

    //查询最后一条
    IndustryParkApplyVO selectLast(@Param("createUser") String createUser, @Param("auditStatus") Integer auditStatus);
}