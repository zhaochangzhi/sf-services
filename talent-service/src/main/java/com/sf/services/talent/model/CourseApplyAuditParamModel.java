package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author guchangliang
 * @date 2021/8/15
 */
@ApiModel(value = "课程报名-审核")
@Data
public class CourseApplyAuditParamModel {

    @ApiModelProperty(value = "主键id（t_course_apply表id）", example = "0483ba8eb8414b6a8703edc0cec807f6")
    private String id;


    @ApiModelProperty(value = "审核状态：1 审核中，2 通过，3 不通过", example = "2")
    private Integer auditStatus;
    @ApiModelProperty(value = "审核不通过原因", example = "不通过原因xxx")
    private String auditReason;

}