package com.sf.services.talent.controller;

import com.sf.services.talent.annotations.CurrentUser;
import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.dto.UserDto;
import com.sf.services.talent.service.IArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api(value = "文章管理", tags = "文章管理")
@RestController
@RequestMapping("/article")
public class ArticleController {

	@Autowired
	private IArticleService articleService;

	/**
	 * 文章列表
	 * @param param
	 * @return
	 */
	@IgnoreSecurity
	@ApiOperation(notes = "文章列表", value="文章列表")
	@PostMapping("/list")
	public BaseResultModel list(@RequestBody ArticleParamModel param) {


		return BaseResultModel.success(articleService.list(param));
	}



	/**
	 * 文章详情
	 * @param param
	 * @return
	 */
	@IgnoreSecurity
	@ApiOperation(notes = "文章详情", value="文章详情")
	@PostMapping("/get")
	public BaseResultModel get(@RequestBody GetOneParamModel param) {

		if(param == null || StringUtils.isBlank(param.getId())){
			return BaseResultModel.badParam("参数不能为空");
		}

		return BaseResultModel.success(articleService.get(param.getId()));
	}

	@ApiOperation(notes = "添加文章", value="添加文章")
	@PostMapping("/add")
	public BaseResultModel add(@RequestBody ArticleAddParamModel param
			, @CurrentUser UserDto currentUser) {

		if(param == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if (currentUser == null || StringUtils.isBlank(currentUser.getUserId())) {
			return BaseResultModel.badParam("请登陆");
		}

		return articleService.add(param, currentUser.getUserId());
	}

	@ApiOperation(notes = "编辑文章", value="编辑文章")
	@PostMapping("/update")
	public BaseResultModel update(@RequestBody ArticleUpdateParamModel param
			, @CurrentUser UserDto currentUser) {

		if(param == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if (currentUser == null || StringUtils.isBlank(currentUser.getUserId())) {
			return BaseResultModel.badParam("请登陆");
		}

		return articleService.update(param, currentUser.getUserId());
	}

	@ApiOperation(notes = "文章-上下架", value="文章-上下架")
	@PostMapping("/updateIsOn")
	public BaseResultModel updateIsOn(@RequestBody ArticleUpdateIsOnParamModel param
			, @CurrentUser UserDto currentUser) {

		if(param == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if (currentUser == null || StringUtils.isBlank(currentUser.getUserId())) {
			return BaseResultModel.badParam("请登陆");
		}

		return articleService.updateIsOn(param, currentUser.getUserId());
	}

	@ApiOperation(notes = "删除文章", value="删除文章")
	@PostMapping("/delete")
	public BaseResultModel delete(@RequestBody GetOneParamModel param
			, @CurrentUser UserDto currentUser) {

		if(param == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if(StringUtils.isBlank(param.getId())){
			return BaseResultModel.badParam("id不能为空");
		}
		if (currentUser == null || StringUtils.isBlank(currentUser.getUserId())) {
			return BaseResultModel.badParam("请登陆");
		}

		return articleService.delete(param.getId(), currentUser.getUserId());
	}

	@ApiOperation(notes = "批量删除文章", value="批量删除文章")
	@PostMapping("/batchDelete")
	public BaseResultModel batchDelete(@RequestBody IdsParamModel param, @CurrentUser UserDto currentUser) {

		if(param == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if(StringUtils.isBlank(param.getIds())){
			return BaseResultModel.badParam("id不能为空");
		}
		if (currentUser == null || StringUtils.isBlank(currentUser.getUserId())) {
			return BaseResultModel.badParam("请登陆");
		}

		return articleService.batchDelete(param.getIds(), currentUser.getUserId());
	}

	@IgnoreSecurity
	@ApiOperation(notes = "文章类别列表", value="文章类别列表")
	@PostMapping("/webSection/list")
	public BaseResultModel webSectionList(@RequestBody ArticleParamModel param) {


		return BaseResultModel.success(articleService.list(param));
	}

	@IgnoreSecurity
	@ApiOperation(notes = "漂浮banner信息", value="漂浮banner信息")
	@PostMapping("/articleFloatBanner/get")
	public BaseResultModel articleFloatBannerGet() {

		return BaseResultModel.success(articleService.articleFloatBannerGet());
	}



	/**
	 * 栏目关联-列表
	 * @param param
	 * @return
	 */
	@IgnoreSecurity
	@ApiOperation(notes = "栏目关联-列表", value="栏目关联-列表")
	@PostMapping("/webSectionLink/list")
	public BaseResultModel webSectionLinkList(@RequestBody ArticleWebSectionLinkListParamModel param) {


		return BaseResultModel.success(articleService.webSectionLinkList(param));
	}

	@IgnoreSecurity
	@ApiOperation(notes = "栏目关联-详情", value="栏目关联-详情")
	@PostMapping("/webSectionLink/get")
	public BaseResultModel webSectionLinkGet(@RequestBody GetOneParamModel param) {


		return articleService.webSectionLinkGet(param);
	}

	@ApiOperation(notes = "栏目关联-添加", value="栏目关联-添加")
	@PostMapping("/webSectionLink/add")
	public BaseResultModel webSectionLinkAdd(@RequestBody ArticleWebSectionLinkAddParamModel param
			, @CurrentUser UserDto currentUser) {

		if(param == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if (currentUser == null || StringUtils.isBlank(currentUser.getUserId())) {
			return BaseResultModel.badParam("请登陆");
		}

		return articleService.webSectionLinkAdd(param, currentUser.getUserId());
	}

	@ApiOperation(notes = "栏目关联-编辑", value="栏目关联-编辑")
	@PostMapping("/webSectionLink/update")
	public BaseResultModel webSectionLinkUpdate(@RequestBody ArticleWebSectionLinkUpdateParamModel param
			, @CurrentUser UserDto currentUser) {

		if(param == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if (currentUser == null || StringUtils.isBlank(currentUser.getUserId())) {
			return BaseResultModel.badParam("请登陆");
		}

		return articleService.webSectionLinkUpdate(param, currentUser.getUserId());
	}

	@ApiOperation(notes = "栏目关联-删除", value="栏目关联-删除")
	@PostMapping("/webSectionLink/delete")
	public BaseResultModel webSectionLinkDelete(@RequestBody GetOneParamModel param
			, @CurrentUser UserDto currentUser) {

		if(param == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if(StringUtils.isBlank(param.getId())){
			return BaseResultModel.badParam("id不能为空");
		}
		if (currentUser == null || StringUtils.isBlank(currentUser.getUserId())) {
			return BaseResultModel.badParam("请登陆");
		}

		return articleService.webSectionLinkDelete(param.getId(), currentUser.getUserId());
	}

	@ApiOperation(notes = "栏目关联-批量删除", value="栏目关联-批量删除")
	@PostMapping("/webSectionLink/batchDelete")
	public BaseResultModel webSectionLinkBatchDelete(@RequestBody IdsParamModel param, @CurrentUser UserDto currentUser) {

		if(param == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if(StringUtils.isBlank(param.getIds())){
			return BaseResultModel.badParam("id不能为空");
		}
		if (currentUser == null || StringUtils.isBlank(currentUser.getUserId())) {
			return BaseResultModel.badParam("请登陆");
		}

		return articleService.webSectionLinkBatchDelete(param.getIds(), currentUser.getUserId());
	}

	@ApiOperation(notes = "栏目关联-更新文章关联栏目标识（0：未开启，1：已开启）", value="栏目关联-更新文章关联栏目标识（0：未开启，1：已开启）")
	@PostMapping("/webSectionLink/updateArticleWebSectionLinkStatus")
	public BaseResultModel webSectionUpdateArticleWebSectionLinkStatus(@RequestBody updateArticleWebSectionLinkStatusParamModel param
			, @CurrentUser UserDto currentUser) {

		if(param == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if (currentUser == null || StringUtils.isBlank(currentUser.getUserId())) {
			return BaseResultModel.badParam("请登陆");
		}

		return articleService.webSectionUpdateArticleWebSectionLinkStatus(param, currentUser.getUserId());
	}

}

