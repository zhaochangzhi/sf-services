package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.vo.EmploymentPositionVO;

/**
 * @ClassName: IEnterpriseUserService
 * @Description: 企业用户接口
 * @author: guchangliang
 * @date: 2021年7月23日 下午11:19:07
 */
public interface IEnterprisePositionService {

    /**
     *获取列表
     * @param params 参数
     * @return 列表
     */
    PageInfo<EmploymentPositionVO> getList(EmploymentPositionParamModel params);

    /**
     *获取列表
     * @param params 参数
     * @return 列表
     */
    EmploymentPositionVO getDetail(EmploymentPositionDetailParamModel params);

    /**
     * 企业中心-职位管理-创建职位
     * @param params 参数
     * @param userId
     * @return
     */
    BaseResultModel add(EmploymentPositionAddParamModel params
            , String userId);

    /**
     * 企业中心-职位管理-更新职位
     * @param params 参数
     * @param userId
     * @return
     */
    BaseResultModel update(EmploymentPositionUpdateParamModel params
            , String userId);

    /**
     * 企业中心-职位管理-审核
     * @param params 参数
     * @param userId
     * @return
     */
    BaseResultModel audit(EmploymentPositionAuditParamModel params
            , String userId);

    /**
     * 企业中心-职位管理-批量审核
     * @param params 参数
     * @param userId
     * @return
     */
    BaseResultModel batchAudit(EmploymentPositionBatchAuditParamModel params
            , String userId);

    /**
     * 企业中心-职位管理-删除职位
     * @param params 参数
     * @param userId
     * @return
     */
    BaseResultModel delete(EmploymentPositionDeleteParamModel params, String userId);

    /**
     * 企业中心-职位管理-批量删除职位
     * @param params 参数
     * @param userId
     * @return
     */
    BaseResultModel batchDelete(IdsParamModel params, String userId);

    /**
     * 增加职位xx数量
     * @param id 职位表id（t_enterprise_position表id）
     * @param resumeNum 增加的收到简历数量
     * @param browseNum 增加的浏览数量
     * @param collectionNum 增加的收藏数量
     * @return
     */
    BaseResultModel addNum(String id, Integer resumeNum, Integer browseNum, Integer collectionNum);

    /**
     * 减掉职位xx数量
     * @param id 职位表id（t_enterprise_position表id）
     * @param resumeNum 减掉的收到简历数量
     * @param browseNum 减掉的浏览数量
     * @param collectionNum 减掉的收藏数量
     * @return
     */
    BaseResultModel reduceNum(String id, Integer resumeNum, Integer browseNum, Integer collectionNum);
}

