package com.sf.services.talent.controller;

import com.alibaba.fastjson.JSON;
import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.exceptions.AuthException;
import com.sf.services.talent.mapper.MemberMapper;
import com.sf.services.talent.model.TestParam;
import com.sf.services.talent.model.po.Member;
import com.sf.services.talent.service.IMerberService;
import com.sf.services.talent.util.JwtUtil;
import com.sf.services.talent.util.OtherUtil;
import com.sf.services.talent.util.RedisUtils;
import com.sf.services.talent.util.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.Charsets;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * @author guchangliang
 * @date 2021/8/19
 */
@Slf4j
@Api(value = "测试", tags = "测试")
@RestController
@RequestMapping("/test")
public class TestController {
	
	@Autowired
	private IMerberService merberService;
	@Autowired
	private MemberMapper memberMapper;
	@Autowired
	private RedisUtils redisUtils;

	/**
	 * createJWT
	 * @return
	 */
	@IgnoreSecurity
	@ApiOperation(notes = "创建JWT", value="创建JWT")
	@PostMapping("/createJWT")
	public ResultUtil createJWT(@RequestBody TestParam params) {


		return ResultUtil.success(JwtUtil.createJWT(params.getId(), params.getSubject(), params.getTtlMillis()));
	}

	@IgnoreSecurity
	@ApiOperation(notes = "获取登陆用户JWT", value="获取登陆用户JWT")
	@GetMapping("/getLoginUserJWT")
	public ResultUtil getLoginUserJWT(String memberId) {
		Member member = memberMapper.selectByPrimaryKey(memberId);

		return ResultUtil.success(JwtUtil.createJWT(member.getType(), member.getMemberId(), 1000*60*60*8));
	}

	@IgnoreSecurity
	@ApiOperation(notes = "解析JWT", value="解析JWT")
	@PostMapping("/explainJWT")
	public ResultUtil explainJWT(@RequestBody TestParam params) {
		String authorization = params.getAuthorization();
		// 解析 authorization
		String[] jwtInfo = authorization.split("\\.");
		if (jwtInfo.length != 3) {
			log.error("token数据异常！");
			throw new AuthException(403, "登录信息有误");
		}
		String payLoad = jwtInfo[1];
		byte[] bytes = Base64.decodeBase64(payLoad);
		String jsonData = new String(bytes, Charsets.UTF_8);

		Map<String,Object> jsonMap = (Map<String,Object>) JSON.parse(jsonData);
		String id = jsonMap.get("jti").toString();
		String subject = jsonMap.get("sub").toString();
		String strartTime = jsonMap.get("iat").toString();
		String expireTime = jsonMap.get("exp").toString();

		System.out.println(id);
		System.out.println(subject);
		System.out.println(expireTime);

		Date date = new Date();
		date.setTime(Long.parseLong(expireTime)*1000);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println(format.format(date));


		return ResultUtil.success(jsonData);
	}

	@IgnoreSecurity
	@ApiOperation(notes = "获取redis值", value="获取redis值")
	@RequestMapping(value = "/getRedisObject", method = { RequestMethod.GET, RequestMethod.POST })
	public ResultUtil getRedisObject(String key) {
		System.out.println(key);
		Object object = redisUtils.getObject(key);

		return ResultUtil.success(object);
	}

	@IgnoreSecurity
	@ApiOperation(notes = "设置redis值", value="设置redis值")
	@RequestMapping(value = "/setRedisObject", method = { RequestMethod.GET, RequestMethod.POST })
	public ResultUtil setRedisObject(String key, String value) {
		System.out.println(key);
		System.out.println(value);


		return ResultUtil.success(redisUtils.setObject(key, value));
	}

	@IgnoreSecurity
	@ApiOperation(notes = "设置redis值带生效时间（单位秒）", value="设置redis值带生效时间（单位秒")
	@RequestMapping(value = "/setRedisObjectTime", method = { RequestMethod.GET, RequestMethod.POST })
	public ResultUtil setRedisObjectTime(String key, String value, long time) {
		System.out.println(key);
		System.out.println(value);
		System.out.println(time);


		return ResultUtil.success(redisUtils.setObjectTime(key, value, time));
	}

	@IgnoreSecurity
	@ApiOperation(notes = "删除redis值", value="删除redis值")
	@RequestMapping(value = "/deleteRedisObject", method = { RequestMethod.GET, RequestMethod.POST })
	public ResultUtil deleteRedisObject(String key) {
		System.out.println(key);
		redisUtils.delKey(key);
		return ResultUtil.success(1);
	}

	@IgnoreSecurity
	@ApiOperation(notes = "查询redis值过期时间", value="查询redis值过期时间")
	@RequestMapping(value = "/getRedisObjectExpire", method = { RequestMethod.GET, RequestMethod.POST })
	public ResultUtil getRedisObjectExpire(String key) {
		System.out.println(key);

		return ResultUtil.success(redisUtils.getExpire(key));
	}

	@IgnoreSecurity
	@GetMapping(value = "/testHtml")
	public Object testHtml(HttpServletRequest request, ModelAndView mv)  {

		System.out.println(1111);
		mv.setViewName("test");
		return mv;
	}

	@IgnoreSecurity
	@ApiOperation(notes = "查询当前时间", value="查询当前时间")
	@RequestMapping(value = "/getReturntime", method = { RequestMethod.GET, RequestMethod.POST })
	public ResultUtil getReturntime(String key) {

		return ResultUtil.success(OtherUtil.returntime());
	}

}
