package com.sf.services.talent.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "简历-教育经历", description = "简历-教育经历")
public class ResumeEducationParamModel {
	
	@ApiModelProperty(value = "主键id", example = "49f36f829ce6449294ad1f19e2a6aa59")
	private String id;
	
	@ApiModelProperty(value = "学校名称", example = "东北大学")
    private String schoolName;
	
	@ApiModelProperty(value = "开始时间", example = "2020-02-07")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date startTime;
	
	@ApiModelProperty(value = "结束时间", example = "2020-02-07")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date endTime;
	@ApiModelProperty(value = "学历", example = "本科")
    private String education;
	@ApiModelProperty(value = "专业", example = "计算机")
	private String major;
}

