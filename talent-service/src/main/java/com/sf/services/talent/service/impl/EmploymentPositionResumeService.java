package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.mapper.EnterprisePositionCollectionMapper;
import com.sf.services.talent.mapper.EnterprisePositionMapper;
import com.sf.services.talent.mapper.EnterprisePositionResumeMapper;
import com.sf.services.talent.mapper.EnterpriseUserMapper;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.po.EnterprisePosition;
import com.sf.services.talent.model.po.EnterprisePositionCollection;
import com.sf.services.talent.model.po.EnterprisePositionResume;
import com.sf.services.talent.model.vo.EmploymentPositionResumeAndUserVO;
import com.sf.services.talent.model.vo.EmploymentPositionResumeVO;
import com.sf.services.talent.model.vo.EnterpriseUserVO;
import com.sf.services.talent.service.IActLogService;
import com.sf.services.talent.service.IEnterprisePositionResumeService;
import com.sf.services.talent.service.IEnterprisePositionService;
import com.sf.services.talent.util.UUIDUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * EmploymentPositionResumeService
 * @author guchangliang
 * @date 2021/7/26
 */

@Slf4j
@Service
public class EmploymentPositionResumeService implements IEnterprisePositionResumeService {

    @Resource
    private EnterprisePositionResumeMapper enterprisePositionResumeMapper;
    @Resource
    private EnterprisePositionCollectionMapper enterprisePositionCollectionMapper;
    @Resource
    private IEnterprisePositionService enterprisePositionService;
    @Resource
    private EnterpriseUserMapper enterpriseUserMapper;
    @Resource
    private EnterprisePositionMapper enterprisePositionMapper;
    @Autowired
    private RedissonClient redisson;
    @Resource
    private IActLogService actLogService;//日志

    @Override
    public PageInfo<EmploymentPositionResumeVO> positionList(EmploymentPositionResumeParamModel params) {

        if(params == null){
            return null;
        }

        if(StringUtils.isBlank(params.getSortKey())){
            params.setSortKey("update_time");
        }
        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<EmploymentPositionResumeVO> list = enterprisePositionResumeMapper.selectPositionList(params);

        //查询企业信息
        if (list != null && list.size() >= 1) {
            for (int i = 0; i < list.size(); i++) {
                EnterpriseUserVO enterpriseUserVO = enterpriseUserMapper.selectById(list.get(i).getEnterpriseUserId());
                //set企业信息
                list.get(i).setEnterpriseUser(enterpriseUserVO);
            }
        }

        return new PageInfo<>(list);
    }

    @Override
    public BaseResultModel add(EmploymentPositionResumeAddParamModel params) {
        //校验非空-列表
        if (params == null) {
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getEnterprisePositionId())) {
            return BaseResultModel.badParam("职位id不能为空");
        }
        if (StringUtils.isBlank(params.getResumeId())) {
            return BaseResultModel.badParam("简历id不能为空");
        }
        if (StringUtils.isBlank(params.getMemberId())) {
            return BaseResultModel.badParam("会员id不能为空");
        }

        //redisson分布式锁
        String lockKey = "EmploymentPositionResumeServiceAdd"+params.getMemberId();
        RLock rlock = redisson.getLock(lockKey);
        try {
            //上锁
            rlock.lock();

            //查询是否已申请
            int applyCount = enterprisePositionResumeMapper.selectApplyCount(params.getEnterprisePositionId(), params.getResumeId(), params.getMemberId());
            if(applyCount >= 1){
                return BaseResultModel.badParam("已申请，无需再次申请");
            }

            //新增
            EnterprisePositionResume enterprisePositionResume = new EnterprisePositionResume();
            BeanUtils.copyProperties(params, enterprisePositionResume);

            enterprisePositionResume.setId(UUIDUtil.getUUid());//id
            enterprisePositionResume.setCreatTime(new Date());//创建时间
            enterprisePositionResume.setCreateUser(params.getMemberId());//创建人
            int count = enterprisePositionResumeMapper.insertSelective(enterprisePositionResume);

            //增加职位申请简历数量
            enterprisePositionService.addNum(params.getEnterprisePositionId(), 1, null, null);

            //保存操作日志
            try {
                EnterprisePosition enterprisePosition = enterprisePositionMapper.selectByPrimaryKey(params.getEnterprisePositionId());
                actLogService.save(params.getMemberId(), "申请职位", enterprisePosition.getEnterprisePositionName());
            } catch (Exception e) {
                log.error("插入日志异常:{}",e);
            }

            return BaseResultModel.success(count);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("EmploymentPositionResumeServiceAdd", e);
            return BaseResultModel.badParam("异常");
        }finally {
            //解锁
            rlock.unlock();
        }
    }

    @Override
    public BaseResultModel collectionAdd(EmploymentPositionCollectionAddParamModel params) {
        //校验非空-列表
        if (params == null) {
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getEnterprisePositionId())) {
            return BaseResultModel.badParam("职位id不能为空");
        }
        if (StringUtils.isBlank(params.getMemberId())) {
            return BaseResultModel.badParam("会员id不能为空");
        }

        //查询是否已收藏
        int applyCount = enterprisePositionCollectionMapper.selectCollectCount(params.getEnterprisePositionId(), params.getMemberId());
        if(applyCount >= 1){
            return BaseResultModel.badParam("已收藏，无需再次收藏");
        }

        //新增
        EnterprisePositionCollection enterprisePositionCollection = new EnterprisePositionCollection();
        BeanUtils.copyProperties(params, enterprisePositionCollection);

        enterprisePositionCollection.setId(UUIDUtil.getUUid());//id
        enterprisePositionCollection.setCreatTime(new Date());//创建时间
        enterprisePositionCollection.setCreateUser(params.getMemberId());//创建人
        int count = enterprisePositionCollectionMapper.insertSelective(enterprisePositionCollection);

        //增加职位收藏简历数量
        if(count >= 1) {
            enterprisePositionService.addNum(params.getEnterprisePositionId(), null, null, 1);
        }

        return BaseResultModel.success(count);
    }


    @Override
    public BaseResultModel collectionDelete(EmploymentPositionCollectionAddParamModel params) {
        //校验非空-列表
        if (params == null) {
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getEnterprisePositionId())) {
            return BaseResultModel.badParam("职位id不能为空");
        }
        if (StringUtils.isBlank(params.getMemberId())) {
            return BaseResultModel.badParam("会员id不能为空");
        }

        //查询是否已收藏
        EnterprisePositionCollection enterprisePositionCollection = enterprisePositionCollectionMapper.selectCollect(params.getEnterprisePositionId(), params.getMemberId());
        if(enterprisePositionCollection == null){
            return BaseResultModel.badParam("未收藏，无法取消收藏");
        }

        //删除
        int count = enterprisePositionCollectionMapper.deleteByPrimaryKey(enterprisePositionCollection.getId());

        //减掉职位收藏简历数量
        if(count >= 1){
            enterprisePositionService.reduceNum(params.getEnterprisePositionId(), null, null, 1);
        }


        return BaseResultModel.success(count);
    }

    @Override
    public PageInfo<EmploymentPositionResumeVO> list(EmploymentPositionResumeListParamModel params) {
        if(params == null){
            return null;
        }
        if(StringUtils.isBlank(params.getMemberId())){
            return null;
        }
        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<EmploymentPositionResumeVO> list = enterprisePositionResumeMapper.selectList(params);

        //查询企业信息
        if (list != null && list.size() >= 1) {
            for (int i = 0; i < list.size(); i++) {
                EnterpriseUserVO enterpriseUserVO = enterpriseUserMapper.selectById(list.get(i).getEnterpriseUserId());
                //set企业信息
                list.get(i).setEnterpriseUser(enterpriseUserVO);
            }
        }

        return new PageInfo<>(list);
    }


    @Override
    public PageInfo<EmploymentPositionResumeVO> collectionList(EmploymentPositionResumeCollectionListParamModel params) {
        if(params == null){
            return null;
        }
        if(StringUtils.isBlank(params.getMemberId())){
            return null;
        }

        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<EmploymentPositionResumeVO> list = enterprisePositionResumeMapper.selectCollectionList(params);

        //查询企业信息
        if (list != null && list.size() >= 1) {
            for (int i = 0; i < list.size(); i++) {
                EnterpriseUserVO enterpriseUserVO = enterpriseUserMapper.selectById(list.get(i).getEnterpriseUserId());
                //set企业信息
                list.get(i).setEnterpriseUser(enterpriseUserVO);
            }
        }

        return new PageInfo<>(list);
    }

    @Override
    public PageInfo<EmploymentPositionResumeAndUserVO> positionResumeList(EmploymentPositionResumeAndUserParamModel params) {

        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<EmploymentPositionResumeAndUserVO> list = enterprisePositionResumeMapper.selectPositionResumeList(params.getEnterpriseUserId());

        return new PageInfo<>(list);
    }
}
