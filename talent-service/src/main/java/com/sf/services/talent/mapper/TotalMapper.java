package com.sf.services.talent.mapper;
/**
 * @ClassName: TotalMapper 
 * @Description: 求和mapper
 * @author: heyang
 * @date: 2021年8月10日 下午11:24:48
 */

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface TotalMapper {
	/**
	 * @Title: overallStatistics 
	 * @Description: 总体统计
	 * @return
	 * @return: Map<String,Object>
	 */
	Map<String, Object> overallStatistics();
	
	/**
	 * @Title: overallArticle 
	 * @Description: 文章统计
	 * @return
	 * @return: Map<String,Integer>
	 */
	Map<String, Integer> overallArticle();
	
	/**
	 * @Title: publishArticle 
	 * @Description: 发布统计
	 * @return
	 * @return: Map<String,Integer>
	 */
	Map<String, Integer> publishArticle();
	
	/**
	 * @Title: overallUser 
	 * @Description: 用户统计
	 * @return
	 * @return: Map<String,Integer>
	 */
	Map<String, Integer> overallUser();
	
	/**
	 * @Title: overallInterface 
	 * @Description: 接口统计
	 * @return
	 * @return: List<Map<String,Integer>>
	 */
	List<Map<String, Integer>> overallInterface();
	
	/**
	 * @Title: userRegister 
	 * @Description: 用户注册量统计
	 * @param type 用户类型
	 * @return
	 * @return: List<Map<String,Integer>>
	 */
	List<Map<String, Integer>> userRegister(String type);
	
	/**
	 * @Title: webRegister 
	 * @Description: 网站点击量
	 * @param url
	 * @return
	 * @return: List<Map<String,Integer>>
	 */
	List<Map<String, Integer>> webRegister(@Param("url")String url);
	
	/**
	 * @Title: webNowDayTotal 
	 * @Description: 就业人才网当天统计
	 * @return
	 * @return: Map<String,Integer>
	 */
	Map<String,Integer> webNowDayTotal(@Param("url")String url);
	
	/**
	 * @Title: webAllTotal 
	 * @Description: 就业人才网总统计
	 * @return
	 * @return: Map<String,Integer>
	 */
	Map<String,Integer> webAllTotal(@Param("url")String url);
	

	/**
	 * @Title: webNowDayTotal 
	 * @Description: 就业人才网按两小时当天统计
	 * @return
	 * @return: Map<String,Integer>
	 */
	Map<String,Integer> webNowDayTotalTwohours(@Param("url")String url);
	
	/**
	 * @Title: webAllTotal 
	 * @Description: 就业人才网按两小时总统计
	 * @return
	 * @return: Map<String,Integer>
	 */
	Map<String,Integer> webAllTotal101Twohours(@Param("url")String url);
	
	/**
	 * @Title: webTotalList 
	 * @Description: 列表统计
	 * @param param
	 * @return
	 * @return: List<Map<String,Object>>
	 */
	List<Map<String, Object>> webTotalList(Map<String, Object> param);
}

