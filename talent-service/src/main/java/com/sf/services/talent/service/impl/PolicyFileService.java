package com.sf.services.talent.service.impl;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.mapper.PolicyFileMapper;
import com.sf.services.talent.model.PolicyFileModel;
import com.sf.services.talent.model.po.PolicyFile;
import com.sf.services.talent.service.IPolicyFileService;
import com.sf.services.talent.util.FillUserInfoUtil;

@Service
public class PolicyFileService implements IPolicyFileService {
	
	@Autowired
	private PolicyFileMapper policyFileMapper;
	
	@Override
	public int add(PolicyFileModel record) {
		PolicyFile policyFile = new PolicyFile();
		BeanUtils.copyProperties(record, policyFile);
		FillUserInfoUtil.fillCreateUserInfo(policyFile);
		return policyFileMapper.insertSelective(policyFile);
	}
	@Override
	public int delete(PolicyFileModel record) {
		
		return policyFileMapper.deleteByPrimaryKey(record.getId());
	}
	@Override
	public PageInfo<PolicyFile> list(PolicyFileModel record) {
		PolicyFile policyFile = new PolicyFile();
		BeanUtils.copyProperties(record, policyFile);
		PageHelper.startPage(record.getPageNum(), record.getPageSize());
		List<PolicyFile> list = policyFileMapper.selectAll(policyFile);
		return new PageInfo<>(list);
	}
	@Override
	public int updateStatus(PolicyFileModel record) {
		PolicyFile policyFile = new PolicyFile();
		BeanUtils.copyProperties(record, policyFile);
		FillUserInfoUtil.fillUpdateUserInfo(policyFile);
		return policyFileMapper.updateByPrimaryKeySelective(policyFile);
	}
}

