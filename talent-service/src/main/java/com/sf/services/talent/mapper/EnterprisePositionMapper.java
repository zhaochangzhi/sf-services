package com.sf.services.talent.mapper;

import com.sf.services.talent.model.EmploymentPositionParamModel;
import com.sf.services.talent.model.po.EnterprisePosition;
import com.sf.services.talent.model.vo.EmploymentPositionVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EnterprisePositionMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(EnterprisePosition record);

    EnterprisePosition selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(EnterprisePosition record);

    /**
     * 查询列表
     *
     * @param params 参数
     * @return 列表数据
     */
    List<EmploymentPositionVO> selectList(EmploymentPositionParamModel params);

    /**
     * 增加职位xx数量
     * @param id    职位表id（t_enterprise_position表id）
     * @param resumeNum 增加的收到简历数量
     * @param browseNum 增加的浏览数量
     * @param collectionNum 增加的收藏数量
     * @return
     */
    int addNum(@Param("id") String id, @Param("resumeNum") Integer resumeNum, @Param("browseNum") Integer browseNum, @Param("collectionNum") Integer collectionNum);

    /**
     * 减掉职位xx数量
     * @param id    职位表id（t_enterprise_position表id）
     * @param resumeNum 减掉的收到简历数量
     * @param browseNum 减掉的浏览数量
     * @param collectionNum 减掉的收藏数量
     * @return
     */
    int reduceNum(@Param("id") String id, @Param("resumeNum") Integer resumeNum, @Param("browseNum") Integer browseNum, @Param("collectionNum") Integer collectionNum);

    //查询发布的职位数量
    Integer selectPositionNum(@Param("enterpriseUserId") String enterpriseUserId);
}