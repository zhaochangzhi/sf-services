package com.sf.services.talent.config;

import com.sf.services.talent.controller.WebSocketServer;
import com.sf.services.talent.service.IChatOnlineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

@Configuration
public class WebSocketConfig {

    /**
     * 注入一个ServerEndpointExporter,该Bean会自动注册使用@ServerEndpoint注解申明的websocket endpoint
     */
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }

    //WebSocketServer中如果想使用service层需要在这里引入
    @Autowired
    public void setSenderService(IChatOnlineService chatOnlineService){
        WebSocketServer.chatOnlineService = chatOnlineService;
    }

}