package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 就业指标详情参数实体
 *
 * @author guchangliang
 * @date 2021/7/13
 */
@Data
@ApiModel(value = "EmploymentTargetDetailParamModel", description = "就业指标详情参数实体")
public class EmploymentTargetDetailParamModel {

    @ApiModelProperty(value = "指标id（t_employment_target表id）", example = "16f86a544ca24e24a1a1b1c5cfda7bc6")
    private String id;



}
