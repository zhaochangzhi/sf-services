package com.sf.services.talent.mapper;

import com.sf.services.talent.model.po.ArticleFloatBanner;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ArticleFloatBannerMapper {
    int deleteByPrimaryKey(String id);

    int insert(ArticleFloatBanner record);

    int insertSelective(ArticleFloatBanner record);

    ArticleFloatBanner selectByPrimaryKey(String id);

    int selectPageListCount(Map<String, Object> searchMap);

    List<ArticleFloatBanner> selectPageList(Map<String, Object> searchMap);

    int updateByPrimaryKeySelective(ArticleFloatBanner record);

    int updateByPrimaryKey(ArticleFloatBanner record);

    //查询最后一条
    ArticleFloatBanner selectLast();

    //查询一条
    ArticleFloatBanner select(@Param("moduleTypeStatus") Integer moduleTypeStatus, @Param("moduleId") String moduleId);
}