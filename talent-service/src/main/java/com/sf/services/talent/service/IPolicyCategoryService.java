package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.PolicyCategoryModel;
import com.sf.services.talent.model.po.PolicyCategory;

/**
 * @ClassName: IPolicyCategory 
 * @Description: 政策类别接口
 * @author: heyang
 * @date: 2021年7月13日 下午8:53:17
 */
public interface IPolicyCategoryService {
	
	/**
	 * @Title: add 
	 * @Description: 添加政策类别
	 * @param record
	 * @return
	 * @return: int
	 */
	 int add(PolicyCategoryModel record);
	 
	 /**
	  * @Title: delete 
	  * @Description: 删除类别
	  * @param record
	  * @return
	  * @return: int
	  */
	 int delete(PolicyCategoryModel record);
	 
	 /**
	  * @Title: list 
	  * @Description: 类别列表
	  * @param record
	  * @return
	  * @return: PageInfo<PolicyCategory>
	  */
	 PageInfo<PolicyCategory> list(PolicyCategoryModel record);
	 
	 /**
	  * @Title: updateStatus 
	  * @Description: 更新状态
	  * @param record
	  * @return
	  * @return: int
	  */
	 int updateStatus(PolicyCategoryModel record);
}

