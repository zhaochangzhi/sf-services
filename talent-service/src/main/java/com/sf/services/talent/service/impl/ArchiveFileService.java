package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.mapper.ArchiveFileMapper;
import com.sf.services.talent.mapper.FileMapper;
import com.sf.services.talent.model.ArchiveFileParamModel;
import com.sf.services.talent.model.ArchiveFileUploadeParamModel;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.po.ArchiveFile;
import com.sf.services.talent.service.IArchiveFileService;
import com.sf.services.talent.util.UUIDUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 *
 * @author guchangliang
 * @date 2021/8/9
 */
@Service
public class ArchiveFileService implements IArchiveFileService {

    @Resource
    private FileMapper fileMapper;
    @Resource
    private ArchiveFileMapper archiveFileMapper;


    @Override
    public BaseResultModel list(ArchiveFileParamModel params) {
        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<ArchiveFile> list = archiveFileMapper.selectList(params);
        return BaseResultModel.success(new PageInfo<>(list));
    }

    @Override
    public BaseResultModel delete(String id, String updateUser) {
        //软删除
        ArchiveFile archiveFile = new ArchiveFile();
        archiveFile.setId(id);
        archiveFile.setUpdateTime(new Date());
        archiveFile.setUpdateUser(updateUser);
        archiveFile.setIsdelete(1);//删除标识
        int count = archiveFileMapper.updateByPrimaryKeySelective(archiveFile);

        return BaseResultModel.success(count);
    }

    @Override
    public BaseResultModel uploadFile(ArchiveFileUploadeParamModel params, String createUser) {
        ArchiveFile archiveFile = new ArchiveFile();
        archiveFile.setFileId(params.getFileId());
        archiveFile.setId(UUIDUtil.getUUid());
        archiveFile.setCreatTime(new Date());
        archiveFile.setCreateUser(createUser);
        archiveFile.setIsdelete(0);
        int count = archiveFileMapper.insertSelective(archiveFile);

        return BaseResultModel.success(count);
    }
}
