package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * RankFileVO
 *
 * @author zhaochangzhi
 * @date 2021/7/19
 */
@ApiModel(value = "RankFileVO", description = "职称文件实体")
@Data
public class RankFileVO {

    @ApiModelProperty(value = "id", example = "uuid")
    private String id;

    @ApiModelProperty(value = "职称码")
    private String rankCode;

    @ApiModelProperty(value = "职称名称")
    private String rankName;

    @ApiModelProperty(value = "附件id")
    private String attachmentId;

    @ApiModelProperty(value = "文件名称")
    private String originalFileName;

    @ApiModelProperty(value = "职称申报方式key（t_dictionary表type=RankApplyType）")
    private String rankApplyTypeKey;

    @ApiModelProperty(value = "职称申报方式（评审/认定）")
    private String rankApplyType;
}
