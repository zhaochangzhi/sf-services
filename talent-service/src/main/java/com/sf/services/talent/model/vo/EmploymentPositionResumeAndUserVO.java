package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 投递职位信息及投递简历用户信息
 * @author guchangliang
 * @date 2021/8/1
 */
@ApiModel(value = "投递职位信息及投递简历用户信息")
@Data
public class EmploymentPositionResumeAndUserVO extends PageVO{

    @ApiModelProperty(value = "主键id（t_enterprise_position_resume表id）")
    private String id;

    @ApiModelProperty(value = "企业-职位id（t_enterprise_position表id）")
    private String enterprisePositionId;

    @ApiModelProperty(value = "企业账号id（t_enterprise_user表id）")
    private String enterpriseUserId;

    @ApiModelProperty(value = "简历id（t_resume表id）")
    private String resumeId;

    @ApiModelProperty(value = "用户姓名")
    private String userName;

    @ApiModelProperty(value = "用户年龄")
    private Integer age;

    @ApiModelProperty(value = "用户工作经验")
    private String workExperience;

    @ApiModelProperty(value = "用户学历")
    private String education;

    @ApiModelProperty(value = "求职状态")
    private String jobStatusValue;

    @ApiModelProperty(value = "投递职位名称")
    private String enterprisePositionName;

    @ApiModelProperty(value = "投递简历时间")
    private String creatTime;

    @ApiModelProperty(value = "简历头像id")
    private String userPhoto;
    @ApiModelProperty(value = "简历头像地址")
    private String userPhotoUrl;



}