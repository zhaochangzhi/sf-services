package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

/**
 * EmploymentTarget
 * 就业指标
 * @author guchangliang
 * @date 2021/7/13
 */
@Data
public class EmploymentTarget {
    private String id;

    private String typeId;

    private String typeName;

    private Integer formStatus;

    private Integer symbolStatus;

    private Integer complexStatus;

    private Integer targetYear;

    private String targetYearTarget;

    private String streetKey;

    private String streetName;

    private String remark;

    private Date creatTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;



}