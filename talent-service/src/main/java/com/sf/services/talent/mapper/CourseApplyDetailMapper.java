package com.sf.services.talent.mapper;

import com.sf.services.talent.model.po.CourseApplyDetail;
import com.sf.services.talent.model.vo.CourseApplyDetailVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CourseApplyDetailMapper {
    int deleteByPrimaryKey(String id);

    int insert(CourseApplyDetail record);

    int insertSelective(CourseApplyDetail record);

    CourseApplyDetail selectByPrimaryKey(String id);

    int selectPageListCount(Map<String, Object> searchMap);

    List<CourseApplyDetail> selectPageList(Map<String, Object> searchMap);

    int updateByPrimaryKeySelective(CourseApplyDetail record);

    int updateByPrimaryKey(CourseApplyDetail record);

    //查询列表
    List<CourseApplyDetailVO> selectList(@Param("courseApplyId") String courseApplyId);
}