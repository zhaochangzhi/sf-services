package com.sf.services.talent.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * JobFairDto
 *
 * @author zhaochangzhi
 * @date 2021/7/23
 */
@Data
public class JobFairDto {

    private String id;

    private Integer typeStatus;

    private String title;

    private String sponsor;

    private String address;

    private Date startTime;

    private Date endTime;

    private String contacts;

    private String contactsPhone;

    private String imageId;

    private String imageUrl;

    private Integer status;

    private String createUser;

    private Date createTime;

    private String updateUser;

    private Date updateTime;

    private Integer deleted;

    private String routes;
    private String introduce;
    private String media;
    private String booth;
    private String joinMethod;

    @ApiModelProperty(value = "是否为悬浮banner", example = "1")
    private Integer isArticleFloatBanner;
    @ApiModelProperty(value = "悬浮banner图片id", example = "0096ff5444014df48c6301751d1f0830")
    private String articleFloatBannerPhotoId;

    @ApiModelProperty(value = "开启状态（0：未开启，1：进行中，2：已结束）", example = "1")
    private Integer onStatus;
}
