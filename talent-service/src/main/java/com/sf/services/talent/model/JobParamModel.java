package com.sf.services.talent.model;

import com.github.pagehelper.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 工作筛选条件参数实体
 *
 * @author zhaochangzhi
 * @date 2021/7/7
 */
@Data
@ApiModel(value = "JobParamModel", description = "工作筛选条件参数实体")
public class JobParamModel extends Page {

    @ApiModelProperty(value = "值（示例）", example = "0")
    private Integer id;
}
