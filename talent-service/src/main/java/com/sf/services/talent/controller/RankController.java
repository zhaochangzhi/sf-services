package com.sf.services.talent.controller;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.RankParamModel;
import com.sf.services.talent.model.dto.RankDto;
import com.sf.services.talent.service.IRankService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * RankController
 *
 * @author zhaochangzhi
 * @date 2021/7/12
 */
@Api(value = "职称管理", tags = "职称管理")
@RestController
@RequestMapping("/rank")
public class RankController {

    private final IRankService rankService;

    @Autowired
    public RankController(IRankService rankService) {
        this.rankService = rankService;
    }

    @ApiOperation(value = "职称列表", notes = "职称列表")
    @PostMapping("/list")
    public BaseResultModel list(@RequestBody RankParamModel params) {
        PageInfo<RankDto> pageInfo = rankService.getList(params);
        return BaseResultModel.success(pageInfo);
    }




}
