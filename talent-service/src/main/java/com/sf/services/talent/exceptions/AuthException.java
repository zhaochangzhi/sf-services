package com.sf.services.talent.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 权限异常类
 *
 * @author zhaochangzhi
 * @date 2021/7/7
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthException extends RuntimeException{

    /**
     * 错误码
     */
    int code;

    /**
     * 错误信息
     */
    String message;
}

