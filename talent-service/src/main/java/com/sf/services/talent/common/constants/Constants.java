package com.sf.services.talent.common.constants;

/**
 * 常量
 *
 * @author zhaochangzhi
 * @date 2021/7/9
 */
public class Constants {

	/**
	 * 当前登录人信息
	 */
	public final static String CURRENT_USER = "currentUser";
}
