package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.RankFileParamModel;
import com.sf.services.talent.model.RankParamModel;
import com.sf.services.talent.model.dto.RankDto;
import com.sf.services.talent.model.dto.RankFileDto;
import com.sf.services.talent.model.vo.RankFileVO;
import com.sf.services.talent.model.vo.RankVO;

/**
 * IRankService
 *
 * @author zhaochangzhi
 * @date 2021/7/10
 */
public interface IRankService {


    /**
     * 获取列表
     *
     * @param params 参数
     * @return 列表
     */
    PageInfo<RankDto> getList(RankParamModel params);

    /**
     * 获取列表(带附件)
     *
     * @param params 参数
     * @return 列表
     */
    PageInfo<RankFileVO> getListWithAttachment(RankFileParamModel params);

    /**
     * 根据id查询详情
     *
     * @param id 主键
     * @return 详情
     */
    RankVO getOneById(Integer id);

    /**
     * 新增
     *
     * @param params 参数
     * @return 结果
     */
    Integer addOne(RankDto params);

    /**
     * 根据id删除
     *
     * @param id 主键
     * @return 结果
     */
    Integer deleteOneById(Integer id);

    /**
     * 修改
     *
     * @param params 职称
     * @return 结果
     */
    Integer updateOneById(RankDto params);

    /**
     * 上传文件
     *
     * @param rankFileDto 参数
     * @return 结果
     */
    Integer uploadRankFile(RankFileDto rankFileDto);

    /**
     * 删除职称文件
     *
     * @param id 参数
     * @return 结果
     */
    Integer deleteOneRankFile(String id);
}
