package com.sf.services.talent.interceptors;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.common.constants.Constants;
import com.sf.services.talent.config.ThreadLocalCache;
import com.sf.services.talent.enums.InterfaceEnum;
import com.sf.services.talent.model.po.InterfaceTotal;
import com.sf.services.talent.service.impl.TotalService;
import com.sf.services.talent.util.IpAddressUtils;
import com.sf.services.talent.util.RequestUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 权限拦截器（拦截CurrentUser注解项）
 *
 * @author zhaochangzhi
 * @date 2021/7/9
 */
@Slf4j
@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {
	
	@Autowired
	private TotalService totalService;

	/**
	 * 在请求处理之前进行调用（Controller方法调用之前）
	 *
	 * @param request 请求体
	 * @param response 结果体
	 * @param handler 对象控制器
	 * @return false 执行拦截
	 * @throws Exception 异常
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

		if (!(handler instanceof HandlerMethod)) {
			return true;
		}

		HandlerMethod handlerMethod = (HandlerMethod) handler;
		Method method = handlerMethod.getMethod();
		String requestPath = request.getRequestURI();

		if (requestPath.contains("/v2/api-docs")
				|| requestPath.contains("/swagger")
				|| requestPath.contains("/configuration/ui")
				|| requestPath.contains("/error")) {
			return true;
		}

		String ip = IpAddressUtils.getIpAddress(request);
		log.info("requestPath : " + requestPath + ", ip: " + ip);
		String name = InterfaceEnum.getName(requestPath);
		if (name != null) {
			try {
				totalService.updateInterfaceTotal(requestPath);
			} catch (Exception e) {
				if (e instanceof DuplicateKeyException) {
					totalService.updateInterfaceTotal(requestPath);
				} else {
					log.error("接口统计异常:{}", e);
				}
			}
		}
		if (method.isAnnotationPresent(IgnoreSecurity.class)) {
			return true;
		}
		//token获取用户信息
		request.setAttribute(Constants.CURRENT_USER, RequestUtils.getCurrentUser());
		return true;
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
			@Nullable Exception ex) throws Exception {
		ThreadLocalCache.removeByKey(Constants.CURRENT_USER);
		ThreadLocalCache.removeByKey("type");
	}
}