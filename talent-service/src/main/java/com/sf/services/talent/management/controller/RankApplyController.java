package com.sf.services.talent.management.controller;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.annotations.CurrentUser;
import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.mapper.RankApplyMapper;
import com.sf.services.talent.mapper.RankApplyOpenTimeMapper;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.RankApplyOpenTimeUpdateParamModel;
import com.sf.services.talent.model.RankApplyParamModel;
import com.sf.services.talent.model.dto.RankApplyDto;
import com.sf.services.talent.model.dto.UserDto;
import com.sf.services.talent.model.po.EnterprisePosition;
import com.sf.services.talent.model.po.RankApplyOpenTime;
import com.sf.services.talent.model.vo.RankApplyOpenTimeVO;
import com.sf.services.talent.model.vo.RankApplyVO;
import com.sf.services.talent.service.IActLogService;
import com.sf.services.talent.service.IRankApplyService;
import com.sf.services.talent.util.UUIDUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * RankApplyController
 *
 * @author zhaochangzhi
 * @date 2021/7/10
 */
@Slf4j
@Api(value = "管理后台-职称申请", tags = "后台管理-职称申请")
@RestController(value = "ManageRankApplyController")
@RequestMapping("/manage/rankapply")
public class RankApplyController {

    private final IRankApplyService rankApplyService;

    @Autowired
    public RankApplyController(IRankApplyService rankApplyService) {
        this.rankApplyService = rankApplyService;
    }

    @Resource
    private RankApplyMapper rankApplyMapper;
    @Resource
    private RankApplyOpenTimeMapper rankApplyOpenTimeMapper;
    @Resource
    private IActLogService actLogService;//日志

    @ApiOperation(value = "职称申请列表", notes = "职称申请列表")
    @ApiImplicitParam(name = "params", dataType = "RankApplyParamModel")
    @PostMapping("/list")
    public BaseResultModel list(@RequestBody RankApplyParamModel params) {
        PageInfo<RankApplyDto> pageInfo = rankApplyService.getList(params);
        return BaseResultModel.success(pageInfo);

    }

    @IgnoreSecurity
    @ApiOperation(value = "职称申请列表导出", notes = "职称申请列表导出")
    @ApiImplicitParam(name = "params", dataType = "RankApplyParamModel")
    @RequestMapping(value="/exportList",method = { RequestMethod.GET })
    public void exportList(String rankApplyTypeKey, String rankCode, Integer auditStatus, HttpServletRequest request, HttpServletResponse response) {
        RankApplyParamModel params = new RankApplyParamModel();
        params.setRankApplyTypeKey(rankApplyTypeKey);
        params.setRankCode(rankCode);
        params.setAuditStatus(auditStatus);
        rankApplyService.exportList(params , request, response);

    }

    @ApiOperation(value = "审核", notes = "审核状态(auditStatus)枚举：1审核中， 2审核通过， 3审核不通过")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "params", dataType = "RankApplyVO",
                    value = "需要关注字段：id(职称申请主键)，auditStatus(审核状态)，appointmentTime(预约处理时间)/auditComment(批退原因)")
    })
    @PostMapping("/actions/approve")
    public BaseResultModel<Integer> approve(@RequestBody RankApplyVO params, @CurrentUser UserDto currentUser) {
        /*
         * 主键id，审核状态，办理时间/原因
         */
        if (params.getId() == null) {
            return BaseResultModel.badParam("主键id不能为空");
        }
        //审核状态不能为空并且 不能为1（待审核状态）
        if (StringUtils.isBlank(params.getAuditStatus()) || "1".equals(params.getAuditStatus())) {
            return BaseResultModel.badParam("审核状态[auditStatus]不能为空并且不能为1（待审核状态）");
        }
        //如果审核状态为2（通过），办理时间不能为空； 如果审核状态为3（不通过），审批原因不能为空
        if ("2".equals(params.getAuditStatus())) {
            if (null == params.getAppointmentStartTime() || null == params.getAppointmentEndTime()) {
                return BaseResultModel.badParam("预约办理时间不能为空");
            }
            //审核原因置空
            params.setAuditComment("");
        }
        if ("3".equals(params.getAuditStatus())) {
            if (StringUtils.isBlank(params.getAuditComment())) {
                return BaseResultModel.badParam("批退原因[auditComment]不能为空");
            }
        }
        RankApplyDto rankApplyDto = new RankApplyDto();
        BeanUtils.copyProperties(params, rankApplyDto);
        rankApplyDto.setAuditUserId(currentUser.getUserId());
        rankApplyDto.setAuditTime(new Date());

        //保存操作日志
        try {
            actLogService.save(currentUser.getUserId(), "审核职称申报", params.getApplyUserName()+params.getRankName());
        } catch (Exception e) {
            log.error("插入日志异常:{}",e);
        }

        return BaseResultModel.success(rankApplyService.updateOneById(rankApplyDto));
    }

    @ApiOperation(value = "查询职称申报开放时间", notes = "查询职称申报开放时间")
    @PostMapping("/OpenTime/get")
    public BaseResultModel openTimeGet() {
        RankApplyOpenTime rankApplyOpenTime = rankApplyOpenTimeMapper.selectFirst();
        if(rankApplyOpenTime == null){
            return BaseResultModel.badParam("数据缺失");
        }
        if(rankApplyOpenTime.getStartTime() == null || rankApplyOpenTime.getEndTime() == null){
            return BaseResultModel.badParam("数据开始/结束时间缺失");
        }

        Integer isOpen = 0;//是否开启
        Date nowDate = new Date();//当前时间
        if(nowDate.after(rankApplyOpenTime.getStartTime()) && nowDate.before(rankApplyOpenTime.getEndTime())){
            isOpen = 1;//是否开启
        }
        RankApplyOpenTimeVO rankApplyOpenTimeVO = new RankApplyOpenTimeVO();
        BeanUtils.copyProperties(rankApplyOpenTime, rankApplyOpenTimeVO);
        rankApplyOpenTimeVO.setIsOpen(isOpen);

        return BaseResultModel.success(rankApplyOpenTimeVO);
    }

    @ApiOperation(value = "更新职称申报开放时间", notes = "更新职称申报开放时间")
    @PostMapping("/OpenTime/update")
    public BaseResultModel openTimeUpdate(@RequestBody RankApplyOpenTimeUpdateParamModel params
            , @CurrentUser UserDto currentUser) {

        //校验
        if(params.getStartTime() == null || params.getEndTime() == null){
            return BaseResultModel.badParam("数据开始/结束时间缺失");
        }
        if(params.getStartTime().after(params.getEndTime())){
            return BaseResultModel.badParam("开始时间不能大于结束时间");
        }

        //判断数据若为空则初始化
        RankApplyOpenTime rankApplyOpenTime = rankApplyOpenTimeMapper.selectFirst();
        if(rankApplyOpenTime == null){
            //数据缺失 初始化
            rankApplyOpenTime = new RankApplyOpenTime();
            rankApplyOpenTime.setId(UUIDUtil.getUUid());
            rankApplyOpenTimeMapper.insertSelective(rankApplyOpenTime);
        }

        //更新
        RankApplyOpenTime rankApplyOpenTimeUpdate = new RankApplyOpenTime();
        BeanUtils.copyProperties(params, rankApplyOpenTimeUpdate);
        rankApplyOpenTimeUpdate.setId(rankApplyOpenTime.getId());
        rankApplyOpenTimeUpdate.setUpdateTime(new Date());
        rankApplyOpenTimeUpdate.setUpdateUser(currentUser.getUserId());
        int count = rankApplyOpenTimeMapper.updateByPrimaryKeySelective(rankApplyOpenTimeUpdate);

        return BaseResultModel.success(count);
    }

}
