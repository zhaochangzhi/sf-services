package com.sf.services.talent.mapper;

import com.sf.services.talent.model.WebSectionParamModel;
import com.sf.services.talent.model.po.Article;
import com.sf.services.talent.model.po.ArticleWebSectionLink;
import com.sf.services.talent.model.po.WebSection;

import java.util.List;


public interface WebSectionMapper {


    WebSection selectByPrimaryKey(String id);

    /**
     * 查询列表
     * @param record
     * @return
     */
    List<WebSection> selectList(WebSectionParamModel record);


    int updateByPrimaryKeySelective(WebSection record);

}