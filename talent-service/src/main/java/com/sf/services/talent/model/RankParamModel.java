package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 职称筛选条件参数实体
 *
 * @author zhaochangzhi
 * @date 2021/7/10
 */
@Data
@ApiModel(value = "RankParamModel", description = "职称筛选条件参数实体")
public class RankParamModel extends PageParamModel {

    @ApiModelProperty(value = "职称码")
    private String rankCode;
}
