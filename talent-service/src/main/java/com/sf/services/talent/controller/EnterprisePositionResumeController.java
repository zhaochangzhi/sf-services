package com.sf.services.talent.controller;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.vo.EmploymentPositionResumeAndUserVO;
import com.sf.services.talent.model.vo.EmploymentPositionResumeVO;
import com.sf.services.talent.service.IEnterprisePositionResumeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Slf4j
@Api(value = "找工作", tags = "找工作")
@RestController
@RequestMapping("/enterprisePositionResume")
public class EnterprisePositionResumeController {
	
	@Resource
	private IEnterprisePositionResumeService enterprisePositionResumeService;

	@IgnoreSecurity
	@ApiOperation(value = "找工作-职位列表", notes = "找工作-职位列表")
	@PostMapping("/positionList")
	public BaseResultModel positionList(@RequestBody EmploymentPositionResumeParamModel params) {


		PageInfo<EmploymentPositionResumeVO> pageInfo = enterprisePositionResumeService.positionList(params);
		return BaseResultModel.success(pageInfo);
	}


	@ApiOperation(notes = "找工作-申请", value="找工作-申请")
	@PostMapping("/add")
	public BaseResultModel add(@RequestBody EmploymentPositionResumeAddParamModel param) {

		return enterprisePositionResumeService.add(param);
	}

	@ApiOperation(value = "找工作-我申请的职位列表", notes = "找工作-我申请的职位列表")
	@PostMapping("/list")
	public BaseResultModel list(@RequestBody EmploymentPositionResumeListParamModel params) {


		PageInfo<EmploymentPositionResumeVO> pageInfo = enterprisePositionResumeService.list(params);
		return BaseResultModel.success(pageInfo);
	}

	@ApiOperation(notes = "找工作-收藏", value="找工作-收藏")
	@PostMapping("/collection/add")
	public BaseResultModel collectionAdd(@RequestBody EmploymentPositionCollectionAddParamModel param) {

		return enterprisePositionResumeService.collectionAdd(param);
	}

	@ApiOperation(notes = "找工作-取消收藏", value="找工作-取消收藏")
	@PostMapping("/collection/delete")
	public BaseResultModel collectionDelete(@RequestBody EmploymentPositionCollectionAddParamModel param) {

		return enterprisePositionResumeService.collectionDelete(param);
	}

	@ApiOperation(value = "找工作-我收藏的职位列表", notes = "找工作-我收藏的职位列表")
	@PostMapping("/collection/list")
	public BaseResultModel collectionList(@RequestBody EmploymentPositionResumeCollectionListParamModel params) {


		PageInfo<EmploymentPositionResumeVO> pageInfo = enterprisePositionResumeService.collectionList(params);
		return BaseResultModel.success(pageInfo);
	}



	@ApiOperation(value = "企业中心-简历管理（企业收到的简历列表）", notes = "企业中心-简历管理（企业收到的简历列表）")
	@PostMapping("/positionResumeList")
	public BaseResultModel positionResumeList(@RequestBody EmploymentPositionResumeAndUserParamModel params) {

		PageInfo<EmploymentPositionResumeAndUserVO> pageInfo = enterprisePositionResumeService.positionResumeList(params);
		return BaseResultModel.success(pageInfo);
	}




}

