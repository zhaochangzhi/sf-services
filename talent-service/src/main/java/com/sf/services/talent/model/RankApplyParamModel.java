package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 职称申请筛选条件参数实体
 *
 * @author zhaochangzhi
 * @date 2021/7/10
 */
@Data
@ApiModel(value = "RankApplyParamModel", description = "职称申请筛选条件参数实体")
public class RankApplyParamModel extends PageParamModel {

    @ApiModelProperty("职称申报方式字典key值")
    private String rankApplyTypeKey;

    @ApiModelProperty("职称申报方式(文字描述)")
    private String rankApplyType;

    @ApiModelProperty(value = "职称码")
    private String rankCode;

    @ApiModelProperty(value = "申请人姓名")
    private String applyUserName;

    @ApiModelProperty(value = "申请人性别")
    private String applyUserSex;

    @ApiModelProperty(value = "申请人身份证号")
    private String applyUserIdNumber;

    @ApiModelProperty(value = "申请人出生日期")
    private String applyUserBirthday;

    @ApiModelProperty(value = "申请人电话", example = "13940711234")
    private String applyUserPhone;

    @ApiModelProperty(value = "申请人学历")
    private String applyUserEducation;

    @ApiModelProperty(value = "申请人第二学历")
    private String applyUserEducation2;

    @ApiModelProperty(value = "申请人第三学历")
    private String applyUserEducation3;

    @ApiModelProperty(value = "所属单位全称")
    private String applyUserCompanyFullName;

    /**
     * 方式：认定 需填 认定专业、认定等级
     */
    @ApiModelProperty(value = "认定专业")
    private String identifyMajor;

    @ApiModelProperty(value = "认定等级")
    private String identifyLevel;

    /**
     * 方式：评审 需填 已取得职称证书专业（等级）、申报专业所属行业（专业）、申报等级、论文数量
     */
    @ApiModelProperty(value = "已取得职称证书名称")
    private String obtainedCredentials;
    @ApiModelProperty(value = "已取得职称证书等级")
    private String obtainedCredentialsLevel;

    @ApiModelProperty(value = "已取得职称证书时间", example = "2021-07-28")
    private String obtainedCredentialsTime;

    @ApiModelProperty(value = "申报专业所属行业（专业）")
    private String reviewMajor;
    @ApiModelProperty(value = "所学专业", example = "xxxx")
    private String learnMajor;
    @ApiModelProperty(value = "第二学历所学专业", example = "xxxx")
    private String learnMajor2;
    @ApiModelProperty(value = "第三学历所学专业", example = "xxxx")
    private String learnMajor3;

    @ApiModelProperty(value = "申报等级")
    private String reviewLevel;

    @ApiModelProperty(value = "论文数量")
    private Integer paperSum;

    @ApiModelProperty(value = "附件列表")
    private List<RankApplyFileParamModel> rankApplyFileList;

    @ApiModelProperty(value = "审核状态：1 审核中，2 审核通过，3 审核不通过", example = "1")
    private Integer auditStatus;
}
