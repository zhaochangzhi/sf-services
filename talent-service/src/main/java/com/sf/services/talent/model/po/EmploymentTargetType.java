package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

/**
 * EmploymentTargetType
 * 就业指标类型
 * @author guchangliang
 * @date 2021/7/13
 */
@Data
public class EmploymentTargetType {
    private String id;

    private String typeName;

    private Integer formState;

    private String remark;

    private Date creatTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;

}