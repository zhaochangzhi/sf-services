package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.PositionModel;
import com.sf.services.talent.model.po.Position;

/**
 * @ClassName: IPositionService
 * @Description: 职位数据字典接口
 * @author: guchangliang
 * @date: 2021年7月15日 下午8:45:27
 */
public interface IPositionService {
	
	 PageInfo<Position> selectAll(PositionModel record);

	/**
	 * 查询全部数据列表（三级数据）
	 * @param record
	 * @return
	 */
	PageInfo<Position> selectAllList(PositionModel record);

	
}

