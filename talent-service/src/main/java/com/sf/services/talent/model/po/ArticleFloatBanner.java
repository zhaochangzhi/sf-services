package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

@Data
public class ArticleFloatBanner {
    private String id;

    private Integer moduleTypeStatus;

    private String moduleTypeName;

    private String moduleId;

    private String photoId;

    private String photoUrl;

    private String bannerLink;

    private Integer sort;

    private String remark;

    private Date createTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;

}