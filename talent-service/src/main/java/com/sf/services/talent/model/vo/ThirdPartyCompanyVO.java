package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ThirdPartyCompanyVO {


    @ApiModelProperty(value = "姓名")
    private String name;
    @ApiModelProperty(value = "公司")
    private String company;
    @ApiModelProperty(value = "第三方类型")
    private String thirdPartyType;
    @ApiModelProperty(value = "数据数量")
    private Integer count;



}