package com.sf.services.talent.model.vo;

import lombok.Data;

@Data
public class CourseTypeVO {
    private String id;

    private String typeName;

    private String photoId;

    private String photoUrl;



}