package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.dto.JobFairDto;
import com.sf.services.talent.model.vo.JobFairVO;

/**
 * IJobFairService
 *
 * @author zhaochangzhi
 * @date 2021/7/23
 */
public interface IJobFairService {

    /**
     * 获取列表
     *
     * @param params 参数
     * @return 列表
     */
    PageInfo getList(JobFairParamModel params);

    /**
     * 根据id查询详情
     *
     * @param id 参数
     * @return 详情
     */
    JobFairVO getOneById(String id);

    /**
     * 新增
     *
     * @param jobFairDto 参数
     * @return 结果
     */
    Integer addOne(JobFairDto jobFairDto);

    /**
     * 修改
     *
     * @param jobFairDto 参数
     * @return 结果
     */
    Integer updateOne(JobFairDto jobFairDto);

    /**
     * 删除
     *
     * @param jobFairDto 参数
     * @return 结果
     */
    Integer deleteOne(JobFairDto jobFairDto);

    /**
     * 招聘会详情-参与企业列表
     *
     * @param params 参数
     * @return 列表
     */
    PageInfo getEnterpriseList(JobFairEnterpriseParamModel params);

    /**
     * 招聘会详情-参与企业详情
     *
     * @param params 参数
     * @return 列表
     */
    BaseResultModel getEnterpriseDetail(JobFairEnterpriseGetParamModel params);

    /**
     * 招聘会-企业报名参会
     * @param params
     * @param createUser
     * @return
     */
    BaseResultModel jobFairEnterprisePositionAdd(JobFairEnterpriseAddParamModel params, String createUser);


    /**
     * 招聘会-企业报名参会-审核
     * @param params 参数
     * @param userId
     * @return
     */
    BaseResultModel audit(JobFairEnterpriseAuditParamModel params, String userId);


    /**
     * 招聘会-个人用户报名参会
     * @param params
     * @param createUser
     * @return
     */
    BaseResultModel jobFairUserAdd(JobUserAddParamModel params, String createUser);

    /**
     * 招聘会详情-参与企业列表
     *
     * @param params 参数
     * @return 列表
     */
    PageInfo getUserList(JobFairUserListParamModel params);

}
