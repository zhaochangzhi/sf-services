package com.sf.services.talent.controller;

import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.BaseResultModel;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * JobController 找工作
 *
 * @author zhaochangzhi
 * @date 2021/7/7
 */
@Api(value = "首页", tags = "首页")
@RestController
@RequestMapping("/")
public class IndexController {

    @IgnoreSecurity
    @GetMapping("")
    public BaseResultModel index() {
        return BaseResultModel.success(0);
    }
}
