package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * EnterpriseUserVO
 * 企业账号
 * @author guchangliang
 * @date 2021/7/23
 */
@ApiModel(value = "用户街道实体")
@Data
public class UserStreetVO {
    @ApiModelProperty(value = "街道key")
    private String streetKey;

    @ApiModelProperty(value = "街道名称")
    private String streetName;
    
    private String id;
    
    private String memberId;





}