package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "注册企业用户", description = "注册企业用户")
public class RegistEnterpriseUserParamModel {

	@ApiModelProperty(value = "用户名", example = "用户名xxx")
	private String userName;
	@ApiModelProperty(value = "密码", example = "密码xxx")
	private String password;

	@ApiModelProperty(value = "企业全称", example = "xxx科技有限公司")
	private String enterpriseName;

	@ApiModelProperty(value = "行业id（z_trade表id）（精确到二级数据）", example = "8f3d033b26f7449fac4c1e11127622e3")
	private String tradeTwoId;

	@ApiModelProperty(value = "企业性质key（字典t_dictionary表type=EnterpriseNature数据dickey）", example = "1")
	private String enterpriseNatureKey;

	@ApiModelProperty(value = "企业规模key（字典t_dictionary表type=EnterpriseScale数据diekey）", example = "1")
	private String enterpriseScaleKey;

	@ApiModelProperty(value = "地址-详细", example = "辽宁省大连市xxx")
	private String addressDetailed;

	@ApiModelProperty(value = "企业简介", example = "xxx科技有限公司是一家xxx")
	private String enterpriseIntroduction;

	@ApiModelProperty(value = "联系电话", example = "13940712345")
	private String mobile;
	@ApiModelProperty(value = "联系人", example = "张三")
	private String contacts;

	@ApiModelProperty(value = "Email", example = "xxx@163.com")
	private String email;

	@ApiModelProperty(value = "企业logoid（t_file表id）", example = "0144f208090f41d1b34b6f1669bd841b")
	private String enterpriseLogoImageId;

	@ApiModelProperty(value = "统一社会信用代码", example = "12345xxx")
	private String enterpriseLcreditCode;

	@ApiModelProperty(value = "营业执照id（t_file表id）", example = "0144f208090f41d1b34b6f1669bd841b")
	private String enterpriseBusinessLicenseImageId;


}

