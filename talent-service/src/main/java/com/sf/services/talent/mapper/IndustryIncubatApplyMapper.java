package com.sf.services.talent.mapper;

import com.sf.services.talent.model.IndustryIncubatApplyListParamModel;
import com.sf.services.talent.model.po.IndustryIncubatApply;
import com.sf.services.talent.model.vo.IndustryIncubatApplyVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IndustryIncubatApplyMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(IndustryIncubatApply record);

    IndustryIncubatApply selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(IndustryIncubatApply record);

    //查询列表
    List<IndustryIncubatApplyVO> selectList(IndustryIncubatApplyListParamModel params);

    Integer selectCount(@Param("createUser") String createUser, @Param("auditStatus") Integer auditStatus);

    //查询最后一条数据
    IndustryIncubatApplyVO selectLast(@Param("createUser") String createUser, @Param("auditStatus") Integer auditStatus);
}