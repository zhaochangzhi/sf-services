package com.sf.services.talent.mapper;

import com.sf.services.talent.model.EnterpriseUserListParamModel;
import com.sf.services.talent.model.po.EnterpriseUser;
import com.sf.services.talent.model.vo.EnterpriseUserVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EnterpriseUserMapper {
    int deleteByPrimaryKey(String id);

    int insert(EnterpriseUser record);

    int insertSelective(EnterpriseUser record);

    EnterpriseUser selectByPrimaryKey(String id);

    int selectPageListCount(Map<String, Object> searchMap);

    List<EnterpriseUser> selectPageList(Map<String, Object> searchMap);

    int updateByPrimaryKeySelective(EnterpriseUser record);

    int updateByPrimaryKey(EnterpriseUser record);

    /*根据会员id查询*/
    EnterpriseUserVO selectByMemberId(@Param("memberId") String memberId);

    /*根据id查询*/
    EnterpriseUserVO selectById(@Param("id") String id);

    /**
     * 查询列表
     * @param params 参数
     * @return 列表数据
     */
    List<EnterpriseUserVO> selectList(EnterpriseUserListParamModel params);
    
    /**
     * @Title: delEnterpriseUser 
     * @Description: 删除用户
     * @param memberId
     * @return
     * @return: int
     */
    int delEnterpriseUser(@Param("memberId") String memberId);
}