package com.sf.services.talent.controller;

import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.PositionModel;
import com.sf.services.talent.service.IPositionService;
import com.sf.services.talent.util.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @ClassName: positionController
 * @Description: 职位字典
 * @author: guchangliang
 * @date: 2021年7月15日 下午9:07:47
 */
@Slf4j
@Api(value = "职位字典", tags = "职位字典")
@RestController
@RequestMapping("/position")
public class positionController {

	@Resource
	private IPositionService positionService;

	@ApiOperation(notes = "职位字典列表", value="职位字典列表")
	@ApiImplicitParam(name = "params", value = "职位字典模型", required = false, dataType = "PositionModel")
	@PostMapping("/list")
	public ResultUtil selectAll(@RequestBody PositionModel record) {
		return ResultUtil.success(positionService.selectAll(record));
	}

	@IgnoreSecurity
	@ApiOperation(notes = "职位字典列表(包含全部三级数据)", value="职位字典列表")
	@ApiImplicitParam(name = "params", value = "职位字典模型", required = false, dataType = "PositionModel")
	@PostMapping("/allList")
	public ResultUtil allList(@RequestBody PositionModel record) {
		return ResultUtil.success(positionService.selectAllList(record));
	}
}

