package com.sf.services.talent.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.po.ArchiveServiceCategory;
import com.sf.services.talent.model.vo.ArchiveServiceCategoryVO;

/**
 * @ClassName: IArchiveServiceCategory
 * @Description: 档案服务类别接口
 * @author: heyang
 * @date: 2021年7月10日 下午4:33:58
 */
public interface IArchiveServiceCategoryService {
	
	/**
	 * @Title: insert 
	 * @Description: 添加
	 * @param record
	 * @return
	 * @return: int
	 */
	int insert(ArchiveServiceCategoryVO record);
	
	/**
	 * @Title: delete 
	 * @Description: 删除
	 * @param record
	 * @return
	 * @return: int
	 */
	int delete(ArchiveServiceCategoryVO record);
	
	/**
	 * @Title: update 
	 * @Description: 修改
	 * @param record
	 * @return
	 * @return: int
	 */
	int update(ArchiveServiceCategoryVO record);
	
	/**
	 * @Title: list 
	 * @Description: 分页列表
	 * @param record
	 * @return
	 * @return: PageInfo<ArchiveServiceCategory>
	 */
	PageInfo<ArchiveServiceCategory> list(ArchiveServiceCategoryVO record);
	
	/**
	 * @Title: getCategoryList 
	 * @Description: 下拉列表
	 * @return
	 * @return: List<Map<String,Object>>
	 */
	List<Map<String, Object>> getCategoryList();
}
