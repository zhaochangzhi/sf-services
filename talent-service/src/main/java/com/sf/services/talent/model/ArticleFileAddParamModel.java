package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author guchangliang
 * @date 2021/8/7
 */
@ApiModel(value = "文章管理-添加文章-文件列表")
@Data
public class ArticleFileAddParamModel {

    @ApiModelProperty(value = "文件id", example = "0096ff5444014df48c6301751d1f0830")
    private String fileId;

}