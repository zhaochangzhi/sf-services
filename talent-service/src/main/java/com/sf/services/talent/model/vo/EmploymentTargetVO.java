package com.sf.services.talent.model.vo;

import com.sf.services.talent.model.po.EmploymentTargetRecord;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * EmploymentTarget
 * 就业指标
 * @author guchangliang
 * @date 2021/7/13
 */
@ApiModel(value = "就业指标实体")
@Data
public class EmploymentTargetVO extends PageVO{


    /**
     *
     */
    private static final long serialVersionUID = 2104150778276761315L;
    @ApiModelProperty(value = "主键id")
    private String id;
    @ApiModelProperty(value = "指标类型id")
    private String typeId;
    @ApiModelProperty(value = "指标类型名称")
    private String typeName;
    @ApiModelProperty(value = "指标形式状态（1：百分比；2：固定数值）")
    private Integer formStatus;
    @ApiModelProperty(value = "运算符号状态（1：大于等于；2：小于等于；3：等于")
    private Integer symbolStatus;
    @ApiModelProperty(value = "复数状态（1：设定值为一个数值；2：设定值为两个数值）")
    private Integer complexStatus;
    @ApiModelProperty(value = "指标年份")
    private Integer targetYear;
    @ApiModelProperty(value = "年份指标设定值（值为第12月份的设定值）")
    private String targetYearTarget;
    @ApiModelProperty(value = "街道key（t_dictionary表dickey）")
    private String streetKey;
    @ApiModelProperty(value = "街道名称（t_dictionary表value）")
    private String streetName;

    @ApiModelProperty(value = "就业指标记录添加实体", example = "[{\"target\":70,\"targetMonth\":1}]")
    private List<EmploymentTargetRecord> employmentTargetRecordList;


}