package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * get型实体，入参只有id
 * @author guchangliang
 * @date 2021/8/1
 */
@ApiModel(value = "get型实体，入参只有id")
@Data
public class GetOneParamModel {

    @ApiModelProperty(value = "主键id", example = "835a3e5fb9c842c594c2e7372bd60d26")
    private String id;





}