package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.RankApplyParamModel;
import com.sf.services.talent.model.dto.RankApplyDto;
import com.sf.services.talent.model.vo.RankApplyVO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * IRankApplyService
 *
 * @author zhaochangzhi
 * @date 2021/7/10
 */
public interface IRankApplyService {


    /**
     * 获取列表
     *
     * @param params 参数
     * @return 列表
     */
    PageInfo<RankApplyDto> getList(RankApplyParamModel params);

    /**
     * 获取根据memberid去重倒序列表
     *
     * @param params 参数
     * @return 列表
     */
    PageInfo<RankApplyDto> getDistinctDescList(RankApplyParamModel params);

    /**
     * 根据id查询详情
     *
     * @param id 主键
     * @return 详情
     */
    RankApplyVO getOneById(String id);

    /**
     * 获取用户最新一条
     *
     * @param params 参数
     * @return 结果
     */
    RankApplyVO getLastOne(RankApplyDto params);

    /**
     * 新增
     *
     * @param params 参数
     * @return 结果
     */
    Integer addOne(RankApplyDto params);

    /**
     * 修改
     *
     * @param params 参数
     * @return 结果
     */
    Integer updateOneById(RankApplyDto params);

    /**
     * 删除一个职称申请关联文件
     *
     * @param id 职称申请关联文件id
     * @return 结果
     */
    Integer deleteOneRankApplyFile(String id);

    /**
     * 获取列表
     *
     * @param params 参数
     * @return 列表
     */
    void exportList(RankApplyParamModel params, HttpServletRequest request, HttpServletResponse response);

}
