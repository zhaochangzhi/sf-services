package com.sf.services.talent.controller;

import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.exceptions.TalentException;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.ForgetPasswordSendEmailVerificationCode;
import com.sf.services.talent.model.LoginParamModel;
import com.sf.services.talent.model.RetrievePasswordParamModel;
import com.sf.services.talent.model.vo.MemberVO;
import com.sf.services.talent.service.IMailService;
import com.sf.services.talent.service.IMerberService;
import com.sf.services.talent.util.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * LoginController 会员登陆
 *
 * @author hy
 * @date 2021/7/8
 */
@Slf4j
@Api(value = "会员", tags = "会员")
@RestController
@RequestMapping("/member")
public class LoginController {
	
	@Autowired
	private IMerberService merberService;

	@Autowired
	private IMailService mailService;

	/**
	 * 登录接口
	 * @param userName
	 * @param password
	 * @return
	 */
	@ApiOperation(notes = "登陆接口", value="登陆接口")
	@ApiImplicitParam(name = "params", value = "登陆模型", required = false, dataType = "LoginParamModel")
	@PostMapping("/login")
	public ResultUtil login(@RequestBody LoginParamModel params) {
		try {
			String token = merberService.login(params.getUserName(), params.getPassword(), params.getPlatform());
			return ResultUtil.success(token);
		}catch(TalentException te){
			return ResultUtil.error(te.getCode(), te.getMessage());
		} catch (Exception e) {
			log.error("登陆异常:{}",e);
			return ResultUtil.error(401, "登录异常");
		}
		
	}
	
	/**
	 * 注册接口
	 * @param user
	 * @return
	 */
	@ApiOperation(notes = "注册接口", value="注册接口")
	@ApiImplicitParam(name = "params", value = "注册模型", required = false, dataType = "memberVO")
	@PostMapping("/register")
	public ResultUtil register(@RequestBody MemberVO memberVO) {
		try {
			return ResultUtil.success(merberService.register(memberVO));
		}catch(TalentException te){
			return ResultUtil.error(te.getCode(), te.getMessage());
		}catch (Exception e) {
			log.error("注册异常:{}",e);
			return ResultUtil.error(401, "注册异常");
		}
		
	}
	
	@ApiOperation(notes = "登出接口", value="登出接口")
	@ApiImplicitParam(name = "params", value = "会员模型", required = false, dataType = "memberVO")
	@PostMapping("/logout")
	@IgnoreSecurity
	public ResultUtil logout(@RequestBody MemberVO memberVO,HttpServletResponse response) {
		try {
			response.setHeader("authorization", null);
			return ResultUtil.success();
		}catch(TalentException te){
			return ResultUtil.error(te.getCode(), te.getMessage());
		}catch (Exception e) {
			log.error("登出接口异常:{}",e);
			return ResultUtil.error(401, "登出接口异常");
		}
		
	}

	@ApiOperation(notes = "获取已登陆账号信息", value="获取已登陆账号信息")
	@PostMapping("/getLoggedInUser")
	public ResultUtil getLoggedInUser() {

		return merberService.getLoginUser();
	}
	@ApiOperation(notes = "获取已登陆账号信息", value="获取已登陆账号信息")
	@PostMapping("/modifyPassword")
	public ResultUtil modifyPassword(@RequestBody MemberVO memberVO) {
		
		merberService.modifyPassword(memberVO.getPassword(),memberVO.getMemberId());
		return ResultUtil.success();
	}

	@IgnoreSecurity
	@ApiOperation(notes = "忘记密码-发送邮箱验证码", value="忘记密码-发送邮箱验证码")
	@PostMapping("/forgetPassword/sendEmailVerificationCode")
	public BaseResultModel forgetPasswordSendEmailVerificationCode(@RequestBody ForgetPasswordSendEmailVerificationCode params) {


		return merberService.sendEmailVerificationCode(params);
	}

	@IgnoreSecurity
	@ApiOperation(notes = "忘记密码-重置密码", value="忘记密码-重置密码")
	@PostMapping("/forgetPassword/resetPassword")
	public BaseResultModel forgetPasswordResetPassword(@RequestBody RetrievePasswordParamModel params) {


		return merberService.resetPassword(params);
	}
}
