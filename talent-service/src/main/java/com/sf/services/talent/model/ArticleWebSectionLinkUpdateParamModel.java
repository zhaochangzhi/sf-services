package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "栏目关联-编辑")
@Data
public class ArticleWebSectionLinkUpdateParamModel {

    @ApiModelProperty(value = "主键id", example = "0e123d79c2fa468ba901426ab21b3fcf")
    private String id;

    @ApiModelProperty(value = "逗号分隔所属栏目id", example = "1002,2001,3001")
    private String webSectionLinkId;


}