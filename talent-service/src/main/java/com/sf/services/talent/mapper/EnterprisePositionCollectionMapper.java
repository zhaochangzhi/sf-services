package com.sf.services.talent.mapper;

import com.sf.services.talent.model.po.EnterprisePositionCollection;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EnterprisePositionCollectionMapper {
    int deleteByPrimaryKey(String id);

    int insert(EnterprisePositionCollection record);

    int insertSelective(EnterprisePositionCollection record);

    EnterprisePositionCollection selectByPrimaryKey(String id);

    int selectPageListCount(Map<String, Object> searchMap);

    List<EnterprisePositionCollection> selectPageList(Map<String, Object> searchMap);

    int updateByPrimaryKeySelective(EnterprisePositionCollection record);

    int updateByPrimaryKey(EnterprisePositionCollection record);


    /**
     * 查询已收藏数量
     * @param enterprisePositionId
     * @param memberId
     * @return
     */
    int selectCollectCount(@Param("enterprisePositionId") String enterprisePositionId, @Param("memberId") String memberId);

    /**
     * 查询已收藏数据
     * @param enterprisePositionId
     * @param memberId
     * @return
     */
    EnterprisePositionCollection selectCollect(@Param("enterprisePositionId") String enterprisePositionId, @Param("memberId") String memberId);

    //查询企业被收藏职位数量
    Integer selectCollectionNum(@Param("enterpriseUserId") String enterpriseUserId);

    //根据职位删除
    int deleteByEnterprisePositionId(@Param("enterprisePositionId") String enterprisePositionId);
}