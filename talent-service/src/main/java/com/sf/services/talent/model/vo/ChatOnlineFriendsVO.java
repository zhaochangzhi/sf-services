package com.sf.services.talent.model.vo;

import lombok.Data;

import java.util.Date;

@Data
public class ChatOnlineFriendsVO {

    private String memberId;

    private String friendMemberId;

    private String friendTypeId;

    private Boolean isOnline;

    private String friendHeadSculpture;

    private String friendNickName;

    private Integer notReadCount;

    private Date LastMessageTime;
}