package com.sf.services.talent.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * swagger 配置
 *
 * @author zhaochangzhi
 * @date 2021/7/7
 */
@Configuration
@EnableSwagger2
public class SwaggerAutoConfiguration {

    @Value("${swagger2.enable:false}")
    private boolean enable = false;

    /**
     * 将 Docket 注入 Spring 容器
     *
     * @return
     */
    @Bean("人才服务")
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("人才服务")
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.sf.services.talent.controller"))
                .paths(PathSelectors.any())
                .build()
                .enable(enable);
    }

    @Bean("后台管理")
    public Docket createRestApi4Management() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("后台管理")
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.sf.services.talent.management.controller"))
                .paths(PathSelectors.any())
                .build()
                .enable(enable);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("TalentService").description("沈抚新区-人才服务")
                .termsOfServiceUrl("").version("V1.0.0").build();
    }
}