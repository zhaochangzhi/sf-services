package com.sf.services.talent.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PolicyApplyModel extends PageParamModel{
	
	@ApiModelProperty(value = "主键id", example = "49f36f829ce6449294ad1f19e2a6aa59")
    private String id;

	@ApiModelProperty(value = "政策类别id", example = "49f36f829ce6449294ad1f19e2a6aa59")
    private String cateogryId;

	@ApiModelProperty(value = "公司名称", example = "某某有限公司")
    private String companyName;

	@ApiModelProperty(value = "公司编号", example = "AFO8800001")
    private String companyNo;

	@ApiModelProperty(value = "申请电话", example = "13111111111")
    private String applicantPhone;

	@ApiModelProperty(value = "状态", example = "\"1\"：待审核；\"2\"：已核可；\"3\"：已批退")
    private String auditStatus;

	@ApiModelProperty(value = "审核时间", example = "2021-07-13 00:00:00")
    private Date auditTime;

	@ApiModelProperty(value = "审核拒绝原因", example = "公司不符合")
    private String auditRejectReason;

	@ApiModelProperty(value = "文件id", example = "49f36f829ce6449294ad1f19e2a6aa59")
    private String[] fileId;
    
	@ApiModelProperty(value = "政策", example = "政策")
    private String policy;

	@ApiModelProperty(value = "街道", example = "街道")
    private String streetDistrict;
	@ApiModelProperty(value = "用户类型", example = "1 个人 2企业")
	private String userType;
	@ApiModelProperty(value = "用户名称", example = "张三")
	private String userName;
	@ApiModelProperty(value = "用户身份证号", example = "21111115558")
    private String identity;
	@ApiModelProperty(value = "审核开始时间", example = "2021-07-13")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date startTime;
	@ApiModelProperty(value = "审核结束时间", example = "2021-07-14")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date endTime;

}