package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.vo.EmploymentPositionResumeAndUserVO;
import com.sf.services.talent.model.vo.EmploymentPositionResumeVO;

/**
 * @ClassName: IEnterprisePositionResumeService
 * @Description: 找工作
 * @author: guchangliang
 * @date: 2021年7月26日 下午11:19:07
 */
public interface IEnterprisePositionResumeService {

    /**
     * 获取列表
     * @param params 参数
     * @return 列表
     */
    PageInfo<EmploymentPositionResumeVO> positionList(EmploymentPositionResumeParamModel params);

    /**
     * 添加（申请职位/投递简历）
     * @param params
     * @return
     */
    BaseResultModel add(EmploymentPositionResumeAddParamModel params);

    /**
     * 添加收藏（收藏职位）
     * @param params
     * @return
     */
    BaseResultModel collectionAdd(EmploymentPositionCollectionAddParamModel params);

    /**
     * 添加收藏（取消收藏职位）
     * @param params
     * @return
     */
    BaseResultModel collectionDelete(EmploymentPositionCollectionAddParamModel params);


    /**
     * 获取列表
     * @param params 参数
     * @return 列表
     */
    PageInfo<EmploymentPositionResumeVO> list(EmploymentPositionResumeListParamModel params);

    /**
     * 获取列表
     * @param params 参数
     * @return 列表
     */
    PageInfo<EmploymentPositionResumeVO> collectionList(EmploymentPositionResumeCollectionListParamModel params);

    /**
     * 企业中心-简历管理（企业收到的简历列表）
     * @param enterpriseUserId 企业用户id
     * @return 列表
     */
    PageInfo<EmploymentPositionResumeAndUserVO> positionResumeList(EmploymentPositionResumeAndUserParamModel params);


}

