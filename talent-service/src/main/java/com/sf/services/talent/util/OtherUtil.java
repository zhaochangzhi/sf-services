package com.sf.services.talent.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @ClassName: OtherUtil
 * @Description: 常用-工具类
 * @author email:Neal.gu@runlin.cn
 * @date 2018年1月19日 下午3:39:27
 *
 */
public class OtherUtil {
	
	protected static Logger logger=LoggerFactory.getLogger(OtherUtil.class);//log
	
	/**
	 * 
	* <p>getTrimString</p>
	* <p>Description:去掉String型字符串中的首尾空格 </p>
	* <p>Copyright: runlin 2018年1月19日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年1月19日 下午3:44:29
	* @version 1.0
	* @param str
	* @return
	 */
	public static String getTrimString(String str) {
		if (str == null) {
			return str;
		} else {
			str = str.trim();
		}
		return str;
	}
	
	public static String getTrimStringFromObject(Object param) {
		String result = null;
		if (param == null) {
			return null;
		}
		result = String.valueOf(param).trim();
		
		return result;
	}

	/**
	 * 
	* <p>toolstr</p>
	* <p>Description:每隔三位数字加逗号 </p>
	* <p>Copyright: runlin 2018年1月19日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年1月19日 下午3:44:38
	* @version 1.0
	* @param str1
	* @return
	 */
	public static String toolstr(String str1) {
		if (str1 == null) {
			return null;
		}
		int num = str1.lastIndexOf(".");
		if (num > 3) {
			StringBuffer newPrice = new StringBuffer();
			newPrice.append(str1);
			for (int j = num - 3; j > 0; j = j - 3) {
				newPrice.insert(j, ",");
			}
			return newPrice.toString();
		}

		str1 = new StringBuilder(str1).reverse().toString();// 先将字符串颠倒顺序
		String str2 = "";
		for (int i = 0; i < str1.length(); i++) {
			if (i * 3 + 3 > str1.length()) {
				str2 += str1.substring(i * 3, str1.length());
				break;
			}
			str2 += str1.substring(i * 3, i * 3 + 3) + ",";
		}
		if (str2.endsWith(",")) {
			str2 = str2.substring(0, str2.length() - 1);
		}
		return new StringBuilder(str2).reverse().toString();
	}

	/**
	 * 
	* <p>getnewnum</p>
	* <p>Description:四舍五入取整 </p>
	* <p>Copyright: runlin 2018年1月19日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年1月19日 下午3:44:46
	* @version 1.0
	* @param num
	* @return
	 */
	public static String getnewnum(String num) {
		if(isEmptyString(num)) {
			return null;
		}
		try {
			NumberFormat nf = NumberFormat.getInstance();
			nf.setRoundingMode(RoundingMode.HALF_UP);// 设置四舍五入
			nf.setMinimumFractionDigits(0);// 设置最小保留几位小数
			nf.setMaximumFractionDigits(0);// 设置最大保留几位小数
			String newnum = nf.format(Float.parseFloat(num)).replaceAll(",", "");
			return newnum;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("getnewnum:num="+num, e);
			return null;
		}

	}

	/**
	 * 
	* <p>getnewFloatnum</p>
	* <p>Description:四舍五入返回String型数值 </p>
	* <p>Copyright: runlin 2018年1月19日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年1月19日 下午3:44:54
	* @version 1.0
	* @param num
	* @return
	 */
	public static String getnewFloatnum(String num) {
		BigDecimal b = new BigDecimal(num);
		BigDecimal one = new BigDecimal("1");
		return String.valueOf(b.divide(one, 2, BigDecimal.ROUND_HALF_UP).doubleValue());

	}

	/**
	 * 
	* <p>getfloat</p>
	* <p>Description: 传入字符串返回浮点</p>
	* <p>Copyright: runlin 2018年1月19日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年1月19日 下午3:45:01
	* @version 1.0
	* @param num
	* @return
	 */
	public static float getfloat(String num) {
		if ("".equals(num) || num == null) {
			return 0;
		} else {
			return Float.parseFloat(num);
		}
	}

	/**
	 * 
	* <p>addStringnum</p>
	* <p>Description: 字符串型数字相加</p>
	* <p>Copyright: runlin 2018年1月19日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年1月19日 下午3:45:08
	* @version 1.0
	* @param num1
	* @param num2
	* @return
	 */
	public static String addStringnum(String num1, String num2) {
		java.text.DecimalFormat df = new java.text.DecimalFormat("#.00");
		if ("0".equals(num1) || num1 == null || "".equals(num1.trim())) {
			num1 = "0";
		}
		if ("0".equals(num2) || num2 == null || "".equals(num2.trim())) {
			num2 = "0";
		}
		if ("0".equals(num1) && "0".equals(num2)) {
			return "0";
		}
		
		BigDecimal b1 = new BigDecimal(df.format(Double.parseDouble(num1)));
		BigDecimal b2 = new BigDecimal(df.format(Double.parseDouble(num2)));

		String result = subZeroAndDot(df.format(Double.parseDouble(b1.add(b2).stripTrailingZeros().toPlainString())));
		if (".".equals(result.substring(0, 1))) {
			result = "0" + result;
		}
		if ("".equals(result)) {
			result = "0";
		}
		return result;
	}

	/**
	 * 
	* <p>mulStringnum</p>
	* <p>Description:字符串型数字相乘 (保留四位小数)</p>
	* <p>Copyright: runlin 2018年1月19日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年1月19日 下午3:45:15
	* @version 1.0
	* @param num1
	* @param num2
	* @return
	 */
	public static String mulStringnum(String num1, String num2) {
		java.text.DecimalFormat df = new java.text.DecimalFormat("#.0000");
		if ("0".equals(num1) || num1 == null || "".equals(num1.trim())) {
			return "0";
		}
		if ("0".equals(num2) || num2 == null || "".equals(num2.trim())) {
			return "0";
		}
		BigDecimal b1 = new BigDecimal(num1);
		BigDecimal b2 = new BigDecimal(num2);

		String result = subZeroAndDot(df.format(Double.parseDouble(b1.multiply(b2).stripTrailingZeros().toPlainString())));
		if (".".equals(result.substring(0, 1))) {
			result = "0" + result;
		}
		
		result =subZeroAndDot(result);
		
		return result;
	}

	/**
	 * 
	* <p>divisionStringnum</p>
	* <p>Description: 字符串型数字相除（保留2位小数-四舍五入）</p>
	* <p>Copyright: runlin 2018年1月19日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年1月19日 下午3:45:23
	* @version 1.0
	* @param num1
	* @param num2
	* @return
	 */
	public static String divisionStringnum(String num1, String num2) {
		java.text.DecimalFormat df = new java.text.DecimalFormat("#.00");
		if ("0".equals(num1) || num1 == null || "".equals(num1.trim())) {
			return "0";
		}
		if ("0".equals(num2) || num2 == null || "".equals(num2.trim())) {
			return "0";
		}
		
		BigDecimal b1 = new BigDecimal(num1);
		BigDecimal b2 = new BigDecimal(num2);

		String result = subZeroAndDot(df.format(Double.parseDouble(b1.divide(b2, 4, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString())));
		if (".".equals(result.substring(0, 1))) {
			result = "0" + result;
		}
		return result;
	}

	/**
	 * 
	* <p>divideStrin</p>
	* <p>Description: 字符串型数字相除(取整-不四舍五入)</p>
	* <p>Copyright: runlin 2018年4月2日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年4月2日 下午4:15:48
	* @version 1.0
	* @param num1
	* @param num2
	* @return
	 */
	public static String divideString(String num1,String num2){
		try {
			if("0".equals(num1) || num1==null || "".equals(num1.trim())){
				return "0";
			}
			if("0".equals(num2) || num2==null || "".equals(num2.trim())){
				return "0";
			}
			BigDecimal BigDecimalNum1 = new BigDecimal(num1);
			BigDecimal BigDecimalNum2 = new BigDecimal(num2);

			BigDecimal BigDecimalResult = BigDecimalNum1.divide(BigDecimalNum2,10, BigDecimal.ROUND_HALF_EVEN);//相除 其中10为精确的小数
			Integer IntResult = BigDecimalResult.intValue();
			String result = String.valueOf(IntResult);
			return result;
		} catch (Exception e) {
			return null;
		}
		
	}
	
	
	/**
	 * 
	* <p>subStringnum</p>
	* <p>Description:字符串型数字相减 num1-num2 </p>
	* <p>Copyright: runlin 2018年1月19日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年1月19日 下午3:45:30
	* @version 1.0
	* @param num1
	* @param num2
	* @return
	 */
	public static String subStringnum(String num1, String num2) {
		java.text.DecimalFormat df = new java.text.DecimalFormat("#.00");
		
		if ("0".equals(num1) || num1 == null || "".equals(num1)) {
			num1 = "0";
		}
		if ("0".equals(num2) || num2 == null || "".equals(num2)) {
			num2 = "0";
		}
		if ("0".equals(num1) && "0".equals(num2)) {
			return "0";
		}
		
		BigDecimal b1 = new BigDecimal(df.format(Double.parseDouble(num1)));
		BigDecimal b2 = new BigDecimal(df.format(Double.parseDouble(num2)));
		String result = subZeroAndDot(df.format(Double.parseDouble(b1.subtract(b2).stripTrailingZeros().toPlainString())));
		if (result != null && result.length() >= 1 && ".".equals(result.substring(0, 1))) {// 给.00 加上0 即：0.00
			result = "0" + result;
		}
		if (result != null && result.length() >= 4 && ".00".equals(result.substring(result.length() - 3, result.length()))) {// 去掉.00
			result = result.substring(0, result.length() - 3);
		}
		return result;
	}

	/**
	 * 
	* <p>isInteger</p>
	* <p>Description:判断字符串是否是整数 </p>
	* <p>Copyright: runlin 2018年1月19日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年1月19日 下午3:45:38
	* @version 1.0
	* @param value
	* @return
	 */
	public static boolean isInteger(String value) {
		try {
			Integer.parseInt(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	//校验是否为正整数
	public static boolean isPositiveInteger(String value) {
		if(isPositive(value)) {
			try {
				Integer.parseInt(value);
				return true;
			} catch (NumberFormatException e) {
				return false;
			}
		}else {
			return false;
		}
		
	}
	
	//校验是否为正整数，当为空值时直接返回ture
	public static boolean isPositiveIntegerWhenIsEmptyString(String value) {
		if(isEmptyString(value)) {
			return true;
		}
		if(!isPositive(value)) {
			return false;
		}
		try {
			Integer.parseInt(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	/**
	 * 
	* <p>returntime</p>
	* <p>Description:获得当前时间，返回为字符串 </p>
	* <p>Copyright: runlin 2018年1月19日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年1月19日 下午3:45:44
	* @version 1.0
	* @return
	 */
	public static String returntime() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String nowTime = simpleDateFormat.format(new Date());
		System.out.println("--------nowTime="+nowTime);
		return nowTime;
		
	}
	
	
	/**
	 * 
	* <p>subTimeSecond</p>
	* <p>Description: 计算时间差，精确到秒</p>
	* <p>Copyright: runlin 2019年4月26日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2019年4月26日 下午1:49:50
	* @version 1.0
	* @param timeBegin
	* @param timeEnd
	* @return
	 */
	public static long subTimeSecond(String timeBegin, String timeEnd) {
		long second = 0;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date parse = format.parse(timeBegin);
			Date date = format.parse(timeEnd);
			long between = date.getTime() - parse.getTime();
			second = between / 1000;
		} catch (Exception e) {
			return 0;
		}
		
		return second;
	}
	
	
	

	/**
	 * 
	* <p>getAge</p>
	* <p>Description:根据出生年月日查询人年龄 </p>
	* <p>Copyright: runlin 2018年1月19日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年1月19日 下午3:45:52
	* @version 1.0
	* @param birthday
	* @return
	 */
	public static int getAge(String birthday) {

		if (birthday == null) {
			return 0;
		}
		Date birthDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");// 小写的mm表示的是分钟
		try {
			birthDate = sdf.parse(birthday);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		int age = 0;

		Date now = new Date();

		SimpleDateFormat format_y = new SimpleDateFormat("yyyy");
		SimpleDateFormat format_M = new SimpleDateFormat("MM");

		String birth_year = format_y.format(birthDate);
		String this_year = format_y.format(now);

		String birth_month = format_M.format(birthDate);
		String this_month = format_M.format(now);

		// 初步，估算
		age = Integer.parseInt(this_year) - Integer.parseInt(birth_year);

		// 如果未到出生月份，则age - 1
		if (this_month.compareTo(birth_month) < 0)
			age -= 1;
		if (age < 0)
			age = 0;
		return age;
	}

	/**
	 * 
	* <p>subZeroAndDot</p>
	* <p>Description:使用java正则表达式去掉多余的.与0 </p>
	* <p>Copyright: runlin 2018年1月19日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年1月19日 下午3:43:47
	* @version 1.0
	* @param s
	* @return
	 */
	public static String subZeroAndDot(String s) {
		if (s.indexOf(".") > 0) {
			s = s.replaceAll("0+?$", "");// 去掉多余的0
			s = s.replaceAll("[.]$", "");// 如最后一位是.则去掉
		}

		if (s.contains("E")) {
			String sarr[] = s.split("E");
			double res1 = Double.valueOf(sarr[0]);
			int ci = Integer.valueOf(sarr[1]);
			double cinum = 10;
			for (int i = 1; i < ci; i++) {
				cinum = cinum * 10;
			}
			double res = res1 * cinum;
			NumberFormat nf = new java.text.DecimalFormat("0");
			return nf.format(res);
		}

		return s;
	}

	/**
	 * 
	* <p>getbefore</p>
	* <p>Description:比较两个字符串时间的大小,返回对应 的符号 </p>
	* <p>Copyright: runlin 2018年1月19日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年1月19日 下午3:43:55
	* @version 1.0
	* @param str1
	* @param str2
	* @return
	 */
	public static String getbefore(String str1, String str2) {
		String pattern = "yyyy-MM-dd";// 格式化日期格式

		SimpleDateFormat sf = new SimpleDateFormat(pattern);

		Date d1 = new Date();
		Date d2 = new Date();
		try {
			d1 = sf.parse(str1);
			d2 = sf.parse(str2);// 把时间格式化
		} catch (ParseException e) {
			e.printStackTrace();
		} // 把时间格式化

		if (d1.getTime() < d2.getTime()) { // 比较大小；
			return "<";
		} else if (d1.getTime() == d2.getTime()) {
			return "=";
		} else {
			return ">";
		}
	}
	/**
	 * 
	* <p>substartdate</p>
	* <p>Description:时间格式：2018-01-01-2019-01-01取得前驱时间 </p>
	* <p>Copyright: runlin 2018年1月22日</p>
	* <p>Company: runlin </p>
	* @author email:Vic.peng@runlin.cn
	* @date 2018年1月22日 下午8:49:06
	* @version 1.0
	* @param date
	* @return
	 */
	public static String substartdate(String date) {
		
		if(date !=null && !date.trim().equals("") && date.length() >= 10) {
			date=date.substring(0,10).trim();
		}
		return date;
	}
	/**
	 * 
	* <p>subendtdate</p>
	* <p>Description:时间格式：2018-01-01-2019-01-01取得后驱时间 </p>
	* <p>Copyright: runlin 2018年1月22日</p>
	* <p>Company: runlin </p>
	* @author email:Vic.peng@runlin.cn
	* @date 2018年1月22日 下午8:49:02
	* @version 1.0
	* @param date
	* @return
	 */
	public static String subendtdate(String date) {
		if(date !=null && !date.trim().equals("")) {
			date=date.substring(12,23).trim();
		}
		return date;
	}
	
	/**
	 * 
	* <p>removeFirstZero</p>
	* <p>Description: 去掉数字开头的所有0</p>
	* <p>Copyright: runlin 2018年1月23日</p>
	* <p>Company: runlin </p>
	* @author email:rodman.jia@runlin.cn
	* @date 2018年1月23日 下午4:43:26
	* @version 1.0
	* @param num
	* @return
	 */
	public static String removeFirstZero(String num){
		if(null != num){
			num = num.replaceFirst("^0*", "");
		}else{
			return "0";
		}
		if(num.equals("")){
			return "0";
		}else{
			return num;
		}
	}
	
	
	/**
	 * 
	* <p>addtimelength</p>
	* <p>Description: 当前时间，增加timelength个月，返回时间点</p>
	* <p>Copyright: runlin 2018年1月31日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年1月31日 下午12:36:05
	* @version 1.0
	* @param timelength
	* @return
	 */
	public static String addtimelength(String timelength) {
		Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, Integer.parseInt(timelength));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String result = sdf.format(calendar.getTime());
        	
        return result;
	}
	
	/**
	 * 
	* <p>addDay</p>
	* <p>Description: 获得增加 dayCount天的时间</p>
	* <p>Copyright: runlin 2018年8月10日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年8月10日 下午7:38:11
	* @version 1.0
	* @param paramTime 传入时间
	* @param dayCount 天数
	* @return
	 */
	public static String addDay(String paramTime, int dayCount){
		String resultTime = null;
		if(isEmptyString(paramTime)) {
			return null;
		}
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = format.parse(paramTime);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.DAY_OF_MONTH, dayCount);//加day天
			resultTime = format.format(calendar.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return resultTime;
	} 
	
	/**
	 * 
	* <p>addDay</p>
	* <p>Description: 获得增加 minuteCount分钟的时间</p>
	* <p>Copyright: runlin 2020年2月26日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2020年2月26日 下午7:38:11
	* @version 1.0
	* @param paramTime 传入时间
	* @param minuteCount 分钟数
	* @return
	 */
	public static String addMinute(String paramTime, int minuteCount){
		String resultTime = null;
		if(isEmptyString(paramTime)) {
			return null;
		}
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = format.parse(paramTime);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.MINUTE, minuteCount);//加minuteCount分钟
			resultTime = format.format(calendar.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return resultTime;
	} 
	
	
	
	
	
	
	/**
	 * 
	* <p>issameday</p>
	* <p>Description: 判断两个字符串时间，是否为同一天（年、月、日都相同）</p>
	* <p>Copyright: runlin 2018年2月19日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年2月19日 下午5:46:52
	* @version 1.0
	* @param time1
	* @param time2
	* @return
	 */
	public static String issameday(String time1, String time2) {
		String result = "否";
		if (time1 != null && time1.length() >= 10 && time2 != null && time2.length() >= 10) {
			time1 = time1.substring(0, 10);
			time2 = time2.substring(0, 10);
			if(time1.equals(time2)){
				result = "是";
			}else{
				result = "否";
			}
		}
		return result;
	}
	
	
	/**
	 * 
	* <p>isSameMonth</p>
	* <p>Description: 判断是否为同一个月（只判断月份，不判断年）</p>
	* <p>Copyright: runlin 2018年2月19日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年2月19日 下午5:53:27
	* @version 1.0
	* @param time1
	* @param time2
	* @return
	 */
	public static String isSameMonth(String time1, String time2) {
		String result = "否";
		if (time1 != null && time1.length() >= 10 && time2 != null && time2.length() >= 10) {
			time1 = time1.substring(5, 7);
			time2 = time2.substring(5, 7);
			if(time1.equals(time2)){
				result = "是";
			}else{
				result = "否";
			}
		}
		return result;
	}
	
	/**
	 * 
	* <p>getYearFirst</p>
	* <p>Description:获取某一年的第一天 </p>
	* <p>Copyright: runlin 2018年3月28日</p>
	* <p>Company: runlin </p>
	* @author email:Jyozeen.xu@runlin.cn
	* @date 2018年3月28日 下午4:56:05
	* @version 1.0
	* @param year
	* @return
	 */
	 public static String getYearFirst(int year){
	 	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        Date currYearFirst = calendar.getTime();
        return sdf.format(currYearFirst);
	        
	 }
	
	/**
	 * 
	* <p>getYearLast</p>
	* <p>Description: 获取某一年的最后一天</p>
	* <p>Copyright: runlin 2018年3月28日</p>
	* <p>Company: runlin </p>
	* @author email:Jyozeen.xu@runlin.cn
	* @date 2018年3月28日 下午5:00:55
	* @version 1.0
	* @param year
	* @return
	 */
    public static String getYearLast(int year){
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        calendar.roll(Calendar.DAY_OF_YEAR, -1);
        Date currYearLast = calendar.getTime();
        return sdf.format(currYearLast);
    }

	/**
	 * 获取当年的第一天
	 * @param
	 * @return
	 */
	public static String getThisYearFirst(){
		Calendar currCal=Calendar.getInstance();
		int currentYear = currCal.get(Calendar.YEAR);
		return getYearFirst(currentYear);
	}

	/**
	 * 获取当年的最后一天
	 * @param
	 * @return
	 */
	public static String getThisYearLast(){
		Calendar currCal=Calendar.getInstance();
		int currentYear = currCal.get(Calendar.YEAR);
		return getYearLast(currentYear);
	}
    
    //获取当前月第一天
    public static String getCurrentMonthFirst(){
    	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 
    	Calendar c = Calendar.getInstance();    
        c.add(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH,1);//设置为1号,当前日期既为本月第一天 
        String first = format.format(c.getTime());
        return first;
	 }
    
    //获取当前月最后一天
    public static String getCurrentMonthLast(){
    	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 
    	Calendar ca = Calendar.getInstance();    
        ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));  
        String last = format.format(ca.getTime());
        return last;
	 }
    
    
    /**
	 * 
	* <p>isBetweenData</p>
	* <p>Description: 判断指定时间，是否在指定时间段内(精确到天)(包含端点值)</p>
	* <p>Copyright: runlin 2018年6月11日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年6月11日 上午10:45:02
	* @version 1.0
	* @param time 指定时间
	* @param starttime 指定时间段开始时间
	* @param endtime 指定时间段结束时间
	* @return
	 */
	public static boolean isBetweenData(String time, String starttime, String endtime) {
		if (OtherUtil.isEmptyString(time) || OtherUtil.isEmptyString(starttime) || OtherUtil.isEmptyString(endtime)) {
			return false;//不在
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
		
		Date starttimeDate = null;
		Date endtimeDate = null;
		Date timeDate = null;
		try {
			starttimeDate = sdf.parse(starttime);//开始时间 
			endtimeDate = sdf.parse(endtime);//结束时间
			timeDate = sdf.parse(time);//时间
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if(timeDate.getTime() < starttimeDate.getTime() || timeDate.getTime() > endtimeDate.getTime()){//判断是否在指定时间
			return false;//不在
		}
		
		return true;//在
	}
	
	/**
	 * 
	* <p>isBefore</p>
	* <p>Description: 判断time1是否在time2之前</p>
	* <p>Copyright: runlin 2018年9月4日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年9月4日 下午5:55:42
	* @version 1.0
	* @param time1
	* @param time2
	* @return
	 */
	public static boolean isBefore(String time1,String time2){
		if (OtherUtil.isEmptyString(time1) || OtherUtil.isEmptyString(time2)) {
			return false;//time1 不在 time2后
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式

		Date d1=new Date();
		Date d2=new Date();
		try {
			d1 = sdf.parse(time1);
			d2 = sdf.parse(time2);//把时间格式化
		} catch (ParseException e) {
			e.printStackTrace();
		}//把时间格式化

		if(d1.getTime() < d2.getTime()){  //比较大小；
			return true;//time1 在 time2前
		}
		
		return false;//time1 不在 time2前
	 }
	
	/**
	 * 
	* <p>isAfter</p>
	* <p>Description: 判断time1是否在time2之后 </p>
	* <p>Copyright: runlin 2018年9月18日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年9月18日 上午10:36:16
	* @version 1.0
	* @param time1
	* @param time2
	* @return
	 */
	public static boolean isAfter(String time1,String time2){
		if (OtherUtil.isEmptyString(time1) || OtherUtil.isEmptyString(time2)) {
			return false;//time1 不在 time2后
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式

		Date d1=new Date();
		Date d2=new Date();
		try {
			d1 = sdf.parse(time1);
			d2 = sdf.parse(time2);//把时间格式化
		} catch (ParseException e) {
			e.printStackTrace();
		}//把时间格式化

		if(d1.getTime() > d2.getTime()){  //比较大小；
			return true;//time1 在 time2后
		}
		
		return false;//time1 不在 time2后
	 }
	
	/**
	 * 
	* <p>isAfter</p>
	* <p>Description: 判断time1是否在time2之后 ，精确到秒</p>
	* <p>Copyright: runlin 2018年9月18日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年9月18日 上午10:36:16
	* @version 1.0
	* @param time1
	* @param time2
	* @return
	 */
	public static boolean isAfterToS(String time1,String time2){
		if (OtherUtil.isEmptyString(time1) || OtherUtil.isEmptyString(time2)) {
			return false;//time1 不在 time2后
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式

		Date d1=new Date();
		Date d2=new Date();
		try {
			d1 = sdf.parse(time1);
			d2 = sdf.parse(time2);//把时间格式化
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}//把时间格式化

		if(d1.getTime() > d2.getTime()){  //比较大小；
			return true;//time1 在 time2后
		}
		
		return false;//time1 不在 time2后
	 }
	
	/**
	 * 
	* <p>getChineseTime</p>
	* <p>Description: 获取中文 年月日时分秒</p>
	* <p>Copyright: runlin 2018年10月11日</p>
	* <p>Company: runlin </p>
	* @author email:Neal.gu@runlin.cn
	* @date 2018年10月11日 上午10:51:36
	* @version 1.0
	* @param paramTime
	* @return
	 */
	public static String getChineseTime(String paramTime){
		String resultTime = null;
		if(isEmptyString(paramTime)) {
			return null;
		}
		
		try {
			String format = "yyyy-MM-dd";
			if (paramTime.length() >= 19) {
				format = "yyyy-MM-dd HH:mm:ss";
			}
			Date date = new SimpleDateFormat(format).parse(paramTime);
	        Calendar calendar = Calendar.getInstance();
	        calendar.setTime(date);
	        
	        int year = calendar.get(Calendar.YEAR);//年
	        int month = calendar.get(Calendar.MONTH) + 1;//月
	        int day = calendar.get(Calendar.DAY_OF_MONTH);//日
	 
	        int hour = calendar.get(Calendar.HOUR_OF_DAY);//时
	        int minute = calendar.get(Calendar.MINUTE);//分
	        int second = calendar.get(Calendar.SECOND);//秒
	        
	        resultTime = year + "年" + month + "月" + day + "日";
	        if (paramTime.length() >= 19) {
	        	resultTime = resultTime + hour + "时" + minute + "分" + second + "秒";
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}
        return resultTime;   
	 }
	
    /**
     * 
    * <p>isNull</p>
    * <p>Description: 校验是否为空 </p>
    * <p>Copyright: runlin 2018年4月4日</p>
    * <p>Company: runlin </p>
    * @author email:Neal.gu@runlin.cn
    * @date 2018年4月4日 下午9:47:04
    * @version 1.0
    * @param str
    * @return
     */
    public static Boolean isEmptyString(String str) {
		Boolean isNull = false;
		if (str == null || "".equals(str.trim())) {
			isNull = true;
		}
		return isNull;
	}
	
    /**
     * 
    * <p>getInteger</p>
    * <p>Description: 获得Integer型</p>
    * <p>Copyright: runlin 2018年4月5日</p>
    * <p>Company: runlin </p>
    * @author email:Neal.gu@runlin.cn
    * @date 2018年4月5日 上午12:35:37
    * @version 1.0
    * @param str
    * @return
     */
    public static Integer getInteger(String str) {
		Integer result = null;
		
		if (str == null || "".equals(str.trim())) {
			return null;
		}
		
		result = (int) Math.floor(Double.parseDouble(str));
		
		return result;
	}
    
    //判断字符串是否为正数
	public static boolean isPositive(String value) {
		if (value==null || "".equals(value) || value.substring(0,1).equals("-")) {
			return false;
		}
		
		return true;
	 }
    
    //String字符串倒序
    public static String getReverseString(String str) {
    	String reverseString = new StringBuffer(str).reverse().toString();
    	
    	return reverseString;
    }
    
	 
    public static String returntimeymd_Type(String type) {
//		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(type);
		return simpleDateFormat.format(new Date());
	}
    
    public static String returntimeymd() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		return simpleDateFormat.format(new Date());
	}
    
    //获取指定时间精确到日
    public static String getYMD(String str) {
    	if (isEmptyString(str)) {
			return null;
		}
    	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    	String result = null;
		try {
			Date date = format.parse(str);
			result = format.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return result;
	}
    
    //获取指定时间精确到秒
    public static String getYMDHMS(String str) {
    	if (isEmptyString(str)) {
			return null;
		}
    	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	String result = null;
		try {
			Date date = format.parse(str);
			result = format.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return result;
	}
    
    //获取当前年
    public static String getNowYear() {
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
		return simpleDateFormat.format(new Date());
	}
    //获取当前月
    public static String getNowMonth() {
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM");
		return simpleDateFormat.format(new Date());
	}
    //获取当前日
    public static String getNowDay() {
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd");
		return simpleDateFormat.format(new Date());
	}

    //对象返回字符串
    public static String valueOf(final Object obj) {
        return (obj == null) ? null : obj.toString();
	}
	
    //校验时间字符串是否合法
  	public static boolean isYMDDate(String str) {
  		if(isEmptyString(str)) {
  			return false;
  		}
  		if(str.length() != 10 && str.length() != 19) {
  			return false;
  		}
  		
  		boolean convertSuccess = true;
  		// 指定日期格式为四位年/两位月份/两位日期，注意yyyy/MM/dd区分大小写；
  		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
  		try {
  			// 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
  			format.setLenient(false);
  			format.parse(str);
		} catch (Exception e) {
			// e.printStackTrace();
  			// 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
  			convertSuccess = false;
		}
  		return convertSuccess;
  	}
  	
  	//校验时间字符串是否合法
  	public static boolean isYMDHMSDate(String str) {
  		if(isEmptyString(str)) {
  			return false;
  		}
  		if(str.length() != 19) {
  			return false;
  		}
  		
  		boolean convertSuccess = true;
  		// 指定日期格式为四位年/两位月份/两位日期，注意yyyy/MM/dd区分大小写；
  		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  		try {
  			// 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
  			format.setLenient(false);
  			format.parse(str);
  		} catch (Exception e) {
  			// e.printStackTrace();
  			// 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
  			convertSuccess = false;
  		}
  		return convertSuccess;
  	}
	//校验时间字符串是否为年份
	public static boolean isYDate(String str) {
		if(isEmptyString(str)) {
			return false;
		}
		if(str.length() != 4) {
			return false;
		}

		boolean convertSuccess = true;
		// 指定日期格式为四位年/两位月份/两位日期，注意yyyy/MM/dd区分大小写；
		SimpleDateFormat format = new SimpleDateFormat("yyyy");
		try {
			// 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
			format.setLenient(false);
			format.parse(str);
		} catch (Exception e) {
			// e.printStackTrace();
			// 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
			convertSuccess = false;
		}
		return convertSuccess;
	}
    
  	//校验是否为数字
  	public static boolean isNumeric(String str){
  		if(OtherUtil.isEmptyString(str)) {
			return false;
		}
        Pattern pattern = Pattern.compile("^[0-9]+\\.{0,1}[0-9]{0,2}$");//正数，且整数或两位以内小数
        Matcher isNum = pattern.matcher(str);
        if( !isNum.matches() ){
        	return false;
        }
        return true;
  	}
  	
  	//校验是否为手机号格式
  	public static boolean isMobile(String mobile) {
		if(OtherUtil.isEmptyString(mobile)) {
			return false;
		}
		
        String telRegex = "[1][123456789]\\d{9}";
        // "[1]"代表第1位为数字1，"[3578]"代表第二位可以为3、5、8中的一个，"\\d{9}"代表后面是可以是0～9的数字，有9位。
        return mobile.matches(telRegex);
    }
  	
  	//获取手机号中间四位隐藏****代替
  	public static String getHideMobile(String mobile) {
  		if(isEmptyString(mobile) || mobile.length() != 11) {
  			return mobile;
  		}
  		String result = mobile.replaceAll("(\\d{3})\\d{4}(\\d{4})","$1****$2");

		return result;
	}
  	
  	//毫秒转时间格式
  	public static String getTimeByMilliSecond(Long millisecond) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//24小时制
		Date date = new Date();
		date.setTime(millisecond);
		String resultTime = simpleDateFormat.format(date);
		
  		return resultTime;
  	}
  	//根据身份证号获取生日
  	public static String getBirthDayByCardnumber(String cardNumber) {
  		String birthDay = null;
  		if (cardNumber.length() == 15){
  			String year = "19" + cardNumber.substring(6,8);
  			String month = cardNumber.substring(8,10);
  			String day = cardNumber.substring(10,12);
  			birthDay = year.concat("-").concat(month).concat("-").concat(day);//生日
  		}
	  	//判断生日
		if (cardNumber.length() == 18) {//18位身份证
			String year = cardNumber.substring(6,10);//出生的年份
	        String month = cardNumber.substring(10,12);//出生的月份
	        String day = cardNumber.substring(12,14);//出生的日期
	        birthDay = year.concat("-").concat(month).concat("-").concat(day);//生日
		}
  		
  		return birthDay;
  	}
  	//获得本季度开始时间
  	public static String getCurrentQuarterStartTime() {
  		SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
  		SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  		
  		Calendar c = Calendar.getInstance();
  		int currentMonth = c.get(Calendar.MONTH) + 1;
  		Date now = null;
  		String nowString = null;
  		try {
  			if (currentMonth >= 1 && currentMonth <= 3) {
  				c.set(Calendar.MONTH, 0);
  			}else if (currentMonth >= 4 && currentMonth <= 6) {
  				c.set(Calendar.MONTH, 3);
  			}else if (currentMonth >= 7 && currentMonth <= 9) {
  				c.set(Calendar.MONTH, 6);
  			}else if (currentMonth >= 10 && currentMonth <= 12) {
  				c.set(Calendar.MONTH, 9);
  			}
  			c.set(Calendar.DATE, 1);
  			now = longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00");
  			nowString = longSdf.format(now);
  		} catch (Exception e) {
  			e.printStackTrace();
  		}
  		return nowString;
  	}
  	
  	//获得本季度结束时间
  	public static String getCurrentQuarterEndTime() {
  		SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
  		SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  		Calendar c = Calendar.getInstance();
  		int currentMonth = c.get(Calendar.MONTH) + 1;
  		Date now = null;
  		String nowString = null;
  		try {
  			if (currentMonth >= 1 && currentMonth <= 3) {
  				c.set(Calendar.MONTH, 2);
  				c.set(Calendar.DATE, 31);
  			} else if (currentMonth >= 4 && currentMonth <= 6) {
  				c.set(Calendar.MONTH, 5);
  				c.set(Calendar.DATE, 30);
  			} else if (currentMonth >= 7 && currentMonth <= 9) {
  				c.set(Calendar.MONTH, 8);
  				c.set(Calendar.DATE, 30);
  			} else if (currentMonth >= 10 && currentMonth <= 12) {
  				c.set(Calendar.MONTH, 11);
  				c.set(Calendar.DATE, 31);
  			}
  			now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
  			nowString = longSdf.format(now);
		} catch (Exception e) {
			e.printStackTrace();
		}
  		return nowString;
	}

	//获得上个季度开始时间
	public static String getPreviousQuarterStartTime() {
		String resultTimeStr = null;
		Date now = null;
		SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String givenQuaterStartTime =  OtherUtil.getCurrentQuarterStartTime();//获取当前季度第一天

		try {
			Calendar c = Calendar.getInstance();
			c.setTime(longSdf.parse(givenQuaterStartTime));
			c.add(Calendar.MONTH,-3); // 向前三个月 （向前一个季度）

			int currentMonth = c.get(Calendar.MONTH) + 1;

			if (currentMonth >= 1 && currentMonth <= 3) {
				c.set(Calendar.MONTH, 0);
			}else if (currentMonth >= 4 && currentMonth <= 6) {
				c.set(Calendar.MONTH, 3);
			}else if (currentMonth >= 7 && currentMonth <= 9) {
				c.set(Calendar.MONTH, 6);
			}else if (currentMonth >= 10 && currentMonth <= 12) {
				c.set(Calendar.MONTH, 9);
			}
			c.set(Calendar.DATE, 1);
			now = longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00");
			resultTimeStr = longSdf.format(now);

		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			return resultTimeStr;
		}
	}

	//获得上个季度结束时间
	public static String getPreviousQuarterEndTime() {
		String resultTimeStr = null;
		Date now = null;
		SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String givenQuaterStartTime =  OtherUtil.getCurrentQuarterEndTime();//获取当前季度最后一天

		try {
			Calendar c = Calendar.getInstance();
			c.setTime(longSdf.parse(givenQuaterStartTime));
			c.add(Calendar.MONTH,-3); // 向前三个月 （向前一个季度）
			int currentMonth = c.get(Calendar.MONTH) + 1;

			if (currentMonth >= 1 && currentMonth <= 3) {
				c.set(Calendar.MONTH, 2);
				c.set(Calendar.DATE, 31);
			} else if (currentMonth >= 4 && currentMonth <= 6) {
				c.set(Calendar.MONTH, 5);
				c.set(Calendar.DATE, 30);
			} else if (currentMonth >= 7 && currentMonth <= 9) {
				c.set(Calendar.MONTH, 8);
				c.set(Calendar.DATE, 30);
			} else if (currentMonth >= 10 && currentMonth <= 12) {
				c.set(Calendar.MONTH, 11);
				c.set(Calendar.DATE, 31);
			}
			now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
			resultTimeStr = longSdf.format(now);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			return resultTimeStr;
		}
	}
  	
  	
  	/**
	 * 判断非负数的整数或者携带一位或者两位的小数
	 * 
	 * @function:  
	 * @param obj 
	 * @return boolean  
	 * @exception 
	 * @author:
	 * @since  1.0.0
	 */
	public static boolean isTwoDecimal(Object obj){
	    boolean flag = false;
	    try {
	    	if(obj != null){
	    		String source = obj.toString();
	    		// 判断是否是整数或者是携带一位或者两位的小数
	    		Pattern pattern = Pattern.compile("^[+]?([0-9]+(.[0-9]{1,2})?)$");
	    		if(pattern.matcher(source).matches()){
	    			flag = true;
	    		}  
	    	}
		} catch (Exception e) {
			e.printStackTrace();
		}
	    return flag;
	}
	

	//String 转 date
    public static Date getStringToDate(String str) {
    	if (isEmptyString(str)) {
			return null;
		}
    	Date date = new Date();
    	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			date = format.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return date;
	}

	//获取时间最大值
	public static Date getMaxCurrentDate(Date date) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String formatDate = format.format(date);
			Calendar cal = Calendar.getInstance();
			cal.setTime(format.parse(formatDate));
			cal.add(Calendar.DAY_OF_YEAR,1);
			return new Date(cal.getTime().getTime()-1);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

}
