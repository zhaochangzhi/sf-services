package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.PolicyFileModel;
import com.sf.services.talent.model.po.PolicyFile;

/**
 * @ClassName: IPolicyFileService 
 * @Description: 政策文件服务
 * @author: heyang
 * @date: 2021年7月13日 下午9:34:17
 */
public interface IPolicyFileService {
	
	/**
	 * @Title: add 
	 * @Description: 添加政策类别
	 * @param record
	 * @return
	 * @return: int
	 */
	 int add(PolicyFileModel record);
	 
	 /**
	  * @Title: delete 
	  * @Description: 删除类别
	  * @param record
	  * @return
	  * @return: int
	  */
	 int delete(PolicyFileModel record);
	 
	 /**
	  * @Title: list 
	  * @Description: 类别列表
	  * @param record
	  * @return
	  * @return: PageInfo<PolicyFile>
	  */
	 PageInfo<PolicyFile> list(PolicyFileModel record);
	 
	 /**
	  * @Title: updateStatus 
	  * @Description: 更新状态
	  * @param record
	  * @return
	  * @return: int
	  */
	 int updateStatus(PolicyFileModel record);
}

