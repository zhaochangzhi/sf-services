package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

@Data
public class ArchiveFile {
    private String id;

    private String fileId;

    private String fileUrl;

    private Integer sort;

    private String remark;

    private Date creatTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;
    private String originalFileName;

}