package com.sf.services.talent.mapper;

import com.sf.services.talent.model.RankApplyParamModel;
import com.sf.services.talent.model.dto.RankApplyDto;
import com.sf.services.talent.model.dto.RankApplyFileDto;
import com.sf.services.talent.model.po.RankApply;
import com.sf.services.talent.model.po.RankApplyFile;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * RankApplyMapper
 *
 * @author zhaochangzhi
 * @date 2021/7/10
 */
@Mapper
public interface RankApplyMapper {

    /**
     * 查询列表
     *
     * @param params 参数
     * @return 列表数据
     */
    List<RankApplyDto> selectList(RankApplyParamModel params);

    /**
     * 获取根据memberid去重倒序列表
     * @param params 参数
     * @return 列表数据
     */
    List<RankApplyDto> getDistinctDescList(RankApplyParamModel params);

    /**
     * 根据id查询详情
     *
     * @param id id
     * @return 详情
     */
    RankApplyDto selectOneById(@Param("id") String id);

    /**
     * 获取用户最后一条
     *
     * @param rankApply 参数
     * @return 结果
     */
    RankApplyDto selectLastOne(RankApply rankApply);

    /**
     * 新增
     *
     * @param rankApply 参数
     * @return 结果
     */
    Integer insertOne(RankApply rankApply);

    /**
     * 修改
     *
     * @param rankApply 职称申请
     * @return 结果
     */
    Integer updateOneById(RankApply rankApply);

    /**
     * 查询职称申请文件id列表
     *
     * @param rankApplyId 职称申请id
     * @return 结果
     */
    List<RankApplyFileDto> selectRankApplyFileIdList(String rankApplyId);

    /**
     * 查询职称申请关联文件
     *
     * @param id 主键
     * @return 结果
     */
    RankApplyFile selectOneRankApplyFile(@Param("id") String id);
    /**
     * 新增职称申请关联文件
     *
     * @param rankApplyFileList 参数
     * @return 结果
     */
    Integer insertBatchRankApplyFile(@Param("rankApplyFileList") List<RankApplyFile> rankApplyFileList);

    /**
     * 删除职称申请关联文件
     *
     * @param id 关联主键id
     * @return 结果
     */
    Integer deleteOneRankApplyFile(@Param("id") String id);


    /**
     * 查询数量
     * @param memberId
     * @param auditStatus
     * @param createTimeStart
     * @param createTimeEnd
     * @return
     */
    Integer selectCount(@Param("applyUserId") String applyUserId, @Param("auditStatus") Integer auditStatus, @Param("createTimeStart") String createTimeStart, @Param("createTimeEnd") String createTimeEnd);

    //软删除
    Integer softDelete(@Param("applyUserId") String applyUserId);
}
