package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

@Data
public class CourseApplyDetail {
    private String id;

    private String courseApplyId;

    private String courseId;

    private String applyMemberId;

    private String applyMemberType;

    private String applyName;

    private String applySex;

    private String applyBirthday;

    private String applyIdnumber;

    private String applyMobile;

    private Integer sort;

    private String remark;

    private Date creatTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;


}