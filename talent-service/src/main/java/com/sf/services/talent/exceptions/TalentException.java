package com.sf.services.talent.exceptions;


import org.springframework.http.HttpStatus;

/**
 * 人才服务异常
 *
 * @author zhaochangzhi
 * @date 2021/7/7
 **/
public class TalentException extends RuntimeException {

    protected final int code;

    public TalentException(String message) {
        this(HttpStatus.INTERNAL_SERVER_ERROR.value(), message, null);
    }

    public TalentException(int code, String message) {
        this(code, message, null);
    }

    public TalentException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }
}
