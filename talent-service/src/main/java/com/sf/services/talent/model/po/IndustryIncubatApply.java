package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

@Data
public class IndustryIncubatApply {
    private String id;

    private String companyName;

    private String registCapital;

    private String legalRepresentativeName;

    private String legalRepresentativeMobile;

    private String contactName;

    private String contactMobile;

    private String contactEmail;

    private String companyIntroduction;

    private String workAddress;

    private String workMobile;

    private Integer auditStatus;

    private String auditReason;

    private Date auditTime;

    private String auditUser;

    private Integer sort;

    private String remark;

    private Date createTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;

}