package com.sf.services.talent.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.mapper.DictionaryMapper;
import com.sf.services.talent.mapper.InterfaceTotalMapper;
import com.sf.services.talent.mapper.TotalMapper;
import com.sf.services.talent.model.po.Archive;
import com.sf.services.talent.model.po.Dictionary;
import com.sf.services.talent.model.po.InterfaceTotal;
import com.sf.services.talent.service.ITotalService;
import com.sf.services.talent.util.ExportExcelUtil;
import com.sf.services.talent.util.FillUserInfoUtil;

@Service
public class TotalService implements ITotalService{

	@Autowired
	private TotalMapper totalMapper;
	@Autowired
	private InterfaceTotalMapper interfaceTotalMapper;
	@Autowired
	private DictionaryMapper dictionaryMapper;
	
	@Override
	public Map<String, Object> overallStatistics() {
		
		return totalMapper.overallStatistics();
	}

	@Override
	public Map<String, Integer> overallArticle() {
		
		return totalMapper.overallArticle();
	}

	@Override
	public Map<String, Integer> publishArticle() {
		
		return totalMapper.publishArticle();
	}

	@Override
	public Map<String, Integer> overallUser() {
		
		return totalMapper.overallUser();
	}

	@Override
	public List<Map<String, Integer>> overallInterface() {
		
		return totalMapper.overallInterface();
	}

	@Override
	public int updateInterfaceTotal(String url) {
		
		return interfaceTotalMapper.updateTotal(url);
	}

	@Override
	public int saveInterface(InterfaceTotal interfaceTotal) {
		FillUserInfoUtil.fillCreateUserInfo(interfaceTotal);
		interfaceTotal.setTotal(1);
		return interfaceTotalMapper.insertSelective(interfaceTotal);
	}

	@Override
	public Map<String, Object> userRegister() {
		Map<String, Object> res = new HashMap<String, Object>();
		res.put("userList", totalMapper.userRegister("1"));
		res.put("companyList", totalMapper.userRegister("2"));
		return res;
	}

	@Override
	public Map<String, Object> webRegister() {
		Map<String, Object> res = new HashMap<String, Object>();
		res.put("webSite1", totalMapper.webRegister("1"));
		res.put("webSite2", totalMapper.webRegister("2"));
		res.put("webSite3", totalMapper.webRegister("3"));
		return res;
	}

	@Override
	public Map<String, Integer> webNowDayTotal(String url) {
		
		return totalMapper.webNowDayTotal(url);
	}

	@Override
	public Map<String, Integer> webAllTotal(String url) {
		
		return totalMapper.webAllTotal(url);
	}

	@Override
	public Map<String, Integer> webNowDayTotalTwohours(String url) {
		
		return totalMapper.webNowDayTotalTwohours(url);
	}

	@Override
	public Map<String, Integer> webAllTotalTwohours(String url) {
		
		return totalMapper.webAllTotal101Twohours(url);
	}

	@Override
	public Dictionary getSumMin() {
		
		return dictionaryMapper.selectOneByTypeAndDickey("sumMin", "1");
	}

	@Override
	public  PageInfo<Map<String, Object>> webTotalList(Map<String, Object> param) {
		List<Map<String, Object>> list = totalMapper.webTotalList(param);
		int totalFlow = 0;
		int totalPeople = 0;
		for (int i = 0; i < list.size(); i++) {
			if(list.get(i).get("totalFlow")!=null) {
				totalFlow = totalFlow+Integer.valueOf(list.get(i).get("totalFlow")+"");
			}
			if(list.get(i).get("totalPeople")!=null) {
				totalPeople = totalPeople+Integer.valueOf(list.get(i).get("totalPeople")+"");
			}
		}
		PageHelper.startPage(Integer.valueOf(param.get("pageNum")+"") , Integer.valueOf(param.get("pageSize")+""));
		List<Map<String, Object>> res = totalMapper.webTotalList(param);
		Map<String, Object> last = new HashMap<String, Object>();
		last.put("name", "总计");
		last.put("createDate", "");
		last.put("totalFlow", totalFlow);
		last.put("totalPeople", totalPeople);
		res.add(last);
		return new PageInfo<>(res);
	}

	@Override
	public void exportList(HttpServletRequest request, HttpServletResponse response) {
				//查询列表
				Map<String, Object> active = new HashMap<String, Object>();
				active.put("name", request.getParameter("name"));
				active.put("startTime", request.getParameter("startTime"));
				active.put("endTime", request.getParameter("endTime"));
				List<Map<String, Object>> list = totalMapper.webTotalList(active);
				if (list == null || list.size() == 0) {
					return;
				}

				//导出列表
				List<Map<String, Object>> dataMapList = new ArrayList<Map<String, Object>>();
				Map<String, Object> headMap = new LinkedHashMap<String, Object>();

				headMap.put("rowNum", "序号");
				headMap.put("name", "网站名称");
				headMap.put("createDate", "创建时间");
				headMap.put("totalFlow", "访问量");
				headMap.put("totalPeople", "访问人数");
				int totalFlow = 0;
				int totalPeople = 0;
				// excel导出内容
				for (int i = 0; i < list.size(); i++) {
					Map<String, Object> data = new HashMap<String, Object>();

					data.put("rowNum", i+1);//序号
					data.put("name", list.get(i).get("name"));
					data.put("createDate", list.get(i).get("createDate"));
					data.put("totalFlow", list.get(i).get("totalFlow"));
					if(list.get(i).get("totalFlow")!=null) {
						totalFlow = totalFlow+Integer.valueOf(list.get(i).get("totalFlow")+"");
					}
					data.put("totalPeople", list.get(i).get("totalPeople"));
					if(list.get(i).get("totalPeople")!=null) {
						totalPeople = totalPeople+Integer.valueOf(list.get(i).get("totalPeople")+"");
					}

					dataMapList.add(data);
				}
				Map<String, Object> last = new HashMap<String, Object>();
				last.put("rowNum", "总计");
				last.put("name", "");
				last.put("createDate", "");
				last.put("totalFlow", totalFlow);
				last.put("totalPeople", totalPeople);
				dataMapList.add(last);
		ExportExcelUtil export = new ExportExcelUtil();
		String fileName = "网站统计列表";//文件名
		String sheetName = "网站统计列表";//页签名
		export.toExcelForStream(request, response, headMap, dataMapList,  fileName, sheetName);
	}
	
	
}

