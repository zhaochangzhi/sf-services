package com.sf.services.talent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(value = "档案预约参数实体", description = "档案预约参数实体")
public class ArchiveServiceAppointmentModel extends PageParamModel{
	
	@ApiModelProperty(value = "主键id", example = "49f36f829ce6449294ad1f19e2a6aa59")
    private String id;

	@ApiModelProperty(value = "档案服务类别id", example = "49f36f829ce6449294ad1f19e2a6aa59")
    private String categoryId;

	@ApiModelProperty(value = "申请人姓名", example = "张三")
    private String applicantName;

	@ApiModelProperty(value = "申请人身份证id", example = "211022111122226587")
    private String applicantId;

	@ApiModelProperty(value = "申请人电话号", example = "13111111111")
    private String applicantPhone;

	@ApiModelProperty(value = "\"1\"：待审核；\"2\"：已核可；\"3\"：已批退", example = "1")
    private String auditStatus;

	@ApiModelProperty(value = "拒绝原因", example = "身份不匹配")
    private String auditRejectReason;

	@ApiModelProperty(value = "预约办理时间", example = "2021-07-10")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date appointmentTime;
	
	@ApiModelProperty(value = "预约办理地址", example = "政务服务中心一楼120窗口")
	private String handlerAddress;

}