package com.sf.services.talent.mapper;

import com.sf.services.talent.model.RankMajorParamModel;
import com.sf.services.talent.model.po.RankMajor;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * MajorMapper
 *
 * @author zhaochangzhi
 * @date 2021/7/20
 */
@Mapper
public interface RankMajorMapper {

    /**
     * 查询列表
     *
     * @param params 参数
     * @return 列表数据
     */
    List<RankMajor> selectList(RankMajorParamModel params);
}
