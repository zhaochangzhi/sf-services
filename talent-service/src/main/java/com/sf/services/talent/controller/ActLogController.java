package com.sf.services.talent.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sf.services.talent.model.ActLogModel;
import com.sf.services.talent.service.IActLogService;
import com.sf.services.talent.util.ResultUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(value = "操作日志", tags = "操作日志")
@RestController
@RequestMapping("/actLog")
public class ActLogController {
	
	@Autowired
	private IActLogService actLogService;
	
	@ApiOperation(notes = "操作日志列表", value="操作日志列表")
	@ApiImplicitParam(name = "params", value = "操作日志模型", required = false, dataType = "actLog")
	@PostMapping("/list")
	public ResultUtil list(@RequestBody ActLogModel actLog) {
		try {
			return ResultUtil.success(actLogService.pageList(actLog));
		} catch (Exception e) {
			log.error("操作日志列表异常:{}",e);
			return ResultUtil.error(401, "操作日志列表异常");
		}
		
	}
	
	@ApiOperation(notes = "操作日志删除", value="操作日志删除")
	@ApiImplicitParam(name = "ids", value = "日志id 用','分割", required = false, dataType = "String")
	@PostMapping("/del")
	public ResultUtil list(@RequestBody Map<String, String> params) {
		try {
			return ResultUtil.success(actLogService.del(params.get("ids")));
		} catch (Exception e) {
			log.error("操作日志删除异常:{}",e);
			return ResultUtil.error(401, "操作日志删除异常");
		}
		
	}
}

