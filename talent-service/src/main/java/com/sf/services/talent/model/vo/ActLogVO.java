package com.sf.services.talent.model.vo;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ActLogVO{
	
	@ApiModelProperty(value = "主键id")
	private String id;
	@ApiModelProperty(value = "日志内容")
	private String actLogContect;
	@ApiModelProperty(value = "日志标题")
	private String actLogTitle;
	@ApiModelProperty(value = "用户类型")
	private String type;
	@ApiModelProperty(value = "用户名")
	private String actUserName;
	@ApiModelProperty(value = "创建时间")
	private Date createDate;
	@ApiModelProperty(value = "用户类型描述")
	private String typeName;
	
	
}

