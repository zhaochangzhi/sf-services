package com.sf.services.talent.mapper;

import com.sf.services.talent.model.PageParamModel;
import com.sf.services.talent.model.po.EmploymentTarget;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EmploymentTargetMapper {
    int deleteByPrimaryKey(String id);

    int insert(EmploymentTarget record);

    int insertSelective(EmploymentTarget record);

    EmploymentTarget selectByPrimaryKey(String id);

    int selectPageListCount(Map<String, Object> searchMap);

    List<EmploymentTarget> selectPageList(Map<String, Object> searchMap);

    int updateByPrimaryKeySelective(EmploymentTarget record);

    int updateByPrimaryKey(EmploymentTarget record);

    /**
     * 查询列表
     *
     * @param params 参数
     * @return 列表数据
     */
    List<EmploymentTarget> selectList(PageParamModel params);

    /**
     * 根据指标类型id/街道key查询
     * @param typeId
     * @param streetKey
     * @param targetYear
     * @return
     */
    EmploymentTarget selectByTypeIdAndStreetKeyAndTargetYear(@Param("typeId") String typeId, @Param("streetKey")String streetKey, @Param("targetYear")Integer targetYear);
}