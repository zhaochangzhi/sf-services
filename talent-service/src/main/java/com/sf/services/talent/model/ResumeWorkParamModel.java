package com.sf.services.talent.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "简历-工作经历", description = "简历-工作经历")
public class ResumeWorkParamModel {
	@ApiModelProperty(value = "主键id", example = "49f36f829ce6449294ad1f19e2a6aa59")
	private String id;
	@ApiModelProperty(value = "公司名称", example = "阿狸巴巴")
    private String companyName;
	@ApiModelProperty(value = "工作职称", example = "java开发")
    private String job;
	@ApiModelProperty(value = "工作开始时间", example = "2020-02-07")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date startTime;
	@ApiModelProperty(value = "工作内容", example = "2020-07-21")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date endTime;
	@ApiModelProperty(value = "工作内容", example = "java开发")
    private String workContent;
	
}

