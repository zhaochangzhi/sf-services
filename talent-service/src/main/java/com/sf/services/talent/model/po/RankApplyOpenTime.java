package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

@Data
public class RankApplyOpenTime {
    private String id;

    private Date startTime;

    private Date endTime;

    private String closeContent;

    private String remark;

    private Date createTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;

}