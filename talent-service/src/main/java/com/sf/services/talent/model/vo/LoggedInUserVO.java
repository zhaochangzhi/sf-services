package com.sf.services.talent.model.vo;

import com.sf.services.talent.model.po.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * LoggedInUserVO
 * 已登陆账号信息实体
 * @author guchangliang
 * @date 2021/7/25
 */
@ApiModel(value = "已登陆账号信息实体")
@Data
public class LoggedInUserVO {

    @ApiModelProperty(value = "会员id")
    private String memberId;

    @ApiModelProperty(value = "会员类型")
    private String type;

    @ApiModelProperty(value = "企业用户信息")
    private EnterpriseUserVO enterpriseUser;

    @ApiModelProperty(value = "个人用户信息")
    private User user;






}