package com.sf.services.talent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * JobFairParamModel 招聘会参数实体
 *
 * @author zhaochangzhi
 * @date 2021/7/23
 */
@ApiModel(value = "JobFairParamModel", description = "招聘会参数实体")
@Data
public class JobFairParamModel extends PageParamModel {

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "招聘会类型标识（1：现场招聘，2：网络招聘会）", example = "1")
    private Integer typeStatus;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "主办方")
    private String sponsor;

    @ApiModelProperty(value = "主举办地址")
    private String address;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "开始时间", example = "2021-01-01")
    private Date startTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "结束时间", example = "2021-12-31")
    private Date endTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "开始时间搜索-开始", example = "2021-01-01")
    private Date startTimeStart;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "开始时间搜索结束", example = "2021-12-31")
    private Date startTimeEnd;

    @ApiModelProperty(value = "联系人")
    private String contacts;

    @ApiModelProperty(value = "联系方式")
    private String contactsPhone;

    @ApiModelProperty(value = "推广图片id")
    private String imageId;

    @ApiModelProperty(value = "删除状态： 0未删除，1 已删除")
    private Integer deleted;

    @ApiModelProperty(value = "状态：0 不可用，1可用")
    private Integer status;

    @ApiModelProperty(value = "列表类型状态：1：即将开始，2：正在进行，3：已结束，4：非正在进行中(即将开始+已结束)")
    private Integer listTypeStatus;

    @ApiModelProperty(value = "交通路线")
    private String routes;
    @ApiModelProperty(value = "招聘会介绍")
    private String introduce;
    @ApiModelProperty(value = "媒体宣传")
    private String media;
    @ApiModelProperty(value = "展位设置方案")
    private String booth;
    @ApiModelProperty(value = "参与办法")
    private String joinMethod;

    @ApiModelProperty(value = "是否为悬浮banner", example = "1")
    private Integer isArticleFloatBanner;
    @ApiModelProperty(value = "悬浮banner图片id", example = "0096ff5444014df48c6301751d1f0830")
    private String articleFloatBannerPhotoId;
}
