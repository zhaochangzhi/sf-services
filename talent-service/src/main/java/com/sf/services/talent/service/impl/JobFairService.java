package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.controller.WebSocketServer;
import com.sf.services.talent.mapper.*;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.dto.JobFairDto;
import com.sf.services.talent.model.po.*;
import com.sf.services.talent.model.vo.JobFairEmploymentPositionVO;
import com.sf.services.talent.model.vo.JobFairEmploymentVO;
import com.sf.services.talent.model.vo.JobFairUserVO;
import com.sf.services.talent.model.vo.JobFairVO;
import com.sf.services.talent.service.IActLogService;
import com.sf.services.talent.service.IJobFairService;
import com.sf.services.talent.util.UUIDUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * JobFairService
 *
 * @author zhaochangzhi
 * @date 2021/7/23
 */

@Slf4j
@Service
public class JobFairService implements IJobFairService {

    private final JobFairMapper jobFairMapper;
    @Resource
    private JobFairEnterprisePositionMapper jobFairEnterprisePositionMapper;
    @Resource
    private EnterprisePositionMapper enterprisePositionMapper;
    @Resource
    private ArticleFloatBannerMapper articleFloatBannerMapper;
    @Resource
    private FileMapper fileMapper;
    @Resource
    private JobFairUserMapper jobFairUserMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    private MemberMapper memberMapper;

    @Value("domain.url")
    String domainUrl;//域名

    @Autowired
    private RedissonClient redisson;

    @Autowired
    private WebSocketServer webSocketServer;

    @Autowired
    public JobFairService(JobFairMapper jobFairMapper) {
        this.jobFairMapper = jobFairMapper;
    }
    @Resource
    private IActLogService actLogService;//日志

    @Override
    public PageInfo<JobFairDto> getList(JobFairParamModel params) {
        List<JobFairVO> jobFairList = new ArrayList<>();

        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<JobFairDto> list = jobFairMapper.selectList(params);
//        if (!list.isEmpty()) {
//            list.stream().forEach(jobFair -> {
//                JobFairVO jobFairVO = new JobFairVO();
//                BeanUtils.copyProperties(jobFair, jobFairVO);
//                jobFairList.add(jobFairVO);
//            });
//        }
        return new PageInfo<>(list);
    }

    @Override
    public JobFairVO getOneById(String id) {
        JobFairVO jobFairVO = new JobFairVO();
        JobFairDto jobFair = jobFairMapper.selectOneById(id);
        if (Objects.nonNull(jobFair)) {
            BeanUtils.copyProperties(jobFair, jobFairVO);
        }

        //查询是否为漂浮banner
        ArticleFloatBanner articleFloatBanner = articleFloatBannerMapper.select(2, jobFair.getId());
        if(articleFloatBanner != null){
            jobFairVO.setIsArticleFloatBanner(1);
            jobFairVO.setArticleFloatBannerPhotoId(articleFloatBanner.getPhotoId());
            jobFairVO.setArticleFloatBannerPhotoUrl(articleFloatBanner.getPhotoUrl());
        }else{
            jobFairVO.setIsArticleFloatBanner(0);
        }

        //判断开始状态
        Date nowTime = new Date();
        Integer onStatus = null;//开始状态：0：即将开始，1：正在进行，2：已结束
        if(nowTime.getTime() < jobFair.getStartTime().getTime()){
            onStatus = 0;
        }else if(nowTime.getTime() >= jobFair.getStartTime().getTime() && nowTime.getTime() <= jobFair.getEndTime().getTime()){
            onStatus = 1;
        }else if(nowTime.getTime() > jobFair.getEndTime().getTime()){
            onStatus = 2;
        }
        jobFairVO.setOnStatus(onStatus);//开始状态：0：即将开始，1：正在进行，2：已结束

        return jobFairVO;
    }

    @Override
    public Integer addOne(JobFairDto params) {
        JobFair jobFair = new JobFair();
        BeanUtils.copyProperties(params, jobFair);
        jobFair.setId(UUIDUtil.getUUid());

        //更新结束时间时分秒为23:59:59
        if(jobFair.getEndTime() != null){
            Calendar endTimeCalendar = Calendar.getInstance();
            endTimeCalendar.setTime(jobFair.getEndTime());
            endTimeCalendar.add(Calendar.HOUR,23);
            endTimeCalendar.add(Calendar.MINUTE,59);
            endTimeCalendar.add(Calendar.SECOND,59);
            jobFair.setEndTime(endTimeCalendar.getTime());
        }

        //更新悬浮banner信息
        if(params.getIsArticleFloatBanner() != null && params.getIsArticleFloatBanner() == 1){
            ArticleFloatBanner articleFloatBanner = articleFloatBannerMapper.selectLast();
            if(articleFloatBanner != null){
                articleFloatBanner.setModuleId(jobFair.getId());
                articleFloatBanner.setModuleTypeStatus(2);//模块类型（1：文章，2：招聘会）
                articleFloatBanner.setModuleTypeName("招聘会");
                if(params.getArticleFloatBannerPhotoId() != null){
                    File file = fileMapper.selectByPrimaryKey(params.getArticleFloatBannerPhotoId());
                    articleFloatBanner.setPhotoId(params.getArticleFloatBannerPhotoId());
                    if(file != null){
                        articleFloatBanner.setPhotoUrl(file.getFileUrl());
                    }
                }else {
                    File file = fileMapper.selectByPrimaryKey(jobFair.getImageId());
                    articleFloatBanner.setPhotoId(jobFair.getImageId());
                    articleFloatBanner.setPhotoUrl(file.getFileUrl());
                }
                articleFloatBanner.setUpdateTime(jobFair.getCreateTime());
                articleFloatBanner.setUpdateUser(jobFair.getCreateUser());
                articleFloatBanner.setRemark(params.getTitle());
                articleFloatBanner.setBannerLink(domainUrl+"/#/talent-service/job-fair/detail?id="+jobFair.getId());
                //更新
                articleFloatBannerMapper.updateByPrimaryKeySelective(articleFloatBanner);
            }
        }
        //保存操作日志
        try {
            actLogService.save(jobFair.getCreateUser(), "创建招聘会", jobFair.getTitle());
        } catch (Exception e) {
            log.error("插入日志异常:{}",e);
        }

        return jobFairMapper.insertOne(jobFair);
    }

    @Override
    public Integer updateOne(JobFairDto params) {
        JobFair jobFair = new JobFair();
        BeanUtils.copyProperties(params, jobFair);

        //更新结束时间时分秒为23:59:59
        if(jobFair.getEndTime() != null){
            Calendar endTimeCalendar = Calendar.getInstance();
            endTimeCalendar.setTime(jobFair.getEndTime());
            endTimeCalendar.add(Calendar.HOUR,23);
            endTimeCalendar.add(Calendar.MINUTE,59);
            endTimeCalendar.add(Calendar.SECOND,59);
            jobFair.setEndTime(endTimeCalendar.getTime());
        }

        //更新悬浮banner信息
        if(params.getIsArticleFloatBanner() != null && params.getIsArticleFloatBanner() == 1){
            ArticleFloatBanner articleFloatBanner = articleFloatBannerMapper.selectLast();
            if(articleFloatBanner != null){
                articleFloatBanner.setModuleId(jobFair.getId());
                articleFloatBanner.setModuleTypeStatus(2);//模块类型（1：文章，2：招聘会）
                articleFloatBanner.setModuleTypeName("招聘会");
                if(params.getArticleFloatBannerPhotoId() != null){
                    File file = fileMapper.selectByPrimaryKey(params.getArticleFloatBannerPhotoId());
                    articleFloatBanner.setPhotoId(params.getArticleFloatBannerPhotoId());
                    if(file != null){
                        articleFloatBanner.setPhotoUrl(file.getFileUrl());
                    }
                }else {
                    File file = fileMapper.selectByPrimaryKey(jobFair.getImageId());
                    articleFloatBanner.setPhotoId(jobFair.getImageId());
                    articleFloatBanner.setPhotoUrl(file.getFileUrl());
                }
                articleFloatBanner.setUpdateTime(jobFair.getUpdateTime());
                articleFloatBanner.setUpdateUser(jobFair.getUpdateUser());
                //更新
                articleFloatBannerMapper.updateByPrimaryKeySelective(articleFloatBanner);
            }
        }

        //保存操作日志
        try {
            actLogService.save(jobFair.getCreateUser(), "更新招聘会", jobFair.getTitle());
        } catch (Exception e) {
            log.error("插入日志异常:{}",e);
        }

        return jobFairMapper.updateOne(jobFair);
    }

    @Override
    public Integer deleteOne(JobFairDto params) {
        JobFair jobFair = new JobFair();
        BeanUtils.copyProperties(params, jobFair);
        return jobFairMapper.updateOne(jobFair);
    }

    @Override
    public PageInfo getEnterpriseList(JobFairEnterpriseParamModel params) {

        //招聘会信息
        JobFairDto jobFair = jobFairMapper.selectOneById(params.getId());

        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<JobFairEmploymentVO> list = jobFairEnterprisePositionMapper.selectEnterpriseList(params.getId(), params.getAuditStatus());

        //查询职位详情列表
        if(list != null && list.size() >= 1){
            for (int i = 0; i < list.size(); i++) {
                //查询企业参会职位列表
                List<JobFairEmploymentPositionVO> jobFairEmploymentPositionVOList = jobFairEnterprisePositionMapper.selectEnterprisePositionList(list.get(i).getJobFairId(), list.get(i).getEnterpriseUserId(), params.getAuditStatus());
                list.get(i).setJobFairEmploymentPositionList(jobFairEmploymentPositionVOList);

                //网络招聘会，查询企业在线状态
                //type_status 招聘会类型标识（1：现场招聘，2：网络招聘会）
                if(jobFair != null && jobFair.getTypeStatus() != null && jobFair.getTypeStatus() == 2){
                    list.get(i).setIsOnline(webSocketServer.isOnline(list.get(i).getMemberId()));
                }
            }
        }

        return new PageInfo<>(list);
    }

    @Override
    public BaseResultModel getEnterpriseDetail(JobFairEnterpriseGetParamModel params) {
        if(params == null){
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getJobFairId())) {
            return BaseResultModel.badParam("招聘会id不能为空");
        }
        if (StringUtils.isBlank(params.getEnterpriseUserId())) {
            return BaseResultModel.badParam("企业账号id不能为空");
        }
        if (params.getAuditStatus() == null) {
            return BaseResultModel.badParam("审核状态不能为空");
        }

        //分页处理
        JobFairEmploymentVO jobFairEmploymentVO = jobFairEnterprisePositionMapper.selectEnterpriseDetail(params.getJobFairId(),params.getEnterpriseUserId(), params.getAuditStatus());

        //查询职位详情列表
        if(jobFairEmploymentVO != null){
            //查询企业参会职位列表
            List<JobFairEmploymentPositionVO> jobFairEmploymentPositionVOList = jobFairEnterprisePositionMapper.selectEnterprisePositionList(params.getJobFairId(),params.getEnterpriseUserId(), params.getAuditStatus());
            jobFairEmploymentVO.setJobFairEmploymentPositionList(jobFairEmploymentPositionVOList);
        }

        return BaseResultModel.success(jobFairEmploymentVO);
    }


    @Override
    public BaseResultModel jobFairEnterprisePositionAdd(JobFairEnterpriseAddParamModel params, String createUser) {
        if (params == null) {
            return BaseResultModel.badParam("参数不能为空");
        }
        if(StringUtils.isBlank(params.getJobFairId())){
            return BaseResultModel.badParam("招聘会id不能为空");
        }
        if(StringUtils.isBlank(params.getEnterpriseUserId())){
            return BaseResultModel.badParam("企业userId不能为空");
        }
        if(params.getEnterprisePositionIdList() == null || params.getEnterprisePositionIdList().size() == 0){
            return BaseResultModel.badParam("职位id列表不能为空");
        }

        //redisson分布式锁
        String lockKey = "JobFairServicejobFairEnterprisePositionAdd"+params.getEnterpriseUserId();
        RLock rlock = redisson.getLock(lockKey);
        try {
            //校验是否并发
            if(rlock.isLocked()){
                return BaseResultModel.badParam("请慢些操作");
            }

            //上锁
            rlock.lock();

            //查询JobFairId
            JobFairDto jobFairDto = jobFairMapper.selectOneById(params.getJobFairId());
            if (jobFairDto == null) {
                return BaseResultModel.badParam("招聘会id不存在");
            }

//        //校验企业参会，则不可以再次参会
//        List<JobFairEmploymentPositionVO> list = jobFairEnterprisePositionMapper.selectEnterprisePositionList(params.getJobFairId(), params.getEnterpriseUserId());
//        if(list != null && list.size() >= 1){
//            return BaseResultModel.badParam("已参加招聘会，无法再次参加");
//        }

            //查询enterprisePositionIdList
            JobFairEnterprisePosition jobFairEnterprisePosition = new JobFairEnterprisePosition();
            jobFairEnterprisePosition.setJobFairId(jobFairDto.getId());
            jobFairEnterprisePosition.setJobFairTitle(jobFairDto.getTitle());
            jobFairEnterprisePosition.setCreatTime(new Date());//创建时间
            jobFairEnterprisePosition.setUpdateTime(jobFairEnterprisePosition.getCreatTime());//更新时间
            jobFairEnterprisePosition.setCreateUser(createUser);//创建人

            //校验是否已申请
            for (int i = 0; i < params.getEnterprisePositionIdList().size(); i++) {
                EnterprisePosition enterprisePosition = enterprisePositionMapper.selectByPrimaryKey(params.getEnterprisePositionIdList().get(i));
                if (enterprisePosition == null) {
                    return BaseResultModel.badParam("职位不存在");
                }

                //校验审核状态：1 审核中，2 通过，3 不通过
                JobFairEnterprisePosition checkJobFairEnterprisePosition = jobFairEnterprisePositionMapper.selectEnterprisePosition(params.getJobFairId(), enterprisePosition.getId());
                if(checkJobFairEnterprisePosition != null){
                    if(checkJobFairEnterprisePosition.getAuditStatus() == 1){
                        return BaseResultModel.badParam(enterprisePosition.getEnterprisePositionName()+"职位为待审核状态，请等待审核");
                    }
                    if(checkJobFairEnterprisePosition.getAuditStatus() == 2){
                        return BaseResultModel.badParam(enterprisePosition.getEnterprisePositionName()+"职位已审核通过，无需再次申请");
                    }
                }
            }

            //插入
            for (int i = 0; i < params.getEnterprisePositionIdList().size(); i++) {
                EnterprisePosition enterprisePosition = enterprisePositionMapper.selectByPrimaryKey(params.getEnterprisePositionIdList().get(i));
                if (enterprisePosition != null) {
                    //插入t_job_fair_enterprise_position表
                    jobFairEnterprisePosition.setId(UUIDUtil.getUUid());//id

                    jobFairEnterprisePosition.setEnterprisePositionId(enterprisePosition.getId());
                    jobFairEnterprisePosition.setEnterprisePositionName(enterprisePosition.getEnterprisePositionName());
                    jobFairEnterprisePosition.setAuditStatus(1);//审核状态：1 审核中，2 通过，3 不通过
                    jobFairEnterprisePositionMapper.insertSelective(jobFairEnterprisePosition);
                }
            }

            //保存操作日志
            try {
                actLogService.save(createUser, "企业报名参加招聘会", jobFairDto.getTitle());
            } catch (Exception e) {
                log.error("插入日志异常:{}",e);
            }

            return BaseResultModel.success(1);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("JobFairServicejobFairEnterprisePositionAdd", e);
            return BaseResultModel.badParam("异常");
        }finally {
            //解锁
            rlock.unlock();
        }

    }


    @Override
    public BaseResultModel audit(JobFairEnterpriseAuditParamModel params, String userId) {
        //校验非空-列表
        if(params == null){
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getJobFairId())) {
            return BaseResultModel.badParam("招聘会id不能为空");
        }
        if (params.getEnterprisePositionIdList() == null || params.getEnterprisePositionIdList().size() == 0) {
            return BaseResultModel.badParam("职位id列表不能为空");
        }
        if (params.getAuditStatus() == null) {
            return BaseResultModel.badParam("审核状态不能为空");
        }
        //审核状态：1 审核中，2 通过，3 不通过
        if (params.getAuditStatus() != 2 && params.getAuditStatus() != 3) {
            return BaseResultModel.badParam("审核状态不正确");
        }
        //校验审核不通过原因
        if(params.getAuditStatus() == 3 ){
            //暂时不校验
        }else {
            //非审核不通过状态，将审核不通过原因置为空
            params.setAuditReason("");
        }

        //校验职位
        for (int i = 0; i < params.getEnterprisePositionIdList().size(); i++) {
            //校验状态
            JobFairEnterprisePosition check = jobFairEnterprisePositionMapper.selectEnterprisePosition(params.getJobFairId(), params.getEnterprisePositionIdList().get(i));
            if (check == null) {
                return BaseResultModel.badParam("参会职位id不存在");
            }
            if(check.getAuditStatus() != null && (check.getAuditStatus() == 2 || check.getAuditStatus() == 3)){
                return BaseResultModel.badParam(check.getEnterprisePositionName()+"已审核，无需再次审核");
            }
        }

        //更新
        for (int i = 0; i < params.getEnterprisePositionIdList().size(); i++) {
            JobFairEnterprisePosition update = jobFairEnterprisePositionMapper.selectEnterprisePosition(params.getJobFairId(), params.getEnterprisePositionIdList().get(i));
            update.setAuditStatus(params.getAuditStatus());
            update.setAuditReason(params.getAuditReason());
            update.setAuditTime(new Date());//审核时间
            update.setAuditUser(userId);//审核人

            jobFairEnterprisePositionMapper.updateByPrimaryKeySelective(update);
        }

        //保存操作日志
        try {
            JobFairDto jobFair = jobFairMapper.selectOneById(params.getJobFairId());
            actLogService.save(userId, "审核参会企业职位", jobFair.getTitle());
        } catch (Exception e) {
            log.error("插入日志异常:{}",e);
        }

        return BaseResultModel.success(params.getEnterprisePositionIdList().size());
    }

    @Override
    public BaseResultModel jobFairUserAdd(JobUserAddParamModel params, String createUser) {
        if (params == null) {
            return BaseResultModel.badParam("参数不能为空");
        }
        if(StringUtils.isBlank(params.getJobFairId())){
            return BaseResultModel.badParam("招聘会id不能为空");
        }
        if(StringUtils.isBlank(params.getMemberId())){
            return BaseResultModel.badParam("用户memberId不能为空");
        }
        //校验memberId是否存在
        Member member = memberMapper.selectByPrimaryKey(params.getMemberId());
        if(member == null){
            return BaseResultModel.badParam("用户memberId不存在");
        }

        //校验用户类型（只能是个人用户 1：个人，2公司，3政府）
        if(!"1".equals(member.getType())){
            return BaseResultModel.badParam("必须为个人用户");
        }

        //校验个人用户信息
        User user = userMapper.selectByMemberId(params.getMemberId());
        if(user == null){
            return BaseResultModel.badParam("个人账号信息异常");
        }

        //redisson分布式锁
        String lockKey = "JobFairServicejobFairUserAdd"+params.getMemberId();
        RLock rlock = redisson.getLock(lockKey);
        try {
            //校验是否并发
            if(rlock.isLocked()){
                return BaseResultModel.badParam("请慢些操作");
            }

            //上锁
            rlock.lock();

            //查询JobFairId
            JobFairDto jobFairDto = jobFairMapper.selectOneById(params.getJobFairId());
            if (jobFairDto == null) {
                return BaseResultModel.badParam("招聘会id不存在");
            }

            //校验用户参会，则不可以再次参会
            JobFairUser jobFairUser = jobFairUserMapper.selectByJobFairIdAndUserId(params.getJobFairId(), params.getMemberId());
            if(jobFairUser != null ){
                return BaseResultModel.badParam("已加入，无需再次申请");
            }

            //新增
            jobFairUser = new JobFairUser();
            BeanUtils.copyProperties(params, jobFairUser);

            jobFairUser.setUserId(user.getId());
            jobFairUser.setMemberId(member.getMemberId());
            jobFairUser.setJobFairTitle(jobFairDto.getTitle());

            jobFairUser.setId(UUIDUtil.getUUid());//id
            jobFairUser.setCreateUser(createUser);
            jobFairUser.setCreatTime(new Date());
            jobFairUser.setUpdateTime(jobFairUser.getCreatTime());//更新时间
            int count = jobFairUserMapper.insertSelective(jobFairUser);

            //保存操作日志
            try {
                actLogService.save(params.getMemberId(), "报名参加招聘会", jobFairDto.getTitle());
            } catch (Exception e) {
                log.error("插入日志异常:{}",e);
            }

            return BaseResultModel.success(count);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("JobFairServicejobFairEnterprisePositionAdd", e);
            return BaseResultModel.badParam("异常");
        }finally {
            //解锁
            rlock.unlock();
        }
    }


    @Override
    public PageInfo getUserList(JobFairUserListParamModel params) {

//        //招聘会信息
        JobFairDto jobFair = jobFairMapper.selectOneById(params.getJobFairId());

        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<JobFairUserVO> list = jobFairUserMapper.selectList(params.getJobFairId(), null);

        if(list != null && list.size() >= 1){
            for (int i = 0; i < list.size(); i++) {
                //网络招聘会，查询个人用户在线状态
                //type_status 招聘会类型标识（1：现场招聘，2：网络招聘会）
                if(jobFair != null && jobFair.getTypeStatus() != null && jobFair.getTypeStatus() == 2){
                    list.get(i).setIsOnline(webSocketServer.isOnline(list.get(i).getMemberId()));
                }
            }
        }

        return new PageInfo<>(list);
    }
}
