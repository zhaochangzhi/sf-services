package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

@Data
public class JobFairEnterprisePosition {
    private String id;

    private String jobFairId;

    private String jobFairTitle;

    private String enterprisePositionId;

    private String enterprisePositionName;

    private Integer auditStatus;
    private String auditReason;
    private Date auditTime;
    private String auditUser;

    private Integer sort;

    private String remark;

    private Date creatTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;

}