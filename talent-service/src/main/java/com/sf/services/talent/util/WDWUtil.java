package com.sf.services.talent.util;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * excel校验工具
 * guchangliang
 */
public class WDWUtil {
    /**
     * @描述：是否是2003的excel，返回true是2003
     * @param filePath
     * @return
     */
    public static boolean isExcel2003(String filePath)  {
        return filePath.matches("^.+\\.(?i)(xls)$");
    }

    /**
     * @描述：是否是2007的excel，返回true是2007
     * @param filePath
     * @return
     */
    public static boolean isExcel2007(String filePath)  {
        return filePath.matches("^.+\\.(?i)(xlsx)$");
    }

    /**
     * 验证是否是EXCEL文件
     * @param filePath
     * @return
     */
    public static boolean validateExcel(String filePath){
        if (filePath == null || !(isExcel2003(filePath) || isExcel2007(filePath))){
            return false;
        }
        return true;
    }

    /**
     * 返回Workbook
     */
    public static Workbook creatWorkbook(MultipartFile mFile){
        // 获取文件名
        String fileName = mFile.getOriginalFilename();
        // 根据文件名判断文件是2003版本还是2007版本
        try {
            Workbook wb = null;
            if (isExcel2003(fileName)) {// 当excel是2003时,创建excel2003
                wb = new HSSFWorkbook(mFile.getInputStream());
            }
            if (isExcel2007(fileName)){// 当excel是2007时,创建excel2007
                wb = new XSSFWorkbook(mFile.getInputStream());
            }
            return wb;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}
