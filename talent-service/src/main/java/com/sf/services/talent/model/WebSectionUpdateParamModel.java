package com.sf.services.talent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 编辑网站栏目
 * @author guchangliang
 * @date 2021/9/29
 */
@ApiModel(value = "编辑网站栏目")
@Data
public class WebSectionUpdateParamModel {

    @ApiModelProperty(value = "主键id", example = "6f92b90f6cb24ee2beadb8a57d0a0eb1")
    private String id;

    @ApiModelProperty(value = "栏目名称", example = "栏目名称xxx")
    private String name;

    @ApiModelProperty(value = "显示栏目名称", example = "显示栏目名称xxx")
    private String displayName;


}