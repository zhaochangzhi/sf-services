package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author guchangliang
 * @date 2021/7/13
 */
@ApiModel(value = "企业中心-职位管理-审核")
@Data
public class EmploymentPositionAuditParamModel {

    @ApiModelProperty(value = "主键id（t_enterprise_position表id）", example = "0483ba8eb8414b6a8703edc0cec807f6")
    private String id;


    @ApiModelProperty(value = "审核状态：1 审核中，2 通过，3 不通过", example = "2")
    private Integer auditStatus;
    @ApiModelProperty(value = "审核不通过原因", example = "不通过原因xxx")
    private String auditReason;

}