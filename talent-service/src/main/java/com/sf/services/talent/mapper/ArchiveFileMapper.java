package com.sf.services.talent.mapper;

import com.sf.services.talent.model.ArchiveFileParamModel;
import com.sf.services.talent.model.po.ArchiveFile;

import java.util.List;

public interface ArchiveFileMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(ArchiveFile record);

    ArchiveFile selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ArchiveFile record);

    /**
     * 查询列表
     * @param params
     * @return
     */
    List<ArchiveFile> selectList(ArchiveFileParamModel params);
}