package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 企业中心-简历管理
 * @author guchangliang
 * @date 2021/8/1
 */
@ApiModel(value = "企业中心-简历管理")
@Data
public class EmploymentPositionResumeAndUserParamModel extends PageParamModel{

    @ApiModelProperty(value = "企业账号id（t_enterprise_user表id）", example = "28b20fbf454f4558ab4c507255906072")
    private String enterpriseUserId;

}