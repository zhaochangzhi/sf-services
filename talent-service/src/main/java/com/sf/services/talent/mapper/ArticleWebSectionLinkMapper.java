package com.sf.services.talent.mapper;

import com.sf.services.talent.model.ArticleWebSectionLinkListParamModel;
import com.sf.services.talent.model.po.ArticleWebSectionLink;
import com.sf.services.talent.model.vo.ArticleWebSectionLinkVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ArticleWebSectionLinkMapper {
    int deleteByPrimaryKey(String id);

    int insert(ArticleWebSectionLink record);

    int insertSelective(ArticleWebSectionLink record);

    ArticleWebSectionLink selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ArticleWebSectionLink record);

    int updateByPrimaryKey(ArticleWebSectionLink record);

    //查询列表
    List<ArticleWebSectionLinkVO> selectList(ArticleWebSectionLinkListParamModel record);

    //查询webSectionLinkId被维护数量
    int selectCount(@Param("webSectionLinkId") String webSectionLinkId, @Param("notContainId") String notContainId);

    //更新文章web_section_link_id
    int updateArticleWebSectionLinkId(@Param("articleId") String articleId);

}