package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.mapper.*;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.po.Dictionary;
import com.sf.services.talent.model.po.*;
import com.sf.services.talent.model.vo.EmploymentTargetRecordVO;
import com.sf.services.talent.model.vo.EmploymentTargetVO;
import com.sf.services.talent.service.IEmploymentTargetService;
import com.sf.services.talent.util.ExportExcelUtil;
import com.sf.services.talent.util.OtherUtil;
import com.sf.services.talent.util.UUIDUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * EmploymentTargetService
 *
 * @author guchangliang
 * @date 2021/7/13
 */
@Slf4j
@Service
public class EmploymentTargetService implements IEmploymentTargetService {

    @Resource
    private EmploymentTargetMapper employmentTargetMapper;
    @Resource
    private EmploymentTargetRecordMapper employmentTargetRecordMapper;
    @Resource
    private EmploymentTargetTypeMapper employmentTargetTypeMapper;
    @Resource
    private DictionaryMapper dictionaryMapper;
    @Resource
    private EmploymentTargetRecordFileMapper employmentTargetRecordFileMapper;
    @Resource
    private FileMapper fileMapper;

    @Autowired
    private RedissonClient redisson;


    @Override
    public PageInfo<EmploymentTargetVO> getList(EmploymentTargetParamModel params) {

        List<EmploymentTargetVO> rankList = new ArrayList<>();
        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<EmploymentTarget> list = employmentTargetMapper.selectList(params);

        if (!list.isEmpty()) {
            list.stream().forEach(rank -> {
                EmploymentTargetVO employmentTargetVO = new EmploymentTargetVO();
                BeanUtils.copyProperties(rank, employmentTargetVO);
                rankList.add(employmentTargetVO);
            });
        }

        if(rankList != null && rankList.size() >= 1){
            for (int i = 0; i < rankList.size(); i++) {
                //就业指标-记录（1至12月）
                List<EmploymentTargetRecord> employmentTargetRecordList = employmentTargetRecordMapper.selectListByTargetId(rankList.get(i).getId());
                rankList.get(i).setEmploymentTargetRecordList(employmentTargetRecordList);
            }
        }

        return new PageInfo<>(rankList);
    }


    @Override
    public BaseResultModel add(EmploymentTargetAddParamModel employmentTargetAddParamModel
            , String userId) {

        List<EmploymentTargetRecordAddParamModel> employmentTargetRecordAddParamModelList = null;
        EmploymentTargetType employmentTargetType = null;//指标类型

        //校验非空-列表
        if(employmentTargetAddParamModel == null){
            return BaseResultModel.badParam("参数不能为空");
        }


        //redisson分布式锁
        String lockKey = "EmploymentTargetServiceAdd"+userId;
        RLock rlock = redisson.getLock(lockKey);
        try {
            //校验是否并发
            if(rlock.isLocked()){
                return BaseResultModel.badParam("请慢些操作");
            }

            //上锁
            rlock.lock();

            //取出就业指标记录
            employmentTargetRecordAddParamModelList = employmentTargetAddParamModel.getEmploymentTargetRecordList();

            //校验非空-指标类型id
            if(StringUtils.isBlank(employmentTargetAddParamModel.getTypeId())){
                return BaseResultModel.badParam("指标类型id不能为空");
            }
            //根据指标类型id查询指标类型信息
            employmentTargetType = employmentTargetTypeMapper.selectByPrimaryKey(employmentTargetAddParamModel.getTypeId());
            //校验非空-指标类型
            if(employmentTargetType == null){
                return BaseResultModel.badParam("指标类型id不存在");
            }

            //校验非空-街道key（t_dictionary表dickey）
            if(StringUtils.isBlank(employmentTargetAddParamModel.getStreetKey())){
                return BaseResultModel.badParam("街道key不能为空");
            }
            //查询街道信息
            Dictionary dictionary = dictionaryMapper.selectOneByDickey(employmentTargetAddParamModel.getStreetKey());
            if(dictionary == null){
                return BaseResultModel.badParam("街道key不存在");
            }
            String streetName = dictionary.getValue();//街道名称

            //校验是否已设置
            if(employmentTargetMapper.selectByTypeIdAndStreetKeyAndTargetYear(employmentTargetAddParamModel.getTypeId(), employmentTargetAddParamModel.getStreetKey(), employmentTargetAddParamModel.getTargetYear()) != null){
                return BaseResultModel.badParam("该指标类型已添加，无需再次添加");
            }

            //校验非空-指标形式状态（1：百分比；2：固定数值）
            if(employmentTargetAddParamModel.getFormStatus() == null){
                return BaseResultModel.badParam("指标形式状态不能为空");
            }
            if(employmentTargetAddParamModel.getFormStatus() != 1 && employmentTargetAddParamModel.getFormStatus() != 2){
                return BaseResultModel.badParam("指标形式状态不存在");
            }
            //校验非空-运算符号状态（1：大于等于；2：小于等于；3：等于）
            if(employmentTargetAddParamModel.getSymbolStatus() == null){
                return BaseResultModel.badParam("运算符号状态不能为空");
            }
            if(employmentTargetAddParamModel.getSymbolStatus() != 1
                    && employmentTargetAddParamModel.getSymbolStatus() != 2
                    && employmentTargetAddParamModel.getSymbolStatus() != 3){
                return BaseResultModel.badParam("运算符号状态不存在");
            }
            //校验非空-复数状态（1：设定值为一个数值；2：设定值为两个数值）
            if(employmentTargetAddParamModel.getComplexStatus() == null){
                return BaseResultModel.badParam("复数状态不能为空");
            }
            if(employmentTargetAddParamModel.getComplexStatus() != 1 && employmentTargetAddParamModel.getComplexStatus() != 2){
                return BaseResultModel.badParam("复数状态不存在");
            }
            //校验非空-指标年份
            if(employmentTargetAddParamModel.getTargetYear() == null){
                return BaseResultModel.badParam("指标年份不能为空");
            }


            //校验-遍历就业指标记录列表
            if(employmentTargetRecordAddParamModelList == null || employmentTargetRecordAddParamModelList.size() == 0){
                return BaseResultModel.badParam("就业指标记录列表不能为空");
            }
            if(employmentTargetRecordAddParamModelList.size() != 12){
                return BaseResultModel.badParam("就业指标记录列表必须为12个月");
            }
            for (int i = 0; i < employmentTargetRecordAddParamModelList.size(); i++) {
                if(employmentTargetRecordAddParamModelList.get(i).getTargetMonth() == null){
                    return BaseResultModel.badParam("就业指标记录指标月份不能为空");
                }
                if(employmentTargetRecordAddParamModelList.get(i).getTargetMonth() != (i+1)){
                    return BaseResultModel.badParam("就业指标记录指标月份请按月份顺序1-12");
                }
                //校验设定值
                if(employmentTargetAddParamModel.getComplexStatus() == 1){
                    if(StringUtils.isBlank(employmentTargetRecordAddParamModelList.get(i).getTarget())){
                        return BaseResultModel.badParam("设定值不能为空");
                    }
                }else if(employmentTargetAddParamModel.getComplexStatus() == 2){
                    if(StringUtils.isBlank(employmentTargetRecordAddParamModelList.get(i).getTargetOne())
                            || StringUtils.isBlank(employmentTargetRecordAddParamModelList.get(i).getTargetTwo())){
                        return BaseResultModel.badParam("复数状态-设定值1/2不能为空");
                    }
                    //复数状态=2时，赋值设定值为设定值1+设定值2
                    employmentTargetRecordAddParamModelList.get(i).setTarget(employmentTargetRecordAddParamModelList.get(i).getTargetOne()+"+"+employmentTargetRecordAddParamModelList.get(i).getTargetTwo());
                }
            }


            //新增
            EmploymentTarget employmentTarget = new EmploymentTarget();
            BeanUtils.copyProperties(employmentTargetAddParamModel, employmentTarget);
            //冗余字段赋值
            employmentTarget.setId(UUIDUtil.getUUid());
            employmentTarget.setTypeName(employmentTargetType.getTypeName());//指标类型名称（冗余）
            employmentTarget.setTargetYearTarget(employmentTargetRecordAddParamModelList.get(11).getTarget());//年份指标设定值（值为第12月份的设定值）
            employmentTarget.setStreetName(streetName);//街道名称
            employmentTarget.setCreatTime(new Date());
            employmentTarget.setCreateUser(userId);
            int count = employmentTargetMapper.insertSelective(employmentTarget);

            //插入就业指标记录列表
            if(count >= 1){
                for (int i = 0; i < employmentTargetRecordAddParamModelList.size(); i++) {
                    EmploymentTargetRecord employmentTargetRecord = new EmploymentTargetRecord();
                    BeanUtils.copyProperties(employmentTargetRecordAddParamModelList.get(i), employmentTargetRecord);
                    //冗余字段赋值
                    employmentTargetRecord.setId(UUIDUtil.getUUid());
                    employmentTargetRecord.setTargetId(employmentTarget.getId());//就业指标id（t_employment_target表id）
                    employmentTargetRecord.setTargetYear(employmentTarget.getTargetYear());//指标年份
                    employmentTargetRecord.setStreetKey(employmentTarget.getStreetKey());//街道key
                    employmentTargetRecord.setStreetName(employmentTarget.getStreetName());//街道名称
                    employmentTargetRecord.setCreatTime(new Date());
                    employmentTargetRecord.setCreateUser(userId);
                    //插入就业指标记录
                    employmentTargetRecordMapper.insertSelective(employmentTargetRecord);
                }
            }

            return BaseResultModel.success(count);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("EmploymentTargetServiceAdd", e);
            return BaseResultModel.badParam("异常");
        }finally {
            //解锁
            rlock.unlock();
        }
    }

    @Override
    public PageInfo<EmploymentTargetType> selectTypeList(EmploymentTargerTypeParamModel record) {
        EmploymentTargetType employmentTargetType = new EmploymentTargetType();
        BeanUtils.copyProperties(record, employmentTargetType);
        PageHelper.startPage(record.getPageNum(), record.getPageSize());

        List<EmploymentTargetType> list = employmentTargetTypeMapper.selectList(employmentTargetType);
        return new PageInfo<>(list);
    }

    @Override
    public EmploymentTargetVO getDetail(String targetId) {

        System.out.println(targetId);
        if(StringUtils.isBlank(targetId)){
            return null;
        }
        EmploymentTarget employmentTarget = employmentTargetMapper.selectByPrimaryKey(targetId);
        if(employmentTarget == null){
            System.out.println("targetId不存在");
            return null;
        }

        EmploymentTargetVO employmentTargetVO = new EmploymentTargetVO();
        BeanUtils.copyProperties(employmentTarget, employmentTargetVO);

        //就业指标-记录（1至12月）
        List<EmploymentTargetRecord> employmentTargetRecordList = employmentTargetRecordMapper.selectListByTargetId(targetId);
        employmentTargetVO.setEmploymentTargetRecordList(employmentTargetRecordList);


        //查询就业指标记录附件
        if (employmentTargetRecordList != null && employmentTargetRecordList.size() >= 1) {
            for (int i = 0; i < employmentTargetRecordList.size(); i++) {
                List<EmploymentTargetRecordFile> fileList = employmentTargetRecordFileMapper.selectList(employmentTargetRecordList.get(i).getId());
                employmentTargetRecordList.get(i).setEmploymentTargetRecordFileList(fileList);
            }
        }
        return employmentTargetVO;
    }


    @Override
    public BaseResultModel update(EmploymentTargetUpdateParamModel employmentTargetUpdateParamModel
            , String userId) {

        List<EmploymentTargetRecordUpdateParamModel> employmentTargetRecordUpdateParamModelList = null;
        EmploymentTargetType employmentTargetType = null;//指标类型

        //校验非空-就业指标
        if(employmentTargetUpdateParamModel == null){
            return BaseResultModel.badParam("参数不能为空");
        }
        if(StringUtils.isBlank(employmentTargetUpdateParamModel.getId())){
            return BaseResultModel.badParam("就业指标id不能为空");
        }
        //查询就业指标
        if (employmentTargetMapper.selectByPrimaryKey(employmentTargetUpdateParamModel.getId()) == null) {
            return BaseResultModel.badParam("就业指标id不存在");
        }

        //取出就业指标记录
        employmentTargetRecordUpdateParamModelList = employmentTargetUpdateParamModel.getEmploymentTargetRecordList();

        //校验非空-指标类型id
        if(StringUtils.isBlank(employmentTargetUpdateParamModel.getTypeId())){
            return BaseResultModel.badParam("指标类型id不能为空");
        }
        //根据指标类型id查询指标类型信息
        employmentTargetType = employmentTargetTypeMapper.selectByPrimaryKey(employmentTargetUpdateParamModel.getTypeId());
        //校验非空-指标类型
        if(employmentTargetType == null){
            return BaseResultModel.badParam("指标类型id不存在");
        }

        //校验非空-指标形式状态（1：百分比；2：固定数值）
        if(employmentTargetUpdateParamModel.getFormStatus() == null){
            return BaseResultModel.badParam("指标形式状态不能为空");
        }
        if(employmentTargetUpdateParamModel.getFormStatus() != 1 && employmentTargetUpdateParamModel.getFormStatus() != 2){
            return BaseResultModel.badParam("指标形式状态不存在");
        }
        //校验非空-运算符号状态（1：大于等于；2：小于等于；3：等于）
        if(employmentTargetUpdateParamModel.getSymbolStatus() == null){
            return BaseResultModel.badParam("运算符号状态不能为空");
        }
        if(employmentTargetUpdateParamModel.getSymbolStatus() != 1
                && employmentTargetUpdateParamModel.getSymbolStatus() != 2
                && employmentTargetUpdateParamModel.getSymbolStatus() != 3){
            return BaseResultModel.badParam("运算符号状态不存在");
        }
        //校验非空-复数状态（1：设定值为一个数值；2：设定值为两个数值）
        if(employmentTargetUpdateParamModel.getComplexStatus() == null){
            return BaseResultModel.badParam("复数状态不能为空");
        }
        if(employmentTargetUpdateParamModel.getComplexStatus() != 1 && employmentTargetUpdateParamModel.getComplexStatus() != 2){
            return BaseResultModel.badParam("复数状态不存在");
        }

        //校验-遍历就业指标记录列表
        if(employmentTargetRecordUpdateParamModelList == null || employmentTargetRecordUpdateParamModelList.size() == 0){
            return BaseResultModel.badParam("就业指标记录列表不能为空");
        }
        if(employmentTargetRecordUpdateParamModelList.size() != 12){
            return BaseResultModel.badParam("就业指标记录列表必须为12个月");
        }
        //校验-记录
        for (int i = 0; i < employmentTargetRecordUpdateParamModelList.size(); i++) {

            //校验记录id
            if(StringUtils.isBlank(employmentTargetRecordUpdateParamModelList.get(i).getId())){
                return BaseResultModel.badParam("就业指标记录id不能为空");
            }
            EmploymentTargetRecord employmentTargetRecord = employmentTargetRecordMapper.selectByPrimaryKey(employmentTargetRecordUpdateParamModelList.get(i).getId());
            if(employmentTargetRecord == null){
                return BaseResultModel.badParam("就业指标记录id不存在");
            }
            //校验月份
            if(employmentTargetRecord.getTargetMonth() != (i+1)){
                return BaseResultModel.badParam("就业指标记录指标月份请按月份顺序1-12");
            }
            //校验设定值
            if(employmentTargetUpdateParamModel.getComplexStatus() == 1){
                if(StringUtils.isBlank(employmentTargetRecordUpdateParamModelList.get(i).getTarget())){
                    return BaseResultModel.badParam("设定值不能为空");
                }
            }else if(employmentTargetUpdateParamModel.getComplexStatus() == 2){
                if(StringUtils.isBlank(employmentTargetRecordUpdateParamModelList.get(i).getTargetOne())
                        || StringUtils.isBlank(employmentTargetRecordUpdateParamModelList.get(i).getTargetTwo())){
                    return BaseResultModel.badParam("复数状态-设定值1/2不能为空");
                }
                //复数状态=2时，赋值设定值为设定值1+设定值2
                employmentTargetRecordUpdateParamModelList.get(i).setTarget(employmentTargetRecordUpdateParamModelList.get(i).getTargetOne()+"+"+ employmentTargetRecordUpdateParamModelList.get(i).getTargetTwo());
            }
        }


        //更新
        EmploymentTarget employmentTarget = new EmploymentTarget();
        BeanUtils.copyProperties(employmentTargetUpdateParamModel, employmentTarget);
        //冗余字段赋值
        employmentTarget.setTypeName(employmentTargetType.getTypeName());//指标类型名称（冗余）
        employmentTarget.setTargetYearTarget(employmentTargetRecordUpdateParamModelList.get(11).getTarget());//年份指标设定值（值为第12月份的设定值）
        employmentTarget.setUpdateTime(new Date());
        employmentTarget.setUpdateUser(userId);
        int count = employmentTargetMapper.updateByPrimaryKeySelective(employmentTarget);

        //更新就业指标记录列表
        if(count >= 1){
            for (int i = 0; i < employmentTargetRecordUpdateParamModelList.size(); i++) {
                EmploymentTargetRecord employmentTargetRecord = new EmploymentTargetRecord();
                BeanUtils.copyProperties(employmentTargetRecordUpdateParamModelList.get(i), employmentTargetRecord);
                //冗余字段赋值
                employmentTargetRecord.setTargetId(employmentTarget.getId());//就业指标id（t_employment_target表id）
                employmentTargetRecord.setStreetName(employmentTarget.getStreetName());//街道名称
                employmentTargetRecord.setUpdateTime(new Date());
                employmentTargetRecord.setUpdateUser(userId);
                //更新就业指标记录
                employmentTargetRecordMapper.updateByPrimaryKeySelective(employmentTargetRecord);
            }
        }

        return BaseResultModel.success(count);
    }

    @Override
    public PageInfo<EmploymentTargetRecordVO> getReportList(EmploymentTargetReportParamModel params) {
        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<EmploymentTargetRecordVO> list = employmentTargetRecordMapper.getReportList(params.getTargetYear(), params.getTargetMonth(), params.getStreetKey());

        //查询就业指标记录附件
        if (list != null && list.size() >= 1) {
            for (int i = 0; i < list.size(); i++) {
                List<EmploymentTargetRecordFile> fileList = employmentTargetRecordFileMapper.selectList(list.get(i).getId());
                list.get(i).setEmploymentTargetRecordFileList(fileList);
            }
        }

        return new PageInfo<>(list);
    }
    
    @Override
	public void exportReportList(EmploymentTargetReportParamModel params, HttpServletRequest request, HttpServletResponse response) {
		//查询列表
    	List<EmploymentTargetRecordVO> list = employmentTargetRecordMapper.getReportList(params.getTargetYear(), params.getTargetMonth(), params.getStreetKey());
        if (list == null || list.size() == 0) {
            return;
        }

        //查询街道信息
        Dictionary dictionary = dictionaryMapper.selectOneByDickey(params.getStreetKey());
        if(dictionary == null){
            log.error("exportReportList-街道key不存在"+params.getStreetKey());
            return;
        }
        String streetName = dictionary.getValue();//街道名称

        //导出列表
        List<Map<String, Object>> dataMapList = new ArrayList<Map<String, Object>>();
        Map<String, Object> headMap = new LinkedHashMap<String, Object>();

        headMap.put("rowNum", "序号");
        headMap.put("typeName", "指标类别");
        headMap.put("target", "目标值");
        headMap.put("actual", "完成值");
        headMap.put("StreetName", "街道名称");

        String symbol = null;//运算符号状态（1：大于等于；2：小于等于；3：等于）
        String form = null;//指标形式状态（1：百分比；2：固定数值）
        String complex = null;//复数状态（1：设定值为一个数值；2：设定值为两个数值）
        // excel导出内容
        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> data = new HashMap<String, Object>();

            data.put("rowNum", i+1);//序号
            data.put("typeName", list.get(i).getTypeName());//指标类别

            //symbolStatus 运算符号状态（1：大于等于；2：小于等于；3：等于）
            if(list.get(i).getSymbolStatus() != null && list.get(i).getSymbolStatus() == 1){
                symbol = ">=";
            }else if(list.get(i).getSymbolStatus() != null && list.get(i).getSymbolStatus() == 2){
                symbol = "<=";
            }else {
                symbol = "";
            }

            //form_status 指标形式状态（1：百分比；2：固定数值）
            if(list.get(i).getFormState() != null && list.get(i).getFormState() == 1) {
                form = "%";
            }else {
                form = "";
            }

            //complex_status 复数状态（1：设定值为一个数值；2：设定值为两个数值）
            if(list.get(i).getComplexStatus() != null && list.get(i).getComplexStatus() == 1) {

            }else {

            }
            data.put("target", symbol+list.get(i).getTarget()+form);//目标值
            data.put("actual", list.get(i).getActual()+form);//完成值
            data.put("StreetName", streetName);//街道名称


            dataMapList.add(data);
        }

        ExportExcelUtil export = new ExportExcelUtil();
        String fileName = "就业指标完成情况";//文件名
        String sheetName = "就业指标完成情况";//页签名
        export.toExcelForStream(request, response, headMap, dataMapList,  fileName, sheetName);
		
	}

    @Override
    public BaseResultModel reportUpdate(EmploymentTargetReportAddParamModel params, String userId) {
        if (params == null) {
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getId())) {
            return BaseResultModel.badParam("就业指标记录id不能为空");
        }
        EmploymentTargetRecord employmentTargetRecord = employmentTargetRecordMapper.selectByPrimaryKey(params.getId());
        if (employmentTargetRecord == null) {
            return BaseResultModel.badParam("就业指标记录id不存在");
        }

        EmploymentTarget employmentTarget = employmentTargetMapper.selectByPrimaryKey(employmentTargetRecord.getTargetId());
        if (employmentTarget == null) {
            return BaseResultModel.badParam("就业指标数据缺失");
        }

        String different = null;//完成情况值（完成值-设定值）
        String differentOne = null;//完成情况值1
        String differentTwo = null;//完成情况值2

        //校验设定值
        if(employmentTarget.getComplexStatus() == 1){//复数状态（1：设定值为一个数值；2：设定值为两个数值）
            if(StringUtils.isBlank(params.getActual())){
                return BaseResultModel.badParam("完成值不能为空");
            }
            params.setActualOne("");
            params.setActualTwo("");

            //计算完成情况值（完成值-设定值）
            different = OtherUtil.subStringnum(employmentTargetRecord.getTarget(), params.getActual());
        }else if(employmentTarget.getComplexStatus() == 2){//复数状态（1：设定值为一个数值；2：设定值为两个数值）
            if(StringUtils.isBlank(params.getActualOne())
                    || StringUtils.isBlank(params.getActualTwo())){
                return BaseResultModel.badParam("复数状态-完成值1/2不能为空");
            }
            //复数状态=2时，赋值设定值为设定值1+设定值2
            params.setActual(params.getActualOne()+"+"+params.getActualTwo());
            //计算完成情况值（完成值-设定值）
            differentOne = OtherUtil.subStringnum(employmentTargetRecord.getTargetOne(), params.getActualOne());
            differentTwo = OtherUtil.subStringnum(employmentTargetRecord.getTargetTwo(), params.getActualTwo());
            different = (differentOne+"+"+differentTwo);
        }

        //更新完成值
        BeanUtils.copyProperties(params, employmentTargetRecord);
        employmentTargetRecord.setUpdateTime(new Date());
        employmentTargetRecord.setUpdateUser(userId);
        employmentTargetRecord.setReportUser(userId);
        employmentTargetRecord.setDifferent(different);
        employmentTargetRecord.setDifferentOne(differentOne);
        employmentTargetRecord.setDifferentTwo(differentTwo);
        int count = employmentTargetRecordMapper.updateByPrimaryKeySelective(employmentTargetRecord);

        //更新附件列表
        List<EmploymentTargetRecordFileUpdateModel> list = params.getEmploymentTargetRecordFileList();
        EmploymentTargetRecordFile employmentTargetRecordFile = null;
        File file = null;
        if (list != null && list.size() >= 1) {
            //软删除该记录所有附件
            employmentTargetRecordFileMapper.updateDeleteByEmploymentTargetRecordId(employmentTargetRecord.getId());

            for (int i = 0; i < list.size(); i++) {
                //查询附件id，若存在则恢复
                employmentTargetRecordFile = employmentTargetRecordFileMapper.selectByPrimaryKey(list.get(i).getFileId());
                if(employmentTargetRecordFile != null){
                    employmentTargetRecordFile.setIsdelete(0);
                    employmentTargetRecordFileMapper.updateByPrimaryKeySelective(employmentTargetRecordFile);
                }else{
                    //不存在则新增
                    employmentTargetRecordFile = new EmploymentTargetRecordFile();
                    employmentTargetRecordFile.setId(UUIDUtil.getUUid());
                    employmentTargetRecordFile.setEmploymentTargetId(params.getId());
                    employmentTargetRecordFile.setEmploymentTargetRecordId(employmentTargetRecord.getId());
                    employmentTargetRecordFile.setStreetKey(employmentTarget.getStreetKey());
                    employmentTargetRecordFile.setStreetName(employmentTarget.getStreetName());
                    employmentTargetRecordFile.setCreatTime(new Date());
                    employmentTargetRecordFile.setCreateUser(userId);


                    //查询file
                    file = fileMapper.selectByPrimaryKey(list.get(i).getFileId());
                    if(file != null){
                        employmentTargetRecordFile.setFileId(file.getFileId());
                        employmentTargetRecordFile.setFileUrl(file.getFileUrl());
                        employmentTargetRecordFileMapper.insertSelective(employmentTargetRecordFile);
                    }else{
                        log.error("指标附件id"+list.get(i).getFileId()+"不存在");
                    }
                }
            }

        }

        return BaseResultModel.success(count);
    }


	

}
