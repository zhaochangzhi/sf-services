package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

@Data
public class ArticleWebSectionLink {
    private String id;

    private String webSectionLinkId;

    private Integer sort;

    private String remark;

    private String datafrom;

    private Date createTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;

}