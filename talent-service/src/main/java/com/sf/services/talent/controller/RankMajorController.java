package com.sf.services.talent.controller;

import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.RankMajorParamModel;
import com.sf.services.talent.service.IRankMajorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * RankMajorController
 *
 * @author zhaochangzhi
 * @date 2021/7/20
 */
@Api(value = "职称行业专业", tags = "职称行业专业")
@RestController
@RequestMapping("/rankmajor")
public class RankMajorController {

    private final IRankMajorService rankMajorService;

    @Autowired
    public RankMajorController(IRankMajorService rankMajorService) {
        this.rankMajorService = rankMajorService;
    }

    @ApiOperation(value = "行业专业列表-不传参数查行业", notes = "行业专业列表")
    @PostMapping("/list")
    public BaseResultModel list(@RequestBody RankMajorParamModel params) {
        if (StringUtils.isBlank(params.getParentId())) {
            //没传parentId，查行业
            params.setLevel(1);
        }
        return BaseResultModel.success(rankMajorService.getList(params));
    }

}
