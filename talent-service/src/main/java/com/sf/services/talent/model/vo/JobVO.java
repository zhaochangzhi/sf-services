package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * JobVO
 *
 * @author zhaochangzhi
 * @date 2021/7/7
 */
@ApiModel(value = "JobVO", description = "工作实体")
@Data
public class JobVO {

    @ApiModelProperty(value = "主键id", example = "1")
    private Integer id;

    @ApiModelProperty(value = "父级id", example = "0")
    private Integer uid;
}
