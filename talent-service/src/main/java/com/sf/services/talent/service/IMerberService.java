package com.sf.services.talent.service;

import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.ForgetPasswordSendEmailVerificationCode;
import com.sf.services.talent.model.RetrievePasswordParamModel;
import com.sf.services.talent.model.vo.MemberVO;
import com.sf.services.talent.util.ResultUtil;

/**
 * 会员接口
 * @author hy
 *
 */
public interface IMerberService {
	
	/**
	 * 登陆接口
	 * @param userName 用户名
	 * @param password 密码
	 * @return token
	 */
	public String login(String userName, String password, Integer platform);
	
	/**
	 * 注册
	 * @param MemberVO 会员信息
	 * @return
	 */
	public String register(MemberVO memberVO);

	/**
	 * 获取已登陆账号信息
	 * @return
	 */
	public ResultUtil getLoginUser();
	
	public int modifyPassword(String password,String memberId);
	
	/**
	 * @Title: existUserName 
	 * @Description: 用户名是否存在
	 * @param userName
	 * @return
	 * @return: Boolean
	 */
	public Boolean existUserName(String userName);

	/**
	 * 找回密码
	 * @param params
	 * @return
	 */
	public BaseResultModel sendEmailVerificationCode(ForgetPasswordSendEmailVerificationCode params);


	/**
	 * 忘记密码-重置密码
	 * @param params
	 * @return
	 */
	public BaseResultModel resetPassword(RetrievePasswordParamModel params);

}
