package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.config.ThreadLocalCache;
import com.sf.services.talent.mapper.ArchiveMapper;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.po.Archive;
import com.sf.services.talent.model.vo.ArchiveVO;
import com.sf.services.talent.service.IActLogService;
import com.sf.services.talent.service.IArchiveService;
import com.sf.services.talent.util.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Service
public class ArchiveService implements IArchiveService {

	@Autowired
	private ArchiveMapper archiveMapper;
	
	@Autowired
	private IActLogService actLogService;

	@Override
	public int insert(ArchiveVO record) {
		Archive active = new Archive();
		BeanUtils.copyProperties(record, active);
		active.setId(UUIDUtil.getUUid());
		FillUserInfoUtil.fillCreateUserInfo(active);
		actLogService.save(ThreadLocalCache.getUser(), "添加档案", record.getOwnerName());
		return archiveMapper.insert(active);
	}

	@Override
	public int update(ArchiveVO record) {
		Archive actives =archiveMapper.selectByPrimaryKey(record.getId());
		Archive active = new Archive();
		BeanUtils.copyProperties(record, active);
		FillUserInfoUtil.fillUpdateUserInfo(active);
		if(actives.getArchiveNumber().equals(record.getArchiveNumber())) {
			active.setArchiveNumber(null);
		}
		return archiveMapper.updateByPrimaryKeySelective(active);
	}

	@Override
	public int delete(String id) {
		
		return archiveMapper.deleteByPrimaryKey(id);
	}

	@Override
	public Archive detail(String id) {
		actLogService.save(ThreadLocalCache.getUser(), "查看档案", "");
		return archiveMapper.selectByPrimaryKey(id);
	}

	@Override
	public PageInfo<Archive> list(ArchiveVO archiveVO) {
		Archive active = new Archive();
		BeanUtils.copyProperties(archiveVO, active);
        //分页处理
        PageHelper.startPage(archiveVO.getPageNum(), archiveVO.getPageSize());
        List<Archive> list = archiveMapper.selectAll(active);
        PageInfo<Archive> pageInfo = new PageInfo<Archive>(list);
        return pageInfo;
	}

	@Override
	public Archive selectDetailByParam(ArchiveVO archiveVO) {
		Archive active = new Archive();
		BeanUtils.copyProperties(archiveVO, active);
		return archiveMapper.selectDetailByParam(active);
	}

	@Override
	public Archive selectByOwnerId(String ownerId) {
		return archiveMapper.selectByOwnerId(ownerId);
	}

	@Override
	public BaseResultModel importExcel(MultipartFile file, String createUser) {
		try {
			//判断文件是否为空
			if (file == null) {
				return BaseResultModel.badParam("文件不能为空");
			}

			//验证文件名
			if (StringUtils.isBlank(file.getOriginalFilename()) && file.getSize() == 0 && !WDWUtil.validateExcel(file.getOriginalFilename())) {
				return BaseResultModel.badParam("请选择上传文件");
			}

			//解析excel，获取信息集合。
			ReadExcel readExcel = new ReadExcel();
			List<Map<String, Object>> list = readExcel.getExcelInfo(file, 0, null);

			//校验数据
			if(list == null || list.size() == 0){
				return BaseResultModel.badParam("表头不正确");
			}
			if(list.size() ==1){
				return BaseResultModel.badParam("上传数据不能为空");
			}

			//插入数据
			int count = 0;
			ArchiveVO record = new ArchiveVO();
			for (int i = 1; i < list.size(); i++) {
				record.setOwnerName(list.get(i).get("key0").toString());//姓名
				record.setOwnerId(list.get(i).get("key1").toString());//身份证号
				record.setStorePlace(list.get(i).get("key2").toString());//存放地址
				try {
					count = count + this.insert(record);
				}catch(Exception e){
					e.printStackTrace();
				}
			}

			return BaseResultModel.success(count);
		} catch (Exception e) {
			e.printStackTrace();
			return BaseResultModel.badParam("Excel导入失败！");
		}
	}


	@Override
	public void exportList(HttpServletRequest request, HttpServletResponse response) {
		//查询列表
		Archive active = new Archive();
		List<Archive> list = archiveMapper.selectAll(active);
		if (list == null || list.size() == 0) {
			return;
		}

		//导出列表
		List<Map<String, Object>> dataMapList = new ArrayList<Map<String, Object>>();
		Map<String, Object> headMap = new LinkedHashMap<String, Object>();

		headMap.put("rowNum", "序号");
//		headMap.put("archiveNumber", "档案号");
		headMap.put("ownerName", "姓名");
		headMap.put("ownerId", "身份证号");
		headMap.put("storePlace", "存放地址");

		// excel导出内容
		for (int i = 0; i < list.size(); i++) {
			Map<String, Object> data = new HashMap<String, Object>();

			data.put("rowNum", i+1);//序号
//			data.put("archiveNumber", list.get(i).getArchiveNumber());//档案号
			data.put("ownerName", list.get(i).getOwnerName());//姓名
			data.put("ownerId", list.get(i).getOwnerId());//身份证号
			data.put("storePlace", list.get(i).getStorePlace());//存放地址

			dataMapList.add(data);
		}

		ExportExcelUtil export = new ExportExcelUtil();
		String fileName = "档案列表";//文件名
		String sheetName = "档案列表";//页签名
		export.toExcelForStream(request, response, headMap, dataMapList,  fileName, sheetName);

	}

	@Override
	public int bashDel(String[] ids) {
		
		return archiveMapper.bashDel(ids);
	}
}
