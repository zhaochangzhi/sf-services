package com.sf.services.talent.service;

import com.sf.services.talent.model.ArchiveFileParamModel;
import com.sf.services.talent.model.ArchiveFileUploadeParamModel;
import com.sf.services.talent.model.BaseResultModel;

/**
 *
 * @author guchangliang
 * @date 2021/8/9
 */
public interface IArchiveFileService {

    /**
     * 列表
     * @param param
     * @return
     */
    BaseResultModel list(ArchiveFileParamModel params);

    /**
     * 软删除
     * @param params
     * @return
     */
    BaseResultModel delete(String id, String updateUser);

    /**
     * 上传附件
     * @param params
     * @return
     */
    BaseResultModel uploadFile(ArchiveFileUploadeParamModel params, String createUser);

}
