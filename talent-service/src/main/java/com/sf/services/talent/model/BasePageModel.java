package com.sf.services.talent.model;

import com.github.pagehelper.PageInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * 分页结果返回值信息
 *
 * @author zhaochangzhi
 * @date 2021/7/10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasePageModel extends BaseResultModel {

    /**
     * 页码
     */
    private int pageNum;

    /**
     * 每页数据量
     */

    private int pageSize;

    /**
     * 数据总量
     */
    private long total;

    public static BasePageModel page(PageInfo pageInfo) {
        BasePageModel result = new BasePageModel(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal());
        result.setCode(HttpStatus.OK.value());
        result.setMsg(HttpStatus.OK.getReasonPhrase());
        result.setData(pageInfo.getList());
        return result;
    }
}
