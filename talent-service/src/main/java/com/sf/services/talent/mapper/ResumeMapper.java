package com.sf.services.talent.mapper;

import java.util.List;
import java.util.Map;

import com.sf.services.talent.model.ResumeListParam;
import com.sf.services.talent.model.po.Resume;
import com.sf.services.talent.model.po.ResumeEducation;
import org.apache.ibatis.annotations.Param;

public interface ResumeMapper {
    int deleteByPrimaryKey(String id);

    int insert(Resume record);

    int insertSelective(Resume record);

    Resume selectByPrimaryKey(Resume record);

    int updateByPrimaryKeySelective(Resume record);

    int updateByPrimaryKey(Resume record);
    
    List<Map<String, Object>> getList(ResumeListParam record);
    
    Map<String, Object> selectClount(String memberId);
    
    int audit(Map<String, Object> params);
    
    int insertBatchJob(List<Map<String, Object>> resumeJobList);
    
    int insertBatchIndustry(List<Map<String, Object>> resumeIndustryList);
    
    int deleteIndustryByResumeId(String resumeId);
    
    int deleteJobByResumeId(String resumeId);
    
    List<String> getJobList(String resumeId);
    
    List<String> getIndustryList(String resumeId);
    
    List<Map<String, String>> getJobLists(String resumeId);
    
    List<Map<String, String>> getIndustryLists(String resumeId);

    //查询简历与企业所属行业匹配数
    int selectResumeTradePiPeiCount(@Param("resumeId") String resumeId, @Param("memberId") String memberId);
    //查询简历与企业发布职位匹配数
    int selectResumeJobPiPeiCount(@Param("resumeId") String resumeId, @Param("memberId") String memberId);

    //查询简历行业，逗号分隔
    String selectResumeIndustryString(@Param("resumeId") String resumeId);
    //查询简历职位，逗号分隔
    String selectResumeJobString(@Param("resumeId") String resumeId);
}