package com.sf.services.talent.exceptions;

import com.sf.services.talent.model.BaseResultModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 全局异常处理
 *
 * @author zhaochangzhi
 * @date 2021/7/7
 */
@Slf4j
@RestControllerAdvice("com.sf.services.talent")
@ConditionalOnWebApplication
public class ExceptionHandlerAdvice {

    @ExceptionHandler(AuthException.class)
    public BaseResultModel handleException(HttpServletResponse response) {
        log.info("没有用户登录信息：");
        response.setStatus(401);
        return BaseResultModel.noAuth();
//        writeJsonResponse(response, "没有登录信息，请先登录");
    }

    @ExceptionHandler(Exception.class)
    public BaseResultModel handleException(Exception e, HttpServletResponse response) {
        log.error("服务器异常，"+ 500, e);
        response.setStatus(500);
        return BaseResultModel.failure("系统异常");
//        writeJsonResponse(response, "系统异常");
    }

    @ExceptionHandler(TalentException.class)
    public BaseResultModel handleException(TalentException e, HttpServletResponse response) {
        log.error("人才网异常，"+ 500, e);
        response.setStatus(e.getCode());
        return BaseResultModel.failure(e.getCode(), e.getMessage());
//        writeJsonResponse(response, "系统异常");
    }

    /**
     * 写回结果
     *
     * @param response 结果集
     * @param message 返回值
     * @throws IOException 异常
     */
    private void writeJsonResponse(HttpServletResponse response, String message) throws IOException {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        response.addHeader("Access-Control-Allow-Headers", "*");
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().print(message);
        response.getWriter().flush();
        response.getWriter().close();
    }
}