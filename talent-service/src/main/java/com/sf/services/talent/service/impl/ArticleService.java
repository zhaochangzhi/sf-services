package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.mapper.*;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.po.*;
import com.sf.services.talent.model.vo.ArticleFileVO;
import com.sf.services.talent.model.vo.ArticleFloatBannerVO;
import com.sf.services.talent.model.vo.ArticleVO;
import com.sf.services.talent.model.vo.ArticleWebSectionLinkVO;
import com.sf.services.talent.service.IActLogService;
import com.sf.services.talent.service.IArticleService;
import com.sf.services.talent.util.UUIDUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class ArticleService implements IArticleService {

	@Resource
	private ArticleMapper articleMapper;
	@Resource
	private ArticleFileMapper articleFileMapper;
	@Resource
	private DictionaryMapper dictionaryMapper;
	@Resource
	private FileMapper fileMapper;
	@Resource
	private ArticleFloatBannerMapper articleFloatBannerMapper;
	@Resource
	private ArticleWebSectionLinkMapper articleWebSectionLinkMapper;
	@Resource
	private WebSectionMapper webSectionMapper;

	@Value("${domain.url}")
	String domainUrl;//域名

	@Autowired
	private RedissonClient redisson;
	@Resource
	private IActLogService actLogService;//日志


	@Override
	public PageInfo<ArticleVO> list(ArticleParamModel params) {

		//分页处理
		PageHelper.startPage(params.getPageNum(), params.getPageSize());
		List<ArticleVO> list = articleMapper.selectList(params);

		//图片列表
		if(list != null && list.size() >= 1){
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getTypeStatus() == 4) {
					//查询图片列表
					List<ArticleFileVO> articleFileVOList = new ArrayList<>();
					List<ArticleFile> articleFileList = articleFileMapper.selectList(list.get(i).getId());
					if (!articleFileList.isEmpty()) {
						articleFileList.stream().forEach(articleFile -> {
							ArticleFileVO articleFileVO = new ArticleFileVO();
							BeanUtils.copyProperties(articleFile, articleFileVO);
							articleFileVOList.add(articleFileVO);
						});
					}
					//插入
					list.get(i).setArticleFileUrlList(articleFileVOList);
					list.get(i).setArticleFileList(String.join(",", articleFileMapper.selectFileIdList(list.get(i).getId())));
				}
			}
		}

		return new PageInfo<>(list);
	}

	@Override
	public ArticleVO get(String id) {
		Article article = articleMapper.selectById(id);

		if(article == null){
			return null;
		}

		//映射
		ArticleVO articleVO = new ArticleVO();
		BeanUtils.copyProperties(article, articleVO);
		//更新类型标识（1：文章，2：图片，3：文件，4：图片列表）
		if(article.getTypeStatus() != null && article.getTypeStatus() == 1){
			articleVO.setTypeStatusName("文章");
		}else if(article.getTypeStatus() != null && article.getTypeStatus() == 2){
			articleVO.setTypeStatusName("图片");
		}else if(article.getTypeStatus() != null && article.getTypeStatus() == 3){
			articleVO.setTypeStatusName("文件");
		}else if(article.getTypeStatus() != null && article.getTypeStatus() == 4){
			articleVO.setTypeStatusName("图片列表");
		}
		//查询文件名
		File file = fileMapper.selectByPrimaryKey(article.getFileId());
		if(file != null){
			articleVO.setFileUrl(file.getFileUrl());
			articleVO.setOriginalFileName(file.getOriginalFileName());
		}

		//附件列表
//		if (article.getTypeStatus() == 4) {
			//查询图片列表
			List<ArticleFileVO> articleFileVOList = new ArrayList<>();
			List<ArticleFile> articleFileList = articleFileMapper.selectList(article.getId());
			if (!articleFileList.isEmpty()) {
				articleFileList.stream().forEach(articleFile -> {
					ArticleFileVO articleFileVO = new ArticleFileVO();
					BeanUtils.copyProperties(articleFile, articleFileVO);
					articleFileVOList.add(articleFileVO);
				});
			}

			if(articleFileVOList != null && articleFileVOList.size() >= 1){
				for (int i = 0; i < articleFileVOList.size(); i++) {
					file = fileMapper.selectByPrimaryKey(articleFileVOList.get(i).getFileId());
					if(file != null){
						articleFileVOList.get(i).setFileUrl(file.getFileUrl());
						articleFileVOList.get(i).setOriginalFileName(file.getOriginalFileName());
					}
				}
			}

			//插入
			articleVO.setArticleFileUrlList(articleFileVOList);
			articleVO.setArticleFileList(String.join(",", articleFileMapper.selectFileIdList(article.getId())));
//		}

		//增加浏览量
		articleMapper.addNum(article.getId(),1);

		//查询是否为漂浮banner
		ArticleFloatBanner articleFloatBanner = articleFloatBannerMapper.select(1, article.getId());
		if(articleFloatBanner != null){
			articleVO.setIsArticleFloatBanner(1);
			articleVO.setArticleFloatBannerPhotoId(articleFloatBanner.getPhotoId());
			articleVO.setArticleFloatBannerPhotoUrl(articleFloatBanner.getPhotoUrl());
		}else{
			articleVO.setIsArticleFloatBanner(0);
		}

		return articleVO;
	}

	@Override
	public BaseResultModel add(ArticleAddParamModel params, String createUser) {

		if(params == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if (StringUtils.isBlank(createUser)) {
			return BaseResultModel.badParam("创建人不能为空");
		}

		//redisson分布式锁
		String lockKey = "ArticleServiceAdd"+createUser;
		RLock rlock = redisson.getLock(lockKey);
		try {
			//校验是否并发
			if(rlock.isLocked()){
				return BaseResultModel.badParam("请慢些操作");
			}

			//上锁
			rlock.lock();

			//校验website_key	所属网站key（t_dictionary表type=Website数据key）
			Dictionary website = dictionaryMapper.selectOneByTypeAndDickey("Website",params.getWebsiteKey());
			if (website == null) {
				return BaseResultModel.badParam("所属网站key不存在");
			}
			//校验web_section_key	所属栏目key（t_web_section表key）
			String webSectionName = articleMapper.selectWebSectionName(params.getWebSectionId());


			//校验small_photo_id
			String SmallPhotoUrl = null;//小图URL
			if(!StringUtils.isBlank(params.getSmallPhotoId())){
				File file = fileMapper.selectByPrimaryKey(params.getSmallPhotoId());
				if (file == null) {
					return BaseResultModel.badParam("小图id不存在");
				}
				SmallPhotoUrl = file.getFileUrl();//小图URL
			}

			//校验big_photo_id
			String BigPhotoUrl = null;//大图URL
			if(!StringUtils.isBlank(params.getBigPhotoId())){
				File file = fileMapper.selectByPrimaryKey(params.getBigPhotoId());
				if (file == null) {
					return BaseResultModel.badParam("大图id不存在");
				}
				BigPhotoUrl = file.getFileUrl();//大图URL
			}

			//校验file_id
			String fileUrl = null;//附件URL
			if(!StringUtils.isBlank(params.getFileId())){
				File file = fileMapper.selectByPrimaryKey(params.getFileId());
				if (file == null) {
					return BaseResultModel.badParam("校验id不存在");
				}
				fileUrl = file.getFileUrl();//附件URL
			}


			//映射
			Article article = new Article();
			BeanUtils.copyProperties(params, article);

			//冗余字段赋值
			article.setWebsiteValue(website.getValue());//所属网站（t_dictionary表type=Website数据value）
			article.setWebSectionName(webSectionName);//所属栏目key（t_web_section表name）
			article.setSmallPhotoUrl(SmallPhotoUrl);//小图url
			article.setBigPhotoUrl(BigPhotoUrl);//大图url
			article.setFileUrl(fileUrl);//附件url
			article.setBrowseNum(0);//浏览量
			article.setIsOn(0);//是否已发布（0：否，1：是）

			article.setId(UUIDUtil.getUUid());//id
			article.setCreatTime(new Date());//创建时间
			if(article.getUpdateTime() == null){
				article.setUpdateTime(article.getCreatTime());//更新时间
			}
			article.setCreateUser(createUser);//创建人
			int count = articleMapper.insertSelective(article);

			//图片列表
			ArticleFile articleFile = new ArticleFile();
			articleFile.setCreatTime(article.getCreatTime());
			articleFile.setCreateUser(article.getCreateUser());

			if(!StringUtils.isBlank(params.getArticleFileList())){
				List<String> articleFileList = Arrays.asList(params.getArticleFileList().split(","));
				for (int i = 0; i < articleFileList.size(); i++) {
					File file = fileMapper.selectByPrimaryKey(articleFileList.get(i));
					if (file != null) {
						articleFile.setId(UUIDUtil.getUUid());
						articleFile.setArticleId(article.getId());
						articleFile.setFileId(file.getFileId());
						articleFile.setFileUrl(file.getFileUrl());
						articleFileMapper.insertSelective(articleFile);
					}
				}
			}

			//更新悬浮banner信息
			if(params.getIsArticleFloatBanner() != null && params.getIsArticleFloatBanner() == 1){
				ArticleFloatBanner articleFloatBanner = articleFloatBannerMapper.selectLast();
				if(articleFloatBanner != null){
					articleFloatBanner.setModuleId(article.getId());
					articleFloatBanner.setModuleTypeStatus(1);
					articleFloatBanner.setModuleTypeName("文章");
					if(params.getArticleFloatBannerPhotoId() != null){
						File file = fileMapper.selectByPrimaryKey(params.getArticleFloatBannerPhotoId());
						articleFloatBanner.setPhotoId(params.getArticleFloatBannerPhotoId());
						if(file != null){
							articleFloatBanner.setPhotoUrl(file.getFileUrl());
						}
					}else {
						articleFloatBanner.setPhotoId(article.getBigPhotoId());
						articleFloatBanner.setPhotoUrl(article.getBigPhotoUrl());
					}
					articleFloatBanner.setUpdateTime(article.getCreatTime());
					articleFloatBanner.setUpdateUser(article.getCreateUser());
					articleFloatBanner.setRemark(params.getTitle());
//					articleFloatBanner.setBannerLink(domainUrl+"/#/talent-service/content-detail?id="+article.getId());
					articleFloatBanner.setBannerLink(params.getLink());
					//更新
					articleFloatBannerMapper.updateByPrimaryKeySelective(articleFloatBanner);
				}
			}

			//保存操作日志
			try {
				actLogService.save(createUser, "创建文章", article.getTitle());
			} catch (Exception e) {
				log.error("插入日志异常:{}",e);
			}

			return BaseResultModel.success(count);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("ArticleServiceAdd", e);
			return BaseResultModel.badParam("异常");
		}finally {
			//解锁
			rlock.unlock();
		}

	}

	@Override
	public BaseResultModel update(ArticleUpdateParamModel params, String updateUser) {

		if(params == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if (StringUtils.isBlank(params.getId())) {
			return BaseResultModel.badParam("id不能为空");
		}
		if (StringUtils.isBlank(updateUser)) {
			return BaseResultModel.badParam("更新人不能为空");
		}

		//校验website_key	所属网站key（t_dictionary表type=Website数据key）
		Dictionary website = dictionaryMapper.selectOneByTypeAndDickey("Website",params.getWebsiteKey());
		if (website == null) {
			return BaseResultModel.badParam("所属网站key不存在");
		}
		//校验web_section_key	所属栏目key（t_web_section表key）
		String webSectionName = articleMapper.selectWebSectionName(params.getWebSectionId());

		//校验small_photo_id
		String SmallPhotoUrl = "";//小图URL
		if(!StringUtils.isBlank(params.getSmallPhotoId())){
			File file = fileMapper.selectByPrimaryKey(params.getSmallPhotoId());
			if (file == null) {
				return BaseResultModel.badParam("小图id不存在");
			}
			SmallPhotoUrl = file.getFileUrl();//小图URL
		}

		//校验big_photo_id
		String BigPhotoUrl = "";//大图URL
		if(!StringUtils.isBlank(params.getBigPhotoId())){
			File file = fileMapper.selectByPrimaryKey(params.getBigPhotoId());
			if (file == null) {
				return BaseResultModel.badParam("大图id不存在");
			}
			BigPhotoUrl = file.getFileUrl();//大图URL
		}

		//校验file_id
		String fileUrl = "";//附件URL
		if(!StringUtils.isBlank(params.getFileId())){
			File file = fileMapper.selectByPrimaryKey(params.getFileId());
			if (file == null) {
				return BaseResultModel.badParam("大图id不存在");
			}
			fileUrl = file.getFileUrl();//附件URL
		}

		//校验类型标识（1：文章，2：图片，3：文件，4：图片列表）
//		if(params.getTypeStatus() != null && params.getTypeStatus() == 4){
			//软删除所有附件
			articleFileMapper.deleteByArticleId(params.getId());

			//新增
			ArticleFile articleFile = new ArticleFile();
			if(!StringUtils.isBlank(params.getArticleFileList())){
				List<String> articleFileList = Arrays.asList(params.getArticleFileList().split(","));
				for (int i = 0; i < articleFileList.size(); i++) {
					if(!StringUtils.isBlank(articleFileList.get(i))){
						//新增
						File file = fileMapper.selectByPrimaryKey(articleFileList.get(i));
						if (file != null) {
							articleFile.setId(UUIDUtil.getUUid());
							articleFile.setCreatTime(new Date());
							articleFile.setCreateUser(updateUser);
							articleFile.setArticleId(params.getId());
							articleFile.setFileId(file.getFileId());
							articleFile.setFileUrl(file.getFileUrl());
							articleFileMapper.insertSelective(articleFile);
						}
					}
				}
			}
//		}

		//映射
		Article article = new Article();
		BeanUtils.copyProperties(params, article);

		//冗余字段赋值
		article.setWebsiteValue(website.getValue());//所属网站（t_dictionary表type=Website数据value）
		article.setWebSectionName(webSectionName);//所属栏目key（t_web_section表name）
		article.setSmallPhotoUrl(SmallPhotoUrl);//小图url
		article.setBigPhotoUrl(BigPhotoUrl);//大图url
		article.setFileUrl(fileUrl);//附件url

		if(article.getUpdateTime() == null){
			article.setUpdateTime(new Date());//更新时间
		}
		article.setUpdateUser(updateUser);//更新人
		int count = articleMapper.updateByPrimaryKeySelective(article);

		//更新悬浮banner信息
		if(params.getIsArticleFloatBanner() != null && params.getIsArticleFloatBanner() == 1){
			ArticleFloatBanner articleFloatBanner = articleFloatBannerMapper.selectLast();
			if(articleFloatBanner != null){
				articleFloatBanner.setModuleId(article.getId());
				articleFloatBanner.setModuleTypeStatus(1);
				articleFloatBanner.setModuleTypeName("文章");
				//以大图作为banner图片
//				if(params.getArticleFloatBannerPhotoId() != null){
//					File file = fileMapper.selectByPrimaryKey(params.getArticleFloatBannerPhotoId());
//					articleFloatBanner.setPhotoId(params.getArticleFloatBannerPhotoId());
//					if(file != null){
//						articleFloatBanner.setPhotoUrl(file.getFileUrl());
//					}
//				}else {
					articleFloatBanner.setPhotoId(article.getBigPhotoId());
					articleFloatBanner.setPhotoUrl(article.getBigPhotoUrl());
//				}
				articleFloatBanner.setUpdateTime(article.getUpdateTime());
				articleFloatBanner.setUpdateUser(article.getUpdateUser());
				articleFloatBanner.setBannerLink(params.getLink());
				//更新
				articleFloatBannerMapper.updateByPrimaryKeySelective(articleFloatBanner);
			}
		}

		//保存操作日志
		try {
			actLogService.save(updateUser, "更新文章", article.getTitle());
		} catch (Exception e) {
			log.error("插入日志异常:{}",e);
		}

		return BaseResultModel.success(count);
	}

	@Override
	public BaseResultModel updateIsOn(ArticleUpdateIsOnParamModel params, String updateUser) {

		if(params == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if (StringUtils.isBlank(params.getId())) {
			return BaseResultModel.badParam("id不能为空");
		}
		if (params.getIsOn() == null) {
			return BaseResultModel.badParam("isOn不能为空");
		}
		if(params.getIsOn() != 0 && params.getIsOn() != 1){
			return BaseResultModel.badParam("isOn不正确");
		}
		if (StringUtils.isBlank(updateUser)) {
			return BaseResultModel.badParam("更新人不能为空");
		}

		//映射
		Article article = new Article();
		BeanUtils.copyProperties(params, article);

//		article.setUpdateTime(new Date());//更新时间 （需要去掉，因为updateTime现在被占用为发布时间）
		article.setUpdateUser(updateUser);//更新人
		int count = articleMapper.updateByPrimaryKeySelective(article);

		return BaseResultModel.success(count);
	}

	@Override
	public BaseResultModel delete(String id, String updateUser) {

		Article article = articleMapper.selectByPrimaryKey(id);
		if (article == null) {
			return BaseResultModel.badParam("id不存在");
		}
		article.setIsdelete(1);
		article.setUpdateTime(new Date());//更新时间
		article.setUpdateUser(updateUser);//更新人
		int count = articleMapper.updateByPrimaryKeySelective(article);
		return BaseResultModel.success(count);
	}

	@Override
	public BaseResultModel batchDelete(String ids, String updateUser) {

		Article article = new Article();
		int count = 0;
		List<String> idList = Arrays.asList(ids.split(","));
		for (int i = 0; i < idList.size(); i++) {
			article = articleMapper.selectByPrimaryKey(idList.get(i));
			if (article == null) {
				continue;
			}

			article.setIsdelete(1);
			article.setUpdateTime(new Date());//更新时间
			article.setUpdateUser(updateUser);//更新人
			count = count + articleMapper.updateByPrimaryKeySelective(article);
		}

		return BaseResultModel.success(count);
	}

    @Override
    public ArticleFloatBannerVO articleFloatBannerGet() {

		ArticleFloatBanner articleFloatBanner = articleFloatBannerMapper.selectLast();
		ArticleFloatBannerVO articleFloatBannerVO = new ArticleFloatBannerVO();
		BeanUtils.copyProperties(articleFloatBanner, articleFloatBannerVO);

		return articleFloatBannerVO;
    }


	@Override
	public PageInfo<ArticleWebSectionLinkVO> webSectionLinkList(ArticleWebSectionLinkListParamModel params) {

		//分页处理
		PageHelper.startPage(params.getPageNum(), params.getPageSize());
		List<ArticleWebSectionLinkVO> list = articleWebSectionLinkMapper.selectList(params);

		List<String> result = null;
		List<WebSection> webSectionList = null;
		for (int i = 0; i < list.size(); i++) {
			result = Arrays.asList(list.get(i).getWebSectionLinkId().split(","));
			webSectionList = new ArrayList<>();

			if(result != null && result.size() >= 1){
				for (int j = 0; j < result.size(); j++) {
					WebSection webSection = webSectionMapper.selectByPrimaryKey(result.get(j));
					if(webSection != null){
						webSectionList.add(webSection);
					}
				}
			}
			list.get(i).setWebSectionList(webSectionList);
		}

		return new PageInfo<>(list);
	}

	@Override
	public BaseResultModel webSectionLinkGet(GetOneParamModel param) {
		if(param == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if(StringUtils.isBlank(param.getId())){
			return BaseResultModel.badParam("id不能为空");
		}
		//查询详情
		ArticleWebSectionLink articleWebSectionLink = articleWebSectionLinkMapper.selectByPrimaryKey(param.getId());
		if(articleWebSectionLink == null){
			return BaseResultModel.badParam("id不存在");
		}

		List<String> webSectionLinkIdList = Arrays.asList(articleWebSectionLink.getWebSectionLinkId().split(","));
		List<WebSection> webSectionList = new ArrayList<>();

		if(webSectionLinkIdList != null && webSectionLinkIdList.size() >= 1){
			for (int j = 0; j < webSectionLinkIdList.size(); j++) {
				WebSection webSection = webSectionMapper.selectByPrimaryKey(webSectionLinkIdList.get(j));
				if(webSection != null){
					webSectionList.add(webSection);
				}
			}
		}

		ArticleWebSectionLinkVO articleWebSectionLinkVO = new ArticleWebSectionLinkVO();
		BeanUtils.copyProperties(articleWebSectionLink, articleWebSectionLinkVO);

		articleWebSectionLinkVO.setWebSectionList(webSectionList);

		return BaseResultModel.success(articleWebSectionLinkVO);
	}


	@Override
	public BaseResultModel webSectionLinkAdd(ArticleWebSectionLinkAddParamModel params, String createUser) {

		if(params == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if(StringUtils.isBlank(params.getWebSectionLinkId())){
			return BaseResultModel.badParam("关联栏目id不能为空");
		}
		if (StringUtils.isBlank(createUser)) {
			return BaseResultModel.badParam("创建人不能为空");
		}


		//redisson分布式锁
		String lockKey = "webSectionLinkAdd"+createUser;
		RLock rlock = redisson.getLock(lockKey);
		try {
			//校验是否并发
			if(rlock.isLocked()){
				return BaseResultModel.badParam("请慢些操作");
			}

			//上锁
			rlock.lock();

			//校验web_section_link_id	逗号分隔所属栏目id(t_web_section表id)
			List<String> webSectionIdList  = Arrays.asList(params.getWebSectionLinkId().split(","));
			if(webSectionIdList == null || webSectionIdList.size() <= 1){
				return BaseResultModel.badParam("关联栏目id至少为2个");
			}

			//校验webSectionId是否已被维护
			for (int i = 0; i < webSectionIdList.size(); i++) {
				if(articleWebSectionLinkMapper.selectCount(webSectionIdList.get(i), null) >= 1){
					//校验栏目是否已被维护，暂时去掉
//					WebSection webSection = webSectionMapper.selectByPrimaryKey(webSectionIdList.get(i));
//					if(webSection != null){
//						return BaseResultModel.badParam(webSection.getName() + "已被维护");
//					}else {
//						return BaseResultModel.badParam("栏目已被维护");
//					}
				}
			}

			//映射
			ArticleWebSectionLink articleWebSectionLink = new ArticleWebSectionLink();
			BeanUtils.copyProperties(params, articleWebSectionLink);

			//冗余字段赋值
			articleWebSectionLink.setId(UUIDUtil.getUUid());//id
			articleWebSectionLink.setCreateTime(new Date());//创建时间
			articleWebSectionLink.setUpdateTime(articleWebSectionLink.getCreateTime());//更新时间
			articleWebSectionLink.setCreateUser(createUser);//创建人
			int count = articleWebSectionLinkMapper.insertSelective(articleWebSectionLink);

			//更新所有文章关联栏目逗号分隔所属栏目id（t_web_section表id）
//			articleWebSectionLinkMapper.updateArticleWebSectionLinkId(null);

			return BaseResultModel.success(count);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("webSectionLinkAdd", e);
			return BaseResultModel.badParam("异常");
		}finally {
			//解锁
			rlock.unlock();
		}
	}


	@Override
	public BaseResultModel webSectionLinkUpdate(ArticleWebSectionLinkUpdateParamModel params, String updateUser) {

		if(params == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if (StringUtils.isBlank(params.getId())) {
			return BaseResultModel.badParam("id不能为空");
		}
		if(StringUtils.isBlank(params.getWebSectionLinkId())){
			return BaseResultModel.badParam("所属栏目id不能为空");
		}
		if (StringUtils.isBlank(updateUser)) {
			return BaseResultModel.badParam("更新人不能为空");
		}

		//校验id是否存在
		if(articleWebSectionLinkMapper.selectByPrimaryKey(params.getId()) == null){
			return BaseResultModel.badParam("id不存在");
		}

		//校验web_section_link_id	逗号分隔所属栏目id(t_web_section表id)
		List<String> webSectionIdList  = Arrays.asList(params.getWebSectionLinkId().split(","));
		if(webSectionIdList == null || webSectionIdList.size() <= 1){
			return BaseResultModel.badParam("关联栏目id至少为2个");
		}
		//校验webSectionId是否已被维护
		for (int i = 0; i < webSectionIdList.size(); i++) {
			if(articleWebSectionLinkMapper.selectCount(webSectionIdList.get(i), params.getId()) >= 1){
				//校验栏目是否已被维护，暂时去掉
//				WebSection webSection = webSectionMapper.selectByPrimaryKey(webSectionIdList.get(i));
//				if(webSection != null){
//					return BaseResultModel.badParam(webSection.getName() + "已被维护");
//				}else {
//					return BaseResultModel.badParam("栏目已被维护");
//				}
			}
		}

		//映射
		ArticleWebSectionLink articleWebSectionLink = new ArticleWebSectionLink();
		BeanUtils.copyProperties(params, articleWebSectionLink);

		//冗余字段赋值
		articleWebSectionLink.setUpdateTime(new Date());//更新时间
		articleWebSectionLink.setUpdateUser(updateUser);//更新人
		int count = articleWebSectionLinkMapper.updateByPrimaryKeySelective(articleWebSectionLink);

		//更新所有文章关联栏目逗号分隔所属栏目id（t_web_section表id）（编辑栏目关联，历史数据不更新，暂时注释掉）
//		articleWebSectionLinkMapper.updateArticleWebSectionLinkId(null);

		return BaseResultModel.success(count);
	}


	@Override
	public BaseResultModel webSectionLinkDelete(String id, String updateUser) {

		ArticleWebSectionLink articleWebSectionLink = articleWebSectionLinkMapper.selectByPrimaryKey(id);
		if (articleWebSectionLink == null) {
			return BaseResultModel.badParam("id不存在");
		}
		articleWebSectionLink.setIsdelete(1);
		articleWebSectionLink.setUpdateTime(new Date());//更新时间
		articleWebSectionLink.setUpdateUser(updateUser);//更新人
		int count = articleWebSectionLinkMapper.updateByPrimaryKeySelective(articleWebSectionLink);
		return BaseResultModel.success(count);
	}


	@Override
	public BaseResultModel webSectionLinkBatchDelete(String ids, String updateUser) {

		ArticleWebSectionLink articleWebSectionLink = new ArticleWebSectionLink();
		int count = 0;
		List<String> idList = Arrays.asList(ids.split(","));
		for (int i = 0; i < idList.size(); i++) {
			articleWebSectionLink = articleWebSectionLinkMapper.selectByPrimaryKey(idList.get(i));
			if (articleWebSectionLink == null) {
				continue;
			}

			articleWebSectionLink.setIsdelete(1);
//			articleWebSectionLink.setUpdateTime(new Date());//更新时间 （需要去掉，因为updateTime现在被占用为发布时间）
			articleWebSectionLink.setUpdateUser(updateUser);//更新人
			count = count + articleWebSectionLinkMapper.updateByPrimaryKeySelective(articleWebSectionLink);
		}

		return BaseResultModel.success(count);
	}


	@Override
	public BaseResultModel webSectionUpdateArticleWebSectionLinkStatus(updateArticleWebSectionLinkStatusParamModel params, String updateUser) {
		if(params == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if (StringUtils.isBlank(params.getArticleId())) {
			return BaseResultModel.badParam("articleId不能为空");
		}
		if(params.getWebSectionLinkStatus() == null){
			return BaseResultModel.badParam("webSectionLinkStatus不能为空");
		}
		if(params.getWebSectionLinkStatus() != 0 && params.getWebSectionLinkStatus() != 1){
			return BaseResultModel.badParam("webSectionLinkStatus只能为0或1");
		}
		if (StringUtils.isBlank(updateUser)) {
			return BaseResultModel.badParam("更新人不能为空");
		}

		//映射
		Article article = new Article();
		article.setId(params.getArticleId());
		article.setWebSectionLinkStatus(params.getWebSectionLinkStatus());

//		article.setUpdateTime(new Date());//更新时间（需要去掉，因为updateTime现在被占用为发布时间）
		article.setUpdateUser(updateUser);//更新人
		int count = articleMapper.updateByPrimaryKeySelective(article);

		//更新文章web_section_link_id
		if(params.getWebSectionLinkStatus() == 1){
			articleWebSectionLinkMapper.updateArticleWebSectionLinkId(params.getArticleId());
		}

		return BaseResultModel.success(count);
	}
}

