package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

@Data
public class CourseType {
    private String id;

    private String typeName;
    private String photoId;
    private String photoUrl;

    private String remark;

    private Date creatTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;

}