package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 文章管理
 * @author guchangliang
 * @date 2021/8/1
 */
@ApiModel(value = "文章管理-列表")
@Data
public class ArticleParamModel extends PageParamModel {

    @ApiModelProperty(value = "主键id", example = "0e123d79c2fa468ba901426ab21b3fcf")
    private String id;

    @ApiModelProperty(value = "所属网站key（t_dictionary表type=Website数据key）", example = "1")
    private String websiteKey;

    @ApiModelProperty(value = "所属栏目key（t_web_section表id）", example = "1001")
    private String webSectionId;

    @ApiModelProperty(value = "类型标识（1：文章，2：图片，3：文件）", example = "1")
    private Integer typeStatus;


    @ApiModelProperty(value = "文章标题", example = "文章标题xxxx")
    private String title;


    @ApiModelProperty(value = "是否已发布（0：否，1：是）", example = "1")
    private Integer isOn;

    @ApiModelProperty(value = "模块（1：政策，2：通知）", example = "1")
    private Integer module;

}