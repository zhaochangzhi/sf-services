package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 企业中心-职位管理-职位详情
 * @author guchangliang
 * @date 2021/7/24
 */
@Data
@ApiModel(description = "企业中心-职位管理-职位详情")
public class EmploymentPositionDetailParamModel {

    @ApiModelProperty(value = "主键id（t_enterprise_position表id）", example = "691bd23ba5174f7c8229f6e9b1168776")
    private String id;



}
