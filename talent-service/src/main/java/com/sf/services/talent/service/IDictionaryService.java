package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.DictionaryModel;
import com.sf.services.talent.model.po.Dictionary;

/**
 * @ClassName: IDictionaryService 
 * @Description: 数据字典接口
 * @author: heyang
 * @date: 2021年7月12日 下午8:45:27
 */
public interface IDictionaryService {
	
	 int delete(String id);

	 int insert(DictionaryModel record);
	 
	 int update(DictionaryModel record);
	 
	 PageInfo<Dictionary> selectAll(DictionaryModel record);

	
}

