package com.sf.services.talent.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.config.ThreadLocalCache;
import com.sf.services.talent.mapper.EnterprisePositionMapper;
import com.sf.services.talent.mapper.EnterprisePositionResumeMapper;
import com.sf.services.talent.mapper.MemberMapper;
import com.sf.services.talent.mapper.PositionMapper;
import com.sf.services.talent.mapper.ResumeEducationMapper;
import com.sf.services.talent.mapper.ResumeMapper;
import com.sf.services.talent.mapper.ResumeWorkMapper;
import com.sf.services.talent.mapper.TradeMapper;
import com.sf.services.talent.model.IdsParamModel;
import com.sf.services.talent.model.ResumeEducationParamModel;
import com.sf.services.talent.model.ResumeListParam;
import com.sf.services.talent.model.ResumeParamModel;
import com.sf.services.talent.model.ResumeWorkParamModel;
import com.sf.services.talent.model.po.Member;
import com.sf.services.talent.model.po.Position;
import com.sf.services.talent.model.po.Resume;
import com.sf.services.talent.model.po.ResumeEducation;
import com.sf.services.talent.model.po.ResumeWork;
import com.sf.services.talent.model.po.Trade;
import com.sf.services.talent.service.IActLogService;
import com.sf.services.talent.service.IResumeService;
import com.sf.services.talent.util.FillUserInfoUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ResumeService implements IResumeService {

	@Resource
	private ResumeMapper resumeMapper;
	
	@Resource
	private TradeMapper tradeMapper;
	
	@Resource
	private PositionMapper positionMapper;

	@Resource
	private ResumeEducationMapper resumeEducationMapper;

	@Resource
	private ResumeWorkMapper resumeWorkMapper;
	@Resource
	private MemberMapper membermapper;
	@Resource
	private EnterprisePositionMapper enterprisePositionMapper;
	@Resource
	private EnterprisePositionResumeMapper enterprisePositionResumeMapper;
	
	@Autowired
	private IActLogService actLogService;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public int add(ResumeParamModel resumeParamModel) {
		try {
			Resume resume = new Resume();
			BeanUtils.copyProperties(resumeParamModel,resume);
			FillUserInfoUtil.fillCreateUserInfo(resume);
			if(StringUtils.isNoneBlank(resumeParamModel.getMemberId())) {
				resume.setMemberId(resumeParamModel.getMemberId());
			}else {
				resume.setMemberId(ThreadLocalCache.getUser());
			}
			Resume result = resumeMapper.selectByPrimaryKey(resume);
			if(result!=null) {
				throw new RuntimeException("个人简历已存在");
			}
			String id = resume.getId();
			List<ResumeWorkParamModel> resumeWorkParamList = resumeParamModel.getResumeWorkList();
			List<ResumeWork> resumeWorkList = new ArrayList<>();
			if(resumeWorkParamList !=null && resumeWorkParamList.size()>0) {
				resumeWorkParamList.stream().forEach(rw->{
					ResumeWork resumeWork = new ResumeWork();
					BeanUtils.copyProperties(rw, resumeWork);
					resumeWork.setResumeId(id);
					resumeWorkList.add(resumeWork);
				}
			 );
				resumeWorkMapper.insertBatch(resumeWorkList);
			}
			List<ResumeEducationParamModel> resumeEducationParamList = resumeParamModel.getResumeEducationList();
			List<ResumeEducation> esumeEducationList = new ArrayList<>();
			if(resumeEducationParamList !=null && resumeEducationParamList.size()>0) {
				resumeEducationParamList.stream().forEach(re->{
					ResumeEducation resumeEducation = new ResumeEducation();
					BeanUtils.copyProperties(re, resumeEducation);
					resumeEducation.setResumeId(id);
					esumeEducationList.add(resumeEducation);
				});
				resumeEducationMapper.insertBatch(esumeEducationList);
			}
			List<Map<String, Object>> resumeJobList = resumeParamModel.getResumeJobList();
			if(resumeJobList!=null && resumeJobList.size()>0) {
				resumeJobList.forEach(resumeJob->{
					resumeJob.put("resumeId",id);
				});
				resumeMapper.insertBatchJob(resumeJobList);
			}
			List<Map<String, Object>> resumeIndustryList = resumeParamModel.getResumeIndustryList();
			if(resumeIndustryList!=null && resumeIndustryList.size()>0) {
				resumeIndustryList.forEach(resumeIndustry->{
					resumeIndustry.put("resumeId",id);
				});
				resumeMapper.insertBatchIndustry(resumeIndustryList);
			}
			Date now = new Date();
			resume.setUpdateDate(now);
			resume.setUpdateUser(ThreadLocalCache.getUser());
			resume.setSwitch("Y");
			resumeMapper.insertSelective(resume);
			
			actLogService.save(ThreadLocalCache.getUser(), "创建简历", resume.getUserName());
			return 0;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("创建个人简历异常");
		}
	}

	@Override
	public PageInfo<Map<String, Object>> getList(ResumeListParam params) {
		SimpleDateFormat formatYYYY = new SimpleDateFormat("YYYY-MM-dd");
		//判断登陆账号类型1 用户 2公司
		String[] loginUserType = new String[1];
		String memberId = params.getMemberId();
		if(!StringUtils.isBlank(params.getMemberId())){
			Member member = membermapper.selectByPrimaryKey(memberId);
			if (member != null) {
				loginUserType[0] =member.getType();//登陆账号类型1 用户 2公司;
			}
		}
		if(StringUtils.isBlank(params.getOrderBycreate()) && StringUtils.isBlank(params.getOrderByUpdate())) {
			params.setOrderByUpdate("1");
		}
		params.setLoginUserType(loginUserType[0]);
		params.setMemberId(memberId);
		PageHelper.startPage(params.getPageNum(), params.getPageSize());
		List<Map<String, Object>> list =resumeMapper.getList(params);
		list.stream().forEach(obj->{
			obj.put("resumeWorkDesc", "");
			obj.put("resumeEducationDesc","");
			List<ResumeWork> resumeWorks = resumeWorkMapper.selectByPrimaryKey(obj.get("id").toString());
			if(resumeWorks!=null && resumeWorks.size()>0) {
				ResumeWork resumeWork = resumeWorks.get(0);
				
				obj.put("resumeWorkDesc", formatYYYY.format(resumeWork.getStartTime()) +"-至今 参加过一份工作，涉及"+resumeWork.getJob()+"等");
			}
			 List<ResumeEducation> resumeEducations = resumeEducationMapper.selectByPrimaryKey(obj.get("id").toString());
			 if(resumeEducations!=null && resumeEducations.size()>0) {
				 ResumeEducation resumeEducation = resumeEducations.get(0);
				 obj.put("resumeEducationDesc", formatYYYY.format(resumeEducation.getStartTime())+"-"+
				 formatYYYY.format(resumeEducation.getEndTime())+" 已完成"+resumeEducation.getEducationDesc());
			 }
		});
		return new PageInfo<>(list);
	}

	@Override
	public Resume getDetail(String id) {
		String memberId = ThreadLocalCache.getUser();
		Resume resume = new Resume();
		if(StringUtils.isNoneBlank(id)) {
			resume.setId(id);
		}else {
			resume.setMemberId(memberId);
		}
		Member member = membermapper.selectByPrimaryKey(memberId);
		Resume res =resumeMapper.selectByPrimaryKey(resume);
		if(res==null) {
			return null;
		}

		//查询简历行业，逗号分隔
		String industryString = resumeMapper.selectResumeIndustryString(id);
		//查询简历职位，逗号分隔
		String jobString = resumeMapper.selectResumeJobString(id);
		res.setIndustry2Desc(industryString);//行业
		res.setJob3Desc(jobString);//职位

//		 //当前简历行业集合
//		 List<String> industryList = resumeMapper.getIndustryList(res.getId());
//		 //当前简历职位集合
//		 List<String> jobindustryList = resumeMapper.getJobList(res.getId());

		 SimpleDateFormat ff = new SimpleDateFormat("yyyy年MM月dd日");
		 res.setBirthdayDesc(ff.format(res.getBirthday()));
		 //公司查看单独处理 登陆账号类型1 用户 2公司;
		System.out.println("member.getType()-------------"+member.getType());
		 if("2".equals(member.getType())) {
//			 // 根据memberid查询企业的招聘行业和招聘职位
//			 EmploymentPositionParamModel model = new EmploymentPositionParamModel();
//			 model.setMemberId(memberId);
//			 List<EmploymentPositionVO> employmentList = enterprisePositionMapper.selectList(model);
//			 // 当前企业所属行业集合
//			 List<String> comIndustryList = new ArrayList<>();
//			 // 当前企业所属职位集合
//			 List<String> comJobindustryList = new ArrayList<>();
//			 if(employmentList==null || employmentList.size()==0) {

			 //简历与企业所属行业匹配数
			 int resumeTradePiPeiCount = resumeMapper.selectResumeTradePiPeiCount(id, memberId);
			 //简历与企业发布职位匹配数
			 int resumeJobPiPeiCount = resumeMapper.selectResumeJobPiPeiCount(id, memberId);
			 //判断简历与企业所属行业匹配数等于0，或者简历与企业发布职位匹配数等于0，则隐藏敏感信息
			 if(resumeTradePiPeiCount == 0 || resumeJobPiPeiCount == 0) {
				 // 隐藏部分字段设置
				 //姓名隐藏
				 String userName = res.getUserName();
				 if("1".equals(res.getUserSex())) {
					 res.setUserName(getRes(userName)+"先生");
				 }else {
					 res.setUserName(getRes(userName)+"女士");
				 }
				 // 生日隐藏
				 SimpleDateFormat f = new SimpleDateFormat("yyyy年");
				 res.setBirthdayDesc(f.format(res.getBirthday())+"XX月XX日");
				 // 身份证隐藏
				 res.setIdentity("XXXXXXXXXXXXXXXXXX");
				 // 电话隐藏
				 res.setPhone("XXXXXXXXXXX");
			 }else {
				 // 设置企业
//				 employmentList.forEach(employment->{
//					 comIndustryList.add(employment.getTradeTwoId());
//					 comJobindustryList.add(employment.getPositionThreeId());
//				 });
//				 if(comIndustryList.retainAll(industryList)||comJobindustryList.retainAll(jobindustryList)) {
//					// 隐藏部分字段设置
//					 //姓名隐藏
//					 String userName = res.getUserName();
//					 if("1".equals(res.getUserSex())) {
//						 res.setUserName(getRes(userName)+"先生");
//					 }else {
//						 res.setUserName(getRes(userName)+"女士");
//					 }
//					 // 生日隐藏
//					 SimpleDateFormat f = new SimpleDateFormat("yyyy年");
//					 res.setBirthdayDesc(f.format(res.getBirthday())+"XX月XX日");
//					 // 身份证隐藏
//					 res.setIdentity("XXXXXXXXXXXXXXXXXX");
//					 // 电话隐藏
//					 res.setPhone("XXXXXXXXXXX");
//				 }
			 }
		 }

		 // 处理多个行业以及职业中卫显示
		 List<List<Map<String, String>>> resIndustryList = new ArrayList<>();
		 List<Map<String, String>> industryLists = resumeMapper.getIndustryLists(res.getId());
		 industryLists.forEach(tradid->{
			 List<Map<String, String>> resIndustrys = new ArrayList<>();
			 Map<String, String> resIndustry = new HashMap<>();
			 Trade trade =tradeMapper.selectByPrimaryKey(tradid.get("industry1")); 
			 resIndustry.put("industry1",tradid.get("industry1"));
			 resIndustry.put("parentId","0");
			 resIndustry.put("name",trade.getName());
			 resIndustrys.add(resIndustry);
			 Map<String, String> resIndustry2 = new HashMap<>();
			 Trade trade2 =tradeMapper.selectByPrimaryKey(tradid.get("industry2")); 
			 resIndustry2.put("industry2",tradid.get("industry2"));
			 resIndustry2.put("parentId",tradid.get("industry1"));
			 resIndustry2.put("name",trade2.getName());
			 resIndustrys.add(resIndustry2);
			 resIndustryList.add(resIndustrys);
//			 res.setIndustry2Desc(trade2.getName());
		 });
		 List<List<Map<String, String>>> resJobList = new ArrayList<>();
		 List<Map<String, String>> resJobLists =resumeMapper.getJobLists(res.getId());
		 resJobLists.forEach(jobId->{
			 List<Map<String, String>> resIndustryLists = new ArrayList<>();
			 Map<String, String> resJob = new HashMap<>();
			 Position postion =positionMapper.selectByPrimaryKey(jobId.get("job1"));
			 resJob.put("job1",jobId.get("job1"));
			 resJob.put("parentId","0");
			 resJob.put("name",postion.getName());
			 resIndustryLists.add(resJob);
			 Map<String, String> resJob2 = new HashMap<>();
			 Position postion2 =positionMapper.selectByPrimaryKey(jobId.get("job2"));
			 resJob2.put("job2",jobId.get("job2"));
			 resJob2.put("parentId",jobId.get("job1"));
			 resJob2.put("name",postion2.getName());
			 resIndustryLists.add(resJob2);
			 Map<String, String> resJob3 = new HashMap<>();
			 Position postion3 =positionMapper.selectByPrimaryKey(jobId.get("job3"));
			 resJob3.put("job3",jobId.get("job3"));
			 resJob3.put("parentId",jobId.get("job2"));
			 resJob3.put("name",postion3.getName());
			 resIndustryLists.add(resJob3);
			 resJobList.add(resIndustryLists);
//			 res.setJob3Desc(postion3.getName());
		 });
		 res.setResumeJobList(resJobList);
		 res.setResumeIndustryList(resIndustryList);
		return res;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public int delete(String id) {
		try {
			String memberId = ThreadLocalCache.getUser();
			Resume resume = new Resume();
			if(StringUtils.isNoneBlank(id)) {
				resume.setId(id);
			}else {
				resume.setMemberId(memberId);
			}
			Resume res =resumeMapper.selectByPrimaryKey(resume);
			actLogService.save(ThreadLocalCache.getUser(), "删除简历", res.getUserName());
			enterprisePositionResumeMapper.deleteByResumeId(id);
			resumeWorkMapper.deleteByPrimaryKey(id);
			resumeEducationMapper.deleteByPrimaryKey(id);
			return resumeMapper.deleteByPrimaryKey(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("删除个人简历异常");
		}
	}

	@Override
	public int batchDelete(IdsParamModel params) {
		if(params == null){
			return 0;
		}
		if (StringUtils.isBlank(params.getIds())) {
			return 0;
		}
		List<String> idList = Arrays.asList(params.getIds().split(","));
		for (int i = 0; i < idList.size(); i++) {
			delete(idList.get(i));
		}

		return idList.size();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public int update(ResumeParamModel resumeParamModel) {
		try {
			Resume resume = new Resume();
			BeanUtils.copyProperties(resumeParamModel,resume);
			String memberId = ThreadLocalCache.getUser();
			resume.setMemberId(memberId);
			String id = resume.getId();
			List<ResumeWorkParamModel> resumeWorkParamList = resumeParamModel.getResumeWorkList();
			List<ResumeWork> resumeWorkList = new ArrayList<>();
			if(resumeWorkParamList !=null && resumeWorkParamList.size()>0) {
				resumeWorkMapper.deleteByPrimaryKey(id);
				resumeWorkParamList.stream().forEach(rw->{
					ResumeWork resumeWork = new ResumeWork();
					BeanUtils.copyProperties(rw, resumeWork);
					resumeWork.setResumeId(id);
					resumeWorkList.add(resumeWork);
				}
			 );
				resumeWorkMapper.insertBatch(resumeWorkList);
			}
			List<ResumeEducationParamModel> resumeEducationParamList = resumeParamModel.getResumeEducationList();
			List<ResumeEducation> esumeEducationList = new ArrayList<>();
			if(resumeEducationParamList !=null && resumeEducationParamList.size()>0) {
				resumeEducationMapper.deleteByPrimaryKey(id);
				resumeEducationParamList.stream().forEach(re->{
					ResumeEducation resumeEducation = new ResumeEducation();
					BeanUtils.copyProperties(re, resumeEducation);
					resumeEducation.setResumeId(id);
					esumeEducationList.add(resumeEducation);
				});
				resumeEducationMapper.insertBatch(esumeEducationList);
			}
			
			List<Map<String, Object>> resumeJobList = resumeParamModel.getResumeJobList();
			if(resumeJobList!=null && resumeJobList.size()>0) {
				resumeMapper.deleteJobByResumeId(id);
				resumeJobList.forEach(resumeJob->{
					resumeJob.put("resumeId",id);
				});
				resumeMapper.insertBatchJob(resumeJobList);
			}
			List<Map<String, Object>> resumeIndustryList = resumeParamModel.getResumeIndustryList();
			if(resumeIndustryList!=null && resumeIndustryList.size()>0) {
				resumeMapper.deleteIndustryByResumeId(id);
				resumeIndustryList.forEach(resumeIndustry->{
					resumeIndustry.put("resumeId",id);
				});
				resumeMapper.insertBatchIndustry(resumeIndustryList);
			}
			Date now = new Date();
			resume.setUpdateDate(now);
			resume.setUpdateUser(ThreadLocalCache.getUser());
			resume.setSwitch(resumeParamModel.getSwitch());
			resumeMapper.updateByPrimaryKeySelective(resume);
			actLogService.save(ThreadLocalCache.getUser(), "修改简历", resume.getUserName());
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("修改个人简历异常");
		}
	}

	@Override
	public Map<String, Object> selectClount(String memberId) {
		
		return resumeMapper.selectClount(memberId);
	}

	@Override
	public int audit(Map<String, Object> params) {
		params.put("auditTime", new Date());
		try {
			Resume resume = new Resume();
			resume.setId(params.get("id").toString());
			Resume res =resumeMapper.selectByPrimaryKey(resume);
			actLogService.save(ThreadLocalCache.getUser(), "审核简历", res.getUserName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resumeMapper.audit(params);
	}
	
	private String getRes(String s) {
		// 找第一个汉字
		for (int index = 0; index <= s.length() - 1; index++) {
			// 将字符串拆开成单个的字符
			String w = s.substring(index, index + 1);
			if (w.compareTo("\u4e00") > 0 && w.compareTo("\u9fa5") < 0) {// \u4e00-\u9fa5 中文汉字的范围
				return w;
			}
		}
		return s;
	}
}

