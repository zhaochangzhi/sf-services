package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 职称文件参数实体
 *
 * @author zhaochangzhi
 * @date 2021/7/19
 */
@Data
@ApiModel(value = "ArchiveFileAddParamModel", description = "档案文件上传")
public class ArchiveFileUploadeParamModel {


    @ApiModelProperty(value = "文件id")
    private String fileId;

}
