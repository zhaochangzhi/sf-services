package com.sf.services.talent.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.sf.services.talent.common.constants.Constants;
import com.sf.services.talent.config.ThreadLocalCache;
import com.sf.services.talent.exceptions.AuthException;
import com.sf.services.talent.model.dto.UserDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.Charsets;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

/**
 * request请求处理
 *
 * @author zhaochangzhi
 * @date 2021/7/9
 */
@Slf4j
public final class RequestUtils {

    private RequestUtils() {

    }

    private static final String USER_TOKEN = "userToken";

    private static final String AUTHORIZATION = "Authorization";

    /**
     * 获取当前登录人的id
     * 1. 优先从cookie获取 userToken
     * 2. 从header获取 authorization
     *
     * @return 当前登录用户id
     */
    public static UserDto getCurrentUser() {
        String authorization = "";
        UserDto userDto = new UserDto();
        ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (Objects.isNull(sra.getRequest().getCookies())) {
            //header
            authorization = sra.getRequest().getHeader(AUTHORIZATION);
        } else {
            Optional<Cookie> opt = Arrays.stream(sra.getRequest().getCookies())
                    .filter(s -> USER_TOKEN.equals(s.getName())).findAny();
            if (opt.isPresent()) {
                authorization = opt.get().getValue();
            } else {
                //header
                authorization = sra.getRequest().getHeader(AUTHORIZATION);
            }
        }

        if (StringUtils.isBlank(authorization)) {
            throw new AuthException(401, "未获取到登录信息");
        }

        // 解析 authorization
        String[] jwtInfo = authorization.split("\\.");
        if (jwtInfo.length != 3) {
            log.error("token数据异常！");
            throw new AuthException(403, "登录信息有误");
        }
        String payLoad = jwtInfo[1];
        byte[] bytes = Base64.decodeBase64(payLoad);
        String jsonData = new String(bytes, Charsets.UTF_8);
        JWTPayload jwtPayload = JSON.parseObject(jsonData, JWTPayload.class);
        if (jwtPayload != null) {
            userDto.setUserId(jwtPayload.getSub());
            //用户类型： 个人/企业
            userDto.setType(jwtPayload.getJti());
            ThreadLocalCache.set(Constants.CURRENT_USER, jwtPayload.getSub());
            ThreadLocalCache.set("type", jwtPayload.getJti());
        } else {
            throw new AuthException(401, "未获取到登录信息");
        }
        return userDto;
    }

    /**
     * 获取当前登录人的id（未登录则返回null，不抛异常）
     * 1. 优先从cookie获取 userToken
     * 2. 从header获取 authorization
     *
     * @return 当前登录用户id
     */
    public static UserDto getCurrentUserNotThrowAuthException() {
        String authorization = "";
        UserDto userDto = new UserDto();
        ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (Objects.isNull(sra.getRequest().getCookies())) {
            //header
            authorization = sra.getRequest().getHeader(AUTHORIZATION);
        } else {
            Optional<Cookie> opt = Arrays.stream(sra.getRequest().getCookies())
                    .filter(s -> USER_TOKEN.equals(s.getName())).findAny();
            if (opt.isPresent()) {
                authorization = opt.get().getValue();
            } else {
                //header
                authorization = sra.getRequest().getHeader(AUTHORIZATION);
            }
        }

        //header为空则返回null
        if (StringUtils.isBlank(authorization)) {
            return null;
        }

        // 解析 authorization
        String[] jwtInfo = authorization.split("\\.");
        if (jwtInfo.length != 3) {
            log.error("token数据异常！");
            return null;
        }
        String payLoad = jwtInfo[1];
        byte[] bytes = Base64.decodeBase64(payLoad);
        String jsonData = new String(bytes, Charsets.UTF_8);
        JWTPayload jwtPayload = JSON.parseObject(jsonData, JWTPayload.class);
        if (jwtPayload != null) {
            userDto.setUserId(jwtPayload.getSub());
            //用户类型： 个人/企业
            userDto.setType(jwtPayload.getJti());
            ThreadLocalCache.set(Constants.CURRENT_USER, jwtPayload.getSub());
            ThreadLocalCache.set("type", jwtPayload.getJti());
        } else {
            return null;
        }
        return userDto;
    }

    private static class JWTPayload {
        private String sub;
        @JSONField(name = "userId")
        private String userId;
        private String ip;
        private Date expDate;
        private String tgt;

        //用户type
        private String jti;
        private String st;
        private Date iat;

        public String getSub() {
            return sub;
        }

        public void setSub(String sub) {
            this.sub = sub;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public Date getExpDate() {
            return expDate;
        }

        public void setExpDate(Date expDate) {
            this.expDate = expDate;
        }

        public String getTgt() {
            return tgt;
        }

        public void setTgt(String tgt) {
            this.tgt = tgt;
        }

        public String getJti() {
            return jti;
        }

        public void setJti(String jti) {
            this.jti = jti;
        }

        public String getSt() {
            return st;
        }

        public void setSt(String st) {
            this.st = st;
        }

        public Date getIat() {
            return iat;
        }

        public void setIat(Date iat) {
            this.iat = iat;
        }
    }

}
