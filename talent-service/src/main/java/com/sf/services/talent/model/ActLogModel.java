package com.sf.services.talent.model;


import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ActLogModel extends PageParamModel{
	
	@ApiModelProperty(value = "日志操作人用户名", example = "test1")
	private String actUserName;
	
	@ApiModelProperty(value = "开始时间", example = "2021-09-23 00:28:41")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
	private Date startTime;
	
	@ApiModelProperty(value = "结束时间", example = "2021-09-23 00:28:41")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
	private Date endTime;
	
	@ApiModelProperty(value = "日志内容", example = "test1")
	private String actLogContect;
	
	@ApiModelProperty(value = "用户类型:1个人,2企业,0后台用户,3街道用户", example = "1")
	private String type;
}

