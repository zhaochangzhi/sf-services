package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "就业指标类型", description = "就业指标类型")
public class EmploymentTargerTypeParamModel extends PageParamModel {
	
	@ApiModelProperty(value = "主键id", example = "835a3e5fb9c842c594c2e7372bd60d21")
	private String id;


}
