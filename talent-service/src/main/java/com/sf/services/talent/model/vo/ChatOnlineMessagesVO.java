package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class ChatOnlineMessagesVO {

    @ApiModelProperty(value = "招聘会id")
    private String jobFairId;

    @ApiModelProperty(value = "消息类型id（1：普通文本，2：通知消息，3：上线通知，4：下线通知）")
    private String messagesTypeId;

    @ApiModelProperty(value = "发送者会员id（t_member表id）")
    private String fromMemberId;

    @ApiModelProperty(value = "接收者会员id（t_member表id）")
    private String toMemberId;

    @ApiModelProperty(value = "消息内容")
    private String messages;

    @ApiModelProperty(value = "消息发送时间")
    private String sendTime;

}