package com.sf.services.talent.util;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

import lombok.Data;
/**
 * 结果集工具类
 * @author hy
 *
 */
@Data
public class ResultUtil implements Serializable{

	private static final long serialVersionUID = -9071246765351341435L;
	
	private Integer code;// 状态码
    private String msg;// 返回信息
    private Object data;// 返回数据


    //自定义code,msg,data
    private ResultUtil(Integer code, String msg, Object data) {
        this.data = data;
        this.code = code;
        this.msg = msg;
    }

    //自定义data
    private ResultUtil(Object data) {
        this.data = data;
        this.code = HttpStatus.OK.value();
        this.msg = HttpStatus.OK.getReasonPhrase();
    }

    //自定义code,msg
    private ResultUtil(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private ResultUtil() {
        this.code = HttpStatus.OK.value();
        this.msg = HttpStatus.OK.getReasonPhrase();
    }

    public static ResultUtil success(Integer code, String msg, Object data) {
        return new ResultUtil(code, msg, data);
    }

    public static ResultUtil success(Integer code, String msg) {
        return new ResultUtil(code, msg);
    }

    public static ResultUtil error(Integer code, String msg) {
        return new ResultUtil(code, msg);
    }

    public static ResultUtil success(Object data) {
        return new ResultUtil(data);
    }

    public static ResultUtil success() {
        return new ResultUtil();
    }

}
