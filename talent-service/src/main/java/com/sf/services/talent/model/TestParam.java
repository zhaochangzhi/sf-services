package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName: AreaParam 
 * @Description: 地区参数
 * @author: heyang
 * @date: 2021年7月21日 下午9:43:50
 */
@Data
@ApiModel(value = "地区", description = "地区")
public class TestParam {
	
	@ApiModelProperty(value = "id", example = "test")
	private String id;

	@ApiModelProperty(value = "subject", example = "aaa")
    private String subject;

	@ApiModelProperty(value = "毫秒数", example = "1000*60*60*8")
    private long ttlMillis;

	@ApiModelProperty(value = "authorization", example = "authorization")
	private String authorization;
}

