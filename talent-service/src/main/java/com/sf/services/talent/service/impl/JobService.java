package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.sf.services.talent.mapper.JobMapper;
import com.sf.services.talent.model.po.SyrcCompanyJob;
import com.sf.services.talent.model.vo.JobVO;
import com.sf.services.talent.service.IJobService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * JobService
 *
 * @author zhaochangzhi
 * @date 2021/7/7
 */
@Service
public class JobService implements IJobService {

    private final JobMapper jobMapper;

    @Autowired
    public JobService(JobMapper jobMapper) {
        this.jobMapper = jobMapper;
    }

    @Override
    public Integer getCount() {
        return jobMapper.selectCount();
    }

    @Override
    public List<JobVO> getList() {
        List<JobVO> jobList = new ArrayList<>();
        //分页处理
        PageHelper.startPage(1, 10);
        List<SyrcCompanyJob> list = jobMapper.selectList();
        if (!list.isEmpty()) {
            list.stream().forEach(l -> {
                JobVO job = new JobVO();
                BeanUtils.copyProperties(l, job);
                jobList.add(job);
            });
        }
        return jobList;
    }

    @Override
    public JobVO getById(Integer id) {
        JobVO jobVO = new JobVO();
        SyrcCompanyJob job = jobMapper.selectById(id);
        if (Objects.nonNull(job)) {
            BeanUtils.copyProperties(job, jobVO);
        }
        return jobVO;
    }
}
