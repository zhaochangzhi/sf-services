package com.sf.services.talent.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.mapper.ArchiveServiceCategoryMapper;
import com.sf.services.talent.model.po.ArchiveServiceCategory;
import com.sf.services.talent.model.vo.ArchiveServiceCategoryVO;
import com.sf.services.talent.service.IArchiveServiceCategoryService;
import com.sf.services.talent.util.FillUserInfoUtil;
import com.sf.services.talent.util.UUIDUtil;


/**
 * @ClassName: ArchiveServiceCategoryService 
 * @Description: 档案服务类别接口实现
 * @author: heyang
 * @date: 2021年7月10日 下午4:38:33
 */
@Service
public class ArchiveServiceCategoryService implements IArchiveServiceCategoryService{

	@Autowired
	private ArchiveServiceCategoryMapper archiveServiceCategoryMapper;
	
	@Override
	public int insert(ArchiveServiceCategoryVO record) {
		ArchiveServiceCategory archiveServiceCategory = new ArchiveServiceCategory();
		BeanUtils.copyProperties(record, archiveServiceCategory);
		archiveServiceCategory.setId(UUIDUtil.getUUid());
		FillUserInfoUtil.fillCreateUserInfo(archiveServiceCategory);
		return archiveServiceCategoryMapper.insert(archiveServiceCategory);
	}

	@Override
	public int delete(ArchiveServiceCategoryVO record) {
		
		return archiveServiceCategoryMapper.deleteByPrimaryKey(record.getId());
	}

	@Override
	public int update(ArchiveServiceCategoryVO record) {
		ArchiveServiceCategory archiveServiceCategory = new ArchiveServiceCategory();
		BeanUtils.copyProperties(record, archiveServiceCategory);
		FillUserInfoUtil.fillUpdateUserInfo(archiveServiceCategory);
		return archiveServiceCategoryMapper.updateByPrimaryKeySelective(archiveServiceCategory);
	}

	@Override
	public PageInfo<ArchiveServiceCategory> list(ArchiveServiceCategoryVO record) {
		PageHelper.startPage(record.getPageNum(), record.getPageSize());
		ArchiveServiceCategory archiveServiceCategory = new ArchiveServiceCategory();
		BeanUtils.copyProperties(record, archiveServiceCategory);
		List<ArchiveServiceCategory> list = archiveServiceCategoryMapper.list(archiveServiceCategory);
		return new PageInfo<ArchiveServiceCategory>(list);
	}

	@Override
	public List<Map<String, Object>> getCategoryList() {
		List<Map<String, Object>> list = archiveServiceCategoryMapper.getCategoryList();
		list.stream().forEach(rank -> {
			List<Map<String, Object>> chirlder = archiveServiceCategoryMapper.getCategoryChirlderList(rank.get("name")+"");
			rank.put("children", chirlder);
			rank.put("id", UUIDUtil.getUUid());
		});
		
		return list;
	}
}

