package com.sf.services.talent.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sf.services.talent.model.DictionaryModel;
import com.sf.services.talent.model.PolicyCategoryModel;
import com.sf.services.talent.service.IDictionaryService;
import com.sf.services.talent.service.IPolicyCategoryService;
import com.sf.services.talent.util.RequestUtils;
import com.sf.services.talent.util.ResultUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: PolicyCategoryController 
 * @Description: 政策类别处理器
 * @author: heyang
 * @date: 2021年7月13日 下午9:09:08
 */
@Slf4j
@Api(value = "政策类别", tags = "政策类别")
@RestController
@RequestMapping("/policyCategory")
public class PolicyCategoryController {
	
	@Autowired
	private IPolicyCategoryService policyCategoryService;
	
	@Autowired
	private IDictionaryService dictionaryService;
	
	/**
	 * @Title: list 
	 * @Description:政策类别列表
	 * @param PolicyCategoryModel
	 * @return
	 * @return: ResultUtil
	 */
	@ApiOperation(notes = "政策类别列表", value="政策类别列表")
	@ApiImplicitParam(name = "params", value = "政策类别模型", required = false, dataType = "PolicyCategoryModel")
	@PostMapping("/list")
	public ResultUtil list(@RequestBody PolicyCategoryModel policyCategoryModel) {
		return ResultUtil.success(policyCategoryService.list(policyCategoryModel));
	}
	
	/**
	 * @Title: appointment 
	 * @Description:政策类别添加
	 * @param PolicyCategoryModel
	 * @return
	 * @return: ResultUtil
	 */
	@ApiOperation(notes = "政策类别添加", value="政策类别添加")
	@ApiImplicitParam(name = "params", value = "政策类别模型", required = false, dataType = "PolicyCategoryModel")
	@PostMapping("/add")
	public ResultUtil appointment(@RequestBody PolicyCategoryModel PolicyCategoryModel) {
		
		return ResultUtil.success(policyCategoryService.add(PolicyCategoryModel));
	}
	
	/**
	 * 政策类别删除
	 * @param PolicyCategoryModel
	 * @return
	 */
	@ApiOperation(notes = "政策类别删除", value="政策类别删除")
	@ApiImplicitParam(name = "params", value = "政策类别模型", required = false, dataType = "PolicyCategoryModel")
	@PostMapping("/delete")
	public ResultUtil audit(@RequestBody PolicyCategoryModel PolicyCategoryModel) {
		return ResultUtil.success(policyCategoryService.delete(PolicyCategoryModel));
		
	}
	
	/**
	 * @Title: updateStatus 
	 * @Description: 政策类别状态
	 * @param PolicyCategoryModel
	 * @return
	 * @return: ResultUtil
	 */
	@ApiOperation(notes = "政策类别更新", value="政策类别更新")
	@ApiImplicitParam(name = "params", value = "政策类别模型", required = false, dataType = "PolicyCategoryModel")
	@PostMapping("/updateStatus")
	public ResultUtil updateStatus(@RequestBody PolicyCategoryModel PolicyCategoryModel) {
		return ResultUtil.success(policyCategoryService.updateStatus(PolicyCategoryModel));
	}
	
	/**
	 * @Title: policyType 
	 * @Description: 根据会员类型获取政策类型列表
	 * @param param
	 * @return
	 * @return: ResultUtil
	 */
	@ApiOperation(notes = "政策类型列表", value="政策类型列表")
	@PostMapping("/policyType")
	public ResultUtil policyType(@RequestBody Map<String, Object> param) {
		DictionaryModel record = new DictionaryModel();
		String type = RequestUtils.getCurrentUser().getType();//会员类型
		if("1".equals(type)) {
			record.setType("PolicyType_persion");
			
		}else {
			record.setType("PolicyType_company");
			
		}
		return ResultUtil.success(dictionaryService.selectAll(record));
	}
	
	
}

