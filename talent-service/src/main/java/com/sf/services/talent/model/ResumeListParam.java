package com.sf.services.talent.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ResumeListParam extends PageParamModel {
	
	@ApiModelProperty(value = "行业id", example = "1")
	private String tradeId;
	@ApiModelProperty(value = "职位id", example = "1")
	private String positionId;
	@ApiModelProperty(value = "期望金额", example = "1")
	private String salaryRequirementKey;
	@ApiModelProperty(value = "工作经验", example = "1")
	private String workExpKey;
	@ApiModelProperty(value = "学历", example = "1")
	private String educationKey;
	@ApiModelProperty(value = "职位输入框", example = "1")
	private String searchInput;
	@ApiModelProperty(value = "会员id", example = "1")
	private String memberId;
	@ApiModelProperty(value =  "审核状态", example = "1")
	private String auditStatus;
	@ApiModelProperty(value =  "到岗时间", example = "1")
	private String arrivalTimeKey;


	@JsonIgnore
	@ApiModelProperty(value = "登陆用户类别，1：用户，2：公司", example = "1")
	private String loginUserType;
	@ApiModelProperty(value =  "用户名称", example = "1")
	private String userName;
	@ApiModelProperty(value =  "证件号", example = "1")
	private String identity;
	@ApiModelProperty(value =  "工作城市", example = "1")
	private String workCity;
	@ApiModelProperty(value =  "更新时间排序", example = "1")
	private String orderByUpdate;
	@ApiModelProperty(value =  "创建时间排序", example = "1")
	private String orderBycreate;
}
