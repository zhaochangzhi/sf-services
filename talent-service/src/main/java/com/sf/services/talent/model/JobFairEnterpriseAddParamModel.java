package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 *
 * @author guchangliang
 * @date 2021/8/3
 */
@ApiModel(value = "JobFairEnterpriseAddParamModel", description = "招聘会-企业报名参会实体")
@Data
public class JobFairEnterpriseAddParamModel {

    @ApiModelProperty(value = "主键id（t_job_fair表id）")
    private String JobFairId;

    @ApiModelProperty(value = "企业userId（t_enterprise_user表id）")
    private String enterpriseUserId;

    @ApiModelProperty(value = "职位id列表（t_enterprise_position表id）")
    private List<String> enterprisePositionIdList;



}
