package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author guchangliang
 * @date 2021/7/13
 */
@ApiModel(value = "招聘会-企业-参会职位实体")
@Data
public class JobFairEmploymentPositionVO {

    @ApiModelProperty(value = "主键id（t_job_fair_enterprise_position表id）")
    private String id;

    @ApiModelProperty(value = "招聘会id（t_job_fair表id）")
    private String jobFairId;

    @ApiModelProperty(value = "企业账号id（t_enterprise_user表id）")
    private String enterpriseUserId;
    @ApiModelProperty(value = "职位id（t_enterprise_position表id）")
    private String enterprisePositionId;

    @ApiModelProperty(value = "企业职位名称")
    private String enterprisePositionName;

    @ApiModelProperty(value = "审核状态：1 审核中，2 通过，3 不通过")
    private Integer auditStatus;
    @ApiModelProperty(value = "审核不通过原因")
    private String auditReason;
    @ApiModelProperty(value = "审核时间")
    private Date auditTime;
    @ApiModelProperty(value = "审核人")
    private String auditUser;

    @ApiModelProperty(value = "薪资要求key（t_dictionary表type=SalaryRequirement数据dickey）")
    private String salaryRequirementKey;
    @ApiModelProperty(value = "薪资要求（冗余）")
    private String salaryRequirement;
    @ApiModelProperty(value = "薪资-最小值")
    private String salaryMin;
    @ApiModelProperty(value = "薪资-最大值")
    private String salaryMax;

    @ApiModelProperty(value = "企业log图片Url")
    private String enterpriseLogoImageUrl;

}