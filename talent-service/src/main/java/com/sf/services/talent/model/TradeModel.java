package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "行业字典", description = "行业字典筛选条件参数实体")
public class TradeModel extends PageParamModel {
	
	@ApiModelProperty(value = "主键id", example = "8f3d033b26f7449fac4c1e11127622a1")
	private String id;
	@ApiModelProperty(value = "数据等级（1：一级，2：二级，3：三级）", example = "1")
	private Integer dataLevel;
	@ApiModelProperty(value = "父类id（数据等级为一级时，该字段值为0）", example = "8f3d033b26f7449fac4c1e11127622e1")
	private String parentId;


}
