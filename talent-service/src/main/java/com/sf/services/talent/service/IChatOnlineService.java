package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.vo.ChatOnlineFriendsVO;
import org.apache.ibatis.annotations.Param;

/**
 * @ClassName: IChatOnlineService
 * @Description: 在线聊天
 * @author: guchangliang
 * @date: 2021年9月8日 下午11:19:07
 */
public interface IChatOnlineService {

    /**
     * 添加消息记录
     * @param message   消息
     * @param status    接收状态（0：未接收，1：已接收）
     * @return
     */
    BaseResultModel add(String message, Integer status);

    /**
     * 查询好友列表
     * @param params
     * @return
     */
    PageInfo<ChatOnlineFriendsVO> friendsList(ChatOnlineFriendsListParamModel params);

    /**
     * 添加好友/联系人
     * @param params
     * @return
     */
    BaseResultModel friendsAdd(ChatOnlineFriendsAddParamModel params);

    /**
     * 未读消息列表
     * @param params
     * @return
     */
    BaseResultModel messagesNotReadList(ChatOnlineMessageNotReadListParamModel params);

    /**
     * 未读消息条数
     * @param params
     * @return
     */
    BaseResultModel notReadCount(ChatOnlineMessageNotReadListParamModel params);


    BaseResultModel messagesList(ChatOnlineMessageListParamModel params);

    BaseResultModel updateStatus(String fromMemberId, String toMemberId, Integer status);

    /**
     * 更新聊天记录为已读
     * @param params
     * @return
     */
    BaseResultModel messagesUpdateMessageRead(ChatOnlineMessageNotReadListParamModel params);
}