package com.sf.services.talent.service.impl;

import java.util.Date;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.sf.services.talent.exceptions.TalentException;
import com.sf.services.talent.mapper.EnterpriseUserMapper;
import com.sf.services.talent.mapper.MemberMapper;
import com.sf.services.talent.mapper.UserMapper;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.CheckEmailVerificationCodeParamModel;
import com.sf.services.talent.model.ForgetPasswordSendEmailVerificationCode;
import com.sf.services.talent.model.RetrievePasswordParamModel;
import com.sf.services.talent.model.SendEmailVerificationCodeParamModel;
import com.sf.services.talent.model.po.Member;
import com.sf.services.talent.model.po.User;
import com.sf.services.talent.model.vo.EnterpriseUserVO;
import com.sf.services.talent.model.vo.LoggedInUserVO;
import com.sf.services.talent.model.vo.MemberVO;
import com.sf.services.talent.service.IActLogService;
import com.sf.services.talent.service.IEnterpriseUserService;
import com.sf.services.talent.service.IMailService;
import com.sf.services.talent.service.IMerberService;
import com.sf.services.talent.service.IUserService;
import com.sf.services.talent.util.JwtUtil;
import com.sf.services.talent.util.MD5Util;
import com.sf.services.talent.util.RedisUtils;
import com.sf.services.talent.util.RequestUtils;
import com.sf.services.talent.util.ResultUtil;
import com.sf.services.talent.util.UUIDUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 会员接口实现
 * @author hy
 *
 */
@Slf4j
@Service
public class MemberService implements IMerberService{

	@Resource
	private MemberMapper memberMapper;

	@Resource
	private UserMapper userMapper;
	@Resource
	private EnterpriseUserMapper enterpriseUserMapper;

	@Resource
	private IEnterpriseUserService enterpriseUserService;

	@Resource
	private IUserService userService;
	@Autowired
	private IMailService mailService;
	@Autowired
	private RedisUtils redisUtils;
	
	@Autowired
	private IActLogService actLogService;

	@Override
	public String login(String userName, String password, Integer platform) {
		Member member = memberMapper.selectByUserName(userName);
		if(member == null) {
			throw new TalentException(1003,"账号不存在");
		}

		//platform 登陆平台 1：产业园，2：就业和人才服务中心网站
		//判断登陆平台为1：产业园。 必须为企业用户登陆
		if(platform != null && platform == 1 && !"2".equals(member.getType())){
			throw new TalentException(1002, "无效的用户名密码");
		}

		String mPassword = member.getPassword();
		String salt = member.getSalt();
		String npassword = MD5Util.md5(password, salt);
		if(!npassword.equals(mPassword)) {
			throw new TalentException(1002,"密码错误");
		}
		actLogService.save(member.getMemberId(), "登陆了", "");
		//登陆成功
		return JwtUtil.createJWT(member.getType(), member.getMemberId(), 1000*60*60*8);
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public String register(MemberVO memberVO) {
		try {
			Member result = memberMapper.selectByUserName(memberVO.getUserName());
			if(result!=null) {
				throw new TalentException(1001,"用户名已存在");
			}
			Member member = new Member();
			BeanUtils.copyProperties(memberVO, member);
			String memberId = UUID.randomUUID().toString().replace("-", "");
			Date time = new Date();
			String salt = MD5Util.salt();
			String npassword = MD5Util.md5(member.getPassword(), salt);
			member.setCreateTime(time);
			member.setSalt(salt);
			member.setMemberId(memberId);
			member.setCreateUser(memberId);
			member.setPassword(npassword);
			int sum =memberMapper.insert(member);
			if("1".equals(memberVO.getType())) {
				User user = new User();
				String id = UUIDUtil.getUUid();
				user.setId(id);
				user.setMemberId(memberId);
				if(StringUtils.isNoneBlank(memberVO.getIsSystem())) {
					user.setIsSystem(memberVO.getIsSystem());
				}
				userMapper.insertSelective(user);
			}else if("2".equals(memberVO.getType())){
				//企业用户初始化
				enterpriseUserService.enterpriseUserInit(memberId, memberId);
			}else if("0".equals(memberVO.getType())) {
				User user = new User();
				String id = UUIDUtil.getUUid();
				user.setId(id);
				user.setMemberId(memberId);
				userMapper.insertSelective(user);
			}
			else if("3".equals(memberVO.getType())) {
				User user = new User();
				String id = UUIDUtil.getUUid();
				user.setId(id);
				user.setMemberId(memberId);
				userMapper.insertSelective(user);
			}
			
			return memberId;
		}catch(TalentException e) {
			throw new TalentException(1001,"用户名已存在");
		}catch (Exception e) {
			log.error("用户注册异常:{}", e);
			throw new RuntimeException("插入异常");
		}
	}


	@Override
	public ResultUtil getLoginUser() {
		String memberId = RequestUtils.getCurrentUser().getUserId();//会员id（t_member表id）
		String type = RequestUtils.getCurrentUser().getType();//会员类型
		//校验非空
		if(StringUtils.isBlank(memberId)){
			return ResultUtil.error(401, "请登陆");
		}
		if(StringUtils.isBlank(type)){
			return ResultUtil.error(401, "用户类型缺失");
		}
		//已登录账号信息
		LoggedInUserVO loggedInUserVO = new LoggedInUserVO();
		loggedInUserVO.setMemberId(memberId);
		loggedInUserVO.setType(type);
		//根据用户类型查询对应（个人/企业）信息
		if("0".equals(type)){
			//管理员用户
			loggedInUserVO.setUser(userService.getUser());
		}else if ("1".equals(type)) {
			//个人用户
			loggedInUserVO.setUser(userService.getUser());
		} else if("2".equals(type)){
			//企业用户
			loggedInUserVO.setEnterpriseUser(enterpriseUserService.getEnterpriseUser(null));
		} else if("3".equals(type)){
			//街道用户
			loggedInUserVO.setUser(userService.getUser());
		}  else  {

			return ResultUtil.error(401, "用户类型异常");
		}

		return ResultUtil.success(loggedInUserVO);
	}

	@Override
	public int modifyPassword(String password,String memberId) {
		if(StringUtils.isBlank(memberId)) {
			 memberId = RequestUtils.getCurrentUser().getUserId();//会员id（t_member表id）
		}
		Member member = memberMapper.selectByPrimaryKey(memberId);
		String salt = member.getSalt();
		String npassword = MD5Util.md5(password, salt);
		Member modifyMemb = new Member();
		modifyMemb.setMemberId(memberId);
		modifyMemb.setPassword(npassword);
		modifyMemb.setUserName(member.getUserName());
		memberMapper.updateByPrimaryKeySelective(modifyMemb);
		return 0;
	}

	@Override
	public Boolean existUserName(String userName) {
		Member member = memberMapper.selectByUserName(userName);
		if(member!=null) {
			return true;
		}else {
			return false;
		}
		
	}

	@Override
	public BaseResultModel sendEmailVerificationCode(ForgetPasswordSendEmailVerificationCode params) {
		//校验非空-列表
		if(params == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if (StringUtils.isBlank(params.getUserName())) {
			return BaseResultModel.badParam("用户名不能为空");
		}

		//查询用户信息
		Member member = memberMapper.selectByUserName(params.getUserName());
		if (member == null) {
			return BaseResultModel.badParam("用户不存在");
		}

		String email = null;//电子邮箱
		String type = member.getType();//1个人 2企业 3街道
		if("1".equals(type) || "3".equals(type)){
			User user = userMapper.selectByMemberId(member.getMemberId());
			if (user == null) {
				return BaseResultModel.badParam("用户不存在");
			}
			email = user.getEmail();//电子邮箱
		}else if("2".equals(type)){
			EnterpriseUserVO enterpriseUser = enterpriseUserMapper.selectByMemberId(member.getMemberId());
			if (enterpriseUser == null) {
				return BaseResultModel.badParam("企业用户不存在");
			}
			email = enterpriseUser.getEmail();//电子邮箱
		}

		//校验是否绑定邮箱
		if (StringUtils.isBlank(email)) {
			return BaseResultModel.badParam("账号未绑定邮箱");
		}


		//发送邮箱验证码
		SendEmailVerificationCodeParamModel sendEmailVerificationCode = new SendEmailVerificationCodeParamModel();
		sendEmailVerificationCode.setUserName(params.getUserName());
		sendEmailVerificationCode.setEmail(email);

		return mailService.sendEmailVerificationCode(sendEmailVerificationCode);
	}

	@Override
	public synchronized BaseResultModel resetPassword(RetrievePasswordParamModel params) {
		//校验非空-列表
		if(params == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if (StringUtils.isBlank(params.getUserName())) {
			return BaseResultModel.badParam("用户名不能为空");
		}

		//synchronized 改为redission针对账号队列锁 TODO


		//查询用户信息
		Member member = memberMapper.selectByUserName(params.getUserName());
		if (member == null) {
			return BaseResultModel.badParam("用户不存在");
		}

		//校验邮箱验证码
		CheckEmailVerificationCodeParamModel CheckParams = new CheckEmailVerificationCodeParamModel();
		CheckParams.setUserName(params.getUserName());
		CheckParams.setVerificationCode(params.getVerificationCode());
		BaseResultModel checkResult = mailService.checkEmailVerificationCode(CheckParams);
		log.info("checkResult:"+JSON.toJSONString(checkResult));
		if(200 != checkResult.getCode()){
			return checkResult;
		}

		log.info("修改密码----begin");
		//修改密码
		String salt = MD5Util.salt();
		String npassword = MD5Util.md5(params.getNewPassWord(), salt);
		member.setMemberId(member.getMemberId());
		member.setSalt(salt);
		member.setPassword(npassword);
		member.setUpdateTime(new Date());
		member.setUpdateUser(member.getMemberId());
		int count = memberMapper.updateByPrimaryKeySelective(member);

		return BaseResultModel.success(count);
	}

}
