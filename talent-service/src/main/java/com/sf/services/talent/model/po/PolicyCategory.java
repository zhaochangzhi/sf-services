package com.sf.services.talent.model.po;

import java.util.Date;

public class PolicyCategory {
    private String id;

    private String streeDistrict;

    private String policy;

    private String isOpen;

    private Date createDate;

    private String createUser;

    private Date updateDate;

    private String updateUser;
    
    private String value;
    
    private String policyValue;
    
    private String focusNews;
	
    private String userType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getStreeDistrict() {
        return streeDistrict;
    }

    public void setStreeDistrict(String streeDistrict) {
        this.streeDistrict = streeDistrict == null ? null : streeDistrict.trim();
    }

    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy == null ? null : policy.trim();
    }

    public String getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(String isOpen) {
        this.isOpen = isOpen == null ? null : isOpen.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getPolicyValue() {
		return policyValue;
	}

	public void setPolicyValue(String policyValue) {
		this.policyValue = policyValue;
	}

	public String getFocusNews() {
		return focusNews;
	}

	public void setFocusNews(String focusNews) {
		this.focusNews = focusNews;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
    
	
}