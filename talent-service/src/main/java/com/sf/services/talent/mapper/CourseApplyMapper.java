package com.sf.services.talent.mapper;

import com.sf.services.talent.model.CourseApplyListParamModel;
import com.sf.services.talent.model.po.CourseApply;
import com.sf.services.talent.model.vo.CourseApplyVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CourseApplyMapper {
    int deleteByPrimaryKey(String id);

    int insert(CourseApply record);

    int insertSelective(CourseApply record);

    CourseApply selectByPrimaryKey(String id);

    int selectPageListCount(Map<String, Object> searchMap);

    List<CourseApply> selectPageList(Map<String, Object> searchMap);

    int updateByPrimaryKeySelective(CourseApply record);

    int updateByPrimaryKey(CourseApply record);

    //查询列表
    List<CourseApplyVO> selectList(CourseApplyListParamModel params);

    //查询最后一条数据
    CourseApplyVO selectLast(@Param("applyMemberId") String applyMemberId);

    //根据id查询
    CourseApplyVO selectById(String id);

    //查询数量
    Integer selectCount(@Param("applyMemberId") String applyMemberId, @Param("auditStatus") Integer auditStatus);

    //通过courseId更新isdelete
    int updateIsdeleteByCourseId(@Param("courseId") String courseId, @Param("isdelete") Integer isdelete);
}