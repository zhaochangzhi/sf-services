package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

@Data
public class Article {
    private String id;

    private String websiteKey;
    private String websiteValue;

    private String webSectionId;
    private String webSectionName;

    private Integer webSectionLinkStatus;
    private String webSectionLinkId;

    private Integer typeStatus;

    private String title;

    private String author;

    private String keywords;

    private String datafrom;

    private String introduction;

    private String describetion;

    private String content;

    private String smallPhotoId;

    private String smallPhotoUrl;

    private String bigPhotoId;

    private String bigPhotoUrl;

    private String fileId;

    private String fileUrl;

    private Integer browseNum;

    private String link;

    private Integer isOn;

    private Integer sort;

    private String remark;

    private Date creatTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;



}