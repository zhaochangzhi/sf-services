package com.sf.services.talent.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.AreaParam;
import com.sf.services.talent.service.IAreaService;
import com.sf.services.talent.util.ResultUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(value = "地区", tags = "地区")
@RestController
@RequestMapping("/area")
public class AreaController {
	@Autowired
	private IAreaService areaService;
	
	@ApiOperation(notes = "地区列表", value="地区列表")
	@ApiImplicitParam(name = "params", value = "档案模型", required = false, dataType = "AreaParam")
	@PostMapping("/getArea")
	@IgnoreSecurity
	public ResultUtil list(@RequestBody AreaParam areaParam) {
		try {
			return ResultUtil.success(areaService.getArea(areaParam));
		} catch (Exception e) {
			log.error("地区列表异常:{}",e);
			return ResultUtil.error(401, "地区列表异常");
		}
		
	}
}

