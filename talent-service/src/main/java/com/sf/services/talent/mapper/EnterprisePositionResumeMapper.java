package com.sf.services.talent.mapper;

import com.sf.services.talent.model.EmploymentPositionResumeCollectionListParamModel;
import com.sf.services.talent.model.EmploymentPositionResumeListParamModel;
import com.sf.services.talent.model.EmploymentPositionResumeParamModel;
import com.sf.services.talent.model.po.EnterprisePositionResume;
import com.sf.services.talent.model.vo.EmploymentPositionResumeAndUserVO;
import com.sf.services.talent.model.vo.EmploymentPositionResumeVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EnterprisePositionResumeMapper {
    int deleteByPrimaryKey(String id);

    int insert(EnterprisePositionResume record);

    int insertSelective(EnterprisePositionResume record);

    EnterprisePositionResume selectByPrimaryKey(String id);

    int selectPageListCount(Map<String, Object> searchMap);

    List<EnterprisePositionResume> selectPageList(Map<String, Object> searchMap);

    int updateByPrimaryKeySelective(EnterprisePositionResume record);

    int updateByPrimaryKey(EnterprisePositionResume record);

    /**
     * 查询列表
     *
     * @param params 参数
     * @return 列表数据
     */
    List<EmploymentPositionResumeVO> selectPositionList(EmploymentPositionResumeParamModel params);

    /**
     * 查询已申请职位数量
     * @param enterprisePositionId
     * @param resumeId
     * @param memberId
     * @return
     */
    int selectApplyCount(@Param("enterprisePositionId") String enterprisePositionId, @Param("resumeId") String resumeId, @Param("memberId") String memberId);


    /**
     * 查询列表
     *
     * @param params 参数
     * @return 列表数据
     */
    List<EmploymentPositionResumeVO> selectList(EmploymentPositionResumeListParamModel params);

    /**
     * 查询收藏列表
     *
     * @param params 参数
     * @return 列表数据
     */
    List<EmploymentPositionResumeVO> selectCollectionList(EmploymentPositionResumeCollectionListParamModel params);

    /**
     * 企业中心-简历管理（企业收到的简历列表）
     *
     * @param enterpriseUserId 参数
     * @return 列表数据
     */
    List<EmploymentPositionResumeAndUserVO> selectPositionResumeList(@Param("enterpriseUserId") String enterpriseUserId);


    //查询企业收到的简历数据量
    Integer selectResumeNum(@Param("enterpriseUserId") String enterpriseUserId);
    
    /**
     * @Title: deleteByResumeId 
     * @Description: 简历id
     * @param resumeId
     * @return
     * @return: Integer
     */
    Integer deleteByResumeId(@Param("resumeId") String resumeId);

    //软删除-根据职位id
    int deleteByEnterprisePositionId(@Param("enterprisePositionId") String enterprisePositionId);

}