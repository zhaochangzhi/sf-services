package com.sf.services.talent.mapper;

import com.sf.services.talent.model.CourseTypeListParamModel;
import com.sf.services.talent.model.po.CourseType;
import com.sf.services.talent.model.vo.CourseTypeVO;

import java.util.List;

public interface CourseTypeMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(CourseType record);

    CourseType selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(CourseType record);


    //查询列表
    List<CourseTypeVO> selectList(CourseTypeListParamModel params);
}