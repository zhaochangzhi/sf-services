package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.config.ThreadLocalCache;
import com.sf.services.talent.mapper.*;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.po.EnterprisePosition;
import com.sf.services.talent.model.po.EnterpriseUser;
import com.sf.services.talent.model.po.Trade;
import com.sf.services.talent.model.vo.EnterpriseUserVO;
import com.sf.services.talent.service.IActLogService;
import com.sf.services.talent.service.IEnterpriseUserService;
import com.sf.services.talent.util.RequestUtils;
import com.sf.services.talent.util.UUIDUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @ClassName: EnterpriseService
 * @Description: 企业用户接口实现
 * @author: guchangliang
 * @date: 2021年7月23日 下午11:20:10
 */
@Slf4j
@Service
public class EnterpriseService implements IEnterpriseUserService {

	@Resource
	private EnterpriseUserMapper enterpriseUserMapper;
	@Resource
	private EnterprisePositionMapper enterprisePositionMapper;
	@Resource
	private EnterprisePositionCollectionMapper enterprisePositionCollectionMapper;
	@Resource
	private EnterprisePositionResumeMapper enterprisePositionResumeMapper;
	@Resource
	private DictionaryMapper dictionaryMapper;
	@Resource
	private TradeMapper tradeMapper;
	@Resource
	private MemberMapper memberMapper;
	@Resource
	private IActLogService actLogService;//日志


	@Override
	public EnterpriseUserVO getEnterpriseUser(EnterpriseUserGetParamModel params) {
		String memberId = null;
		if(params != null){
			memberId = params.getMemberId();
		}
		if(params == null || (StringUtils.isBlank(params.getId()) && StringUtils.isBlank(memberId))){
			memberId = RequestUtils.getCurrentUser().getUserId();
		}

		EnterpriseUserVO enterpriseUserVO = null;
		if(params != null && !StringUtils.isBlank(params.getId())){
			enterpriseUserVO = enterpriseUserMapper.selectById(params.getId());
		}else {
			enterpriseUserVO = enterpriseUserMapper.selectByMemberId(memberId);
		}

		if(enterpriseUserVO == null){
			return null;
		}

		//发布的职位数量
		int positionNum	= enterprisePositionMapper.selectPositionNum(enterpriseUserVO.getId());
		//被收藏的职位数量
		int collectionNum = enterprisePositionCollectionMapper.selectCollectionNum(enterpriseUserVO.getId());

		//收到的简历数量
		int resumeNum = enterprisePositionResumeMapper.selectResumeNum(enterpriseUserVO.getId());

		enterpriseUserVO.setPositionNum(positionNum);//发布的职位数量
		enterpriseUserVO.setCollectionNum(collectionNum);//企业职位被收藏数量
		enterpriseUserVO.setResumeNum(resumeNum);//收到的简历数量
		return enterpriseUserVO;
	}
	@Override
	public BaseResultModel update(EnterpriseUserParamModel params) {
		//校验非空
		if(params == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if(StringUtils.isBlank(params.getTradeTwoId())){
			return BaseResultModel.badParam("行业id不能为空");
		}

		//根据memberId或id校验企业账号信息
		//可以根据入参memberId或id查询企业账号信息
		String memberId = params.getMemberId();
		if(StringUtils.isBlank(memberId)){
			memberId = ThreadLocalCache.getUser();
		}
		EnterpriseUserVO enterpriseUserVO = null;
		if(!StringUtils.isBlank(params.getId())){
			//若传入id，则以id为准
			enterpriseUserVO = enterpriseUserMapper.selectById(params.getId());
		}else {
			enterpriseUserVO = enterpriseUserMapper.selectByMemberId(memberId);
		}

		if(enterpriseUserVO == null){
			return BaseResultModel.badParam("企业不存在");
		}
		String id = enterpriseUserVO.getId();


		//校验行业
		Trade trade = tradeMapper.selectByPrimaryKey(params.getTradeTwoId());
		if (trade == null) {
			return BaseResultModel.badParam("行业id不存在");
		}
		if (trade.getDataLevel() != 2) {
			return BaseResultModel.badParam("行业请选择二级数据");
		}

		EnterpriseUser enterpriseUser = new EnterpriseUser();
		BeanUtils.copyProperties(params, enterpriseUser);

		enterpriseUser.setId(id);//重新赋值id
		enterpriseUser.setTradeId(params.getTradeTwoId());//赋值职位id
		enterpriseUser.setUpdateTime(new Date());
		enterpriseUser.setUpdateUser(ThreadLocalCache.getUser());

		int count = enterpriseUserMapper.updateByPrimaryKeySelective(enterpriseUser);

		//保存操作日志
		try {
			actLogService.save(params.getMemberId(), "编辑企业用户", enterpriseUser.getEnterpriseName());
		} catch (Exception e) {
			log.error("插入日志异常:{}",e);
		}

		return BaseResultModel.success(count);
	}

	@Override
	public BaseResultModel updateIsfamous(EnterpriseUserUpdateIsfamousParamModel params, String userId) {
		//校验非空
		if(params == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if(StringUtils.isBlank(params.getId())){
			return BaseResultModel.badParam("企业账号id不能为空");
		}
		//校验是否为名企
		if(params.getIsfamous() == null){
			return BaseResultModel.badParam("是否为名企不能为空");
		}
		if(params.getIsfamous() != 0 && params.getIsfamous() != 1){
			return BaseResultModel.badParam("是否为名企不正确");
		}

		EnterpriseUser enterpriseUser = new EnterpriseUser();
		BeanUtils.copyProperties(params, enterpriseUser);

		enterpriseUser.setUpdateTime(new Date());
		enterpriseUser.setUpdateUser(userId);

		int count = enterpriseUserMapper.updateByPrimaryKeySelective(enterpriseUser);
		return BaseResultModel.success(count);
	}

	@Override
	public BaseResultModel enterpriseUserInit(String memberId, String createUser) {

		if(enterpriseUserMapper.selectByMemberId(memberId) != null){
			return BaseResultModel.badParam("已初始化");
		}

		try{
			EnterpriseUser enterpriseUser = new EnterpriseUser();
			enterpriseUser.setId(UUIDUtil.getUUid());
			enterpriseUser.setMemberId(memberId);
			enterpriseUser.setIsfamous(0);//是否为著名企业（0：否，1：是）
			enterpriseUser.setCreateUser(createUser);
			enterpriseUser.setCreatTime(new Date());
			enterpriseUserMapper.insertSelective(enterpriseUser);
		} catch(DuplicateKeyException e){
			//唯一键UNIQUE重复
			return BaseResultModel.badParam("已初始化");
		}

		return BaseResultModel.success(1);
	}

	@Override
	public PageInfo<EnterpriseUserVO> getList(EnterpriseUserListParamModel params) {

		if(params == null){
			return null;
		}
		//分页处理
		PageHelper.startPage(params.getPageNum(), params.getPageSize());
		List<EnterpriseUserVO> list = enterpriseUserMapper.selectList(params);


		return new PageInfo<>(list);
	}
	@Override
	@Transactional
	public int delEnterpriseUser(String memberId, String updateUser) {
		EnterpriseUserVO enterpriseUser = enterpriseUserMapper.selectByMemberId(memberId);

		memberMapper.deleteByPrimaryKey(memberId);
		int count = enterpriseUserMapper.delEnterpriseUser(memberId);

		//保存操作日志
		try {
			actLogService.save(updateUser, "删除企业用户", enterpriseUser.getEnterpriseName());
		} catch (Exception e) {
			log.error("插入日志异常:{}",e);
		}

		return count;
	}

	@Override
	public BaseResultModel batchDelete(String ids, String updateUser) {

		int count = 0;
		List<String> idList = Arrays.asList(ids.split(","));
		for (int i = 0; i < idList.size(); i++) {
			count = count + delEnterpriseUser(idList.get(i), updateUser);
		}

		return BaseResultModel.success(count);
	}


}

