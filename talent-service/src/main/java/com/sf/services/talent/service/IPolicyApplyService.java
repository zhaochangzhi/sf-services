package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.PolicyApplyModel;
import com.sf.services.talent.model.po.PolicyApply;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @ClassName: IPolicyApplyService 
 * @Description: 政策申请接口
 * @author: heyang
 * @date: 2021年7月13日 下午10:24:34
 */
public interface IPolicyApplyService {
	
	/**
	 * @Title: next 
	 * @Description: 下一步
	 * @param policyApply
	 * @return
	 * @return: boolean
	 */
	Map<String,String> next(PolicyApplyModel policyApply);
	
	/**
	 * @Title: apply 
	 * @Description: 申请
	 * @param policyApply
	 * @return
	 * @return: int
	 */
	int apply(PolicyApplyModel policyApply);
	
	/**
	 * @Title: audit 
	 * @Description: 审核
	 * @param policyApply
	 * @return
	 * @return: int
	 */
	int audit(PolicyApplyModel policyApply);
	
	/**
	 * @Title: list 
	 * @Description: 分页列表
	 * @param record
	 * @return
	 * @return: PageInfo<PolicyApply>
	 */
	PageInfo<PolicyApply> list(PolicyApplyModel record);

	/**
	 * 导出列表
	 * @param record
	 */
	void exportList(PolicyApplyModel record, HttpServletRequest request, HttpServletResponse response);
	
	/**
	 * @Title: getNewestDetail 
	 * @Description: 获取详情
	 * @return
	 * @return: Map<String,Object>
	 */
	Map<String, Object> getNewestDetail();


	
}

