package com.sf.services.talent.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.TimeUnit;
/**
 * redicache 工具类
 * 
 */
@SuppressWarnings("unchecked")
@Component
public class RedisUtils {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /*
     *指定缓存实习时间
     */
    public Boolean expire(String key, long time){
        try {
            if (time > 0){
                redisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
            return true;

        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /*
     *获取过期时间 判断key是否存在
     * 返回0代表永久有效
     */
    public long getExpire(String key){
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 判断key是否存在
     */
    public boolean hasKey(String key){
        try {
            return redisTemplate.hasKey(key);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除缓存 key可以传多个或者一个
     */
    public void delKey(String... key){
        if (key != null && key.length > 0){
            if (key.length == 1){
                redisTemplate.delete(key[0]);
            }else {
                for (int i = 0; i < key.length; i++){
                    redisTemplate.delete(key[i]);
                }
            }
        }
    }

    /**
     * 普通缓存获取
     */
    public Object getObject(String key){
        return key == null ? null : redisTemplate.opsForValue().get(key);
    }

    /**
     * 普通缓存存入
     */
    public boolean setObject(String key, Object value){
        try{
            redisTemplate.opsForValue().set(key, value);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 普通缓存存入并设置时间
     * 如果time小于或等于0则设置无限期
     */
    public boolean setObjectTime(String key, Object value, long time){
        try{
            if (time > 0){
                redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            }else {
                redisTemplate.opsForValue().set(key, value);
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     *递增
     * key 键
     * data 值 要增加几大于0
     */
    public long incr(String key, long data){
        if (data < 0){
            throw new RuntimeException("递增因子必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, data);
    }

    /**
     *递减
     * key 键
     * data 值 要减少几大于0
     */
    public long decr(String key, long data){
        if (data < 0){
            throw new RuntimeException("递减因子必须大于0");
        }
        return redisTemplate.opsForValue().decrement(key, -data);
    }

    /**
     * 获取hashMap
     * key 键
     * item 项
     */
    public Object hashGet(String key, String item){
        return redisTemplate.opsForHash().get(key, item);
    }

    /**
     * 获取hashMap对应的所有键值
     * key 键
     */
    public Map<Object, Object> hashGetAll(String key){
        return redisTemplate.opsForHash().entries(key);
    }


    /**
     * 存储hashMap
     * key 键
     * item 项
     */
    public boolean hashSet(String key, Map<String, Object> map){
        try{
            redisTemplate.opsForHash().putAll(key, map);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 存入hash并设置时间
     */
    public boolean hashSetTime(String key, Map<String, Object> map, long time){
        try{
            redisTemplate.opsForHash().putAll(key, map);
            if (time > 0){
                expire(key, time);
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
    
}
