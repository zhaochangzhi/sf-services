package com.sf.services.talent.model.po;

import java.util.Date;

/**
 * @ClassName: BaseEntity
 * @Description: 基础类
 * @author: heyang
 * @date: 2021年7月10日 下午3:40:36
 */
public class BaseEntity {
	
	private Date createDate;
	
	private String createUser;
	
	private Date updateDate;
	
	private String updateUser;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
	
	
}
