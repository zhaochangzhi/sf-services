package com.sf.services.talent.service.impl;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sf.services.talent.mapper.AreaMapper;
import com.sf.services.talent.model.AreaParam;
import com.sf.services.talent.model.po.Area;
import com.sf.services.talent.service.IAreaService;

@Service
public class AreaService implements IAreaService {
	
	@Autowired
	private AreaMapper areaMapper;
	
	@Override
	public List<Area> getArea(AreaParam areaParam) {
		Area area = new Area();
		BeanUtils.copyProperties(areaParam, area);
		return areaMapper.getArea(area);
	}
}

