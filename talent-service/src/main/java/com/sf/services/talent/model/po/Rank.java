package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

/**
 * Rank
 *
 * @author zhaochangzhi
 * @date 2021/7/11
 */
@Data
public class Rank {

    private Integer id;

    private String rankCode;

    private String rankName;

    private String address;

    private String attachmentId;

    private String createUser;

    private Date createTime;

    private String updateUser;

    private Date updateTime;
}
