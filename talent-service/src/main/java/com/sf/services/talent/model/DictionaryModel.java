package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "数据字典", description = "数据字典")
public class DictionaryModel extends PageParamModel {
	
	@ApiModelProperty(value = "主键id", example = "49f36f829ce6449294ad1f19e2a6aa59")
	private String id;
	@ApiModelProperty(value = "字典类型", example = "dicType")
	private String type;
	@ApiModelProperty(value = "字典key", example = "1")
	private String dickey;
	@ApiModelProperty(value = "字典vlaue", example = "创业担保贷款")
	private String value;
	@ApiModelProperty(value = "备注", example = "备注")
	private String memo;
	@ApiModelProperty(value = "排序字段", example = "1")
	private Integer sort;
}
