package com.sf.services.talent.controller;

import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.exceptions.TalentException;
import com.sf.services.talent.service.IFileService;
import com.sf.services.talent.util.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.UUID;

@Slf4j
@Api(value = "文件", tags = "文件")
@RestController
@RequestMapping("/file")
public class FileController {
	@Autowired
	private IFileService fileService;
	@Value("${file.path}")
	private String filePath;
	@Value("${file.url}")
	private String fileUrl;

	/**
	 * @Title: upload
	 * @Description: 文件上传接口
	 * @param file 文件
	 * @return 结果
	 */
	@ApiOperation(notes = "文件上传", value = "文件上传")
	@PostMapping("/upload")
	public ResultUtil upload(@RequestParam("file") MultipartFile file) {
		com.sf.services.talent.model.po.File file1 = new com.sf.services.talent.model.po.File();
		// 获取原始名字
		String fileName = file.getOriginalFilename();
		file1.setOriginalFileName(fileName);
		// 获取后缀名
		String suffixName = fileName.substring(fileName.lastIndexOf("."));
		file1.setSuffixName(suffixName);
		// 文件保存路径
		// 文件重命名，防止重复
		String system = System.getProperty("os.name");
		String newFileName = UUID.randomUUID() + fileName;
	    if (system.indexOf("Windows") >= 0) {
	    	fileName = "D:/update/" + newFileName;
	    } else {
	    	fileName = filePath + newFileName;
	    }
		file1.setFilePath(fileName);
		file1.setFileUrl(fileUrl+newFileName);
		// 文件对象
		File dest = new File(fileName);
		// 判断路径是否存在，如果不存在则创建
		if (!dest.getParentFile().exists()) {
			dest.getParentFile().mkdirs();
		}
		try {
			// 保存到服务器中
			file.transferTo(dest);

			String fileId = fileService.upload(file1);
			file1.setFileId(fileId);

			return ResultUtil.success(file1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResultUtil.error(401, "上传失败");
	}
	/**
	 * @Title: download
	 * @Description: 文件下载接口
	 * @param fileId
	 * @return: String
	 * @throws IOException
	 */
	@ApiOperation(notes = " 文件下载", value = " 文件下载")
	@GetMapping("/download")
	@IgnoreSecurity
	public void download(@RequestParam("fileId") String fileId, HttpServletResponse response) {
		com.sf.services.talent.model.po.File localFile = fileService.download(fileId);

		if (localFile != null) {
			//设置文件路径
			File file = new File(localFile.getFilePath());
			String fileName = null;
			try {
				fileName = URLEncoder.encode(localFile.getOriginalFileName(), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

			if (file.exists()) {
				response.setHeader("Content-Type", "application/octet-stream;charset=utf-8");
				response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
				byte[] buffer = new byte[1024];
				BufferedInputStream bis = null;
				try {
					bis = new BufferedInputStream(new FileInputStream(file));
					OutputStream os = response.getOutputStream();
					int i = bis.read(buffer);
					while (i != -1) {
						os.write(buffer, 0, i);
						i = bis.read(buffer);
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (bis != null) {
						try {
							bis.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			} else {
				//文件不存在
				throw new TalentException(400, "文件不存在");
			}
		}
	}
}
