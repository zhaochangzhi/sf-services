package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.vo.ArticleFloatBannerVO;
import com.sf.services.talent.model.vo.ArticleVO;
import com.sf.services.talent.model.vo.ArticleWebSectionLinkVO;

/**
 * @ClassName: IArticleUserService
 * @Description: 文章管理
 * @author: guchangliang
 * @date: 2021年8月1日 下午11:19:07
 */
public interface IArticleService {

    /**
     * 查询列表
     * @param param
     * @return
     */
    PageInfo<ArticleVO> list(ArticleParamModel param);

    /**
     * 查询
     * @param id
     * @return
     */
    ArticleVO get(String id);

    /**
     * 添加
     * @param param
     * @param createUser
     * @return
     */
    BaseResultModel add(ArticleAddParamModel param,String createUser);

    /**
     * 编辑
     * @param param
     * @param updateUser
     * @return
     */
    BaseResultModel update(ArticleUpdateParamModel param, String updateUser);

    /**
     * 编辑-上架/下架
     * @param param
     * @param updateUser
     * @return
     */
    BaseResultModel updateIsOn(ArticleUpdateIsOnParamModel param, String updateUser);

    /**
     * 删除（软删除）
     * @param id
     * @param updateUser
     * @return
     */
    BaseResultModel delete(String id, String updateUser);

    /**
     * 批量删除（软删除）
     * @param ids
     * @param updateUser
     * @return
     */
    BaseResultModel batchDelete(String ids, String updateUser);

    /**
     * 查询漂浮banner信息
     * @return
     */
    ArticleFloatBannerVO articleFloatBannerGet();


    /**
     * 查询栏目关联-列表
     * @param param
     * @return
     */
    PageInfo<ArticleWebSectionLinkVO> webSectionLinkList(ArticleWebSectionLinkListParamModel param);

    /**
     * 栏目关联-详情
     * @param param
     * @return
     */
    BaseResultModel webSectionLinkGet(GetOneParamModel param);

    /**
     * 栏目关联-添加
     * @param param
     * @param createUser
     * @return
     */
    BaseResultModel webSectionLinkAdd(ArticleWebSectionLinkAddParamModel param, String createUser);

    /**
     * 栏目关联-编辑
     * @param param
     * @param updateUser
     * @return
     */
    BaseResultModel webSectionLinkUpdate(ArticleWebSectionLinkUpdateParamModel param, String updateUser);


    /**
     * 栏目关联-删除（软删除）
     * @param id
     * @param updateUser
     * @return
     */
    BaseResultModel webSectionLinkDelete(String id, String updateUser);

    /**
     * 栏目关联-批量删除（软删除）
     * @param ids
     * @param updateUser
     * @return
     */
    BaseResultModel webSectionLinkBatchDelete(String ids, String updateUser);

    /**
     * 栏目关联-更新文章关联栏目标识（0：未开启，1：已开启）
     * @param param
     * @param createUser
     * @return
     */
    BaseResultModel webSectionUpdateArticleWebSectionLinkStatus(updateArticleWebSectionLinkStatusParamModel param, String createUser);
}