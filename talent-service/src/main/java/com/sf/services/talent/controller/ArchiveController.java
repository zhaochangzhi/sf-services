package com.sf.services.talent.controller;

import com.sf.services.talent.annotations.CurrentUser;
import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.exceptions.TalentException;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.dto.UserDto;
import com.sf.services.talent.model.po.Archive;
import com.sf.services.talent.model.vo.ArchiveVO;
import com.sf.services.talent.service.IArchiveService;
import com.sf.services.talent.util.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 档案
 * @author hy
 *
 */
@Slf4j
@Api(value = "档案", tags = "档案")
@RestController
@RequestMapping("/archive")
public class ArchiveController {
	
	@Autowired
	private IArchiveService archiveService;
	
	/**
	 * 档案列表
	 * @param userName
	 * @param password
	 * @return
	 */
	@ApiOperation(notes = "档案列表", value="档案列表")
	@ApiImplicitParam(name = "params", value = "档案模型", required = false, dataType = "ArchiveVO")
	@PostMapping("/list")
	public ResultUtil list(@RequestBody ArchiveVO archiveVO) {
		try {
			return ResultUtil.success(archiveService.list(archiveVO));
		} catch (Exception e) {
			log.error("档案列表异常:{}",e);
			return ResultUtil.error(401, "档案列表异常");
		}
		
	}
	
	/**
	 * 档案详情
	 * @param user
	 * @return
	 */
	@ApiOperation(notes = "档案详情", value="档案详情")
	@ApiImplicitParam(name = "params", value = "主键id", required = false, dataType = "ArchiveVO")
	@PostMapping("/detail")
	public ResultUtil detail(@RequestBody ArchiveVO archiveVO) {
		try {
			return ResultUtil.success(archiveService.detail(archiveVO.getId()));
		}catch (Exception e) {
			log.error("档案详情异常:{}",e);
			return ResultUtil.error(401, "档案详情异常");
		}
		
	}
	/**
	 * 档案插入
	 * @param archiveVO
	 * @return
	 */
	@ApiOperation(notes = "档案插入", value="档案插入")
	@ApiImplicitParam(name = "params", value = "档案模型", required = false, dataType = "ArchiveVO")
	@PostMapping("/insert")
	public ResultUtil insert(@RequestBody ArchiveVO archiveVO) {
		try {
			// 插入档案时,按身份证号查重,如果已经存在,不允许再插入
			Archive existArchive =  archiveService.selectByOwnerId(archiveVO.getOwnerId());
			if(existArchive!=null)
			{
				return ResultUtil.error(400, "身份证号码已存在，不能重复录入");
			}
			archiveService.insert(archiveVO);
			return ResultUtil.success();
		}catch(TalentException te){
			return ResultUtil.error(te.getCode(), te.getMessage());
		}catch (Exception e) {
			log.error("档案插入异常:{}",e);
			return ResultUtil.error(401, "档案插入异常");
		}
		
	}
	
	/**
	 * 档案修改
	 * @param archiveVO
	 * @return
	 */
	@ApiOperation(notes = "档案修改", value="档案修改")
	@ApiImplicitParam(name = "params", value = "档案模型", required = false, dataType = "ArchiveVO")
	@PostMapping("/update")
	public ResultUtil update(@RequestBody ArchiveVO archiveVO) {
		try {
			archiveService.update(archiveVO);
			return ResultUtil.success();
		}catch(TalentException te){
			return ResultUtil.error(te.getCode(), te.getMessage());
		}catch (Exception e) {
			log.error("档案修改异常:{}",e);
			return ResultUtil.error(401, "档案修改异常");
		}
		
	}
	
	/**
	 * 档案删除
	 * @param id
	 * @return
	 */
	@ApiOperation(notes = "档案删除", value="档案删除")
	@ApiImplicitParam(name = "params", value = "档案模型", required = false, dataType = "ArchiveVO")
	@PostMapping("/delete")
	public ResultUtil delete(@RequestBody ArchiveVO archiveVO) {
		try {
			archiveService.delete(archiveVO.getId());
			return ResultUtil.success();
		}catch(TalentException te){
			return ResultUtil.error(te.getCode(), te.getMessage());
		}catch (Exception e) {
			log.error(" 档案删除:{}",e);
			return ResultUtil.error(401, " 档案删除异常");
		}
		
	}
	
	/**
	 * 档案批量删除
	 * @param id
	 * @return
	 */
	@ApiOperation(notes = "档案批量删除", value="档案批量删除")
	@PostMapping("/bashDel")
	public ResultUtil delete(@RequestBody Map<String,String> param) {
		try {
			String ids = param.get("ids");
			archiveService.bashDel(ids.split(","));
			return ResultUtil.success();
		}catch(TalentException te){
			return ResultUtil.error(te.getCode(), te.getMessage());
		}catch (Exception e) {
			log.error(" 档案删除:{}",e);
			return ResultUtil.error(401, " 档案删除异常");
		}
		
	}
	@ApiOperation(notes = "根据参数查询档案", value="根据参数查询档案")
	@ApiImplicitParam(name = "params", value = "档案模型", required = false, dataType = "ArchiveVO")
	@PostMapping("/getDetailByParam")
	@IgnoreSecurity
	public ResultUtil selectDetailByParam(@RequestBody ArchiveVO archiveVO) {
		try {
			return ResultUtil.success(archiveService.selectDetailByParam(archiveVO));
		}catch(TalentException te){
			return ResultUtil.error(te.getCode(), te.getMessage());
		}catch (Exception e) {
			log.error(" 查询档案:{}",e);
			return ResultUtil.error(401, " 查询档案异常");
		}
		
	}

	@ApiOperation(value = "导入档案Excel", notes = "导入档案Excel")
	@PostMapping("/importExcel")
	public BaseResultModel importExcel(@RequestParam("file") MultipartFile file, @CurrentUser UserDto currentUser) {

		return archiveService.importExcel(file,currentUser.getUserId());
	}

	@IgnoreSecurity
	@ApiOperation(value = "导出档案Excel", notes = "导出档案Excel")
	@RequestMapping(value="/exportList",method = { RequestMethod.GET })
	public void exportList(HttpServletRequest request, HttpServletResponse response) {

		archiveService.exportList(request, response);
	}

}
