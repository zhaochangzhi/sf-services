package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * RankVO
 *
 * @author zhaochangzhi
 * @date 2021/7/11
 */
@ApiModel(value = "RankVO", description = "职称实体")
@Data
public class RankVO {

    @ApiModelProperty(value = "id", example = "1")
    private Integer id;

    @ApiModelProperty(value = "职称码")
    private String rankCode;

    @ApiModelProperty(value = "职称名称")
    private String rankName;

    @ApiModelProperty(value = "附件id")
    private String attachmentId;

    @ApiModelProperty(value = "文件名称")
    private String originalFileName;

    @ApiModelProperty(value = "办理地址")
    private String address;
}
