package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.config.ThreadLocalCache;
import com.sf.services.talent.mapper.FileMapper;
import com.sf.services.talent.mapper.PolicyApplyMapper;
import com.sf.services.talent.mapper.PolicyCategoryMapper;
import com.sf.services.talent.model.PolicyApplyModel;
import com.sf.services.talent.model.po.File;
import com.sf.services.talent.model.po.PolicyApply;
import com.sf.services.talent.model.po.PolicyCategory;
import com.sf.services.talent.model.vo.JobFairVO;
import com.sf.services.talent.service.IActLogService;
import com.sf.services.talent.service.IPolicyApplyService;
import com.sf.services.talent.util.ExportExcelUtil;
import com.sf.services.talent.util.FillUserInfoUtil;
import com.sf.services.talent.util.RequestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Service
public class PolicyApplyService implements IPolicyApplyService {

	@Resource
	private PolicyCategoryMapper policyCategoryMapper;

	@Resource
	private PolicyApplyMapper policyApplyMapper;
	
	@Resource
	private FileMapper fileMapper;
	
	@Autowired
	private IActLogService actLogService;


	
	@Override
	public Map<String,String> next(PolicyApplyModel policyApply) {
		Map<String, String> result = new HashMap<>();
		PolicyCategory policyCategory = new PolicyCategory();
		policyCategory.setIsOpen("Y");
		policyCategory.setPolicy(policyApply.getPolicy());
		policyCategory.setStreeDistrict(policyApply.getStreetDistrict());
		String userType = RequestUtils.getCurrentUser().getType();
		policyCategory.setUserType(userType);
		List<PolicyCategory> list = policyCategoryMapper.selectAll(policyCategory);
		if(list!=null && list.size()>0) {
			result.put("cateogryId", list.get(0).getId());
			result.put("focusNews", list.get(0).getFocusNews());
			return result;
		}
		return null;
	}
	@Override
	public int apply(PolicyApplyModel record) {
		PolicyApply policyApply = new PolicyApply();
		String memberId = ThreadLocalCache.getUser();
		BeanUtils.copyProperties(record, policyApply);
		policyApply.setFileId(StringUtils.join(record.getFileId(),","));
		policyApply.setApplyMemberId(memberId);
		policyApply.setAppointmentTime(new Date());
		FillUserInfoUtil.fillCreateUserInfo(policyApply);
		actLogService.save(memberId, "政策申报申请", "");
		return policyApplyMapper.insertSelective(policyApply);
	}
	@Override
	public int audit(PolicyApplyModel record) {
		String memberId = ThreadLocalCache.getUser();
		PolicyApply policyApply = new PolicyApply();
		BeanUtils.copyProperties(record, policyApply);
		FillUserInfoUtil.fillUpdateUserInfo(policyApply);
		policyApply.setAuditMemberId(memberId);
		return policyApplyMapper.updateByPrimaryKeySelective(policyApply);
	}
	@Override
	public PageInfo<PolicyApply> list(PolicyApplyModel record) {
		PolicyApply policyCategory = new PolicyApply();
		BeanUtils.copyProperties(record, policyCategory);
		PageHelper.startPage(record.getPageNum(), record.getPageSize());
		List<PolicyApply> list = policyApplyMapper.selectAll(policyCategory);
		 if (!list.isEmpty()) {
	            list.stream().forEach(policyApply -> {
	            	 List<File> fileList = fileMapper.selectByIds(policyApply.getFileId().split(","));
	            	 policyApply.setFiles(fileList);
	            });
	        }
		return new PageInfo<>(list);
	}

	@Override
	public void exportList(PolicyApplyModel record, HttpServletRequest request, HttpServletResponse response) {
		//查询列表
		PolicyApply policyCategory = new PolicyApply();
		BeanUtils.copyProperties(record, policyCategory);
		List<PolicyApply> list = policyApplyMapper.selectAll(policyCategory);
		if (list == null || list.size() == 0) {
			return;
		}

		//导出列表
		List<Map<String, Object>> dataMapList = new ArrayList<Map<String, Object>>();
		Map<String, Object> headMap = new LinkedHashMap<String, Object>();

		headMap.put("rowNum", "序号");
		headMap.put("streetValue", "所属街道");
		headMap.put("policyValue", "政策类别");
		headMap.put("applyMainType", "申报主体类型");
		headMap.put("name", "申报主体名称");
		headMap.put("applicantPhone", "联系电话");
		headMap.put("identityOrCompanyNo", "身份证号/统一社会信用号码");
//		headMap.put("companyName", "企业名");

		// excel导出内容
		for (int i = 0; i < list.size(); i++) {
			Map<String, Object> data = new HashMap<String, Object>();

			data.put("rowNum", i+1);//序号
			data.put("streetValue", list.get(i).getStreetValue());//所属街道
			data.put("policyValue", list.get(i).getPolicyValue());//政策类别
			//申报主体类型，公司名有值为公司，无值为个人
			if(StringUtils.isBlank(list.get(i).getCompanyName())){
				//个人
				data.put("applyMainType", "个人");//申报主体类型
			}else{
				//企业
				data.put("applyMainType", "企业");//申报主体类型
			}
			//申报主体名称
			if(StringUtils.isBlank(list.get(i).getCompanyName())){
				//个人
				data.put("name", list.get(i).getUserName());//申报主体名称
			}else{
				//企业
				data.put("name", list.get(i).getCompanyName());//企业名
			}
			data.put("applicantPhone", list.get(i).getApplicantPhone());//手机号
			//身份证号/统一社会信用号码
			if(StringUtils.isBlank(list.get(i).getCompanyName())){
				//个人
				data.put("identityOrCompanyNo", list.get(i).getIdentity());//身份证号
			}else{
				//企业
				data.put("identityOrCompanyNo", list.get(i).getCompanyNo());//统一社会信用号码
			}




			dataMapList.add(data);
		}

		ExportExcelUtil export = new ExportExcelUtil();
		String fileName = "政策申请列表";//文件名
		String sheetName = "政策申请列表";//页签名
		export.toExcelForStream(request, response, headMap, dataMapList,  fileName, sheetName);

	}

	@Override
	public Map<String, Object> getNewestDetail() {
		String memberId = ThreadLocalCache.getUser();
		return policyApplyMapper.getNewestDetail(memberId);
	}
}

