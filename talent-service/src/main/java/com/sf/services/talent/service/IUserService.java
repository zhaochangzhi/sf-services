package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.UserParamModel;
import com.sf.services.talent.model.po.User;
import com.sf.services.talent.model.vo.UserStreetVO;

/**
 * @ClassName: IUserService 
 * @Description: 用户接口
 * @author: heyang
 * @date: 2021年7月11日 下午11:19:07
 */
public interface IUserService {
	/**
	 * @Title: getUser 
	 * @Description: 获取用户
	 * @return
	 * @return: User
	 */
	User getUser();
	
	/**
	 * @Title: update 
	 * @Description: 添加用户
	 * @param userParamModel
	 * @return
	 * @return: int
	 */
	int update(UserParamModel userParamModel);

	/**
	 * 获取用户街道key
	 * @return
	 */
	UserStreetVO getUserStreet(String memberId);
	
	/**
	 * @Title: list 
	 * @Description: 用户列表
	 * @param record
	 * @return
	 * @return: List<User>
	 */
	PageInfo<User> list(UserParamModel record);
	
	/**
	 * @Title: getUserDetail 
	 * @Description: 用户详情
	 * @param id
	 * @return
	 * @return: User
	 */
	User getUserDetail(String id);
	
	/**
	 * @Title: registUser 
	 * @Description: 注册用户
	 * @param record
	 * @return: void
	 */
	void registUser(UserParamModel record);
	
	/**
	 * @Title: delUser 
	 * @Description: 删除用户
	 * @param memberId
	 * @return
	 * @return: int
	 */
	int delUser(String memberId);

	/**
	 * 批量删除
	 * @param ids
	 * @param updateUser
	 * @return
	 */
	BaseResultModel batchDelete(String ids, String updateUser);
}

