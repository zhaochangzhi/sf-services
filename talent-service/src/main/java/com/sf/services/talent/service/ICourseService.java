package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.vo.CourseApplyVO;
import com.sf.services.talent.model.vo.CourseTypeVO;
import com.sf.services.talent.model.vo.CourseVO;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Description: 课程
 * @author: guchangliang
 * @date: 2021/8/13
 */
public interface ICourseService {

    /**
     * 获取列表
     * @param params 参数
     * @return 列表
     */
    PageInfo<CourseVO> getList(CourseListParamModel params);

    /**
     * 获取列表
     * @return 列表
     */
    PageInfo<CourseTypeVO> getTypeList(CourseTypeListParamModel params);

    /**
     * 创建课程类型
     * @param params 参数
     * @param userId
     * @return
     */
    BaseResultModel typeAdd(CourseTypeAddParamModel params, String userId);

    /**
     * 更新课程
     * @param params 参数
     * @param userId
     * @return
     */
    BaseResultModel typeUpdate(CourseTypeUpdateParamModel params, String userId);

    /**
     * 删除课程类型（软删除）
     * @param id
     * @param updateUser
     * @return
     */
    BaseResultModel typeDelete(String id, String updateUser);

    /**
     * 创建课程
     * @param params 参数
     * @param userId
     * @return
     */
    BaseResultModel add(CourseAddParamModel params
            , String userId);

    /**
     * 更新课程
     * @param params 参数
     * @param userId
     * @return
     */
    BaseResultModel update(CourseUpdateParamModel params
            , String userId);

    /**
     * 获取申请课程列表
     * @param params 参数
     * @return 列表
     */
    PageInfo<CourseApplyVO> getCourseApplyList(CourseApplyListParamModel params);

    /**
     * 获取申请课程记录详情
     * @param id 参数
     * @return 列表
     */
    BaseResultModel getCourseApply(String id);


    /**
     * 报名课程
     * @param params 参数
     * @param userId
     * @return
     */
    BaseResultModel courseApplyAdd(CourseApplyAddParamModel params
            , String userId);

    /**
     * 课程报名-审核
     * @param params 参数
     * @param userId
     * @return
     */
    BaseResultModel courseApplyAudit(CourseApplyAuditParamModel params
            , String userId);

    /**
     * 获取课程报名Excel内容
     * @param file
     * @param createUser
     * @return
     */
    BaseResultModel getCourseApplyExcelMessage(MultipartFile file);

    /**
     * 查询最后一条数据
     * @param createUser 参数
     * @return 列表
     */
    BaseResultModel getLast(String createUser);


    /**
     * 删除（软删除）
     * @param id
     * @param updateUser
     * @return
     */
    BaseResultModel delete(String id, String updateUser);


}

