package com.sf.services.talent.service;

import com.sf.services.talent.model.RankMajorParamModel;
import com.sf.services.talent.model.po.RankMajor;

import java.util.List;

/**
 * IRankMajorService
 *
 * @author zhaochangzhi
 * @date 2021/7/20
 */
public interface IRankMajorService {


    /**
     * 获取列表
     *
     * @param params 参数
     * @return 列表
     */
    List<RankMajor> getList(RankMajorParamModel params);


}
