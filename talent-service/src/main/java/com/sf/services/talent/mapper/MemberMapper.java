package com.sf.services.talent.mapper;

import com.sf.services.talent.model.po.Member;

public interface MemberMapper {
    int deleteByPrimaryKey(String memberId);

    int insert(Member record);

    int insertSelective(Member record);

    Member selectByPrimaryKey(String memberId);

    int updateByPrimaryKeySelective(Member record);

    int updateByPrimaryKey(Member record);
    
    Member selectByUserName(String userName);
}