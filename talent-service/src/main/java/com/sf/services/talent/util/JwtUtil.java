package com.sf.services.talent.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.bouncycastle.util.encoders.Base64;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Date;

public class JwtUtil {

	/**
     * 签发JWT
     * 
     * @param id
     * @param subject   可以是JSON数据 尽可能少
     * @param ttlMillis
     * @return String
     *
     */
    public static String createJWT(String id, String subject, long ttlMillis) {
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        SecretKey secretKey = generalKey();
        JwtBuilder builder = Jwts.builder().setId(id).setSubject(subject) // 主题
                .setIssuer("user") // 签发者
                .setIssuedAt(now) // 签发时间
                .signWith(signatureAlgorithm, secretKey); // 签名算法以及密匙
        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date expDate = new Date(expMillis);
            builder.setExpiration(expDate); // 过期时间
        }
        return builder.compact();
    }

    /**
     * 验证JWT
     * 
     * @param jwtStr
     * @return
     */
    public static ResultUtil validateJWT(String jwtStr) {
        Claims claims = null;
        try {
            claims = parseJWT(jwtStr);
            return ResultUtil.success(claims);
        } catch (Exception e) {
        	 return ResultUtil.error(201, "验签失败");
        }
    }

    public static SecretKey generalKey() {
        byte[] encodedKey = Base64.decode("llsf");
        SecretKey key = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
        return key;
    }

    /**
     * 
     * 解析JWT字符串
     * 
     * @param jwt
     * @return
     * @throws Exception
     */
    public static Claims parseJWT(String jwt) throws Exception {
        SecretKey secretKey = generalKey();
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(jwt).getBody();
    }

//    public static void main(String[] args) {
//        String memberType = "1";//0：管理员，1：个人，2：企业，3：街道
//        String memberId = "a51f0aaf5fc3438bb3d3c33c2e414c75";
//        long ttlMillis = 1000*60*60*8;
//
//        String authorization = createJWT(memberType, memberId, ttlMillis);
//        System.out.println("authorization="+authorization);
//
//        // 解析 authorization
//        String[] jwtInfo = authorization.split("\\.");
//        System.out.println(jwtInfo.length);
//        String payLoad = jwtInfo[1];
//        byte[] bytes = org.apache.commons.codec.binary.Base64.decodeBase64(payLoad);
//        String jsonData = new String(bytes, Charsets.UTF_8);
//        System.out.println(jsonData);
//    }
}
