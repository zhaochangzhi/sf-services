package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.mapper.TradeMapper;
import com.sf.services.talent.model.TradeModel;
import com.sf.services.talent.model.po.Trade;
import com.sf.services.talent.service.ITradeService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class TradeService implements ITradeService {

	@Resource
	private TradeMapper tradeMapper;
	

	@Override
	public PageInfo<Trade> selectAll(TradeModel record) {
		Trade trade = new Trade();
		BeanUtils.copyProperties(record, trade);
		PageHelper.startPage(record.getPageNum(), record.getPageSize());
		List<Trade> list = tradeMapper.selectAll(trade);
		return new PageInfo<>(list);
	}

	@Override
	public PageInfo<Trade> selectAllList(TradeModel record) {
		Trade trade = new Trade();
		//查询一级列表
		BeanUtils.copyProperties(record, trade);
		trade.setDataLevel(1);
		PageHelper.startPage(record.getPageNum(), record.getPageSize());
		List<Trade> oneList = tradeMapper.selectAll(trade);

		//查询二级列表
		for (int i = 0; i < oneList.size(); i++) {
			trade.setDataLevel(2);
			trade.setParentId(oneList.get(i).getId());
			List<Trade> twoList = tradeMapper.selectAll(trade);
			oneList.get(i).setTradeList(twoList);
		}

		return new PageInfo<>(oneList);
	}
}

