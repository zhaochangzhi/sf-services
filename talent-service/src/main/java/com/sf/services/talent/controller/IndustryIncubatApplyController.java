package com.sf.services.talent.controller;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.annotations.CurrentUser;
import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.dto.UserDto;
import com.sf.services.talent.model.vo.IndustryIncubatApplyVO;
import com.sf.services.talent.service.IIndustryIncubatApplyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Slf4j
@Api(value = "产业园网站-入孵申请", tags = "产业园网站-入孵申请")
@RestController
@RequestMapping("/industryIncubatApply")
public class IndustryIncubatApplyController {
	
	@Resource
	private IIndustryIncubatApplyService industryIncubatApplyService;

	@IgnoreSecurity
	@ApiOperation(value = "查询入孵申请列表", notes = "查询入孵申请列表")
	@PostMapping("/list")
	public BaseResultModel list(@RequestBody IndustryIncubatApplyListParamModel params) {

		PageInfo<IndustryIncubatApplyVO> pageInfo = industryIncubatApplyService.getList(params);
		return BaseResultModel.success(pageInfo);
	}

	@ApiOperation(value = "查询入孵申请详情", notes = "查询入孵申请详情")
	@PostMapping("/get")
	public BaseResultModel get(@RequestBody GetOneParamModel params) {

		if (params == null) {
			return BaseResultModel.badParam("传入参数不能为空");
		}
		if (StringUtils.isBlank(params.getId())) {
			return BaseResultModel.badParam("id不能为空");
		}

		return industryIncubatApplyService.get(params.getId());
	}

	@ApiOperation(value = "入孵申请", notes = "入孵申请")
	@PostMapping("/add")
	public BaseResultModel courseApplyAdd(@RequestBody IndustryIncubatApplyAddParamModel params, @CurrentUser UserDto currentUser) {

		return industryIncubatApplyService.add(params, currentUser.getUserId());
	}

	@ApiOperation(value = "入孵申请-审核", notes = "入孵申请-审核")
	@PostMapping("/audit")
	public BaseResultModel courseApplyAudit(@RequestBody IndustryIncubatApplyAuditParamModel params, @CurrentUser UserDto currentUser) {


		return industryIncubatApplyService.audit(params, currentUser.getUserId());
	}

	@ApiOperation(value = "查询入孵申请最后一条数据", notes = "查询入孵申请最后一条数据")
	@PostMapping("/getLast")
	public BaseResultModel getLast(@CurrentUser UserDto currentUser) {

		return industryIncubatApplyService.getLast(currentUser.getUserId());
	}


}

