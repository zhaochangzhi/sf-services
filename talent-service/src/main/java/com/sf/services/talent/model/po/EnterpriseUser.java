package com.sf.services.talent.model.po;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
public class EnterpriseUser implements Serializable {
    private String id;

    private String memberId;

    private String enterpriseName;

    private String tradeId;

    private String enterpriseNatureKey;

    private String enterpriseScaleKey;

    private String addressDetailed;

    private String enterpriseIntroduction;

    private String mobile;

    private String email;

    private String enterpriseLogoImageId;

    private String enterpriseLcreditCode;

    private String enterpriseBusinessLicenseImageId;

    private Integer isfamous;

    private Integer sort;

    private String remark;

    private Date creatTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;
    private String contacts;


}