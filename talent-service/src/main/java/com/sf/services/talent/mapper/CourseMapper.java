package com.sf.services.talent.mapper;

import com.sf.services.talent.model.CourseListParamModel;
import com.sf.services.talent.model.po.Course;
import com.sf.services.talent.model.vo.CourseVO;

import java.util.List;
import java.util.Map;

public interface CourseMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(Course record);

    Course selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Course record);

    //查询列表
    List<CourseVO> selectList(CourseListParamModel params);
}