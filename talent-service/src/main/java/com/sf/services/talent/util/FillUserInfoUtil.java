package com.sf.services.talent.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

import com.sf.services.talent.config.ThreadLocalCache;
import com.sf.services.talent.controller.ArchiveServiceAppointmentController;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: FillUserInfoUtil 
 * @Description: 填充用户信息(创建人、创建时间、修改人、修改时间)工具类
 * @author: heyang
 * @date: 2021年7月10日 下午3:36:47
 */
@Slf4j
public class FillUserInfoUtil {
	
	/**
     * @author: hy
     * @date: 2021年7月10日 下午3:36:47
     * @param: obj
     * @return: void
     * @throws:
     * @description: 填充创建人、创建时间信息通过反射填充到obj对象中
    */
    public static void fillCreateUserInfo(Object obj){
        try {
            Class<?> clazz = obj.getClass();
            Method setId = clazz.getMethod("setId", String.class);
            Method setCreateDate = clazz.getMethod("setCreateDate", Date.class);
            Method setCreateUser = clazz.getMethod("setCreateUser", String.class);
            setId.invoke(obj,UUIDUtil.getUUid());
            setCreateUser.invoke(obj,ThreadLocalCache.getUser());
            setCreateDate.invoke(obj,new Date());
            
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
           log.warn("NoSuchMethod", e.getMessage());
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * @author: hy
     * @date: 2021年7月10日 下午3:36:47
     * @param: obj
     * @return: void
     * @throws:
     * @description:填充修改人、修改时间信息通过反射填充到obj对象中
     */
    public static void fillUpdateUserInfo(Object obj){
        try {
            Class<?> clazz = obj.getClass();
            Method setUpdateDate = clazz.getMethod("setUpdateDate", Date.class);
            Method setUpdateUser = clazz.getMethod("setUpdateUser", String.class);
            setUpdateUser.invoke(obj,ThreadLocalCache.getUser());
            setUpdateDate.invoke(obj,new Date());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

}

