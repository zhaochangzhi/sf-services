package com.sf.services.talent.model.vo;

import com.sf.services.talent.model.po.WebSection;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ArticleWebSectionLinkVO {
    private String id;

    private String webSectionLinkId;

    private List<WebSection> webSectionList;

}