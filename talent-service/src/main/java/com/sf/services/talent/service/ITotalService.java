package com.sf.services.talent.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.po.Dictionary;
import com.sf.services.talent.model.po.InterfaceTotal;

/**
 * @ClassName: ITotalService 
 * @Description: 统计接口
 * @author: heyang
 * @date: 2021年8月10日 下午11:26:17
 */
public interface ITotalService {
	/**
	 * @Title: overallStatistics 
	 * @Description: 总体统计
	 * @return
	 * @return: Map<String,Object>
	 */
	Map<String, Object> overallStatistics();
	
	/**
	 * @Title: overallArticle 
	 * @Description: 文章统计
	 * @return
	 * @return: Map<String,Integer>
	 */
	Map<String, Integer> overallArticle();
	
	/**
	 * @Title: publishArticle 
	 * @Description: 发布统计
	 * @return
	 * @return: Map<String,Integer>
	 */
	Map<String, Integer> publishArticle();
	
	/**
	 * @Title: overallUser 
	 * @Description: 用户统计
	 * @return
	 * @return: Map<String,Integer>
	 */
	Map<String, Integer> overallUser();
	
	/**
	 * @Title: overallInterface 
	 * @Description: 接口统计
	 * @return
	 * @return: List<Map<String,Integer>>
	 */
	List<Map<String, Integer>> overallInterface();
	
	/**
	 * @Title: updateInterfaceTotal 
	 * @Description: 更新接口数
	 * @param url
	 * @return
	 * @return: int
	 */
	int updateInterfaceTotal(String url);
	
	/**
	 * @Title: saveInterface 
	 * @Description: 保存接口数
	 * @param interfaceTotal
	 * @return
	 * @return: int
	 */
	int saveInterface(InterfaceTotal interfaceTotal);
	
	/**
	 * @Title: userRegister 
	 * @Description: 用户注册量
	 * @return
	 * @return: Map<String,Object>
	 */
	Map<String, Object> userRegister();
	
	/**
	 * @Title: webRegister 
	 * @Description: 网站点击量
	 * @return
	 * @return: Map<String,Object>
	 */
	Map<String, Object> webRegister();
	
	/**
	 * @Title: webNowDayTotal 
	 * @Description: 就业人才网当天统计
	 * @return
	 * @return: Map<String,Integer>
	 */
	Map<String,Integer> webNowDayTotal(String url);
	
	/**
	 * @Title: webAllTotal 
	 * @Description: 就业人才网总统计
	 * @return
	 * @return: Map<String,Integer>
	 */
	Map<String,Integer> webAllTotal(String url);
	
	/**
	 * @Title: webNowDayTotal 
	 * @Description: 就业人才网按两小时当天统计
	 * @return
	 * @return: Map<String,Integer>
	 */
	Map<String,Integer> webNowDayTotalTwohours(String url);
	
	/**
	 * @Title: webAllTotal 
	 * @Description: 就业人才网按两小时总统计
	 * @return
	 * @return: Map<String,Integer>
	 */
	Map<String,Integer> webAllTotalTwohours(String url);
	
	/**
	 * @Title: getSumMin 
	 * @Description: 获取分钟数
	 * @return
	 * @return: Dictionary
	 */
	Dictionary getSumMin();
	
	/**
	 * @Title: webTotalList 
	 * @Description: 列表统计
	 * @param param
	 * @return
	 * @return: List<Map<String,Object>>
	 */
	 PageInfo<Map<String, Object>> webTotalList(Map<String, Object> param);
	 
	 /**
	  * @Title: exportList 
	  * @Description: 导出
	  * @param request
	  * @param response
	  * @return: void
	  */
	 void exportList(HttpServletRequest request, HttpServletResponse response);
}

