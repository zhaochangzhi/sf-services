package com.sf.services.talent.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.exceptions.TalentException;
import com.sf.services.talent.model.vo.ArchiveServiceCategoryVO;
import com.sf.services.talent.service.IArchiveServiceCategoryService;
import com.sf.services.talent.util.ResultUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: ArchiveServiceCategoryController 
 * @Description: 档案服务类别
 * @author: heyang
 * @date: 2021年7月10日 下午4:51:09
 */
@Slf4j
@Api(value = "档案服务类别", tags = "档案服务类别")
@RestController
@RequestMapping("/archiveServiceCategory")
public class ArchiveServiceCategoryController {
	
	@Autowired
	private IArchiveServiceCategoryService archiveServiceCategoryService;
	
	@ApiOperation(notes = "档案服务类别列表", value="档案服务类别列表")
	@ApiImplicitParam(name = "params", value = "档案服务类别模型", required = false, dataType = "ArchiveServiceCategoryVO")
	@PostMapping("/list")
	@IgnoreSecurity
	public ResultUtil list(@RequestBody ArchiveServiceCategoryVO ArchiveServiceCategoryVO) {
		try {
			return ResultUtil.success(archiveServiceCategoryService.list(ArchiveServiceCategoryVO));
		} catch (Exception e) {
			log.error("档案列表异常:{}",e);
			return ResultUtil.error(401, "档案列表异常");
		}
		
	}
	
	/**
	 * 档案服务类别插入
	 * @param ArchiveServiceCategoryVO
	 * @return
	 */
	@ApiOperation(notes = "档案服务类别插入", value="档案服务类别插入")
	@ApiImplicitParam(name = "params", value = "档案服务类别模型", required = false, dataType = "ArchiveServiceCategoryVO")
	@PostMapping("/insert")
	public ResultUtil insert(@RequestBody ArchiveServiceCategoryVO ArchiveServiceCategoryVO) {
		try {
			archiveServiceCategoryService.insert(ArchiveServiceCategoryVO);
			return ResultUtil.success();
		}catch(TalentException te){
			return ResultUtil.error(te.getCode(), te.getMessage());
		}catch (Exception e) {
			log.error("档案服务类别插入异常:{}",e);
			return ResultUtil.error(401, "档案服务类别插入异常");
		}
		
	}
	
	/**
	 * 档案服务类别修改
	 * @param ArchiveServiceCategoryVO
	 * @return
	 */
	@ApiOperation(notes = "档案服务类别修改", value="档案服务类别修改")
	@ApiImplicitParam(name = "params", value = "档案服务类别模型", required = false, dataType = "ArchiveServiceCategoryVO")
	@PostMapping("/update")
	public ResultUtil update(@RequestBody ArchiveServiceCategoryVO ArchiveServiceCategoryVO) {
		try {
			archiveServiceCategoryService.update(ArchiveServiceCategoryVO);
			return ResultUtil.success();
		}catch(TalentException te){
			return ResultUtil.error(te.getCode(), te.getMessage());
		}catch (Exception e) {
			log.error("档案服务类别修改异常:{}",e);
			return ResultUtil.error(401, "档案服务类别修改异常");
		}
		
	}
	
	/**
	 * 档案服务类别删除
	 * @param id
	 * @return
	 */
	@ApiOperation(notes = "档案服务类别删除", value="档案服务类别删除")
	@ApiImplicitParam(name = "params", value = "档案服务类别模型", required = false, dataType = "ArchiveServiceCategoryVO")
	@PostMapping("/delete")
	public ResultUtil delete(@RequestBody ArchiveServiceCategoryVO ArchiveServiceCategoryVO) {
		try {
			archiveServiceCategoryService.delete(ArchiveServiceCategoryVO);
			return ResultUtil.success();
		}catch(TalentException te){
			return ResultUtil.error(te.getCode(), te.getMessage());
		}catch (Exception e) {
			log.error(" 档案服务类别删除:{}",e);
			return ResultUtil.error(401, " 档案服务类别删除异常");
		}
		
	}
	@ApiOperation(notes = "档案服务类别下拉列表", value="档案服务类别下拉列表")
	@ApiImplicitParam(name = "params", value = "档案服务类别模型", required = false, dataType = "java.util.Map")
	@PostMapping("/getCategoryList")
	public ResultUtil getCategoryList(@RequestBody Map<String, Object> param) {
		return ResultUtil.success(archiveServiceCategoryService.getCategoryList());
	}
}

