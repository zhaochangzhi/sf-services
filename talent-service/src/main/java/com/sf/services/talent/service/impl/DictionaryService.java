package com.sf.services.talent.service.impl;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.mapper.DictionaryMapper;
import com.sf.services.talent.model.DictionaryModel;
import com.sf.services.talent.model.po.Dictionary;
import com.sf.services.talent.service.IDictionaryService;
import com.sf.services.talent.util.FillUserInfoUtil;


@Service
public class DictionaryService implements IDictionaryService {
	
	@Autowired
	private DictionaryMapper dictionaryMapper;
	
	@Override
	public int delete(String id) {
		
		return dictionaryMapper.deleteByPrimaryKey(id);
	}
	@Override
	public int insert(DictionaryModel record) {
		Dictionary dictionary = new Dictionary();
		BeanUtils.copyProperties(record, dictionary);
		FillUserInfoUtil.fillCreateUserInfo(dictionary);
		return dictionaryMapper.insert(dictionary);
	}
	@Override
	public int update(DictionaryModel record) {
		Dictionary dictionary = new Dictionary();
		BeanUtils.copyProperties(record, dictionary);
		FillUserInfoUtil.fillUpdateUserInfo(dictionary);
		return dictionaryMapper.updateByPrimaryKeySelective(dictionary);
	}
	@Override
	public PageInfo<Dictionary> selectAll(DictionaryModel record) {
		Dictionary dictionary = new Dictionary();
		BeanUtils.copyProperties(record, dictionary);
		PageHelper.startPage(record.getPageNum(), record.getPageSize());
		List<Dictionary> list = dictionaryMapper.selectAll(dictionary);
		return new PageInfo<>(list);
	}
}

