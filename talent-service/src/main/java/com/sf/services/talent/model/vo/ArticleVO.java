package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ArticleVO {

    @ApiModelProperty(value = "主键id")
    private String id;

    @ApiModelProperty(value = "所属网站key（t_dictionary表type=Website数据key）")
    private String websiteKey;
    @ApiModelProperty(value = "所属网站key（t_dictionary表type=Website数据value）")
    private String websiteValue;

    @ApiModelProperty(value = "所属栏目key（t_web_section表id）", example = "1001")
    private String webSectionId;
    @ApiModelProperty(value = "所属栏目名称（t_web_section表name）")
    private String webSectionName;

    @ApiModelProperty(value = "关联栏目标识（0：未开启，1：已开启）", example = "0")
    private Integer webSectionLinkStatus;

    @ApiModelProperty(value = "关联栏目逗号分隔所属栏目id（t_web_section表id）")
    private String webSectionLinkId;

    @ApiModelProperty(value = "类型标识（1：文章，2：图片，3：文件）")
    private Integer typeStatus;

    @ApiModelProperty(value = "类型标识名称")
    private String typeStatusName;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "作者")
    private String author;

    @ApiModelProperty(value = "关键词（逗号拼接）")
    private String keywords;

    @ApiModelProperty(value = "来源")
    private String datafrom;

    @ApiModelProperty(value = "导言")
    private String introduction;

    @ApiModelProperty(value = "描述")
    private String describetion;

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "缩略图id")
    private String smallPhotoId;

    @ApiModelProperty(value = "缩略图地址")
    private String smallPhotoUrl;

    @ApiModelProperty(value = "大图id")
    private String bigPhotoId;

    @ApiModelProperty(value = "大图地址")
    private String bigPhotoUrl;

    @ApiModelProperty(value = "文件id")
    private String fileId;

    @ApiModelProperty(value = "文件地址")
    private String fileUrl;

    @ApiModelProperty(value = "创建时间")
    private Date creatTime;
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "浏览量")
    private Integer browseNum;

    @ApiModelProperty(value = "链接")
    private String link;

    @ApiModelProperty(value = "是否已发布（0：否，1：是）")
    private Integer isOn;

    @ApiModelProperty(value = "文件id列表")
    private String articleFileList;
    @ApiModelProperty(value = "文件列表")
    private List<ArticleFileVO> articleFileUrlList;

    @ApiModelProperty(value = "文件名")
    private String originalFileName;

    @ApiModelProperty(value = "是否为悬浮banner")
    private Integer isArticleFloatBanner;
    @ApiModelProperty(value = "悬浮banner图片id")
    private String articleFloatBannerPhotoId;
    @ApiModelProperty(value = "悬浮banner图片url")
    private String articleFloatBannerPhotoUrl;


}