package com.sf.services.talent.controller;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.annotations.CurrentUser;
import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.mapper.DictionaryMapper;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.dto.UserDto;
import com.sf.services.talent.model.po.Dictionary;
import com.sf.services.talent.model.po.EmploymentTargetType;
import com.sf.services.talent.model.vo.EmploymentTargetRecordVO;
import com.sf.services.talent.model.vo.EmploymentTargetVO;
import com.sf.services.talent.service.IEmploymentTargetService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * EmploymentTargetController
 * 就业指标
 * @author guchangliang
 * @date 2021/7/13
 */
@Slf4j
@Api(value = "就业指标", tags = "就业指标")
@RestController
@RequestMapping("/employmentTarget")
public class EmploymentTargetController {

    @Resource
    private IEmploymentTargetService employmentTargetService;
    @Resource
    private DictionaryMapper dictionaryMapper;


    @ApiOperation(value = "就业指标列表", notes = "就业指标列表")
    @PostMapping("/list")
    public BaseResultModel list(@RequestBody EmploymentTargetParamModel params) {
        //校验非空-街道key
        if (StringUtils.isBlank(params.getStreetKey())) {
            return BaseResultModel.badParam("街道不能为空");
        }
        //校验非空-指标年份
        if (params.getTargetYear() == null) {
            return BaseResultModel.badParam("指标年份不能为空");
        }
        //校验格式-指标年份
        if(params.getTargetYear() > 9999){
            return BaseResultModel.badParam("指标年份错误");
        }


        //判断街道key是否存在
        Dictionary dictionary = dictionaryMapper.selectOneByDickey(params.getStreetKey());
        if(dictionary == null){
            return BaseResultModel.badParam("街道key不存在");
        }


        PageInfo<EmploymentTargetVO> pageInfo = employmentTargetService.getList(params);
        return BaseResultModel.success(pageInfo);
    }


    @ApiOperation(notes = "就业指标设定。EmploymentTargetRecordAddParamModelList为1-12月正序数据。", value="就业指标设定")
    @PostMapping("/add")
    public BaseResultModel add(@RequestBody EmploymentTargetAddParamModel employmentTargetAddParamModel
            , @CurrentUser UserDto currentUser) {

        return employmentTargetService.add(employmentTargetAddParamModel
                , currentUser.getUserId());
    }

    @ApiOperation(value = "指标类型列表", notes = "指标类型列表")
    @PostMapping("/type/list")
    public BaseResultModel typeList(@RequestBody EmploymentTargerTypeParamModel params) {


        PageInfo<EmploymentTargetType> pageInfo = employmentTargetService.selectTypeList(params);
        return BaseResultModel.success(pageInfo);
    }

    @ApiOperation(value = "就业指标详情", notes = "就业指标详情")
    @PostMapping("/detail")
    public BaseResultModel detail(@RequestBody EmploymentTargetDetailParamModel param) {
        //校验非空-街道key
        if (param == null) {
            return BaseResultModel.badParam("参数不能为空");
        }
        if(StringUtils.isBlank(param.getId())){
            return BaseResultModel.badParam("指标id不能为空");
        }

        EmploymentTargetVO employmentTargetVO = employmentTargetService.getDetail(param.getId());
        return BaseResultModel.success(employmentTargetVO);
    }


    @ApiOperation(notes = "就业指标更新。EmploymentTargetRecordAddParamModelList为1-12月正序数据。", value="就业指标更新")
    @PostMapping("/update")
    public BaseResultModel update(@RequestBody EmploymentTargetUpdateParamModel employmentTargetUpdateParamModel
            , @CurrentUser UserDto currentUser) {

        return employmentTargetService.update(employmentTargetUpdateParamModel
                , currentUser.getUserId());
    }


    @ApiOperation(value = "就业指标上报列表", notes = "就业指标上报列表")
    @PostMapping("/report/list")
    public BaseResultModel reportList(@RequestBody EmploymentTargetReportParamModel params) {
        //校验非空-街道key
        if (StringUtils.isBlank(params.getStreetKey())) {
            return BaseResultModel.badParam("街道不能为空");
        }
        //校验非空-指标年份
        if (params.getTargetYear() == null) {
            return BaseResultModel.badParam("指标年份不能为空");
        }
        //校验格式-指标年份
        if(params.getTargetYear() > 9999){
            return BaseResultModel.badParam("指标年份错误");
        }
        //校验非空-指标月份
        if (params.getTargetMonth() == null) {
            return BaseResultModel.badParam("指标月份不能为空");
        }
        //校验格式-指标月份
        if(params.getTargetMonth() > 12){
            return BaseResultModel.badParam("指标月份错误");
        }

        //判断街道key是否存在
        Dictionary dictionary = dictionaryMapper.selectOneByDickey(params.getStreetKey());
        if(dictionary == null){
            return BaseResultModel.badParam("街道key不存在");
        }


        PageInfo<EmploymentTargetRecordVO> pageInfo = employmentTargetService.getReportList(params);
        return BaseResultModel.success(pageInfo);
    }
    
    @IgnoreSecurity
    @ApiOperation(value = "就业指标上报列表导出", notes = "就业指标上报列表导出")
    @RequestMapping(value="/report/exportList",method = { RequestMethod.GET })
    public void exportList(String streetKey, Integer targetYear, Integer targetMonth, HttpServletRequest request, HttpServletResponse response) {
    	EmploymentTargetReportParamModel params = new EmploymentTargetReportParamModel();
        params.setStreetKey(streetKey);
        params.setTargetYear(targetYear);
        params.setTargetMonth(targetMonth);
        
        employmentTargetService.exportReportList(params, request, response);
    }


    @ApiOperation(notes = "就业指标上报(更新就业指标完成值)", value="就业指标上报")
    @PostMapping("/report/update")
    public BaseResultModel reportUpdate(@RequestBody EmploymentTargetReportAddParamModel params
            , @CurrentUser UserDto currentUser) {

        return employmentTargetService.reportUpdate(params, currentUser.getUserId());
    }



}
