package com.sf.services.talent.mapper;

import java.util.List;

import com.sf.services.talent.model.po.Area;

public interface AreaMapper {
    int deleteByPrimaryKey(Short id);

    int insert(Area record);

    int insertSelective(Area record);

    Area selectByPrimaryKey(Short id);

    int updateByPrimaryKeySelective(Area record);

    int updateByPrimaryKey(Area record);
    
    List<Area> getArea(Area record);
}