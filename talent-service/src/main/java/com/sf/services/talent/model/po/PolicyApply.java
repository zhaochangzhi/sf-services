package com.sf.services.talent.model.po;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

public class PolicyApply {
    private String id;

    private String cateogryId;

    private String applyMemberId;

    private String companyName;

    private String companyNo;

    private String applicantPhone;

    private String auditStatus;

    private String auditMemberId;

    private Date auditTime;

    private String auditRejectReason;

    private Date appointmentTime;

    private String fileId;

    private Date createDate;

    private String createUser;

    private Date updateDate;

    private String updateUser;
    
    private String value;
    
    private String userName;
    
    private String identity;
    private Date startTime;
    private Date endTime;
    
    private String streetDistrict;
    
    private String policy;
    
    private String streetValue;
    
    private String policyValue;
    
    private String userType;
    
    private List<File> files;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getCateogryId() {
        return cateogryId;
    }

    public void setCateogryId(String cateogryId) {
        this.cateogryId = cateogryId == null ? null : cateogryId.trim();
    }

    public String getApplyMemberId() {
        return applyMemberId;
    }

    public void setApplyMemberId(String applyMemberId) {
        this.applyMemberId = applyMemberId == null ? null : applyMemberId.trim();
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    public String getCompanyNo() {
        return companyNo;
    }

    public void setCompanyNo(String companyNo) {
        this.companyNo = companyNo == null ? null : companyNo.trim();
    }

    public String getApplicantPhone() {
        return applicantPhone;
    }

    public void setApplicantPhone(String applicantPhone) {
        this.applicantPhone = applicantPhone == null ? null : applicantPhone.trim();
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus == null ? null : auditStatus.trim();
    }

    public String getAuditMemberId() {
        return auditMemberId;
    }

    public void setAuditMemberId(String auditMemberId) {
        this.auditMemberId = auditMemberId == null ? null : auditMemberId.trim();
    }
    public Date getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }

    public String getAuditRejectReason() {
        return auditRejectReason;
    }

    public void setAuditRejectReason(String auditRejectReason) {
        this.auditRejectReason = auditRejectReason == null ? null : auditRejectReason.trim();
    }

    public Date getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(Date appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId == null ? null : fileId.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}
	@JsonFormat(pattern="yyyy-MM-dd")
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	@JsonFormat(pattern="yyyy-MM-dd")
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getStreetDistrict() {
		return streetDistrict;
	}

	public void setStreetDistrict(String streetDistrict) {
		this.streetDistrict = streetDistrict;
	}

	public String getPolicy() {
		return policy;
	}

	public void setPolicy(String policy) {
		this.policy = policy;
	}

	public String getStreetValue() {
		return streetValue;
	}

	public void setStreetValue(String streetValue) {
		this.streetValue = streetValue;
	}

	public String getPolicyValue() {
		return policyValue;
	}

	public void setPolicyValue(String policyValue) {
		this.policyValue = policyValue;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public List<File> getFiles() {
		return files;
	}

	public void setFiles(List<File> files) {
		this.files = files;
	}

	
	
	
    
}