package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

@Data
public class EnterprisePositionCollection {
    private String id;

    private String enterprisePositionId;

    private String memberId;

    private String remark;

    private Date creatTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;

}