package com.sf.services.talent.service.impl;

import com.sf.services.talent.mapper.RankMajorMapper;
import com.sf.services.talent.model.RankMajorParamModel;
import com.sf.services.talent.model.po.RankMajor;
import com.sf.services.talent.service.IRankMajorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * RankMajorService
 *
 * @author zhaochangzhi
 * @date 2021/7/10
 */
@Service
public class RankMajorService implements IRankMajorService {

    private final RankMajorMapper rankMajorMapper;

    @Autowired
    public RankMajorService(RankMajorMapper rankMajorMapper) {
        this.rankMajorMapper = rankMajorMapper;
    }

    @Override
    public List<RankMajor> getList(RankMajorParamModel params) {
         return rankMajorMapper.selectList(params);
    }

}
