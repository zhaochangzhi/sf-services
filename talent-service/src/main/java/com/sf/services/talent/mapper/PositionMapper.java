package com.sf.services.talent.mapper;

import com.sf.services.talent.model.po.Position;

import java.util.List;
import java.util.Map;

public interface PositionMapper {
    int deleteByPrimaryKey(String id);

    int insert(Position record);

    int insertSelective(Position record);

    Position selectByPrimaryKey(String id);

    int selectPageListCount(Map<String, Object> searchMap);

    List<Position> selectPageList(Map<String, Object> searchMap);

    int updateByPrimaryKeySelective(Position record);

    int updateByPrimaryKey(Position record);

    /**
     * @Title: selectAll
     * @Description: 查询全部
     * @param record
     * @return
     * @return: List<Position>
     */
    List<Position> selectAll(Position record);
}