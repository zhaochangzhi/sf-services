package com.sf.services.talent.util;

import java.util.UUID;

/**
 * uuid工具类
 * @author Administrator
 *
 */
public class UUIDUtil {
	
	public static String getUUid() {
		return UUID.randomUUID().toString().replace("-", "");
	}

}
