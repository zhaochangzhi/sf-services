package com.sf.services.talent.controller;

import com.sf.services.talent.annotations.CurrentUser;
import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.config.ThreadLocalCache;
import com.sf.services.talent.exceptions.TalentException;
import com.sf.services.talent.mapper.EnterpriseUserMapper;
import com.sf.services.talent.mapper.MemberMapper;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.dto.UserDto;
import com.sf.services.talent.model.po.Member;
import com.sf.services.talent.model.vo.MemberVO;
import com.sf.services.talent.service.IEnterpriseUserService;
import com.sf.services.talent.service.IMerberService;
import com.sf.services.talent.util.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Slf4j
@Api(value = "企业用户", tags = "企业用户")
@RestController
@RequestMapping("/enterprise/user")
public class EnterpriseUserController {
	
	@Resource
	private IEnterpriseUserService enterpriseUserService;
	@Resource
	private EnterpriseUserMapper enterpriseUserMapper;
	@Resource
	private IMerberService merberService;
	@Resource
	private MemberMapper memberMapper;

	@IgnoreSecurity
	@ApiOperation(notes = "企业用户信息", value="企业用户信息")
	@PostMapping("/get")
	public ResultUtil get(@RequestBody EnterpriseUserGetParamModel params) {
		
		return ResultUtil.success(enterpriseUserService.getEnterpriseUser(params));
	}
	
	@ApiOperation(notes = "企业用户更新", value="企业用户更新")
	@PostMapping("/update")
	public BaseResultModel update(@RequestBody EnterpriseUserParamModel enterpriseUserParamModel) {
		
		return enterpriseUserService.update(enterpriseUserParamModel);
		
	}

	@ApiOperation(notes = "企业用户注册", value="企业用户注册")
	@PostMapping("/registEnterpriseUser")
	public BaseResultModel registEnterpriseUser(@RequestBody RegistEnterpriseUserParamModel params, @CurrentUser UserDto currentUser) {
		//校验企业账号是否已注册
		Member member = memberMapper.selectByUserName(params.getUserName());
		if(member != null) {
			return BaseResultModel.badParam("用户名已存在");
		}

		//注册member信息,并初始化企业账号信息
		MemberVO memberVO = new MemberVO();
		memberVO.setUserName(params.getUserName());
		memberVO.setPassword(params.getPassword());
		memberVO.setType("2");//类型1：个人，2公司，3政府
		String memberId = merberService.register(memberVO);

		//更新企业账号信息
		EnterpriseUserParamModel enterpriseUserParamModel = new EnterpriseUserParamModel();
		BeanUtils.copyProperties(params, enterpriseUserParamModel);
		enterpriseUserParamModel.setMemberId(memberId);//更新memberid
		return enterpriseUserService.update(enterpriseUserParamModel);
	}

	@ApiOperation(notes = "企业用户初始化", value="企业用户初始化")
	@PostMapping("/init")
	public BaseResultModel init() {
		String memberId = ThreadLocalCache.getUser();
		return enterpriseUserService.enterpriseUserInit(memberId, memberId);
	}

	@IgnoreSecurity
	@ApiOperation(notes = "企业用户列表", value="企业用户列表")
	@PostMapping("/list")
	public ResultUtil list(@RequestBody EnterpriseUserListParamModel params) {

		return ResultUtil.success(enterpriseUserService.getList(params));

	}

	@ApiOperation(notes = "更新企业用户是否为名企", value="更新企业用户是否为名企")
	@PostMapping("/updateIsfamous")
	public BaseResultModel updateIsfamous(@RequestBody EnterpriseUserUpdateIsfamousParamModel params, @CurrentUser UserDto currentUser) {

		return enterpriseUserService.updateIsfamous(params, currentUser.getUserId());

	}
	@ApiOperation(notes = "删除企业用户", value="删除企业用户")
	@PostMapping("/delEnterpriseUser")
	public ResultUtil delEnterpriseUser(@RequestBody Map<String, String> param, @CurrentUser UserDto currentUser) {
		return ResultUtil.success(enterpriseUserService.delEnterpriseUser(param.get("memberId"), currentUser.getUserId()));
	}

	@ApiOperation(notes = "批量删除企业用户", value="批量删除企业用户")
	@PostMapping("/batchDelete")
	public BaseResultModel batchDelete(@RequestBody MemberIdsParamModel param, @CurrentUser UserDto currentUser) {

		if(param == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if(StringUtils.isBlank(param.getMemberIds())){
			return BaseResultModel.badParam("会员id不能为空");
		}
		if (currentUser == null || StringUtils.isBlank(currentUser.getUserId())) {
			return BaseResultModel.badParam("请登陆");
		}

		return enterpriseUserService.batchDelete(param.getMemberIds(), currentUser.getUserId());
	}
}

