package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.base.Joiner;
import com.sf.services.talent.mapper.*;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.dto.UserDto;
import com.sf.services.talent.model.po.*;
import com.sf.services.talent.model.vo.EmploymentPositionVO;
import com.sf.services.talent.service.IActLogService;
import com.sf.services.talent.service.IEnterprisePositionService;
import com.sf.services.talent.util.RequestUtils;
import com.sf.services.talent.util.UUIDUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * EmploymentPositionService
 *
 * @author guchangliang
 * @date 2021/7/13
 */

@Slf4j
@Service
public class EmploymentPositionService implements IEnterprisePositionService {

    @Resource
    private EnterprisePositionMapper enterprisePositionMapper;
    @Resource
    private EnterpriseUserMapper enterpriseUserMapper;
    @Resource
    private TradeMapper tradeMapper;
    @Resource
    private PositionMapper positionMapper;
    @Resource
    private DictionaryMapper dictionaryMapper;
    @Resource
    private AreaMapper areaMapper;
    @Resource
    private EnterprisePositionResumeMapper enterprisePositionResumeMapper;
    @Resource
    private EnterprisePositionCollectionMapper enterprisePositionCollectionMapper;
    @Resource
    private JobFairEnterprisePositionMapper jobFairEnterprisePositionMapper;
    @Autowired
    private RedissonClient redisson;
    @Resource
    private IActLogService actLogService;//日志

    @Override
    public PageInfo<EmploymentPositionVO> getList(EmploymentPositionParamModel params) {

        if(params == null){
            return null;
        }
        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<EmploymentPositionVO> list = enterprisePositionMapper.selectList(params);

        return new PageInfo<>(list);
    }

    @Override
    public EmploymentPositionVO getDetail(EmploymentPositionDetailParamModel params) {
        if(params == null || StringUtils.isBlank(params.getId())){
            return null;
        }

        EmploymentPositionParamModel employmentPositionParamModel = new EmploymentPositionParamModel();
        BeanUtils.copyProperties(params, employmentPositionParamModel);

        List<EmploymentPositionVO> list = enterprisePositionMapper.selectList(employmentPositionParamModel);
        if(list == null || list.size() == 0){
            return null;
        }

        EmploymentPositionVO employmentPositionVO = list.get(0);

        //判断如果用户已登陆，且type=1（个人），则查询用户是否申请/是否收藏该职位
        UserDto userDto = RequestUtils.getCurrentUserNotThrowAuthException();
        if(userDto != null && "1".equals(userDto.getType())){//type(用户类型1 用户 2公司)
            //查询是否已申请
            int applyCount = enterprisePositionResumeMapper.selectApplyCount(employmentPositionVO.getId(), null, userDto.getUserId());
            if(applyCount >= 1){
                employmentPositionVO.setIsApply(1);
            }

            //查询是否已收藏
            int collectionCount = enterprisePositionCollectionMapper.selectCollectCount(employmentPositionVO.getId(), userDto.getUserId());
            if(collectionCount >= 1){
                employmentPositionVO.setIsCollection(1);
            }
        }

        //职位浏览量+1
        addNum(employmentPositionVO.getId(),null, 1, null);

        return employmentPositionVO;
    }


    @Override
    public BaseResultModel add(EmploymentPositionAddParamModel params, String userId) {
        //校验非空-列表
        if(params == null){
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getEnterpriseUserId())) {
            return BaseResultModel.badParam("企业账号id不能为空");
        }
        if (StringUtils.isBlank(params.getEnterprisePositionName())) {
            return BaseResultModel.badParam("企业职位名称不能为空");
        }
        if (StringUtils.isBlank(params.getTradeTwoId())) {
            return BaseResultModel.badParam("行业id不能为空");
        }
        if (StringUtils.isBlank(params.getPositionThreeId())) {
            return BaseResultModel.badParam("职位id不能为空");
        }
        if (params.getWorkAddressProvinceId() == null || params.getWorkAddressCityId() == null) {
            return BaseResultModel.badParam("省/市不能为空");
        }
        if (StringUtils.isBlank(params.getSalaryRequirementKey())) {
            return BaseResultModel.badParam("薪资要求key不能为空");
        }
        if (StringUtils.isBlank(params.getWorkExpKey())) {
            return BaseResultModel.badParam("工作经验key不能为空");
        }
        if (StringUtils.isBlank(params.getEducationKey())) {
            return BaseResultModel.badParam("学历要求key不能为空");
        }
        if (StringUtils.isBlank(params.getPositionDescribe())) {
            return BaseResultModel.badParam("职位描述不能为空");
        }
        if (params.getRecruitNum() == null) {
            return BaseResultModel.badParam("招聘人数不能为空");
        }
        if (StringUtils.isBlank(params.getWelfareKey())) {
            return BaseResultModel.badParam("福利待遇key不能为空");
        }

        //redisson分布式锁
        String lockKey = "EmploymentPositionServiceAdd"+userId;
        RLock rlock = redisson.getLock(lockKey);
        try {
            //校验是否并发
            if(rlock.isLocked()){
                return BaseResultModel.badParam("请慢些操作");
            }

            //上锁
            rlock.lock();

            //查询企业账号id
            EnterpriseUser enterpriseUser = enterpriseUserMapper.selectByPrimaryKey(params.getEnterpriseUserId());
            if (enterpriseUser == null) {
                return BaseResultModel.badParam("企业账号id不存在");
            }

            //查询行业
            Trade trade = tradeMapper.selectByPrimaryKey(params.getTradeTwoId());
            if (trade == null) {
                return BaseResultModel.badParam("行业id不存在");
            }

            //查询职位
            Position position = positionMapper.selectByPrimaryKey(params.getPositionThreeId());
            if (position == null) {
                return BaseResultModel.badParam("职位id不存在");
            }
            //查询省/市
            Area areaProvince = areaMapper.selectByPrimaryKey(params.getWorkAddressProvinceId().shortValue());
            Area areaCity = areaMapper.selectByPrimaryKey(params.getWorkAddressCityId().shortValue());
            if(areaProvince == null){
                return BaseResultModel.badParam("工作地省id不存在");
            }
//        if (areaProvince.getLevel() != 1) {
//            return BaseResultModel.badParam("工作地省level不正确");
//        }
            if (areaCity == null) {
                return BaseResultModel.badParam("工作地市id不存在");
            }
//        if (areaCity.getLevel() != 2) {
//            return BaseResultModel.badParam("工作地市level不正确");
//        }

            //查询薪资要求（t_dictionary表type=SalaryRequirement数据dickey）
            Dictionary salaryRequirement = dictionaryMapper.selectOneByTypeAndDickey("SalaryRequirement",params.getSalaryRequirementKey());
            if (salaryRequirement == null) {
                return BaseResultModel.badParam("薪资要求key不存在");
            }
            //查询工作经验（t_dictionary表type=WorkExp数据dickey）
            Dictionary workExp = dictionaryMapper.selectOneByTypeAndDickey("WorkExp", params.getWorkExpKey());

            //查询学历要求（t_dictionary表type=Education数据dickey）
            Dictionary education = dictionaryMapper.selectOneByTypeAndDickey("Education", params.getEducationKey());

            //遍历查询福利待遇（t_dictionary表type=Welfare数据dickey）
            String welfareValues = null;//福利待遇（逗号分隔）
            if (!StringUtils.isBlank(params.getWelfareKey())) {
                String welfareS = params.getWelfareKey();
                List<String> welfareList = Arrays.asList(welfareS.split(","));
                List<String> welfareValueList = new ArrayList<>();//福利待遇值list

                Dictionary welfare = null;
                for (int i = 0; i < welfareList.size(); i++) {
                    welfare = dictionaryMapper.selectOneByTypeAndDickey("Welfare", welfareList.get(i));
                    if (welfare == null) {
                        return BaseResultModel.badParam("福利待遇key不存在");
                    }
                    welfareValueList.add(welfare.getValue());
                }
                welfareValues = Joiner.on(",").join(welfareValueList);//福利待遇（逗号分隔）
            }

            //新增
            EnterprisePosition enterprisePosition = new EnterprisePosition();
            BeanUtils.copyProperties(params, enterprisePosition);
            enterprisePosition.setId(UUIDUtil.getUUid());//id
            enterprisePosition.setTradeName(trade.getName());//行业（冗余）
            enterprisePosition.setPositionName(position.getName());//职位（冗余）
            enterprisePosition.setWorkAddressProvince(areaProvince.getDistrict());//工作地点省
            enterprisePosition.setWorkAddressCity(areaCity.getDistrict());//工作地点市
            enterprisePosition.setSalaryRequirement(salaryRequirement.getValue());//薪资要求（冗余）
            if (salaryRequirement.getValue().contains("-")) {
                String salaryMin=salaryRequirement.getValue().substring(0, salaryRequirement.getValue().indexOf("-"));
                String salaryMax=salaryRequirement.getValue().substring(salaryMin.length()+1, salaryRequirement.getValue().length());
                enterprisePosition.setSalaryMin(salaryMin);//薪资-最小值
                enterprisePosition.setSalaryMax(salaryMax);//薪资-最大值
            }if(salaryRequirement.getValue().contains(">")){
                String salaryMin=salaryRequirement.getValue().substring(salaryRequirement.getValue().substring(0, salaryRequirement.getValue().indexOf(">")).length()+1, salaryRequirement.getValue().length());
                enterprisePosition.setSalaryMin(salaryMin);//薪资-最小值
            }

            enterprisePosition.setWorkExp(workExp.getValue());//工作经验（冗余）
            enterprisePosition.setEducation(education.getValue());//学历要求（冗余）
            enterprisePosition.setWelfare(welfareValues);//福利待遇（逗号分隔）
            enterprisePosition.setResumeNum(0);//收到简历数量
            enterprisePosition.setBrowseNum(0);//浏览数量
            enterprisePosition.setCollectionNum(0);//收藏数量
            enterprisePosition.setCreatTime(new Date());//创建时间
            enterprisePosition.setUpdateTime(enterprisePosition.getCreatTime());//更新时间
            enterprisePosition.setCreateUser(userId);//创建人

            enterprisePosition.setTradeId(trade.getId());
            enterprisePosition.setTradeName(trade.getName());
            enterprisePosition.setPositionId(position.getId());
            enterprisePosition.setPositionName(position.getName());

            if(params.getAuditStatus() != null){
                enterprisePosition.setAuditStatus(params.getAuditStatus());//审核状态：1 审核中，2 通过，3 不通过
                enterprisePosition.setRemark("指定审核状态"+params.getAuditStatus());
            }else {
                enterprisePosition.setAuditStatus(1);//审核状态：1 审核中，2 通过，3 不通过
            }

            int count = enterprisePositionMapper.insertSelective(enterprisePosition);

            //保存操作日志
            try {
                actLogService.save(userId, "创建企业职位", enterprisePosition.getEnterprisePositionName());
            } catch (Exception e) {
                log.error("插入日志异常:{}",e);
            }

            return BaseResultModel.success(count);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("EmploymentPositionServiceAdd", e);
            return BaseResultModel.badParam("异常");
        }finally {
            //解锁
            rlock.unlock();
        }
    }


    @Override
    public BaseResultModel update(EmploymentPositionUpdateParamModel params, String userId) {
        //校验非空-列表
        if(params == null){
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getEnterpriseUserId())) {
            return BaseResultModel.badParam("企业账号id不能为空");
        }
        if (StringUtils.isBlank(params.getEnterprisePositionName())) {
            return BaseResultModel.badParam("企业职位名称不能为空");
        }
        if (StringUtils.isBlank(params.getTradeTwoId())) {
            return BaseResultModel.badParam("行业id不能为空");
        }
        if (StringUtils.isBlank(params.getPositionThreeId())) {
            return BaseResultModel.badParam("职位id不能为空");
        }
        if (params.getWorkAddressProvinceId() == null || params.getWorkAddressCityId() == null) {
            return BaseResultModel.badParam("省/市不能为空");
        }
        if (StringUtils.isBlank(params.getSalaryRequirementKey())) {
            return BaseResultModel.badParam("薪资要求key不能为空");
        }
        if (StringUtils.isBlank(params.getWorkExpKey())) {
            return BaseResultModel.badParam("工作经验key不能为空");
        }
        if (StringUtils.isBlank(params.getEducationKey())) {
            return BaseResultModel.badParam("学历要求key不能为空");
        }
        if (StringUtils.isBlank(params.getPositionDescribe())) {
            return BaseResultModel.badParam("职位描述不能为空");
        }
        if (params.getRecruitNum() == null) {
            return BaseResultModel.badParam("招聘人数不能为空");
        }
        if (StringUtils.isBlank(params.getWelfareKey())) {
            return BaseResultModel.badParam("福利待遇key不能为空");
        }

        //查询企业账号id
        EnterpriseUser enterpriseUser = enterpriseUserMapper.selectByPrimaryKey(params.getEnterpriseUserId());
        if (enterpriseUser == null) {
            return BaseResultModel.badParam("企业账号id不存在");
        }

        //查询行业
        Trade trade = tradeMapper.selectByPrimaryKey(params.getTradeTwoId());
        if (trade == null) {
            return BaseResultModel.badParam("行业id不存在");
        }

        //查询职位
        Position position = positionMapper.selectByPrimaryKey(params.getPositionThreeId());
        if (position == null) {
            return BaseResultModel.badParam("职位id不存在");
        }
        //查询省/市
        Area areaProvince = areaMapper.selectByPrimaryKey(params.getWorkAddressProvinceId().shortValue());
        Area areaCity = areaMapper.selectByPrimaryKey(params.getWorkAddressCityId().shortValue());
        if(areaProvince == null){
            return BaseResultModel.badParam("工作地省id不存在");
        }
//        if (areaProvince.getLevel() != 1) {
//            return BaseResultModel.badParam("工作地省level不正确");
//        }
        if (areaCity == null) {
            return BaseResultModel.badParam("工作地市id不存在");
        }
//        if (areaCity.getLevel() != 2) {
//            return BaseResultModel.badParam("工作地市level不正确");
//        }

        //查询薪资要求（t_dictionary表type=SalaryRequirement数据dickey）
        Dictionary salaryRequirement = dictionaryMapper.selectOneByTypeAndDickey("SalaryRequirement",params.getSalaryRequirementKey());
        if (salaryRequirement == null) {
            return BaseResultModel.badParam("薪资要求key不存在");
        }
        //查询工作经验（t_dictionary表type=WorkExp数据dickey）
        Dictionary workExp = dictionaryMapper.selectOneByTypeAndDickey("WorkExp", params.getWorkExpKey());

        //查询学历要求（t_dictionary表type=Education数据dickey）
        Dictionary education = dictionaryMapper.selectOneByTypeAndDickey("Education", params.getEducationKey());

        //遍历查询福利待遇（t_dictionary表type=Welfare数据dickey）
        String welfareValues = null;//福利待遇（逗号分隔）
        if (!StringUtils.isBlank(params.getWelfareKey())) {
            String welfareS = params.getWelfareKey();
            List<String> welfareList = Arrays.asList(welfareS.split(","));
            List<String> welfareValueList = new ArrayList<>();//福利待遇值list

            Dictionary welfare = null;
            for (int i = 0; i < welfareList.size(); i++) {
                welfare = dictionaryMapper.selectOneByTypeAndDickey("Welfare", welfareList.get(i));
                if (welfare == null) {
                    return BaseResultModel.badParam("福利待遇key不存在");
                }
                welfareValueList.add(welfare.getValue());
            }
            welfareValues = Joiner.on(",").join(welfareValueList);//福利待遇（逗号分隔）
        }

        //更新
        EnterprisePosition enterprisePosition = new EnterprisePosition();
        BeanUtils.copyProperties(params, enterprisePosition);
        enterprisePosition.setTradeName(trade.getName());//行业（冗余）
        enterprisePosition.setPositionName(position.getName());//职位（冗余）
        enterprisePosition.setWorkAddressProvince(areaProvince.getDistrict());//工作地点省
        enterprisePosition.setWorkAddressCity(areaCity.getDistrict());//工作地点市
        enterprisePosition.setSalaryRequirement(salaryRequirement.getValue());//薪资要求（冗余）
        if (salaryRequirement.getValue().contains("-")) {
            String salaryMin=salaryRequirement.getValue().substring(0, salaryRequirement.getValue().indexOf("-"));
            String salaryMax=salaryRequirement.getValue().substring(salaryMin.length()+1, salaryRequirement.getValue().length());
            enterprisePosition.setSalaryMin(salaryMin);//薪资-最小值
            enterprisePosition.setSalaryMax(salaryMax);//薪资-最大值
        }if(salaryRequirement.getValue().contains(">")){
            String salaryMin=salaryRequirement.getValue().substring(salaryRequirement.getValue().substring(0, salaryRequirement.getValue().indexOf(">")).length()+1, salaryRequirement.getValue().length());
            enterprisePosition.setSalaryMin(salaryMin);//薪资-最小值
        }

        enterprisePosition.setWorkExp(workExp.getValue());//工作经验（冗余）
        enterprisePosition.setEducation(education.getValue());//学历要求（冗余）
        enterprisePosition.setWelfare(welfareValues);//福利待遇（逗号分隔）
        enterprisePosition.setUpdateTime(new Date());//更新时间
        enterprisePosition.setUpdateUser(userId);//更新人

        enterprisePosition.setTradeId(trade.getId());
        enterprisePosition.setTradeName(trade.getName());
        enterprisePosition.setPositionId(position.getId());
        enterprisePosition.setPositionName(position.getName());
        int count = enterprisePositionMapper.updateByPrimaryKeySelective(enterprisePosition);


        //保存操作日志
        try {
            actLogService.save(userId, "更新企业职位", enterprisePosition.getEnterprisePositionName());
        } catch (Exception e) {
            log.error("插入日志异常:{}",e);
        }

        return BaseResultModel.success(count);
    }

    @Override
    public BaseResultModel audit(EmploymentPositionAuditParamModel params, String userId) {
        //校验非空-列表
        if(params == null){
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getId())) {
            return BaseResultModel.badParam("id不能为空");
        }
        if (params.getAuditStatus() == null) {
            return BaseResultModel.badParam("审核状态不能为空");
        }
        //审核状态：1 审核中，2 通过，3 不通过
        if (params.getAuditStatus() != 2 && params.getAuditStatus() != 3) {
            return BaseResultModel.badParam("审核状态不正确");
        }
        //校验审核不通过原因
        if(params.getAuditStatus() == 3 ){
            //暂时不校验
        }else {
            //非审核不通过状态，将审核不通过原因置为空
            params.setAuditReason("");
        }

        //校验状态
        EnterprisePosition check = enterprisePositionMapper.selectByPrimaryKey(params.getId());
        if (check == null) {
            return BaseResultModel.badParam("id不存在");
        }
        if(check.getAuditStatus() != null && (check.getAuditStatus() == 2 || check.getAuditStatus() == 3)){
            return BaseResultModel.badParam("已审核，无需再次审核");
        }

        //更新
        EnterprisePosition enterprisePosition = new EnterprisePosition();
        BeanUtils.copyProperties(params, enterprisePosition);

        enterprisePosition.setAuditTime(new Date());//审核时间
        enterprisePosition.setAuditUser(userId);//审核人

        int count = enterprisePositionMapper.updateByPrimaryKeySelective(enterprisePosition);

        //保存操作日志
        try {
            actLogService.save(userId, "审核职位", check.getEnterprisePositionName());
        } catch (Exception e) {
            log.error("插入日志异常:{}",e);
        }

        return BaseResultModel.success(count);
    }

    @Override
    public BaseResultModel batchAudit(EmploymentPositionBatchAuditParamModel params, String userId) {
        //校验非空-列表
        if(params == null){
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getIds())) {
            return BaseResultModel.badParam("ids不能为空");
        }

        //遍历审核
        EmploymentPositionAuditParamModel auditParams = new EmploymentPositionAuditParamModel();
        BeanUtils.copyProperties(params, auditParams);
        List<String> idList = Arrays.asList(params.getIds().split(","));
        for (int i = 0; i < idList.size(); i++) {
            auditParams.setId(idList.get(i));
            //审核
            audit(auditParams, userId);
        }

        return BaseResultModel.success(idList.size());
    }

    @Override
    public BaseResultModel delete(EmploymentPositionDeleteParamModel params, String userId) {
        if(params == null){
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getId())) {
            return BaseResultModel.badParam("id不能为空");
        }

        //软删除
        EnterprisePosition enterprisePosition = new EnterprisePosition();
        enterprisePosition.setId(params.getId());
        enterprisePosition.setUpdateTime(enterprisePosition.getCreatTime());//更新时间
        enterprisePosition.setUpdateUser(userId);//更新人
        enterprisePosition.setIsdelete(1);//删除标识（0：未删除，1：已删除）

        int count =  enterprisePositionMapper.updateByPrimaryKeySelective(enterprisePosition);

        //删除，企业职位-收藏记录
        enterprisePositionCollectionMapper.deleteByEnterprisePositionId(enterprisePosition.getId());

        //软删除，企业职位-申请简历记录（找工作）
        enterprisePositionResumeMapper.deleteByEnterprisePositionId(enterprisePosition.getId());

        //软删除，招聘会-企业职位-关联表
        jobFairEnterprisePositionMapper.deleteByEnterprisePositionId(enterprisePosition.getId());

        //保存操作日志
        try {
            EnterprisePosition enterprisePositionLog = enterprisePositionMapper.selectByPrimaryKey(params.getId());
            actLogService.save(userId, "删除职位", enterprisePositionLog.getEnterprisePositionName());
        } catch (Exception e) {
            log.error("插入日志异常:{}",e);
        }

        return BaseResultModel.success(count);
    }


    @Override
    public BaseResultModel batchDelete(IdsParamModel params, String userId) {
        if(params == null){
            return BaseResultModel.badParam("参数不能为空");
        }
        if (StringUtils.isBlank(params.getIds())) {
            return BaseResultModel.badParam("ids不能为空");
        }

        EmploymentPositionDeleteParamModel deleteParams = new EmploymentPositionDeleteParamModel();
        List<String> idList = Arrays.asList(params.getIds().split(","));
        for (int i = 0; i < idList.size(); i++) {
            deleteParams.setId(idList.get(i));
            delete(deleteParams, userId);
        }

        return BaseResultModel.success(idList.size());
    }


    @Override
    public BaseResultModel addNum(String id, Integer resumeNum, Integer browseNum, Integer collectionNum) {
        if(StringUtils.isBlank(id)){
            return BaseResultModel.badParam("id不能为空");
        }
        if (resumeNum == null && browseNum == null && collectionNum == null) {
            return BaseResultModel.badParam("至少选择一项");
        }

        int count =  enterprisePositionMapper.addNum(id,resumeNum,browseNum,collectionNum);
        return BaseResultModel.success(count);
    }

    @Override
    public BaseResultModel reduceNum(String id, Integer resumeNum, Integer browseNum, Integer collectionNum) {
        if(StringUtils.isBlank(id)){
            return BaseResultModel.badParam("id不能为空");
        }
        if (resumeNum == null && browseNum == null && collectionNum == null) {
            return BaseResultModel.badParam("至少选择一项");
        }
        int count =  enterprisePositionMapper.reduceNum(id,resumeNum,browseNum,collectionNum);
        return BaseResultModel.success(count);
    }


}
