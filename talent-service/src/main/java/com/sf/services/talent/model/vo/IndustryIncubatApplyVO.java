package com.sf.services.talent.model.vo;

import lombok.Data;

import java.util.Date;

@Data
public class IndustryIncubatApplyVO {
    private String id;

    private String companyName;

    private String registCapital;

    private String legalRepresentativeName;

    private String legalRepresentativeMobile;

    private String contactName;

    private String contactMobile;

    private String contactEmail;

    private String companyIntroduction;

    private String workAddress;

    private String workMobile;

    private Integer auditStatus;

    private String auditReason;

    private Date auditTime;

    private String auditUser;

    private String createUser;

    private EnterpriseUserVO enterpriseUser;

}