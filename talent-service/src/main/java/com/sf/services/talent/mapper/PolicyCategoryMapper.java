package com.sf.services.talent.mapper;

import java.util.List;

import com.sf.services.talent.model.po.PolicyCategory;

public interface PolicyCategoryMapper {
    int deleteByPrimaryKey(String id);

    int insert(PolicyCategory record);

    int insertSelective(PolicyCategory record);

    PolicyCategory selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PolicyCategory record);

    int updateByPrimaryKey(PolicyCategory record);
    
    /**
     * @Title: selectAll 
     * @Description: 查询列表
     * @param record
     * @return
     * @return: List<PolicyCategory>
     */
    List<PolicyCategory> selectAll(PolicyCategory record);
}