package com.sf.services.talent.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.IdsParamModel;
import com.sf.services.talent.model.ResumeListParam;
import com.sf.services.talent.model.ResumeParamModel;
import com.sf.services.talent.model.po.Resume;

/**
 * @ClassName: IResumeService 
 * @Description: 个人简历接口
 * @author: heyang
 * @date: 2021年7月21日 下午9:59:04
 */
public interface IResumeService {
	
	/**
	 * @Title: add 
	 * @Description: 简历插入
	 * @param resumeParamModel
	 * @return
	 * @return: int
	 */
	int add(ResumeParamModel resumeParamModel);
	
	/**
	 * @Title: getList 
	 * @Description: 简历列表
	 * @return
	 * @return: PageInfo<Resume>
	 */
	PageInfo<Map<String, Object>> getList(ResumeListParam resumeListParam);
	
	/**
	 * @Title: getDetail 
	 * @Description: 简历详情
	 * @param id
	 * @return
	 * @return: Resume
	 */
	Resume getDetail(String id);
	
	/**
	 * @Title: delete 
	 * @Description: 简历删除
	 * @param id
	 * @return
	 * @return: int
	 */
	int delete(String id);

	/**
	 * @Title: batchDelete
	 * @Description: 简历批量删除
	 * @param params
	 * @return
	 * @return: int
	 */
	int batchDelete(IdsParamModel params);
	
	/**
	 * @Title: update 
	 * @Description: 更新简历
	 * @param resumeParamModel
	 * @return
	 * @return: int
	 */
	int update(ResumeParamModel resumeParamModel);
	
	/**
	 * @Title: selectClount 
	 * @Description: 简历我的投递 我的收藏次数
	 * @param memberId
	 * @return
	 * @return: Map<String,Object>
	 */
	Map<String, Object> selectClount(String memberId);
	/**
	 * @Title: audit 
	 * @Description: 审核
	 * @param params
	 * @return
	 * @return: int
	 */
	 int audit(Map<String, Object> params);
}

