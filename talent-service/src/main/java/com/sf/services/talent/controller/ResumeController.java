package com.sf.services.talent.controller;

import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.config.ThreadLocalCache;
import com.sf.services.talent.mapper.FileMapper;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.po.File;
import com.sf.services.talent.model.po.Resume;
import com.sf.services.talent.model.po.ResumeEducation;
import com.sf.services.talent.model.po.ResumeWork;
import com.sf.services.talent.service.IFileService;
import com.sf.services.talent.service.IResumeService;
import com.sf.services.talent.util.ResultUtil;
import com.sf.services.talent.util.WordTemplate;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @ClassName: ResumeController 
 * @Description: 个人简历
 * @author: heyang
 * @date: 2021年7月21日 下午10:23:06
 */
@Slf4j
@Api(value = "个人简历", tags = "个人简历")
@RestController
@RequestMapping("/resume")
public class ResumeController {
	
	@Autowired
	private IResumeService resumeService;
	@Autowired
	private FileMapper fileservice;
	@Value("${file.path}")
	private String filePath;
	@Value("${file.url}")
	private String fileUrl;
	
	@ApiOperation(notes = "个人简历添加", value="个人简历添加")
	@ApiImplicitParam(name = "params", value = "个人简历模型", required = false, dataType = "ResumeParamModel")
	@PostMapping("/add")
	public ResultUtil add(@RequestBody ResumeParamModel resumeParamModel) {
		
		return ResultUtil.success(resumeService.add(resumeParamModel));
	}

	@IgnoreSecurity
	@ApiOperation(notes = "个人简历列表", value="个人简历列表")
	@ApiImplicitParam(name = "params", value = "个人简历模型", required = false, dataType = "ResumeParamModel")
	@PostMapping("/getList")
	public ResultUtil getList(@RequestBody ResumeListParam resumeListParam) {
		
		return ResultUtil.success(resumeService.getList(resumeListParam));
	}
	
	@ApiOperation(notes = "个人详情", value="个人详情")
	@ApiImplicitParam(name = "params", value = "个人简历模型", required = false, dataType = "ResumeParamModel")
	@PostMapping("/getDetail")
	public ResultUtil getDetail(@RequestBody ResumeParamModel resumeParamModel) {
		
		return ResultUtil.success(resumeService.getDetail(resumeParamModel.getId()));
	}
	
	@ApiOperation(notes = "个人简历更新", value="个人简历更新")
	@ApiImplicitParam(name = "params", value = "个人简历模型", required = false, dataType = "ResumeParamModel")
	@PostMapping("/update")
	public ResultUtil update(@RequestBody ResumeParamModel resumeParamModel) {
		
		return ResultUtil.success(resumeService.update(resumeParamModel));
	}
	
	@ApiOperation(notes = "个人简历删除", value="个人简历删除")
	@ApiImplicitParam(name = "params", value = "个人简历模型", required = false, dataType = "ResumeParamModel")
	@PostMapping("/delete")
	public ResultUtil delete(@RequestBody ResumeParamModel resumeParamModel) {
		
		return ResultUtil.success(resumeService.delete(resumeParamModel.getId()));
	}

	@ApiOperation(notes = "个人简历批量删除", value="个人简历批量删除")
	@PostMapping("/batchDelete")
	public BaseResultModel batchDelete(@RequestBody IdsParamModel param) {

		if(param == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if (StringUtils.isBlank(param.getIds())) {
			return BaseResultModel.badParam("ids不能为空");
		}

		return BaseResultModel.success(resumeService.batchDelete(param));
	}
	
	@SuppressWarnings("unchecked")
	@ApiOperation(notes = "个人简历下载", value="个人简历下载")
	@PostMapping("/download")
	public ResultUtil download(@RequestBody Map<String, Object> param) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Resume resume = resumeService.getDetail(param.get("id")+"");
		Map<String, Object> wordDataMap = new HashMap<String, Object>();// 存储报表全部数据
        Map<String, Object> parametersMap = new HashMap<String, Object>();// 存储报表中不循环的数据
        parametersMap.put("userName", resume.getUserName());
        parametersMap.put("age", resume.getAge());
        parametersMap.put("phone", resume.getPhone());
        parametersMap.put("email", resume.getEmail());
        parametersMap.put("workExperience", resume.getWorkExperience());
        parametersMap.put("workCity2Desc", resume.getWorkCity2Desc());
        parametersMap.put("job3Desc", resume.getJob3Desc());
        parametersMap.put("salaryRequirement", resume.getSalaryRequirement());

		parametersMap.put("arrivalTimeDesc", resume.getArrivalTimeDesc());//到访时间
		parametersMap.put("education", resume.getEducation());//学历
		parametersMap.put("sexDesc", resume.getSexDesc());// 性别
		parametersMap.put("birthdayDesc", resume.getBirthdayDesc());// 生日
		parametersMap.put("identity", resume.getIdentity());// 身份证
		parametersMap.put("jobStatusDesc", resume.getJobStatusDesc());// 在职状态

        File file1 = fileservice.selectByPrimaryKey(resume.getUserPhoto());
        if(file1!=null) {
        	parametersMap.put("imageUrl", file1.getFilePath());
        }else {
        	parametersMap.put("imageUrl", null);
        }
        

        List<ResumeWork> table1 = resume.getResumeWorkList();
        List<Map<String, Object>> list1 = new ArrayList<>();
        table1.stream().forEach(work->{
        	try {
				Map<String, Object> returnMap = BeanUtils.describe(work);
				if(returnMap.containsKey("startTime")) {
					returnMap.put("startTime", formatter.format(work.getStartTime()));
				}
				if(returnMap.containsKey("endTime")) {
					returnMap.put("endTime", formatter.format(work.getEndTime()));
				}
				list1.add(returnMap);
			} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
				e.printStackTrace();
			}
        });
        List<ResumeEducation> table2= resume.getResumeEducationList();
        List<Map<String, Object>> list2 = new ArrayList<>();
        table2.stream().forEach(education->{
        	Map<String, Object> returnMap;
			try {
				returnMap = BeanUtils.describe(education);
				if(returnMap.containsKey("startTime")) {
					returnMap.put("startTime", formatter.format(education.getStartTime()));
				}
				if(returnMap.containsKey("endTime")) {
					returnMap.put("endTime", formatter.format(education.getEndTime()));
				}
				list2.add(returnMap);
			} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
				e.printStackTrace();
			}
        	
        });
        wordDataMap.put("table1", list1);
        wordDataMap.put("table2", list2);
        wordDataMap.put("parametersMap", parametersMap);
        java.io.File file =null;
        String system = System.getProperty("os.name");
        if(system.indexOf("Windows") >= 0) {
        	file = new java.io.File("D:\\upload\\template.docx");
        }else {
        	log.info(filePath+"template.docx");
        	file = new java.io.File(filePath+"template.docx");
        }

        // 读取word模板
        FileInputStream fileInputStream = new FileInputStream(file);
        WordTemplate template = new WordTemplate(fileInputStream);

        // 替换数据
        template.replaceDocument(wordDataMap);
        String memberId = ThreadLocalCache.getUser();

        //生成文件
        java.io.File outputFile =null;
        if(system.indexOf("Windows") >= 0) {
        	outputFile = new java.io.File("D:\\\\upload\\"+memberId+".docx");
        }else {
        	outputFile = new java.io.File(filePath+resume.getUserName()+"人才简历"+".docx");
        }
        FileOutputStream fos  = new FileOutputStream(outputFile);
        template.getDocument().write(fos);
        return ResultUtil.success(fileUrl+resume.getUserName()+"人才简历"+".docx");
	}
	
	
	@ApiOperation(notes = "个人简历投递 收藏", value="个人简历投递 收藏")
	@ApiImplicitParam(name = "params", value = "个人简历模型", required = false, dataType = "ResumeParamModel")
	@PostMapping("/collections")
	public ResultUtil collections(@RequestBody Map<String, Object> param) {
		String memberId = ThreadLocalCache.getUser();
		return ResultUtil.success(resumeService.selectClount(memberId));
	}
	
	@ApiOperation(notes = "个人简历审核", value="个人简历审核")
	@PostMapping("/audit")
	public ResultUtil audit( @RequestBody Map<String, Object> param) {
		
		return ResultUtil.success(resumeService.audit(param));
	}

	@ApiOperation(notes = "个人简历批量审核", value="个人简历批量审核")
	@PostMapping("/batchAudit")
	public BaseResultModel batchAudit( @RequestBody Map<String, Object> param) {
		//校验非空-列表
		if(param == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if (param.get("ids") == null) {
			return BaseResultModel.badParam("ids不能为空");
		}

		//遍历审核
		List<String> idList = Arrays.asList(param.get("ids").toString().split(","));
		for (int i = 0; i < idList.size(); i++) {
			param.put("id", idList.get(i));
			//审核
			resumeService.audit(param);
		}

		return BaseResultModel.success(idList.size());
	}
	
}

