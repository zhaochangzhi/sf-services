package com.sf.services.talent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 课程
 * @author guchangliang
 * @date 2021/8/13
 */
@Data
@ApiModel(description = "课程列表")
public class CourseListParamModel extends PageParamModel {


    @ApiModelProperty(value = "课程名称", example = "xxx课程")
    private String courseName;

    @ApiModelProperty(value = "课程类型id（t_course_type表id）", example = "ae2d6aae9fa4443fa86a23cd3a0a9f68")
    private String courseTypeId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "开课时间-开始", example = "2021-08-13")
    private Date startTimeStart;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "开课时间-结束", example = "2021-08-15")
    private Date startTimeEnd;

//    @ApiModelProperty(value = "审核状态：1 审核中，2 通过，3 不通过", example = "2")
//    private Integer auditStatus;

}
