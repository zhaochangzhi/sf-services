package com.sf.services.talent.mapper;

import com.sf.services.talent.model.po.User;
import com.sf.services.talent.model.vo.UserStreetVO;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    int deleteByPrimaryKey(@Param("memberId") String memberId);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(User record);
    
    int updateByMemberIdSelective(User record);

    int updateByPrimaryKey(User record);
    
    User selectByMemberId(String memberId);

    /**
     * 获取用户街道
     * @param memberId
     * @return
     */
    UserStreetVO getUserStreet(@Param("memberId") String memberId);
    /**
     * 
     * @Title: selectList 
     * @Description: 用户列表
     * @return
     * @return: List<User>
     */
    List<User> selectList(User record);
    
    /**
     * @Title: insertUserStreet 
     * @Description: 插入街道
     * @param userStreetVO
     * @return
     * @return: int
     */
    int insertUserStreet(UserStreetVO userStreetVO);
    
    /**
     * @Title: updateUserStreet 
     * @Description: 更新街道
     * @param userStreetVO
     * @return
     * @return: int
     */
    int updateUserStreet(UserStreetVO userStreetVO);
}