package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.mapper.WebSectionMapper;
import com.sf.services.talent.model.ArticleUpdateParamModel;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.WebSectionParamModel;
import com.sf.services.talent.model.WebSectionUpdateParamModel;
import com.sf.services.talent.model.po.*;
import com.sf.services.talent.model.vo.ArticleFileVO;
import com.sf.services.talent.model.vo.ArticleVO;
import com.sf.services.talent.model.vo.WebSectionVO;
import com.sf.services.talent.service.IWebSectionService;
import com.sf.services.talent.util.UUIDUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class WebSectionService implements IWebSectionService {

	@Resource
	private WebSectionMapper webSectionMapper;

	@Override
	public PageInfo<WebSection> list(WebSectionParamModel params) {
		//分页处理
		PageHelper.startPage(params.getPageNum(), params.getPageSize());
		List<WebSection> list = webSectionMapper.selectList(params);

//		//映射
//		List<WebSectionVO> rankList = new ArrayList<>();
//		if (!list.isEmpty()) {
//			list.stream().forEach(rank -> {
//				WebSectionVO webSectionVO = new WebSectionVO();
//				BeanUtils.copyProperties(rank, webSectionVO);
//				rankList.add(webSectionVO);
//			});
//		}

		return new PageInfo<>(list);
	}

	@Override
	public WebSection get(String id) {
		WebSection webSection = webSectionMapper.selectByPrimaryKey(id);


		return webSection;
	}

	@Override
	public BaseResultModel update(WebSectionUpdateParamModel params, String updateUser) {

		if(params == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if (StringUtils.isBlank(params.getId())) {
			return BaseResultModel.badParam("id不能为空");
		}
		if (StringUtils.isBlank(updateUser)) {
			return BaseResultModel.badParam("更新人不能为空");
		}

		//映射
		WebSection webSection = webSectionMapper.selectByPrimaryKey(params.getId());
		BeanUtils.copyProperties(params, webSection);

		//冗余字段赋值
		webSection.setUpdateTime(new Date());//更新时间
		webSection.setUpdateUser(updateUser);//更新人
		int count = webSectionMapper.updateByPrimaryKeySelective(webSection);

		return BaseResultModel.success(count);
	}
}

