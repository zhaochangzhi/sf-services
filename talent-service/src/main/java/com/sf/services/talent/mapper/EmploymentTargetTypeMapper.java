package com.sf.services.talent.mapper;

import com.sf.services.talent.model.po.EmploymentTargetType;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EmploymentTargetTypeMapper {
    int deleteByPrimaryKey(String id);

    int insert(EmploymentTargetType record);

    int insertSelective(EmploymentTargetType record);

    EmploymentTargetType selectByPrimaryKey(String id);

    int selectPageListCount(Map<String, Object> searchMap);

    List<EmploymentTargetType> selectPageList(Map<String, Object> searchMap);

    int updateByPrimaryKeySelective(EmploymentTargetType record);

    int updateByPrimaryKey(EmploymentTargetType record);

    List<EmploymentTargetType> selectList(EmploymentTargetType record);
}