package com.sf.services.talent.mapper;

import java.util.List;

import com.sf.services.talent.model.po.Archive;

public interface ArchiveMapper {
    int deleteByPrimaryKey(String id);

    int insert(Archive record);

    int insertSelective(Archive record);

    Archive selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Archive record);

    int updateByPrimaryKey(Archive record);
    
    List<Archive> selectAll(Archive record);
    
    /**
     * @Title: selectDetailByParam 
     * @Description: 根据参数查询档案
     * @param record
     * @return
     * @return: Archive
     */
    Archive selectDetailByParam(Archive record);

    /**
     * @Author:Songyf
     * @Title:selectByOwnerId
     * @Description:根据身份证号查询
     * @param:ownerId
     * @return:Archive
     */
    Archive selectByOwnerId(String ownerId);
    
    /**
     * @Title: bashDel 
     * @Description: 批量删除
     * @param ids
     * @return
     * @return: int
     */
    int bashDel(String[] ids);
}