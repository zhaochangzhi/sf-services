package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author guchangliang
 * @date 2021/7/13
 */
@ApiModel(value = "招聘会-企业实体")
@Data
public class JobFairEmploymentVO extends PageVO{

    @ApiModelProperty(value = "招聘会id（t_job_fair表id）")
    private String jobFairId;

    @ApiModelProperty(value = "企业账号id")
    private String enterpriseUserId;
    @ApiModelProperty(value = "企业memberid")
    private String memberId;

    @ApiModelProperty(value = "企业全称")
    private String enterpriseName;

    @ApiModelProperty(value = "行业一级名称（z_trade表name）")
    private String tradeOneName;

    @ApiModelProperty(value = "行业二名称（z_trade表name）")
    private String tradeTwoName;

    @ApiModelProperty(value = "企业性质（字典t_dictionary表type=EnterpriseNature数据value）")
    private String enterpriseNatureValue;

    @ApiModelProperty(value = "企业规模（字典t_dictionary表type=EnterpriseScale数据value）")
    private String enterpriseScaleValue;

    @ApiModelProperty(value = "企业参与招聘会职位列表")
    private List<JobFairEmploymentPositionVO> jobFairEmploymentPositionList;

    @ApiModelProperty(value = "招聘会信息")
    private JobFairVO jobFair;

    @ApiModelProperty(value = "审核状态：1 审核中，2 通过，3 不通过")
    private Integer auditStatus;
    @ApiModelProperty(value = "审核不通过原因")
    private String auditReason;
    @ApiModelProperty(value = "审核时间")
    private Date auditTime;
    @ApiModelProperty(value = "审核人")
    private String auditUser;

    @ApiModelProperty(value = "是否在线")
    private Boolean isOnline;

    @ApiModelProperty(value = "企业log图片url")
    private String enterpriseLogoImageUrl;


}