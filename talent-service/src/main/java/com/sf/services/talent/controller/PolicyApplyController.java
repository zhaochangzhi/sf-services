package com.sf.services.talent.controller;

import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.PolicyApplyModel;
import com.sf.services.talent.service.IPolicyApplyService;
import com.sf.services.talent.util.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Api(value = "政策申请", tags = "政策申请")
@RestController
@RequestMapping("/policyApply")
public class PolicyApplyController {
	
	@Autowired
	private IPolicyApplyService policyApplyService;
	
	/**
	 * @Title: list 
	 * @Description: 政策申请列表
	 * @param PolicyApplyModel
	 * @return
	 * @return: ResultUtil
	 */
	@ApiOperation(notes = "政策申请列表", value="政策申请列表")
	@ApiImplicitParam(name = "params", value = "政策申请模型", required = false, dataType = "PolicyApplyModel")
	@PostMapping("/list")
	public ResultUtil list(@RequestBody PolicyApplyModel PolicyApplyModel) {
		return ResultUtil.success(policyApplyService.list(PolicyApplyModel));
	}

	@IgnoreSecurity
	@ApiOperation(value = "政策申请列表导出", notes = "政策申请列表导出")
	@RequestMapping(value="/exportList",method = { RequestMethod.GET })
	public void exportList(String auditStatus, String streetDistrict, HttpServletRequest request, HttpServletResponse response) {

		PolicyApplyModel policyApplyModel = new PolicyApplyModel();
		policyApplyModel.setAuditStatus(auditStatus);
		policyApplyModel.setStreetDistrict(streetDistrict);
		policyApplyService.exportList(policyApplyModel, request, response);
	}
	
	/**
	 * @Title: next 
	 * @Description: 政策申请下一步
	 * @param PolicyApplyModel
	 * @return
	 * @return: ResultUtil
	 */
	@ApiOperation(notes = "政策申请下一步", value="政策申请下一步")
	@ApiImplicitParam(name = "params", value = "政策申请模型", required = false, dataType = "PolicyApplyModel")
	@PostMapping("/next")
	public ResultUtil next(@RequestBody PolicyApplyModel PolicyApplyModel) {
		return ResultUtil.success(policyApplyService.next(PolicyApplyModel));
	}
	
	/**
	 * @Title: apply 
	 * @Description: 政策申请
	 * @param PolicyApplyModel
	 * @return
	 * @return: ResultUtil
	 */
	@ApiOperation(notes = "政策申请", value="政策申请")
	@ApiImplicitParam(name = "params", value = "政策申请模型", required = false, dataType = "PolicyApplyModel")
	@PostMapping("/apply")
	public ResultUtil apply(@RequestBody PolicyApplyModel PolicyApplyModel) {
		return ResultUtil.success(policyApplyService.apply(PolicyApplyModel));
	}
	
	/**
	 * @Title: audit 
	 * @Description: 政策审核
	 * @param PolicyApplyModel
	 * @return
	 * @return: ResultUtil
	 */
	@ApiOperation(notes = "政策申请审核", value="政策申请审核")
	@ApiImplicitParam(name = "params", value = "政策申请模型", required = false, dataType = "PolicyApplyModel")
	@PostMapping("/audit")
	public ResultUtil audit(@RequestBody PolicyApplyModel PolicyApplyModel) {
		return ResultUtil.success(policyApplyService.audit(PolicyApplyModel));
	}
	
	/**
	 * @Title: getNewestDetail 
	 * @Description: 政策申请详情
	 * @return
	 * @return: ResultUtil
	 */
	@ApiOperation(notes = "政策申请详情", value="政策申请详情")
	@PostMapping("/getNewestDetail")
	public ResultUtil getNewestDetail() {
		return ResultUtil.success(policyApplyService.getNewestDetail());
	}
}

