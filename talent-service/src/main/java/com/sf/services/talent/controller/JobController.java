package com.sf.services.talent.controller;

import com.sf.services.talent.annotations.CurrentUser;
import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.JobParamModel;
import com.sf.services.talent.model.dto.UserDto;
import com.sf.services.talent.model.vo.JobVO;
import com.sf.services.talent.service.IJobService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * JobController 找工作
 *
 * @author zhaochangzhi
 * @date 2021/7/7
 */
@Api(value = "找工作", tags = "找工作")
@RestController
@RequestMapping("/job")
public class JobController {

    private final IJobService jobService;

    @Autowired
    public JobController(IJobService jobService) {
        this.jobService = jobService;
    }

    /*
     * swagger doc 注释 属性解释
     *
     * 类注释 @Api ， 属性包含 value和tags
     *
     * 方法注释（接口注释） @ApiOperation, 属性包含 notes 和 value
     *
     * 方法参数注释（接口参数注释）@ApiImplicitParam（单个参数），属性包含 name（字段名），value（字段解释），required（是否必填）， dataType（数据类型）
     *
     * @ApiImplicitParams ， 多个参数注释 示例如下：
     *
     * @ApiImplicitParams({
            @ApiImplicitParam(name = "masterbrandid", value = "主品牌ID", required = true, dataType = "int"),
            @ApiImplicitParam(name = "cityids", value = "城市ID", required = true, dataType = "String"),
            @ApiImplicitParam(name = "count", value = "返回数量", required = true, dataType = "int")})
     *
     */

    @ApiOperation(notes = "工作详情", value="工作详情")
    @ApiImplicitParam(name = "jobId", value = "工作ID", required = true, dataType = "int")
    @GetMapping("/{jobId}")
    public BaseResultModel<JobVO> detail(@PathVariable Integer jobId) {
        return BaseResultModel.success(jobService.getById(jobId));
    }

    @ApiOperation(notes = "工作列表", value="工作列表")
    @PostMapping("/")
    @IgnoreSecurity
    public BaseResultModel<List<JobVO>> list(@RequestBody JobParamModel params) {
        return BaseResultModel.success(jobService.getList());
    }

    @ApiOperation(notes = "工作总数量", value = "工作总数量")
    @GetMapping("/count")
    public BaseResultModel<Integer> getCount(@CurrentUser UserDto currentUser) {
        return BaseResultModel.success(jobService.getCount());
    }




}
