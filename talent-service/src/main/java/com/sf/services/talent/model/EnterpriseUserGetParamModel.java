package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "企业用户详情", description = "企业用户详情")
public class EnterpriseUserGetParamModel {

	@ApiModelProperty(value = "主键id（t_enterprise_user表id）", example = "12a53ef7c0af4a52ac9f0e8c1001a0ad")
	private String id;

	@ApiModelProperty(value = "会员id（t_member表id）", example = "a21d2353c85c430a97d465f51e6eb0b4")
	private String memberId;


}

