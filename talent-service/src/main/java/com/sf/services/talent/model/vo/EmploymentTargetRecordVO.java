package com.sf.services.talent.model.vo;

import com.sf.services.talent.model.po.EmploymentTargetRecordFile;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * EmploymentTarget
 * 就业指标
 * @author guchangliang
 * @date 2021/7/13
 */
@ApiModel(value = "就业指标-记录实体")
@Data
public class EmploymentTargetRecordVO extends PageVO{


    /**
     *
     */
    private static final long serialVersionUID = 2104150778276761315L;
    @ApiModelProperty(value = "主键id")
    private String id;
    @ApiModelProperty(value = "指标类型名称（t_employment_target_type表name）")
    private String targetName;

    @ApiModelProperty(value = "指标年份")
    private Integer targetYear;
    @ApiModelProperty(value = "指标月份（1至12）")
    private Integer targetMonth;

    @ApiModelProperty(value = "设定值（当复数状态=2时，存入字符串例：123+321")
    private String target;
    @ApiModelProperty(value = "设定值1（当复数状态=2时，该字段拆解target第一个数字）")
    private String targetOne;
    @ApiModelProperty(value = "设定值2（当复数状态=2时，该字段拆解target第二个数字）")
    private String targetTwo;

    @ApiModelProperty(value = "完成值（当复数状态=2时，存入字符串例：123+321）")
    private String actual;
    @ApiModelProperty(value = "完成值1（当复数状态=2时，该字段拆解actual第一个数字）")
    private String actualOne;
    @ApiModelProperty(value = "完成值2（当复数状态=2时，该字段拆解actual第二个数字）")
    private String actualTwo;

    @ApiModelProperty(value = "指标类型名称")
    private String typeName;
    @ApiModelProperty(value = "指标形式状态（1：百分比；2：固定数值）")
    private Integer formState;
    @ApiModelProperty(value = "运算符号状态（1：大于等于；2：小于等于；3：等于")
    private Integer symbolStatus;
    @ApiModelProperty(value = "复数状态（1：设定值为一个数值；2：设定值为两个数值）")
    private Integer complexStatus;

    @ApiModelProperty(value = "就业指标记录-附件列表")
    List<EmploymentTargetRecordFile> employmentTargetRecordFileList;

}