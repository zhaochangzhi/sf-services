package com.sf.services.talent.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class LoginParamModel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 665411473802607972L;

	@ApiModelProperty(value = "用户名", example = "用户名xxx")
	private String userName;

	@ApiModelProperty(value = "密码", example = "密码xxx")
	private String password;

	@ApiModelProperty(value = "1：个人，2公司，3政府")
	private String type;

	@ApiModelProperty(value = "会员id", example = "05f955b79bbc4711a6824720aebff4e7")
	private String memberId;

	@ApiModelProperty(value = "会员id", example = "05f955b79bbc4711a6824720aebff4e7")
	private String isSystem;

	@ApiModelProperty(value = "1：产业园，2：就业和人才服务中心网站", example = "1")
	private Integer platform;


}