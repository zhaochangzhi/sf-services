package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 添加课程
 * @author guchangliang
 * @date 2021/8/13
 */
@Data
@ApiModel(description = "添加课程")
public class CourseApplyDetailAddParamModel {

    @ApiModelProperty(value = "报名人姓名", example = "报名人姓名")
    private String applyName;

    @ApiModelProperty(value = "报名人性别", example = "男")
    private String applySex;

    @ApiModelProperty(value = "报名人生日", example = "1989-09-19")
    private String applyBirthday;

    @ApiModelProperty(value = "报名人身份证号码", example = "21088119890919xxxx")
    private String applyIdnumber;

    @ApiModelProperty(value = "报名人手机号", example = "1394071xxxx")
    private String applyMobile;

    @ApiModelProperty(value = "报名人所在单位", example = "报名人所在单位")
    private String applyMemberName;
}
