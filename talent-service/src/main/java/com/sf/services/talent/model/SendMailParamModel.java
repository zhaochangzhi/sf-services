package com.sf.services.talent.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @author xufan1
 * 邮箱发送-前端传输参数
 */
@Data
public class SendMailParamModel implements Serializable {

    /**
     * 接受邮箱账户
     */
    @NotEmpty(message = "邮箱不可为空")
    @ApiModelProperty(value = "邮箱", example = "xxx@163.com")
    private String mail;

    /**
     * 邮箱标题
     */
    @NotEmpty(message = "邮箱标题不可为空")
    @ApiModelProperty(value = "邮件标题", example = "邮件标题xxx")
    private String title;

    /**
     * 要发送的内容
     */
    @NotEmpty(message = "内容不可为空")
    @ApiModelProperty(value = "邮件内容", example = "邮件内容xxx")
    private String content;

}

