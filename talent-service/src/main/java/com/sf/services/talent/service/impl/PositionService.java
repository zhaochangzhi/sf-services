package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.mapper.PositionMapper;
import com.sf.services.talent.model.PositionModel;
import com.sf.services.talent.model.po.Position;
import com.sf.services.talent.service.IPositionService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class PositionService implements IPositionService {

	@Resource
	private PositionMapper positionMapper;
	

	@Override
	public PageInfo<Position> selectAll(PositionModel record) {
		Position position = new Position();
		BeanUtils.copyProperties(record, position);
		PageHelper.startPage(record.getPageNum(), record.getPageSize());
		List<Position> list = positionMapper.selectAll(position);
		return new PageInfo<>(list);
	}

	@Override
	public PageInfo<Position> selectAllList(PositionModel record) {
		Position position = new Position();

		//查询一级列表
		BeanUtils.copyProperties(record, position);
		position.setDataLevel(1);
		PageHelper.startPage(record.getPageNum(), record.getPageSize());
		List<Position> oneList = positionMapper.selectAll(position);

		//查询二级列表
		for (int i = 0; i < oneList.size(); i++) {
			position.setDataLevel(2);
			position.setParentId(oneList.get(i).getId());
			List<Position> twoList = positionMapper.selectAll(position);
			oneList.get(i).setPositionList(twoList);

			//查询三级列表
			for (int j = 0; j < twoList.size(); j++) {
				position.setDataLevel(3);
				position.setParentId(twoList.get(j).getId());
				List<Position> threeList = positionMapper.selectAll(position);
				twoList.get(j).setPositionList(threeList);
			}
		}

		return new PageInfo<>(oneList);
	}
}

