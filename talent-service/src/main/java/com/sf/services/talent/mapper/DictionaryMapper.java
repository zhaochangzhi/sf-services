package com.sf.services.talent.mapper;

import com.sf.services.talent.model.po.Dictionary;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DictionaryMapper {
    int deleteByPrimaryKey(String id);

    int insert(Dictionary record);

    int insertSelective(Dictionary record);

    Dictionary selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Dictionary record);

    int updateByPrimaryKey(Dictionary record);
    
    /**
     * @Title: selectAll 
     * @Description: 查询全部
     * @param record
     * @return
     * @return: List<Dictionary>
     */
    List<Dictionary> selectAll(Dictionary record);

    /**
     * 根据dickey查询
     * @param dickey
     * @return
     */
    Dictionary selectOneByDickey(String dickey);

    /**
     * 根据type和dickey查询
     * @param type
     * @param dickey
     * @return
     */
    Dictionary selectOneByTypeAndDickey(@Param("type") String type, @Param("dickey")String dickey);
}