package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 找工作-我的收藏
 * @author guchangliang
 * @date 2021/7/26
 */
@ApiModel(value = "找工作-我的收藏")
@Data
public class EmploymentPositionResumeCollectionListParamModel extends PageParamModel{

    @ApiModelProperty(value = "会员id（t_member表id）", example = "ae2d6aae9fa4443fa86a23cd3a0a9f68")
    private String memberId;

}