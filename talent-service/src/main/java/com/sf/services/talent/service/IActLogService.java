package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.ActLogModel;
import com.sf.services.talent.model.vo.ActLogVO;

public interface IActLogService {
	/**
	 * @Title: save 
	 * @Description: 保存操作日志
	 * @param actUserId
	 * @param actLogContect
	 * @param actLogTitle
	 * @return: void
	 */
	void save(String actUserId,String actLogContect,String actLogTitle);
	
	/**
	 * @Title: del 
	 * @Description: 删除日志
	 * @param id
	 * @return: int
	 */
	int del(String id);
	
	/**
	 * @Title: pageList 
	 * @Description: 分页列表
	 * @param actLog
	 * @return: PageInfo<ActLogVO>
	 */
	PageInfo<ActLogVO> pageList(ActLogModel actLog);
}

