package com.sf.services.talent.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.mapper.FileMapper;
import com.sf.services.talent.mapper.RankApplyMapper;
import com.sf.services.talent.model.RankApplyParamModel;
import com.sf.services.talent.model.dto.RankApplyDto;
import com.sf.services.talent.model.po.RankApply;
import com.sf.services.talent.model.po.RankApplyFile;
import com.sf.services.talent.model.vo.RankApplyVO;
import com.sf.services.talent.service.IRankApplyService;
import com.sf.services.talent.util.ExportExcelUtil;
import com.sf.services.talent.util.UUIDUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * RankApplyService
 *
 * @author zhaochangzhi
 * @date 2021/7/10
 */
@Service
public class RankApplyService implements IRankApplyService {

    private final RankApplyMapper rankApplyMapper;

    private final FileMapper fileMapper;

    @Autowired
    public RankApplyService(RankApplyMapper rankApplyMapper, FileMapper fileMapper) {
        this.rankApplyMapper = rankApplyMapper;
        this.fileMapper = fileMapper;
    }

    @Override
    public PageInfo<RankApplyDto> getList(RankApplyParamModel params) {

        List<RankApplyVO> rankApplyList = new ArrayList<>();
        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<RankApplyDto> list = rankApplyMapper.selectList(params);
        if (!list.isEmpty()) {
            list.stream().forEach(rankApply -> {
                if("1".equals(rankApply.getApplyUserSex())){
                    rankApply.setApplyUserSexDesc("男");
                }else if("2".equals(rankApply.getApplyUserSex())){
                    rankApply.setApplyUserSexDesc("女");
                }else {
                    rankApply.setApplyUserSexDesc("");
                }
                //查询职称申请文件列表
                rankApply.setAttachmentList(rankApplyMapper.selectRankApplyFileIdList(rankApply.getId()));
            });
        }
        return new PageInfo<>(list);
    }

    @Override
    public PageInfo<RankApplyDto> getDistinctDescList(RankApplyParamModel params) {

        List<RankApplyVO> rankApplyList = new ArrayList<>();
        //分页处理
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<RankApplyDto> list = rankApplyMapper.getDistinctDescList(params);
        if (!list.isEmpty()) {
            list.stream().forEach(rankApply -> {
                if("1".equals(rankApply.getApplyUserSex())){
                    rankApply.setApplyUserSexDesc("男");
                }else if("2".equals(rankApply.getApplyUserSex())){
                    rankApply.setApplyUserSexDesc("女");
                }else {
                    rankApply.setApplyUserSexDesc("");
                }
                //查询职称申请文件列表
                rankApply.setAttachmentList(rankApplyMapper.selectRankApplyFileIdList(rankApply.getId()));
            });
        }
        return new PageInfo<>(list);
    }

    @Override
    public RankApplyVO getOneById(String id) {
        RankApplyVO rankApplyVO = new RankApplyVO();
        RankApplyDto rankApply = rankApplyMapper.selectOneById(id);
        if (Objects.nonNull(rankApply)) {
            BeanUtils.copyProperties(rankApply, rankApplyVO);
        }
        setAuditStatusDesc(rankApplyVO);
        setApplyUserSexDesc(rankApplyVO);
        return rankApplyVO;
    }

    @Override
    public RankApplyVO getLastOne(RankApplyDto params) {
        RankApplyVO rankApplyVO = new RankApplyVO();
        RankApply rankApply = new RankApply();
        BeanUtils.copyProperties(params, rankApply);
        RankApplyDto rankApplyDto = rankApplyMapper.selectLastOne(rankApply);
        if (Objects.nonNull(rankApplyDto)) {
            BeanUtils.copyProperties(rankApplyDto, rankApplyVO);
        }
        setAuditStatusDesc(rankApplyVO);
        setApplyUserSexDesc(rankApplyVO);
        return rankApplyVO;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer addOne(RankApplyDto params) {
        RankApply rankApply = new RankApply();
        BeanUtils.copyProperties(params, rankApply);
        String rankApplyId = UUIDUtil.getUUid();
        rankApply.setId(rankApplyId);
        rankApply.setIsdelete(0);
        if (null != params.getRankApplyFileList() && params.getRankApplyFileList().size() > 0) {
            List<RankApplyFile> rankApplyFileList = new ArrayList<>();
            //添加 职称申请文件关联表数据
            params.getRankApplyFileList().stream().forEach(rank -> {
                RankApplyFile rankApplyFile = new RankApplyFile();
                String uuid = UUIDUtil.getUUid();
                rankApplyFile.setId(uuid);
                rankApplyFile.setRankApplyId(rankApplyId);
                rankApplyFile.setFileId(rank.getFileId());
                rankApplyFile.setTypeStatus(rank.getTypeStatus());
                rankApplyFileList.add(rankApplyFile);
            });
            rankApplyMapper.insertBatchRankApplyFile(rankApplyFileList);
        }
        return rankApplyMapper.insertOne(rankApply);
    }

    @Override
    public Integer updateOneById(RankApplyDto params) {
        RankApply rankApply = new RankApply();
        BeanUtils.copyProperties(params, rankApply);
        return rankApplyMapper.updateOneById(rankApply);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer deleteOneRankApplyFile(String id) {
        Integer flag = 0;
        RankApplyFile rankApplyFile = rankApplyMapper.selectOneRankApplyFile(id);
        if (null != rankApplyFile) {
            //删除职称申请关联数据
            rankApplyMapper.deleteOneRankApplyFile(id);
            //删除文件表数据
            if (null != fileMapper.selectByPrimaryKey(rankApplyFile.getFileId())) {
                fileMapper.deleteByPrimaryKey(rankApplyFile.getFileId());
            }
            flag = 1;
        }
        return flag;
    }



    /**
     * 设置审核状态文字描述
     *
     * @param rankApplyVO 参数
     */
    private void setAuditStatusDesc(RankApplyVO rankApplyVO) {
        if (StringUtils.isNotBlank(rankApplyVO.getAuditStatus())) {
            switch (rankApplyVO.getAuditStatus()) {
                case "2" :
                    rankApplyVO.setAuditStatusDesc("审核通过");
                    break;
                case "3" :
                    rankApplyVO.setAuditStatusDesc("审核不通过");
                    break;
                default:
                    rankApplyVO.setAuditStatusDesc("审核中");
                    break;
            }
        } else {
            rankApplyVO.setAuditStatus("1");
            rankApplyVO.setAuditStatusDesc("审核中");
        }

    }

    /**
     * 设置性别描述
     *
     * @param rankApplyVO 参数
     */
    private void setApplyUserSexDesc(RankApplyVO rankApplyVO) {
        if (StringUtils.isNotBlank(rankApplyVO.getApplyUserSex())) {
            switch (rankApplyVO.getApplyUserSex()) {
                case "1" :
                    rankApplyVO.setApplyUserSexDesc("男");
                    break;
                case "2" :
                    rankApplyVO.setApplyUserSexDesc("女");
                    break;
                default:
                    rankApplyVO.setApplyUserSexDesc("未知");
                    break;
            }
        } else {
            rankApplyVO.setApplyUserSex("0");
            rankApplyVO.setApplyUserSexDesc("未知");
        }
    }


    @Override
    public void exportList(RankApplyParamModel params, HttpServletRequest request, HttpServletResponse response) {
        //查询列表
        List<RankApplyDto> list = rankApplyMapper.selectList(params);

        //获取根据memberid去重倒序列表
//        List<RankApplyDto> list = rankApplyMapper.getDistinctDescList(params);
        if (list == null || list.size() == 0) {
            return;
        }

        //导出列表
        List<Map<String, Object>> dataMapList = new ArrayList<Map<String, Object>>();
        Map<String, Object> headMap = new LinkedHashMap<String, Object>();

        headMap.put("rowNum", "序号");
        headMap.put("rankName", "职称类别");
        headMap.put("rankApplyType", "申报方式");
        headMap.put("applyUserName", "姓名");
        headMap.put("applyUserSex", "性别");
        headMap.put("applyUserIdNumber", "身份证");
        headMap.put("applyUserBirthday", "出生日期");
        headMap.put("applyUserEducation", "学历(第一取得)");
        headMap.put("learnMajor", "所学专业");
        headMap.put("applyUserEducation2", "学历(第二取得)");
        headMap.put("learnMajor2", "所学专业");
        headMap.put("applyUserEducation3", "学历(第三取得)");
        headMap.put("learnMajor3", "所学专业");
        headMap.put("applyUserCompanyFullName", "所在单位全称");
        headMap.put("obtainedCredentials", "已取得专业技术职业证书专业");
        headMap.put("obtainedCredentialsLevel", "已取得专业技术职业证书等级");
        headMap.put("obtainedCredentialsTime", "已取得专业技术职业证书时间");
        headMap.put("paperSum", "论文数量");
        headMap.put("applyUserPhone", "联系电话");
        headMap.put("reviewMajor", "申报行业");
        headMap.put("identifyMajor", "申报专业");
        headMap.put("identifyLevel", "认定/申报等级");
        headMap.put("auditTime", "审核时间");

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        // excel导出内容
        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> data = new HashMap<String, Object>();

            data.put("rowNum", i+1);
            data.put("rankName", list.get(i).getRankName());
            data.put("rankApplyType", list.get(i).getRankApplyType());
            data.put("applyUserName", list.get(i).getApplyUserName());

            if("1".equals(list.get(i).getApplyUserSex())){
                data.put("applyUserSex", "男");
            }else if("2".equals(list.get(i).getApplyUserSex())){
                data.put("applyUserSex", "女");
            }else {
                data.put("applyUserSex", "");
            }

            if("".equals(list.get(i).getIdentifyLevel()))
            {
                data.put("identifyLevel",list.get(i).getReviewLevel());
            }
            else
            {
                data.put("identifyLevel", list.get(i).getIdentifyLevel());
            }

            data.put("applyUserIdNumber", list.get(i).getApplyUserIdNumber());
            data.put("applyUserBirthday", list.get(i).getApplyUserBirthday());
            data.put("applyUserEducation", list.get(i).getApplyUserEducation());
            data.put("learnMajor", list.get(i).getLearnMajor());
            data.put("applyUserEducation2", "".equals(list.get(i).getApplyUserEducation2())?"无":list.get(i).getApplyUserEducation2());
            data.put("learnMajor2", "".equals(list.get(i).getLearnMajor2())?"无":list.get(i).getLearnMajor2());
            data.put("applyUserEducation3", "".equals(list.get(i).getApplyUserEducation3())?"无":list.get(i).getApplyUserEducation3());
            data.put("learnMajor3", "".equals(list.get(i).getLearnMajor3())?"无":list.get(i).getLearnMajor3());
            data.put("applyUserCompanyFullName", list.get(i).getApplyUserCompanyFullName());
            data.put("obtainedCredentials", "".equals(list.get(i).getObtainedCredentials())?"无":list.get(i).getObtainedCredentials());
            data.put("obtainedCredentialsLevel", "".equals(list.get(i).getObtainedCredentialsLevel())?"无":list.get(i).getObtainedCredentialsLevel());
            data.put("obtainedCredentialsTime", "".equals(list.get(i).getObtainedCredentialsTime())?"无":list.get(i).getObtainedCredentialsTime());

            data.put("paperSum", list.get(i).getPaperSum());

            data.put("applyUserPhone", list.get(i).getApplyUserPhone());
            data.put("reviewMajor", list.get(i).getReviewMajor());
            data.put("identifyMajor", list.get(i).getIdentifyMajor());
            if(list.get(i).getAuditTime() != null) {
                data.put("auditTime", format.format(list.get(i).getAuditTime()));
            }
            else
            {
                data.put("auditTime", list.get(i).getAuditTime());
            }
            if(list.get(i).getRankApplyTypeKey().equals("1001"))
            {
                data.put("obtainedCredentials", "无需填报");
                data.put("obtainedCredentialsLevel", "无需填报");
                data.put("obtainedCredentialsTime", "无需填报");
                data.put("paperSum","无需填报");
                data.put("reviewMajor", "无需填报");
            }
            dataMapList.add(data);
        }

        ExportExcelUtil export = new ExportExcelUtil();
        String fileName = "职称申请列表";//文件名
        String sheetName = "职称申请列表";//页签名
        export.toExcelForStream(request, response, headMap, dataMapList,  fileName, sheetName);
    }
}
