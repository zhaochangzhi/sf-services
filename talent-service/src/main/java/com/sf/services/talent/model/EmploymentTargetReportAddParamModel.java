package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 就业指标设定实体
 *
 * @author guchangliang
 * @date 2021/7/13
 */
@Data
@ApiModel(value = "EmploymentTargetReportAddParamModel", description = "就业指标上报添加实体")
public class EmploymentTargetReportAddParamModel {

    @ApiModelProperty(value = "就业指标-记录id（t_employment_target_record表id）", example = "6b466adbbb864c13854cec7daf20a1a5")
    private String id;

    @ApiModelProperty(value = "完成值（t_employment_target_record表actual）", example = "70")
    private String actual;

    @ApiModelProperty(value = "完成值1（t_employment_target_record表actual_one）", example = "70")
    private String actualOne;

    @ApiModelProperty(value = "完成值（t_employment_target_record表actual_two）", example = "70")
    private String actualTwo;

    @ApiModelProperty(value = "附件列表")
    private List<EmploymentTargetRecordFileUpdateModel> employmentTargetRecordFileList;

}
