package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "栏目关联-列表")
@Data
public class ArticleWebSectionLinkListParamModel extends PageParamModel{

    @ApiModelProperty(value = "所属栏目id", example = "1002")
    private String webSectionLinkId;



}