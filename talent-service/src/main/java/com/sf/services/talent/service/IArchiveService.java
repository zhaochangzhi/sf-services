package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.po.Archive;
import com.sf.services.talent.model.vo.ArchiveVO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IArchiveService {

	/**
	 * 添加
	 * 
	 * @param record 档案对象
	 * @return
	 */
	int insert(ArchiveVO record);

	/**
	 * 修改
	 * 
	 * @param record 档案对象
	 * @return
	 */
	int update(ArchiveVO record);

	/**
	 * 删除
	 * 
	 * @param id 主键id
	 * @return
	 */
	int delete(String id);

	/**
	 * 详情
	 * 
	 * @param id 主键id
	 * @return
	 */
	Archive detail(String id);

	/**
	 * 查询全部
	 * 
	 * @return
	 */
	PageInfo<Archive> list(ArchiveVO archiveVO);
	
	/**
	 * @Title: selectDetailByParam 
	 * @Description: 根据条件参数查询
	 * @param archiveVO
	 * @return
	 * @return: Archive
	 */
	Archive selectDetailByParam(ArchiveVO archiveVO);

	/**
	 * @Author:Songyf
	 * @Title:selectByOwnerId
	 * @Description:根据身份证号查询
	 * @param:ownerId
	 * @return:Archive
	 */
	Archive selectByOwnerId(String ownerId);

	/**
	 * 导入excel
	 * @param file
	 * @param createUser
	 * @return
	 */
	BaseResultModel importExcel(MultipartFile file, String createUser);

	/**
	 * 导出excel
	 * @param request
	 * @param response
	 */
	void exportList(HttpServletRequest request, HttpServletResponse response);
	
	/**
	 * 
	 * @Title: bashDel 
	 * @Description: 批量删除
	 * @param ids
	 * @return
	 * @return: int
	 */
	 int bashDel(String[] ids);
}
