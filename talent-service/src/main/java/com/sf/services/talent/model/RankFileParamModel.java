package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 职称文件参数实体
 *
 * @author zhaochangzhi
 * @date 2021/7/19
 */
@Data
@ApiModel(value = "RankFileParamModel", description = "职称文件参数实体")
public class RankFileParamModel extends PageParamModel {

    @ApiModelProperty(value = "职称码")
    private String rankCode;

    @ApiModelProperty(value = "文件id")
    private String[] fileId;

    @ApiModelProperty(value = "职称申报方式key（t_dictionary表type=RankApplyType）")
    private String rankApplyTypeKey;
}
