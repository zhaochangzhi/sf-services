package com.sf.services.talent.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.mapper.ActLogMapper;
import com.sf.services.talent.model.ActLogModel;
import com.sf.services.talent.model.po.ActLog;
import com.sf.services.talent.model.vo.ActLogVO;
import com.sf.services.talent.service.IActLogService;
import com.sf.services.talent.util.FillUserInfoUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ActLogService implements IActLogService {
	
	@Autowired
	private ActLogMapper actLogMapper;
	
	@Override
	public void save(String actUserId, String actLogContect, String actLogTitle) {
		try {
			ActLog actLog = new ActLog();
			actLog.setActUserId(actUserId);
			actLog.setActLogTitle(actLogTitle);
			actLog.setActLogContect(actLogContect);
			FillUserInfoUtil.fillCreateUserInfo(actLog);
			actLogMapper.insertSelective(actLog);
		} catch (Exception e) {
			log.error("插入日志异常:{}",e);
		}
	}
	@Override
	public int del(String id) {
		
		return actLogMapper.deleteByPrimaryKey(id.split(","));
	}
	@Override
	public PageInfo<ActLogVO> pageList(ActLogModel param) {
		PageHelper.startPage(param.getPageNum(), param.getPageSize());
		return new PageInfo<>(actLogMapper.selectAll(param));
	}
}

