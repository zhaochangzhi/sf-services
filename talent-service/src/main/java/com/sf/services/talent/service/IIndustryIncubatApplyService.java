package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.IndustryIncubatApplyAddParamModel;
import com.sf.services.talent.model.IndustryIncubatApplyAuditParamModel;
import com.sf.services.talent.model.IndustryIncubatApplyListParamModel;
import com.sf.services.talent.model.vo.IndustryIncubatApplyVO;

/**
 * @Description: 课程
 * @author: guchangliang
 * @date: 2021/8/13
 */
public interface IIndustryIncubatApplyService {

    /**
     * 获取列表
     * @param params 参数
     * @return 列表
     */
    PageInfo<IndustryIncubatApplyVO> getList(IndustryIncubatApplyListParamModel params);


    /**
     * 获取详情
     * @param id 参数
     * @return 列表
     */
    BaseResultModel get(String id);


    /**
     * 申请入孵
     * @param params 参数
     * @param userId
     * @return
     */
    BaseResultModel add(IndustryIncubatApplyAddParamModel params
            , String userId);

    /**
     * 申请入孵-审核
     * @param params 参数
     * @param userId
     * @return
     */
    BaseResultModel audit(IndustryIncubatApplyAuditParamModel params
            , String userId);

    /**
     * 查询最后一条数据
     * @param createUser 参数
     * @return 列表
     */
    BaseResultModel getLast(String createUser);

}

