package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.po.Dictionary;
import com.sf.services.talent.model.po.EmploymentTargetType;
import com.sf.services.talent.model.vo.EmploymentTargetRecordVO;
import com.sf.services.talent.model.vo.EmploymentTargetVO;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * IEmploymentTargetService
 *
 * @author guchangliang
 * @date 2021/7/13
 */
public interface IEmploymentTargetService {


    /**
     *获取列表
     * @param params 参数
     * @return 列表
     */
    PageInfo<EmploymentTargetVO> getList(EmploymentTargetParamModel params);



    /**
     * 设定指标-添加或更新-列表形式
     * @param employmentTargetAddParamModel t_employment_target实体
     * @param userId
     * @return
     */
    BaseResultModel add(EmploymentTargetAddParamModel employmentTargetAddParamModel
            , String userId);


    /**
     * 查询就业指标类型列表
     * @param record
     * @return
     */
    PageInfo<EmploymentTargetType> selectTypeList(EmploymentTargerTypeParamModel record);


    /**
     *获取就业指标详情
     * @param targetId 参数
     * @return 列表
     */
    EmploymentTargetVO getDetail(String targetId);

    /**
     * 设定指标-添加或更新-列表形式
     * @param employmentTargetUpdateParamModel t_employment_target实体
     * @param userId
     * @return
     */
    BaseResultModel update(EmploymentTargetUpdateParamModel employmentTargetUpdateParamModel
            , String userId);


    /**
     * 查询就业指标上报列表
     * @param params
     * @return
     */
    PageInfo<EmploymentTargetRecordVO> getReportList(EmploymentTargetReportParamModel params);
    
    /**
     * 查询就业指标上报列表导出
     *
     * @param params 参数
     * @return 列表
     */
    void exportReportList(EmploymentTargetReportParamModel params, HttpServletRequest request, HttpServletResponse response);

    /**
     * 就业指标上报(更新就业指标完成值)
     * @param params t_employment_target_record实体
     * @param userId
     * @return
     */
    BaseResultModel reportUpdate(EmploymentTargetReportAddParamModel params, String userId);
}
