package com.sf.services.talent.model.dto;

import lombok.Data;

import java.util.Date;

/**
 * RankDto
 *
 * @author zhaochangzhi
 * @date 2021/7/11
 */
@Data
public class RankDto {

    private Integer id;

    private String rankCode;

    private String rankName;

    private String address;

    private String attachmentId;

    private String originalFileName;

    private String createUser;

    private Date createTime;

    private String updateUser;

    private Date updateTime;
}
