package com.sf.services.talent.controller;

import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.CheckEmailVerificationCodeParamModel;
import com.sf.services.talent.model.SendEmailVerificationCodeParamModel;
import com.sf.services.talent.model.SendMailParamModel;
import com.sf.services.talent.service.IMailService;
import com.sf.services.talent.util.RedisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api(value = "邮箱", tags = "邮箱")
@RestController
@RequestMapping("/mail")
public class MailController {

	@Autowired
	private IMailService mailService;
	@Autowired
	private RedisUtils redisUtils;

	/**
	 * 文章列表
	 * @param params
	 * @return
	 */
	@IgnoreSecurity
	@ApiOperation(notes = "发送邮件", value="发送邮件")
	@PostMapping("/send")
	public BaseResultModel list(@RequestBody SendMailParamModel params) {

		//发送邮件
		return mailService.send(params);
	}

	@IgnoreSecurity
	@ApiOperation(notes = "发送邮件验证码", value="发送邮件验证码")
	@PostMapping("/sendEmailVerificationCode")
	public BaseResultModel sendEmailVerificationCode(@RequestBody SendEmailVerificationCodeParamModel params) {

		return mailService.sendEmailVerificationCode(params);
	}

	@IgnoreSecurity
	@ApiOperation(notes = "校验邮件验证码", value="校验邮件验证码")
	@PostMapping("/checkEmailVerificationCode")
	public BaseResultModel checkEmailVerificationCode(@RequestBody CheckEmailVerificationCodeParamModel params) {

		return mailService.checkEmailVerificationCode(params);
	}



}

