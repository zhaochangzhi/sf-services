package com.sf.services.talent.controller;

import com.sf.services.talent.annotations.CurrentUser;
import com.sf.services.talent.mapper.RankApplyMapper;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.RankApplyFileParamModel;
import com.sf.services.talent.model.RankApplyParamModel;
import com.sf.services.talent.model.dto.RankApplyDto;
import com.sf.services.talent.model.dto.UserDto;
import com.sf.services.talent.model.po.EnterprisePosition;
import com.sf.services.talent.model.vo.RankApplyVO;
import com.sf.services.talent.service.IActLogService;
import com.sf.services.talent.service.IRankApplyService;
import com.sf.services.talent.util.OtherUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;

/**
 * RankApplyController
 *
 * @author zhaochangzhi
 * @date 2021/7/12
 */

@Slf4j
@Api(value = "职称申请", tags = "职称申请")
@RestController
@RequestMapping("/rankapply")
public class RankApplyController {

    private final IRankApplyService rankApplyService;
    @Resource
    private RankApplyMapper rankApplyMapper;

    @Autowired
    private RedissonClient redisson;

    @Autowired
    public RankApplyController(IRankApplyService rankApplyService) {
        this.rankApplyService = rankApplyService;
    }
    @Resource
    private IActLogService actLogService;//日志

    @ApiOperation(value = "最新一条申请详情", notes = "最新一条申请详情")
    @PostMapping("/last")
    public BaseResultModel<RankApplyVO> last(@CurrentUser UserDto currentUser) {
        RankApplyDto params = new RankApplyDto();
        params.setApplyUserId(currentUser.getUserId());
        return BaseResultModel.success(rankApplyService.getLastOne(params));
    }

    @ApiOperation(value = "网上申报", notes = "网上申报")
    @ApiImplicitParam(name = "params", dataType = "RankApplyParamModel")
    @PostMapping("/add")
    public BaseResultModel add(@RequestBody RankApplyParamModel params, @CurrentUser UserDto currentUser) {
        RankApplyDto rankApplyDto = new RankApplyDto();
        //通用字段校验
        BaseResultModel validateResult = validateParams(params);
        if (validateResult.isBadParam()) {
            return validateResult;
        }

        //redisson分布式锁
        String lockKey = "RankApplyControllerAdd"+currentUser.getUserId();
        RLock rlock = redisson.getLock(lockKey);
        try {
            //上锁
            rlock.lock();

            //校验每自然年只能有一条审核通过数据
            //审核状态：1 审核中，2 通过，3 不通过
            int thisYearPassCount = rankApplyMapper.selectCount(currentUser.getUserId(), 2, OtherUtil.getThisYearFirst(), OtherUtil.getThisYearLast());
            if (thisYearPassCount >= 1) {
                return BaseResultModel.badParam("每自然年只能申请通过一次");
            }

            //校验当前已有待审核状态数据
            //审核状态：1 审核中，2 通过，3 不通过
            int waitReviewCount = rankApplyMapper.selectCount(currentUser.getUserId(), 1, null, null);
            if (waitReviewCount >= 1) {
                return BaseResultModel.badParam("已申请职称，请等待审核");
            }

            //软删除历史申请记录
            rankApplyMapper.softDelete(currentUser.getUserId());

            //新增记录
            BeanUtils.copyProperties(params, rankApplyDto);
            String currentUserId = currentUser.getUserId();
            rankApplyDto.setApplyUserId(currentUserId);
            rankApplyDto.setCreateUser(currentUserId);
            rankApplyDto.setUpdateUser(currentUserId);
            Date nowDate = new Date();
            rankApplyDto.setCreateTime(nowDate);
            rankApplyDto.setUpdateTime(nowDate);

            //保存操作日志
            try {
                actLogService.save(currentUser.getUserId(), "申报职称", null);
            } catch (Exception e) {
                log.error("插入日志异常:{}",e);
            }

            return BaseResultModel.success(rankApplyService.addOne(rankApplyDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("RankApplyControllerAdd", e);
            return BaseResultModel.badParam("异常");
        }finally {
            //解锁
            rlock.unlock();
        }
    }

    @ApiOperation(value = "删除一个职称申请文件", notes = "删除一个职称申请文件")
    @PostMapping("rankapplyfile/delete")
    public BaseResultModel<Integer> deleteRankApplyFile(@RequestBody RankApplyFileParamModel params) {
        if (StringUtils.isBlank(params.getFileId())) {
            return BaseResultModel.badParam("文件id不能为空");
        }
        return BaseResultModel.success(rankApplyService.deleteOneRankApplyFile(params.getFileId()));
    }

    private BaseResultModel validateParams(RankApplyParamModel params) {
        //通用字段校验
        BaseResultModel validateResult = validateCommon(params);
        if (validateResult.isBadParam()) {
            return validateResult;
        }
        String rankApplyTypeKey = params.getRankApplyTypeKey();
        switch (rankApplyTypeKey) {
            case "1001" :
                // 认定
                validateResult = validateIdentify(params);
                break;
            case "1002" :
                validateResult = validateReview(params);
                break;
            default:
                validateResult = BaseResultModel.badParam("申报方式不存在");
                break;
        }
        return validateResult;
    }

    /**
     * 校验通用字段
     * 姓名、手机号、性别、身份证号码、出生日期、学历、所在单位全称、
     *
     * @param params 参数
     * @return 结果
     */
    private BaseResultModel validateCommon(RankApplyParamModel params) {
        if (StringUtils.isBlank(params.getRankCode())) {
            return BaseResultModel.badParam("请选择职称类别");
        }
        if (StringUtils.isBlank(params.getApplyUserName())) {
            return BaseResultModel.badParam("姓名不能为空");
        }
        if (StringUtils.isBlank(params.getApplyUserIdNumber())) {
            return BaseResultModel.badParam("身份证号不能为空");
        }
        if (StringUtils.isBlank(params.getApplyUserBirthday())) {
            return BaseResultModel.badParam("出生日期不能为空");
        }
        if (StringUtils.isBlank(params.getApplyUserEducation())) {
            return BaseResultModel.badParam("学历不能为空");
        }
//        if (StringUtils.isBlank(params.getApplyUserPhone())) {
//            return BaseResultModel.badParam("手机号不能为空");
//        }
        if (StringUtils.isBlank(params.getRankApplyTypeKey())) {
            return BaseResultModel.badParam("职称申报方式字典key值不能为空");
        }
        return BaseResultModel.noData();
    }

    /**
     * 认定方式字段校验
     * 认定专业、认定等级
     *
     * @param params 参数
     * @return 结果
     */
    private BaseResultModel validateIdentify(RankApplyParamModel params) {
        if (StringUtils.isBlank(params.getIdentifyLevel())) {
            return BaseResultModel.badParam("认定等级不能为空");
        }
        if (StringUtils.isBlank(params.getIdentifyMajor())) {
            return BaseResultModel.badParam("认定专业不能为空");
        }
        return BaseResultModel.noData();
    }

    /**
     * 评审方式字段校验
     * 已取得职称证书专业（等级）、申报专业所属行业（专业）、申报等级、论文数量
     *
     * @param params 参数
     * @return 结果
     */
    private BaseResultModel validateReview(RankApplyParamModel params) {
//        if (StringUtils.isBlank(params.getObtainedCredentials())) {
//            return BaseResultModel.badParam("已取得职称证书专业名称不能为空");
//        }
//        if (StringUtils.isBlank(params.getReviewMajor())) {
//            return BaseResultModel.badParam("申报专业所属行业（专业）不能为空");
//        }
//        if (StringUtils.isBlank(params.getReviewLevel())) {
//            return BaseResultModel.badParam("申报等级不能为空");
//        }
        if (params.getPaperSum() == null) {
            return BaseResultModel.badParam("论文数量不能为null");
        }
        return BaseResultModel.noData();
    }

}
