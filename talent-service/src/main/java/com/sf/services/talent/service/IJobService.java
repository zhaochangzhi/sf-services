package com.sf.services.talent.service;

import com.sf.services.talent.model.po.SyrcCompanyJob;
import com.sf.services.talent.model.vo.JobVO;

import java.util.List;

/**
 * JobService
 *
 * @author zhaochangzhi
 * @date 2021/7/7
 */
public interface IJobService {

    /**
     * 查询数量
     *
     * @return 数量
     */
    Integer getCount();

    /**
     * 获取列表
     *
     * @return 列表
     */
    List<JobVO> getList();

    /**
     * 根据id查询详情
     *
     * @return 详情
     */
    JobVO getById(Integer id);

}
