package com.sf.services.talent.model.po;

import lombok.Data;

/**
 * 工作
 *
 * @author zhangchangzhi
 * @date 2021/7/1
 */
@Data
public class SyrcCompanyJob {

    private Integer id;

    private Integer uid;

    private String name;

    private String comName;

    private Integer hy;

    private Integer job1;

    private Integer job1Son;

    private Integer jobPost;

    private Integer provinceid;

    private Integer cityid;

    private Integer threeCityid;

    private String cert;

    private Integer type;

    private Integer number;

    private Integer exp;

    private Integer report;

    private Integer sex;

    private Integer edu;

    private Integer marriage;

    private Integer xuanshang;

    private Integer xsdate;

    private Integer sdate;

    private Integer edate;

    private Integer jobhits;

    private String lastupdate;

    private Integer rec;

    private Integer cloudtype;

    private Integer state;

    private String statusbody;

    private Integer age;

    private Integer pr;

    private Integer mun;

    private Integer comProvinceid;

    private Integer rating;

    private Integer status;

    private Integer urgent;

    private Integer rStatus;

    private Integer endEmail;

    private Integer urgentTime;

    private String comLogo;

    private Integer autotype;

    private Integer autotime;

    private Integer isLink;

    private Integer linkType;

    private Integer source;

    private Integer recTime;

    private Integer snum;

    private Integer operatime;

    private Integer did;

    private Integer isEmail;

    private Integer minsalary;

    private Integer maxsalary;

    private Integer sharepack;

    private Integer rewardpack;

    private Integer isGraduate;

    private String x;

    private String y;
}