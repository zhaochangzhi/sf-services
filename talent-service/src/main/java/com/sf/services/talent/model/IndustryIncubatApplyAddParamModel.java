package com.sf.services.talent.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class IndustryIncubatApplyAddParamModel {

    @ApiModelProperty(value = "公司名称", example = "公司名称")
    private String companyName;

    @ApiModelProperty(value = "注册资本", example = "1000000")
    private String registCapital;

    @ApiModelProperty(value = "法定代表人", example = "法定代表人姓名")
    private String legalRepresentativeName;

    @ApiModelProperty(value = "法定代表人电话", example = "139xxxxxxxx")
    private String legalRepresentativeMobile;

    @ApiModelProperty(value = "联系人", example = "联系人")
    private String contactName;

    @ApiModelProperty(value = "联系人电话", example = "139xxxxxxxx")
    private String contactMobile;

    @ApiModelProperty(value = "联系人邮箱", example = "xxxx@xxxx.com")
    private String contactEmail;

    @ApiModelProperty(value = "公司简介及主营业务方向", example = "公司简介及主营业务方向xxx")
    private String companyIntroduction;


}