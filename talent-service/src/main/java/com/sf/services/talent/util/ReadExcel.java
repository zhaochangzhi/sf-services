package com.sf.services.talent.util;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 读Excel工具
 * guchangliang
 */
public class ReadExcel {
    //总行数
    private int totalRows = 0;
    //总条数
    private int totalCells = 0;
    //构造方法
    public ReadExcel(){}

    /**
     * 读EXCEL文件，获取信息集合
     * @param
     * @return
     */
    public List< Map<String,Object>> getExcelInfo(MultipartFile multipartFile, Integer index,String format)throws IOException{

        List< Map<String,Object>> list=new ArrayList<>();
        //初始化输入流
        FileInputStream is = null;
        InputStream inputStream = multipartFile.getInputStream();
        Workbook wb = null;
        try{
            //根据新建的文件实例化输入流
            if (multipartFile.getOriginalFilename() != null && WDWUtil.isExcel2003(multipartFile.getOriginalFilename())){
                //当excel是2003时
                wb = new HSSFWorkbook(inputStream);
            }
            if (multipartFile.getOriginalFilename() != null &&  WDWUtil.isExcel2007(multipartFile.getOriginalFilename())){
                //当excel是2007时
                wb = new XSSFWorkbook(inputStream);
            }
            list=readExcelValue(wb,index,format);
            inputStream.close();
        }catch(Exception e){
            e.printStackTrace();
        } finally{
            if(inputStream!=null)
            {
                try{
                    inputStream.close();
                }catch(IOException e){
                    inputStream = null;
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

    /**
     * 读取Excel里面的信息
     * @param wb
     * @return
     */
    private List< Map<String,Object>> readExcelValue(Workbook wb, Integer index,String format)throws Exception{
        //得到第一个shell
        Sheet sheet=wb.getSheetAt(0);

        //得到Excel的行数
        this.totalRows=sheet.getPhysicalNumberOfRows();

        //得到Excel的列数(前提是有行数)
        if(totalRows>=1 && sheet.getRow(0) != null){//判断行数大于一，并且第一行必须有标题（这里有bug若文件第一行没值就完了）
            this.totalCells=sheet.getRow(0).getPhysicalNumberOfCells();
        }else{
            return null;
        }

        List<Map<String,Object>> list=new ArrayList<>();//声明一个对象集合

        Map<String,Object> map;//声明一个对象

        //循环Excel行数,从第二行开始。标题不入库
        for(int r=index;r<totalRows;r++){
            Row row = sheet.getRow(r);
            if (row == null) {
                removeRow(sheet,r);
                row = sheet.getRow(r);
            }
            map = new LinkedHashMap<>();

            //循环Excel的列
            for(int c = 0; c <this.totalCells; c++){
                Cell cell = row.getCell(c);
                if (null != cell && !(cell.toString()).equals("")){
                    map.put("key"+c,getCellStringVal(cell,format));//得到行中第一个值
                }
            }
            //添加对象到集合中
            if(map.size()>0){
                list.add(map);
            }
        }
        return list;
    }
    
    
    private static String getCellStringVal(Cell cell,String format) {
        int cellType = cell.getCellType();
        switch (cellType) {
            case 0:
                String value= "";
                 double dValue = cell.getNumericCellValue();
                 DecimalFormat df = new DecimalFormat("0");
                value = df.format(dValue);
                return value;
            case 1:
                return cell.getStringCellValue();
            case 2:
                return cell.getCellFormula();
            case 3:
                return "";
            case 4:
                return String.valueOf(cell.getBooleanCellValue());
            case 5:
                return String.valueOf(cell.getErrorCellValue());
            default:
                return "";
        }
    }
    
    
    /**
     * 校验必填
     * @param str
     * @return
     */
    public  static boolean checkRequired(Object str){
        return null == str || "".equals(str);
    }
    
    
    /**
     * 校验指定长度
     * @param str
     * @param length
     * @return
     */
    public  static boolean checkLength(Object str,Integer length){
        if(null!=str){
            String obj = (String)str;
            return obj.length() > length;
        }else {
            return false;
        }
    }

    /**
     * Remove a row by its index
     * @param sheet a Excel sheet
     * @param rowIndex a 0 based index of removing row
     */
    public static void removeRow(Sheet sheet, int rowIndex) {
        int lastRowNum=sheet.getLastRowNum();
        if(rowIndex>=0&&rowIndex<lastRowNum)
            sheet.shiftRows(rowIndex+1,lastRowNum,-1);//将行号为rowIndex+1一直到行号为lastRowNum的单元格全部上移一行，以便删除rowIndex行
        if(rowIndex==lastRowNum){
            Row removingRow=sheet.getRow(rowIndex);
            if(removingRow!=null)
                sheet.removeRow(removingRow);
        }
    }

    /**
     * 读EXCEL文件，获取信息集合 可以读去指定sheet页
     */
    public List< Map<String,Object>> getExcelInfoBySheet(MultipartFile multipartFile, Integer index,String format,Integer sheetIndex)throws IOException{

        List< Map<String,Object>> list=new ArrayList<>();
        //初始化输入流
        FileInputStream is = null;
        InputStream inputStream = multipartFile.getInputStream();
        Workbook wb = null;
        try{
            //根据新建的文件实例化输入流
            if (multipartFile.getOriginalFilename() != null && WDWUtil.isExcel2003(multipartFile.getOriginalFilename())){
                //当excel是2003时
                wb = new HSSFWorkbook(inputStream);
            }
            if (multipartFile.getOriginalFilename() != null &&  WDWUtil.isExcel2007(multipartFile.getOriginalFilename())){
                //当excel是2007时
                wb = new XSSFWorkbook(inputStream);
            }
            list = readExcelValueBySheet(wb,index,format,sheetIndex);
            inputStream.close();
        }catch(Exception e){
            e.printStackTrace();
        } finally{
            if(inputStream!=null)
            {
                try{
                    inputStream.close();
                }catch(IOException e){
                    inputStream = null;
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

    /**
     * 读取Excel里面的信息
     * @param wb
     * @return
     */
    private List< Map<String,Object>> readExcelValueBySheet(Workbook wb, Integer index,String format,Integer sheetIndex)throws Exception{
        //得到第一个shell
        Sheet sheet=wb.getSheetAt(sheetIndex);

        //得到Excel的行数
        this.totalRows=sheet.getPhysicalNumberOfRows();

        //得到Excel的列数(前提是有行数)
        if(totalRows>=1 && sheet.getRow(0) != null){//判断行数大于一，并且第一行必须有标题（这里有bug若文件第一行没值就完了）
            this.totalCells=sheet.getRow(0).getPhysicalNumberOfCells();
        }else{
            return null;
        }

        List<Map<String,Object>> list=new ArrayList<>();//声明一个对象集合

        Map<String,Object> map;//声明一个对象

        //循环Excel行数,从第二行开始。标题不入库
        for(int r=index;r<totalRows;r++){
            Row row = sheet.getRow(r);
            if (row == null) {
                removeRow(sheet,r);
                row = sheet.getRow(r);
            }
            map = new LinkedHashMap<>();

            //循环Excel的列
            for(int c = 0; c <this.totalCells; c++){
                Cell cell = row.getCell(c);
                if (null != cell && !(cell.toString()).equals("")){
                    map.put("key"+c,getCellStringVal(cell,format));//得到行中第一个值
                }
            }
            //添加对象到集合中
            if(map.size()>0){
                list.add(map);
            }
        }
        return list;
    }

}
