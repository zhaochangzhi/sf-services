package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * EnterpriseUserVO
 * 企业账号
 * @author guchangliang
 * @date 2021/7/23
 */
@ApiModel(value = "企业账号实体")
@Data
public class EnterpriseUserVO extends PageVO {
    @ApiModelProperty(value = "主键id")
    private String id;

    @ApiModelProperty(value = "会员id")
    private String memberId;

    @ApiModelProperty(value = "企业全称")
    private String enterpriseName;

    @ApiModelProperty(value = "行业一级id（z_trade表id）")
    private String tradeOneId;
    @ApiModelProperty(value = "行业一级名称（z_trade表name）")
    private String tradeOneName;
    @ApiModelProperty(value = "行业二级id（z_trade表id）")
    private String tradeTwoId;
    @ApiModelProperty(value = "行业二名称（z_trade表name）")
    private String tradeTwoName;

    @ApiModelProperty(value = "企业性质key（字典t_dictionary表type=EnterpriseNature数据dickey）")
    private String enterpriseNatureKey;
    @ApiModelProperty(value = "企业性质（字典t_dictionary表type=EnterpriseNature数据value）")
    private String enterpriseNatureValue;

    @ApiModelProperty(value = "企业规模key（字典t_dictionary表type=EnterpriseScale数据dickey）")
    private String enterpriseScaleKey;
    @ApiModelProperty(value = "企业规模（字典t_dictionary表type=EnterpriseScale数据value）")
    private String enterpriseScaleValue;

    @ApiModelProperty(value = "地址-详细")
    private String addressDetailed;

    @ApiModelProperty(value = "企业简介")
    private String enterpriseIntroduction;

    @ApiModelProperty(value = "联系电话")
    private String mobile;

    @ApiModelProperty(value = "Email")
    private String email;

    @ApiModelProperty(value = "企业logoid（t_file表file_id）")
    private String enterpriseLogoImageId;
    @ApiModelProperty(value = "企业logo图片地址")
    private String enterpriseLogoImage;

    @ApiModelProperty(value = "统一社会信用代码")
    private String enterpriseLcreditCode;

    @ApiModelProperty(value = "营业执照id（t_file表file_id）")
    private String enterpriseBusinessLicenseImageId;
    @ApiModelProperty(value = "营业执照图片地址")
    private String enterpriseBusinessLicenseImage;


    @ApiModelProperty(value = "发布的职位数量")
    private Integer positionNum;
    @ApiModelProperty(value = "企业职位被收藏数量")
    private Integer collectionNum;
    @ApiModelProperty(value = "收到的简历数量")
    private Integer resumeNum;
    @ApiModelProperty(value = "是否为著名企业（0：否，1：是）")
    private Integer isfamous;
    @ApiModelProperty(value = "联系人")
    private String contacts;

    @ApiModelProperty(value = "用户名")
    private String userName;



}