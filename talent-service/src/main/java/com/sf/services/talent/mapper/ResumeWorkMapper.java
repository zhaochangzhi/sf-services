package com.sf.services.talent.mapper;

import java.util.List;

import com.sf.services.talent.model.po.ResumeWork;

public interface ResumeWorkMapper {
    int deleteByPrimaryKey(String id);

    int insert(ResumeWork record);

    int insertSelective(ResumeWork record);

    List<ResumeWork> selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ResumeWork record);

    int updateByPrimaryKey(ResumeWork record);
    
    int insertBatch(List<ResumeWork> resumeWorkList);
}