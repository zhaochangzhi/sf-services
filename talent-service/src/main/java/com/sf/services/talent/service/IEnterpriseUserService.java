package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.vo.EnterpriseUserVO;

/**
 * @ClassName: IEnterpriseUserService
 * @Description: 企业用户接口
 * @author: guchangliang
 * @date: 2021年7月23日 下午11:19:07
 */
public interface IEnterpriseUserService {
	/**
	 * @Title: getEnterpriseUser
	 * @Description: 获取用户
	 * @return
	 * @return: EnterpriseUser
	 */
	EnterpriseUserVO getEnterpriseUser(EnterpriseUserGetParamModel params);
	
	/**
	 * @Title: update 
	 * @Description: 更新企业用户信息
	 * @param enterpriseUserParamModel
	 * @return
	 * @return: int
	 */
	BaseResultModel update(EnterpriseUserParamModel enterpriseUserParamModel);

	/**
	 * @Title: updateIsfamous
	 * @Description: 更新企业用户是否为名企
	 * @param params
	 * @return
	 * @return: int
	 */
	BaseResultModel updateIsfamous(EnterpriseUserUpdateIsfamousParamModel params, String userId);

	/**
	 * 企业账号初始化
	 * @param memberId
	 * @param createUser
	 * @return
	 */
	BaseResultModel enterpriseUserInit(String memberId, String createUser);

	/**
	 *获取列表
	 * @param params 参数
	 * @return 列表
	 */
	PageInfo<EnterpriseUserVO> getList(EnterpriseUserListParamModel params);
	
	/**
	 * @Title: delEnterpriseUser 
	 * @Description: 删除公司用户
	 * @param memberId
	 * @return
	 * @return: int
	 */
	int delEnterpriseUser(String memberId, String updateUser);

	/**
	 * 批量删除
	 * @param ids
	 * @param updateUser
	 * @return
	 */
	BaseResultModel batchDelete(String ids, String updateUser);
}

