package com.sf.services.talent.util;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.net.ssl.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
* <p>Description: 用okhttp进行get和post的访问</p>
* @date 2021年8月24日
*/
@Slf4j
@Component  
public class OKhttpUtil {

	protected static Logger interfaceLog = LoggerFactory.getLogger("interfaceLog");//interfaceLog
	private static OkHttpClient okHttpClient;

	private static final MediaType CONTENT_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");
	public static final MediaType FORM_CONTENT_TYPE = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");


	/**
	 * 绕过验证
	 *
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 */
	//获取这个SSLSocketFactory
	public static SSLSocketFactory getSSLSocketFactory() {
		try {
			SSLContext sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, getTrustManager(), new SecureRandom());
			return sslContext.getSocketFactory();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	//获取TrustManager
	private static TrustManager[] getTrustManager() {
		TrustManager[] trustAllCerts = new TrustManager[]{
			new X509TrustManager() {
				@Override
				public void checkClientTrusted(X509Certificate[] chain, String authType) {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] chain, String authType) {
				}

				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[]{};
				}
			}
		};
		return trustAllCerts;
	}

	//获取TrustManager
	private static TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
		@Override
		public void checkClientTrusted(X509Certificate[] chain, String authType) {
		}

		@Override
		public void checkServerTrusted(X509Certificate[] chain, String authType) {
		}

		@Override
		public X509Certificate[] getAcceptedIssuers() {
						return new X509Certificate[]{};
					}
	}};

	//获取HostnameVerifier
	public static HostnameVerifier getHostnameVerifier() {
		HostnameVerifier hostnameVerifier = new HostnameVerifier() {
			@Override
			public boolean verify(String s, SSLSession sslSession) {
				return true;
			}
		};
		return hostnameVerifier;
	}


	@SuppressWarnings("deprecation")
	public OKhttpUtil() {
		if (okHttpClient == null) {
			ConnectionPool pool = new ConnectionPool(5, 10, TimeUnit.MINUTES);
			// 配置请求的超时设置  设置连接池属性 设置ssl绕过验证
			X509TrustManager trustAllCert = (X509TrustManager) trustAllCerts[0];
			okHttpClient = new OkHttpClient.Builder() //
					.connectTimeout(10, TimeUnit.SECONDS) //连接时长
					.followRedirects(true) //
					.readTimeout(10, TimeUnit.SECONDS) //
					.retryOnConnectionFailure(false) //
					.writeTimeout(10, TimeUnit.SECONDS) //
					.connectionPool(pool) //
					.sslSocketFactory(getSSLSocketFactory(), trustAllCert)
					.hostnameVerifier(getHostnameVerifier())
					.build();
		}
	}

	private void config(Request.Builder builder) {
		builder.addHeader("User-Agent", "Mozilla/5.0");
		builder.addHeader("Accept", "text/html,application/xhtml+xml,application/xml,application/json;q=0.9,*/*;q=0.8");
		builder.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");//"en-US,en;q=0.5");  
		builder.addHeader("Accept-Charset", "ISO-8859-1,utf-8,gbk,gb2312;q=0.7,*;q=0.7");
		builder.addHeader("Connection", "close");
	}


	/**
	 * <p>setHeaders</p>
	 * <p>Description: 设置请求头 </p>
	 * @param headersMap
	 * @return
	 * @author guchangliang
	 */
	private Headers setHeaders(Map<String, String> headersMap) {
		Headers headers = null;
		Headers.Builder headersbuilder = new Headers.Builder();
		if (headersMap != null) {
			Iterator<String> iterator = headersMap.keySet().iterator();
			String key = "";
			while (iterator.hasNext()) {
				key = iterator.next().toString();
				headersbuilder.add(key, headersMap.get(key));
			}
		}
		headers = headersbuilder.build();
		return headers;
	}


	/**
	 * <p>httpGet</p>
	 * <p>Description: get访问</p>
	 * @param host       域名或ip
	 * @param url        接口名
	 * @param paramsMap  入参
	 * @param headersMap header
	 * @return
	 */
	public Map<String, Object> httpGet(String host, String url, Map<String, String> paramsMap, Map<String, String> headersMap) {
		Map<String, Object> returnMap = new HashMap<String, Object>();//返回map	成功：returnStatus=SUCCEED	
		String timeBegin = OtherUtil.returntime();//接口调用开始时间
		try {
			String urlParam = "";//url传入参数
			//遍历签名map
			int i = 0;
			for (Map.Entry<String, String> entry : paramsMap.entrySet()) {
				if (i == 0) {
					urlParam += "?" + entry.getKey() + "=" + entry.getValue();
				} else {
					urlParam += "&" + entry.getKey() + "=" + entry.getValue();
				}
				i++;
			}

			//完整url
			url = host + url + urlParam;

			Request.Builder builder = new Request.Builder();
			config(builder);
			Request request = builder.headers(setHeaders(headersMap)).url(url).get().build();

			Response response = okHttpClient.newCall(request).execute();

			returnMap = JSON.parseObject(response.body().string(), Map.class);

			if (!response.isSuccessful()) {
				return returnMap;
			}

			return returnMap;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("httpGet========异常信息：" + e + " ,请求地址：" + url + ",请求参数：" + JSON.toJSONString(paramsMap));
			return returnMap;
		} finally {
			String timeEnd = OtherUtil.returntime();
			long second = OtherUtil.subTimeSecond(timeBegin, timeEnd);
			interfaceLog.info("httpGet========返回值：" + returnMap + " ,请求地址：" + url + ",请求参数：" + JSON.toJSONString(paramsMap) + "second=" + second);
		}

	}




}
