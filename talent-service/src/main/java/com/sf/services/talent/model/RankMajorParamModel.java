package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 职称专业参数实体
 *
 * @author zhaochangzhi
 * @date 2021/7/10
 */
@Data
@ApiModel(value = "RankMajorParamModel", description = "职称专业参数实体")
public class RankMajorParamModel {

    @ApiModelProperty(value = "父级id")
    private String parentId;

    @ApiModelProperty(value = "级别", hidden = true)
    private Integer level;
}
