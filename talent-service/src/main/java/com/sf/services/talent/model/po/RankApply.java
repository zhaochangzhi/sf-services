package com.sf.services.talent.model.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * RankApply
 *
 * @author zhaochangzhi
 * @date 2021/7/10
 */
@Data
public class RankApply {

    private String id;

    private String rankCode;

    private String rankApplyTypeKey;
    private String rankApplyType;

    private String applyUserId;

    private String applyUserAccount;

    private String applyUserName;

    private String applyUserSex;

    private String applyUserIdNumber;

    private String applyUserBirthday;

    private String applyUserPhone;

    private String applyUserEducation;

    private String applyUserEducation2;

    private String applyUserEducation3;

    private String applyUserCompanyFullName;

    private String identifyMajor;

    private String identifyLevel;

    private String obtainedCredentials;
    private String obtainedCredentialsLevel;
    private String obtainedCredentialsTime;

    private String reviewMajor;
    private String learnMajor;
    private String learnMajor2;
    private String learnMajor3;

    private String reviewLevel;

    private Integer paperSum;

    private String auditStatus;

    private String auditUserId;

    private String auditUserName;

    private Date auditTime;

    private String auditComment;

    private Date appointmentStartTime;

    private Date appointmentEndTime;

    private String createUser;

    private Date createTime;

    private String updateUser;

    private Date updateTime;

    private Integer isdelete;

    @JsonFormat(pattern="yyyy-MM-dd")
    public Date getAppointmentStartTime() {
        return appointmentStartTime;
    }

    public void setAppointmentStartTime(Date appointmentStartTime) {
        this.appointmentStartTime = appointmentStartTime;
    }

    @JsonFormat(pattern="yyyy-MM-dd")
    public Date getAppointmentEndTime() {
        return appointmentEndTime;
    }

    public void setAppointmentEndTime(Date appointmentEndTime) {
        this.appointmentEndTime = appointmentEndTime;
    }
}
