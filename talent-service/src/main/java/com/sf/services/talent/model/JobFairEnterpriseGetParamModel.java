package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * @author guchangliang
 * @date 2021/8/3
 */
@ApiModel(value = "JobFairEnterpriseGetParamModel", description = "招聘会详情-参与企业详情")
@Data
public class JobFairEnterpriseGetParamModel {

    @ApiModelProperty(value = "招聘会id（t_job_fair表id）", example = "56b2f8f7e6d1494481050904aeeb75db")
    private String JobFairId;

    @ApiModelProperty(value = "企业账号id（t_enterprise_user表id）", example = "12a53ef7c0af4a52ac9f0e8c1001a0ad")
    private String enterpriseUserId;

    @ApiModelProperty(value = "审核状态：1 审核中，2 通过，3 不通过", example = "2")
    private Integer auditStatus;


}
