package com.sf.services.talent.mapper;

import com.sf.services.talent.model.po.RankApplyOpenTime;

public interface RankApplyOpenTimeMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(RankApplyOpenTime record);

    RankApplyOpenTime selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(RankApplyOpenTime record);

    //查询第一条
    RankApplyOpenTime selectFirst();

}