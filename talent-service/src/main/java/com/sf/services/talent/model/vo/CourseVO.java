package com.sf.services.talent.model.vo;

import lombok.Data;

import java.util.Date;

@Data
public class CourseVO {
    private String id;

    private String courseName;

    private String courseTypeId;

    private String courseTypeName;

    private Date startTime;

    private String startAddress;
}