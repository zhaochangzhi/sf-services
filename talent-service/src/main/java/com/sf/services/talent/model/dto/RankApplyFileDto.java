package com.sf.services.talent.model.dto;

import lombok.Data;

/**
 * RankApplyFileDto
 *
 * @author zhaochangzhi
 * @date 2021/7/11
 */
@Data
public class RankApplyFileDto {

    private String fileId;

    private String originalFileName;

    private String fileUrl;
    private String typeStatus;
    private String typeName;
}
