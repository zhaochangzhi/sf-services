package com.sf.services.talent.model.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MemberVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 665411473802607972L;

	@ApiModelProperty(value = "用户名")
	private String userName;
	@ApiModelProperty(value = "密码")
	private String password;
	@ApiModelProperty(value = "1：个人，2公司，3政府")
	private String type;
	
	private String memberId;
	
	private String isSystem;


	
}