package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class Position {
    private String id;

    private String parentId;

    private Integer dataLevel;

    private String name;

    private Integer sort;

    private String remark;

    private Date creatTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;

    private List<Position> positionList;


}