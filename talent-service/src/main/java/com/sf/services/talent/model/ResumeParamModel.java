package com.sf.services.talent.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "简历", description = "简历")
public class ResumeParamModel extends BasePageModel{
	@ApiModelProperty(value = "主键id", example = "49f36f829ce6449294ad1f19e2a6aa59")
	private String id;
	@ApiModelProperty(value = "工作省", example = "10")
	private String workCity1;
	@ApiModelProperty(value = "工作城市", example = "123")
	private String workCity2;
	@ApiModelProperty(value = "工作1级", example = "49f36f829ce6449294ad1f19e2a6aa59")
	private String job1;
	@ApiModelProperty(value = "工作2级", example = "49f36f829ce6449294ad1f19e2a6aa59")
	private String job2;
	@ApiModelProperty(value = "工作3级", example = "49f36f829ce6449294ad1f19e2a6aa59")
	private String job3;
	@ApiModelProperty(value = "行业1级", example = "49f36f829ce6449294ad1f19e2a6aa59")
	private String industry1;
	@ApiModelProperty(value = "行业2级", example = "49f36f829ce6449294ad1f19e2a6aa59")
	private String industry2;
	@ApiModelProperty(value = "薪资最小", example = "1000")
	private String salaryMin;
	@ApiModelProperty(value = "薪资最大", example = "3000")
	private String salaryMax;
	@ApiModelProperty(value = "工作状态", example = "49f36f829ce6449294ad1f19e2a6aa59")
	private String jobStatus;
	@ApiModelProperty(value = "到达时间", example = "49f36f829ce6449294ad1f19e2a6aa59")
	private String arrivalTime;
	
	@ApiModelProperty(value = "工作经历", example = "[]")
	private List<ResumeWorkParamModel> resumeWorkList;
	
	@ApiModelProperty(value = "教育经历", example = "[]")
	private List<ResumeEducationParamModel> resumeEducationList;
	private String userName;
	private String userSex;
	private String identity;
	private String phone;
	private String political;
	private String education;
	private String workExperience;
	private String email;
	private String address;
	private String userPhoto;
	private String Switch;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
	private Date birthday;
	    
	private Integer age;
	
	private String memberId;
	
	@ApiModelProperty(value = "工作对象列表", example = "[]")
	private List<Map<String, Object>> resumeJobList;
	@ApiModelProperty(value = "行业列表", example = "[]")
	private List<Map<String, Object>> resumeIndustryList;
}
