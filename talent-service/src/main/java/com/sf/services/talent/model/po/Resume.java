package com.sf.services.talent.model.po;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class Resume {
    private String id;

    private String workCity1;
    
    private String workCity1Desc;

    private String workCity2;
    
    private String workCity2Desc;

    private String job1;

    private String job1Desc;

    private String job2;
    
    private String job2Desc;

    private String job3;

    private String job3Desc;

    private String industry1;
    
    private String industry1Desc;

    private String industry2;
    
    private String industry2Desc;

    private String salaryMin;

    private String salaryMax;

    private String jobStatus;
    
    private String jobStatusDesc;

    private String arrivalTime;
    
    private String arrivalTimeDesc;

    private Date createDate;

    private String createUser;

    private Date updateDate;

    private String updateUser;
    
    private String memberId;
    
    private List<ResumeWork> resumeWorkList;
    
    private List<ResumeEducation>  resumeEducationList;
    
    private String salaryRequirement;
    
    private String userName;
    
    private String userSex;
    
    private String identity;
    
    private String phone;
    
    private String political;
    
    private String education;
    
    private String workExperience;
    
    private String email;
    
    private String address;
    
    private String userPhoto;
    
    private String Switch;
    
    private String sexDesc;
    
    private String imageId;
    private String imageUrl;

    private Date birthday;
    
    private Integer age;

    private String politicalDesc;
    
    private String birthdayDesc;
    
    private List<List<Map<String, String>>> resumeIndustryList;
    
    private List<List<Map<String, String>>> resumeJobList;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getWorkCity1() {
        return workCity1;
    }

    public void setWorkCity1(String workCity1) {
        this.workCity1 = workCity1 == null ? null : workCity1.trim();
    }

    public String getWorkCity2() {
        return workCity2;
    }

    public void setWorkCity2(String workCity2) {
        this.workCity2 = workCity2 == null ? null : workCity2.trim();
    }

    public String getJob1() {
        return job1;
    }

    public void setJob1(String job1) {
        this.job1 = job1 == null ? null : job1.trim();
    }

    public String getJob2() {
        return job2;
    }

    public void setJob2(String job2) {
        this.job2 = job2 == null ? null : job2.trim();
    }

    public String getJob3() {
        return job3;
    }

    public void setJob3(String job3) {
        this.job3 = job3 == null ? null : job3.trim();
    }

    public String getIndustry1() {
        return industry1;
    }

    public void setIndustry1(String industry1) {
        this.industry1 = industry1 == null ? null : industry1.trim();
    }

    public String getIndustry2() {
        return industry2;
    }

    public void setIndustry2(String industry2) {
        this.industry2 = industry2 == null ? null : industry2.trim();
    }

    public String getSalaryMin() {
        return salaryMin;
    }

    public void setSalaryMin(String salaryMin) {
        this.salaryMin = salaryMin == null ? null : salaryMin.trim();
    }

    public String getSalaryMax() {
        return salaryMax;
    }

    public void setSalaryMax(String salaryMax) {
        this.salaryMax = salaryMax == null ? null : salaryMax.trim();
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus == null ? null : jobStatus.trim();
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime == null ? null : arrivalTime.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

	public List<ResumeWork> getResumeWorkList() {
		return resumeWorkList;
	}

	public void setResumeWorkList(List<ResumeWork> resumeWorkList) {
		this.resumeWorkList = resumeWorkList;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public List<ResumeEducation> getResumeEducationList() {
		return resumeEducationList;
	}

	public void setResumeEducationList(List<ResumeEducation> resumeEducationList) {
		this.resumeEducationList = resumeEducationList;
	}

	public String getJobStatusDesc() {
		return jobStatusDesc;
	}

	public void setJobStatusDesc(String jobStatusDesc) {
		this.jobStatusDesc = jobStatusDesc;
	}

	public String getArrivalTimeDesc() {
		return arrivalTimeDesc;
	}

	public void setArrivalTimeDesc(String arrivalTimeDesc) {
		this.arrivalTimeDesc = arrivalTimeDesc;
	}

	public String getIndustry1Desc() {
		return industry1Desc;
	}

	public void setIndustry1Desc(String industry1Desc) {
		this.industry1Desc = industry1Desc;
	}

	public String getIndustry2Desc() {
		return industry2Desc;
	}

	public void setIndustry2Desc(String industry2Desc) {
		this.industry2Desc = industry2Desc;
	}

	public String getWorkCity1Desc() {
		return workCity1Desc;
	}

	public void setWorkCity1Desc(String workCity1Desc) {
		this.workCity1Desc = workCity1Desc;
	}

	public String getWorkCity2Desc() {
		return workCity2Desc;
	}

	public void setWorkCity2Desc(String workCity2Desc) {
		this.workCity2Desc = workCity2Desc;
	}

	public String getJob1Desc() {
		return job1Desc;
	}

	public void setJob1Desc(String job1Desc) {
		this.job1Desc = job1Desc;
	}

	public String getJob2Desc() {
		return job2Desc;
	}

	public void setJob2Desc(String job2Desc) {
		this.job2Desc = job2Desc;
	}

	public String getJob3Desc() {
		return job3Desc;
	}

	public void setJob3Desc(String job3Desc) {
		this.job3Desc = job3Desc;
	}

	public String getSalaryRequirement() {
		return salaryRequirement;
	}

	public void setSalaryRequirement(String salaryRequirement) {
		this.salaryRequirement = salaryRequirement;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserSex() {
		return userSex;
	}

	public void setUserSex(String userSex) {
		this.userSex = userSex;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPolitical() {
		return political;
	}

	public void setPolitical(String political) {
		this.political = political;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getWorkExperience() {
		return workExperience;
	}

	public void setWorkExperience(String workExperience) {
		this.workExperience = workExperience;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUserPhoto() {
		return userPhoto;
	}

	public void setUserPhoto(String userPhoto) {
		this.userPhoto = userPhoto;
	}

	public String getSwitch() {
		return Switch;
	}

	public void setSwitch(String switch1) {
		Switch = switch1;
	}

	public String getSexDesc() {
		return sexDesc;
	}

	public void setSexDesc(String sexDesc) {
		this.sexDesc = sexDesc;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getPoliticalDesc() {
		return politicalDesc;
	}

	public void setPoliticalDesc(String politicalDesc) {
		this.politicalDesc = politicalDesc;
	}

	public String getBirthdayDesc() {
		return birthdayDesc;
	}

	public void setBirthdayDesc(String birthdayDesc) {
		this.birthdayDesc = birthdayDesc;
	}

	public List<List<Map<String, String>>> getResumeIndustryList() {
		return resumeIndustryList;
	}

	public void setResumeIndustryList(List<List<Map<String, String>>> resumeIndustryList) {
		this.resumeIndustryList = resumeIndustryList;
	}

	public List<List<Map<String, String>>> getResumeJobList() {
		return resumeJobList;
	}

	public void setResumeJobList(List<List<Map<String, String>>> resumeJobList) {
		this.resumeJobList = resumeJobList;
	}


}