package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 就业指标筛选条件参数实体
 *
 * @author guchangliang
 * @date 2021/7/13
 */
@Data
@ApiModel(value = "EmploymentTargetParamModel", description = "就业指标筛选条件参数实体")
public class EmploymentTargetParamModel extends PageParamModel {

    @ApiModelProperty(value = "街道key（t_dictionary表dickey）", example = "Street0001")
    private String streetKey;

    @ApiModelProperty(value = "指标年份", example = "2021")
    private Integer targetYear;

}
