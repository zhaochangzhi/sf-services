package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class JobFairUserVO {

    @ApiModelProperty(value = "主键id（t_job_fair_user表id）")
    private String id;

    @ApiModelProperty(value = "招聘会id（t_job_fair表id）")
    private String jobFairId;

    @ApiModelProperty(value = "招聘会标题（job_fair表title）")
    private String jobFairTitle;

    @ApiModelProperty(value = "个人用户userId")
    private String userId;

    @ApiModelProperty(value = "个人用户memberId")
    private String memberId;

    @ApiModelProperty(value = "姓名")
    private String userName;

    @ApiModelProperty(value = "头像")
    private String imageUrl;

    @ApiModelProperty(value = "性别")
    private String sex;

    @ApiModelProperty(value = "年龄")
    private String age;

    @ApiModelProperty(value = "简历id")
    private String resumeId;

    @ApiModelProperty(value = "工作经验")
    private String workExperience;

    @ApiModelProperty(value = "学历")
    private String education;

    @ApiModelProperty(value = "工作1级名称")
    private String job1Name;
    @ApiModelProperty(value = "工作2级名称")
    private String job2Name;
    @ApiModelProperty(value = "工作3级名称")
    private String job3Name;

    @ApiModelProperty(value = "行业1级名称")
    private String industry1Name;
    @ApiModelProperty(value = "行业2级名称")
    private String industry2Name;

    @ApiModelProperty(value = "是否在线")
    private Boolean isOnline;

}