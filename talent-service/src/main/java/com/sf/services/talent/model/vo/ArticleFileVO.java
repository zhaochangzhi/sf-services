package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ArticleFileVO {

    @ApiModelProperty(value = "主键id")
    private String id;

    @ApiModelProperty(value = "文件id")
    private String fileId;

    @ApiModelProperty(value = "文件地址")
    private String fileUrl;

    @ApiModelProperty(value = "文件名称")
    private String originalFileName;


}