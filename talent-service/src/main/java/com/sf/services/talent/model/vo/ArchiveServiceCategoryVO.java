package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@ApiModel(value = "档案预约类别实体")
@Data
public class ArchiveServiceCategoryVO extends PageVO{
	
	
    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = 5246435867519399819L;

	@ApiModelProperty(value = "主键id")
	private String id;

	@ApiModelProperty(value = "档案类别")
    private String name;
	
	@ApiModelProperty(value = "档案办案地址")
    private String processAddress;
    
    @ApiModelProperty(value = "档案字类别")
    private String childName;
    
    @ApiModelProperty(value = "档案处理要件")
    private String handlingElements;


}