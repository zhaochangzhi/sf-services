package com.sf.services.talent.controller;

import com.sf.services.talent.annotations.CurrentUser;
import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.dto.UserDto;
import com.sf.services.talent.service.IWebSectionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Slf4j
@Api(value = "网站栏目", tags = "网站栏目")
@RestController
@RequestMapping("/webSection")
public class WebSectionController {
	
	@Resource
	private IWebSectionService webSectionService;

	@IgnoreSecurity
	@ApiOperation(notes = "网站栏目列表", value="网站栏目列表")
	@PostMapping("/list")
	public BaseResultModel list(@RequestBody WebSectionParamModel param) {


		return BaseResultModel.success(webSectionService.list(param));
	}

	/**
	 * 文章详情
	 * @param param
	 * @return
	 */
	@IgnoreSecurity
	@ApiOperation(notes = "网站栏目详情", value="网站栏目详情")
	@PostMapping("/get")
	public BaseResultModel get(@RequestBody GetOneParamModel param) {

		if(param == null || StringUtils.isBlank(param.getId())){
			return BaseResultModel.badParam("参数不能为空");
		}

		return BaseResultModel.success(webSectionService.get(param.getId()));
	}

	@ApiOperation(notes = "编辑网站栏目", value="编辑网站栏目")
	@PostMapping("/update")
	public BaseResultModel update(@RequestBody WebSectionUpdateParamModel param
			, @CurrentUser UserDto currentUser) {

		if(param == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if (currentUser == null || StringUtils.isBlank(currentUser.getUserId())) {
			return BaseResultModel.badParam("请登陆");
		}

		return webSectionService.update(param, currentUser.getUserId());
	}
}

