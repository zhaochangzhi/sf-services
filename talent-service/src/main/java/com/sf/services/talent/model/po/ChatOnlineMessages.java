package com.sf.services.talent.model.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class ChatOnlineMessages {
    private String id;

    private String jobFairId;

    private String fromMemberId;

    private String toMemberId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private Date sendTime;

    private Date receiveTime;

    private Integer status;

    private String messagesTypeId;

    private String messages;

    private String remark;

    private Date createTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;

}