package com.sf.services.talent.config;
 
 
import org.apache.commons.lang3.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
 
/**
 * 分布式事务锁
 * @author guchangliang
 *
 */
@Configuration
public class RedissonClientConfig {
 
    @Value("${spring.redis.host}")
    private String host;
    @Value("${spring.redis.port}")
    private String port;
    @Value("${spring.redis.password}")
    private String password;
 
    @Bean
    public RedissonClient getRedisson(){
        Config config = new Config();
        config.useSingleServer().setAddress("redis://" + host + ":" + port);
        //密码不为空则设置密码
        if(!StringUtils.isBlank(password)){
        	config.useSingleServer().setPassword(password);
        }
        
        return Redisson.create(config);
    }
}
