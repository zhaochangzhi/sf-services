package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询入园申请列表
 * @author guchangliang
 * @date 2021/8/13
 */
@Data
@ApiModel(description = "查询入园申请列表")
public class IndustryIncubatApplyListParamModel extends PageParamModel {


    @ApiModelProperty(value = "审核状态：1 审核中，2 通过，3 不通过", example = "2")
    private Integer auditStatus;

    @ApiModelProperty(value = "创建人id", example = "bc30f325323e4498ad455365f7603ac7")
    private String createUser;


}
