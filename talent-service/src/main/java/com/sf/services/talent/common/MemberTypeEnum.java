package com.sf.services.talent.common;

/**
 * MemberTypeEnum
 *
 * @author zhaochangzhi
 * @date 2021/7/23
 */
public enum MemberTypeEnum {

    PERSONAL("1", "个人"), COMPANY("2", "企业"), ADMIN("0", "管理员");

    MemberTypeEnum(String type, String name){
        this.type = type;
        this.name = name;
    }

    private String type;

    private String name;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
