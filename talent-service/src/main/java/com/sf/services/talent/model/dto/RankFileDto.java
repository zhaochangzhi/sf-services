package com.sf.services.talent.model.dto;

import lombok.Data;

/**
 * RankDto
 *
 * @author zhaochangzhi
 * @date 2021/7/11
 */
@Data
public class RankFileDto {

    private String id;

    private String rankCode;

    private String rankName;

    private String attachmentId;

    private String originalFileName;

    private String[] fileId;

    private String rankApplyTypeKey;
    private String rankApplyType;

}
