package com.sf.services.talent.model.po;

import lombok.Data;

import java.util.Date;

/**
 * JobFair
 *
 * @author zhaochangzhi
 * @date 2021/7/23
 */
@Data
public class JobFair {

    private String id;

    private Integer typeStatus;

    private String title;

    private String sponsor;

    private String address;

    private Date startTime;

    private Date endTime;

    private String contacts;

    private String contactsPhone;

    private String imageId;

    private Integer status;

    private String createUser;

    private Date createTime;

    private String updateUser;

    private Date updateTime;

    private Integer deleted;

    private String routes;
    private String introduce;
    private String media;
    private String booth;
    private String joinMethod;
}
