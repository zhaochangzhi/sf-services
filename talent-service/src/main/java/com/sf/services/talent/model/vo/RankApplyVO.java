package com.sf.services.talent.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sf.services.talent.model.dto.RankApplyFileDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * RankApplyVO
 *
 * @author zhaochangzhi
 * @date 2021/7/10
 */
@ApiModel(value = "RankApplyVO", description = "职称申请实体")
@Data
public class RankApplyVO {

    @ApiModelProperty(value = "主键", example = "uuid")
    private String id;

    @ApiModelProperty(value = "职称码")
    private String rankCode;

    @ApiModelProperty("职称申报方式字典key值")
    private String rankApplyTypeKey;

    @ApiModelProperty("职称申报方式(文字描述)")
    private String rankApplyType;

    @ApiModelProperty(value = "职称名称")
    private String rankName;

    @ApiModelProperty(value = "申请人id")
    private String applyUserId;

    @ApiModelProperty(value = "申请人账号")
    private String applyUserAccount;

    @ApiModelProperty(value = "申请人姓名")
    private String applyUserName;

    @ApiModelProperty(value = "申请人性别")
    private String applyUserSex;

    @ApiModelProperty(value = "申请人性别文字")
    private String applyUserSexDesc;

    @ApiModelProperty(value = "申请人身份证号")
    private String applyUserIdNumber;

    @ApiModelProperty(value = "申请人出生日期")
    private String applyUserBirthday;

    @ApiModelProperty(value = "申请人电话", example = "13940711234")
    private String applyUserPhone;

    @ApiModelProperty(value = "申请人学历")
    private String applyUserEducation;

    @ApiModelProperty(value = "申请人第二学历")
    private String applyUserEducation2;

    @ApiModelProperty(value = "申请人第三学历")
    private String applyUserEducation3;

    @ApiModelProperty(value = "所属单位全称")
    private String applyUserCompanyFullName;

    @ApiModelProperty(value = "认定专业")
    private String identifyMajor;

    @ApiModelProperty(value = "认定等级")
    private String identifyLevel;

    @ApiModelProperty(value = "已取得职称证书名称")
    private String obtainedCredentials;
    @ApiModelProperty(value = "已取得职称证书等级")
    private String obtainedCredentialsLevel;
    @ApiModelProperty(value = "已取得职称证书时间", example = "2021-07-28")
    private String obtainedCredentialsTime;

    @ApiModelProperty(value = "申报专业所属行业（专业）")
    private String reviewMajor;
    @ApiModelProperty(value = "所学专业", example = "xxxx")
    private String learnMajor;
    @ApiModelProperty(value = "所学专业2", example = "xxxx")
    private String learnMajor2;
    @ApiModelProperty(value = "所学专业3", example = "xxxx")
    private String learnMajor3;

    @ApiModelProperty(value = "申报等级")
    private String reviewLevel;

    @ApiModelProperty(value = "论文数量")
    private Integer paperSum;

    @ApiModelProperty(value = "审核状态：1 审核中， 2 审核通过， 3 审核不通过", example = "1")
    private String auditStatus;

    @ApiModelProperty(value = "审核状态：1 审核中， 2 审核通过， 3 审核不通过", example = "审核中")
    private String auditStatusDesc;

    @ApiModelProperty(value = "审核人id")
    private String auditUserId;

    @ApiModelProperty(value = "审核人姓名")
    private String auditUserName;

    @ApiModelProperty(value = "审核时间")
    private Date auditTime;

    @ApiModelProperty(value = "审核备注")
    private String auditComment;

    @ApiModelProperty(value = "附件列表")
    private List<RankApplyFileDto> attachmentList;

    @ApiModelProperty(value = "预约办理开始时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date appointmentStartTime;

    @ApiModelProperty(value = "预约办理结束时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date appointmentEndTime;
}
