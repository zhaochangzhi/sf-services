package com.sf.services.talent.mapper;

import java.util.List;
import java.util.Map;

import com.sf.services.talent.model.po.PolicyApply;

public interface PolicyApplyMapper {
    int deleteByPrimaryKey(String id);

    int insert(PolicyApply record);

    int insertSelective(PolicyApply record);

    PolicyApply selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PolicyApply record);

    int updateByPrimaryKey(PolicyApply record);
    
    List<PolicyApply> selectAll(PolicyApply record);
    
    Map<String,Object> getNewestDetail(String memberId);
}