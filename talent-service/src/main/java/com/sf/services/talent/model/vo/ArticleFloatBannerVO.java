package com.sf.services.talent.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ArticleFloatBannerVO {

    @ApiModelProperty(value = "模块类型（1：文章，2：招聘会）")
    private Integer moduleTypeStatus;

    @ApiModelProperty(value = "图片地址")
    private String photoUrl;

    @ApiModelProperty(value = "模块id（t_article表id，t_job_fair表id）")
    private String moduleId;

    @ApiModelProperty(value = "banner链接地址")
    private String bannerLink;

    @ApiModelProperty(value = "备注")
    private String remark;

}