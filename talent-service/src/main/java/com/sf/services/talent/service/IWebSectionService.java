package com.sf.services.talent.service;


import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.ArticleUpdateParamModel;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.WebSectionParamModel;
import com.sf.services.talent.model.WebSectionUpdateParamModel;
import com.sf.services.talent.model.po.WebSection;
import com.sf.services.talent.model.vo.ArticleVO;

public interface IWebSectionService {


    /**
     * 查询列表
     * @param param
     * @return
     */
    PageInfo<WebSection> list(WebSectionParamModel param);

    /**
     * 查询
     * @param id
     * @return
     */
    WebSection get(String id);

    /**
     * 编辑
     * @param param
     * @param updateUser
     * @return
     */
    BaseResultModel update(WebSectionUpdateParamModel param, String updateUser);
}