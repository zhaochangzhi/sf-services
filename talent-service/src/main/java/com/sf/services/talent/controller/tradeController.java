package com.sf.services.talent.controller;

import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.TradeModel;
import com.sf.services.talent.service.ITradeService;
import com.sf.services.talent.util.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @ClassName: tradeController
 * @Description: 行业字典
 * @author: guchangliang
 * @date: 2021年7月15日 下午9:07:47
 */
@Slf4j
@Api(value = "行业字典", tags = "行业字典")
@RestController
@RequestMapping("/trade")
public class tradeController {

	@Resource
	private ITradeService tradeService;

	@ApiOperation(notes = "行业字典列表", value="行业字典列表")
	@ApiImplicitParam(name = "params", value = "行业字典模型", required = false, dataType = "TradeModel")
	@PostMapping("/list")
	public ResultUtil selectAll(@RequestBody TradeModel record) {
		return ResultUtil.success(tradeService.selectAll(record));
	}

	@IgnoreSecurity
	@ApiOperation(notes = "行业字典列表(包含全部二级数据)", value="行业字典列表")
	@ApiImplicitParam(name = "params", value = "行业字典模型", required = false, dataType = "TradeModel")
	@PostMapping("/allList")
	public ResultUtil allList(@RequestBody TradeModel record) {
		return ResultUtil.success(tradeService.selectAllList(record));
	}
}

