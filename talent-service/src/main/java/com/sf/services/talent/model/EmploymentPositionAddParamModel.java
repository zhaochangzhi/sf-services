package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 企业中心-职位管理-创建职位
 * @author guchangliang
 * @date 2021/7/13
 */
@ApiModel(value = "企业中心-职位管理-创建职位")
@Data
public class EmploymentPositionAddParamModel {

    @ApiModelProperty(value = "企业账号id（t_enterprise_user表id）", example = "28b20fbf454f4558ab4c507255906072")
    private String enterpriseUserId;

    @ApiModelProperty(value = "企业职位名称", example = "企业职位名称xxx")
    private String enterprisePositionName;

    @ApiModelProperty(value = "行业id（z_trade表id）（精确到二级数据）", example = "8f3d033b26f7449fac4c1e11127622e3")
    private String tradeTwoId;

    @ApiModelProperty(value = "职位id（z_position表id）（精确到三级数据）", example = "8f3d033b26f7449fac4c1e11127622a4")
    private String positionThreeId;

    @ApiModelProperty(value = "工作地点省id（t_area表id", example = "2")
    private Integer workAddressProvinceId;
    @ApiModelProperty(value = "工作地点市id（t_area表id）", example = "52")
    private Integer workAddressCityId;

    @ApiModelProperty(value = "薪资要求key（t_dictionary表type=SalaryRequirement数据dickey）", example = "2")
    private String salaryRequirementKey;


    @ApiModelProperty(value = "工作经验key（t_dictionary表type=WorkExp数据dickey）", example = "1")
    private String workExpKey;

    @ApiModelProperty(value = "学历要求key（t_dictionary表type=Education数据dickey", example = "1")
    private String educationKey;

    @ApiModelProperty(value = "职位描述", example = "职位描述text")
    private String positionDescribe;

    @ApiModelProperty(value = "招聘人数", example = "10")
    private Integer recruitNum;

    @ApiModelProperty(value = "福利待遇key（逗号分隔）（t_dictionary表type=Welfare数据dickey）", example = "01,02")
    private String welfareKey;

    @ApiModelProperty(value = "是否为热点职位（0：否，1：是）", example = "0")
    private Integer isHot;

    @ApiModelProperty(value = "审核状态：1 审核中，2 通过，3 不通过", example = "2")
    private Integer auditStatus;

}