package com.sf.services.talent.service.impl;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sf.services.talent.mapper.PolicyCategoryMapper;
import com.sf.services.talent.model.PolicyCategoryModel;
import com.sf.services.talent.model.po.PolicyCategory;
import com.sf.services.talent.service.IPolicyCategoryService;
import com.sf.services.talent.util.FillUserInfoUtil;


@Service
public class PolicyCategoryService implements IPolicyCategoryService {
	
	@Autowired
	private PolicyCategoryMapper policyCategoryMapper;
	
	@Override
	public int add(PolicyCategoryModel record) {
		PolicyCategory policyCategory = new PolicyCategory();
		BeanUtils.copyProperties(record, policyCategory);
		FillUserInfoUtil.fillCreateUserInfo(policyCategory);
		return policyCategoryMapper.insertSelective(policyCategory);
	}
	@Override
	public int delete(PolicyCategoryModel record) {
		
		return policyCategoryMapper.deleteByPrimaryKey(record.getId());
	}
	@Override
	public PageInfo<PolicyCategory> list(PolicyCategoryModel record) {
		PolicyCategory policyCategory = new PolicyCategory();
		BeanUtils.copyProperties(record, policyCategory);
		PageHelper.startPage(record.getPageNum(), record.getPageSize());
		List<PolicyCategory> list = policyCategoryMapper.selectAll(policyCategory);
		return new PageInfo<>(list);
	}
	@Override
	public int updateStatus(PolicyCategoryModel record) {
		PolicyCategory policyCategory = new PolicyCategory();
		BeanUtils.copyProperties(record, policyCategory);
		FillUserInfoUtil.fillUpdateUserInfo(policyCategory);
		return policyCategoryMapper.updateByPrimaryKeySelective(policyCategory);
	}
}

