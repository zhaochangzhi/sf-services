package com.sf.services.talent.annotations;

import java.lang.annotation.*;

/**
 * 忽略权限过滤
 *
 * @author zhaochangzhi
 * @date 2021/7/9
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IgnoreSecurity {
}