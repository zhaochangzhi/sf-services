package com.sf.services.talent.model.po;

public class ActLog extends BaseEntity{
	
	private String id;
	
	private String actUserId;
	
	private String actLogContect;
	
	private String actLogTitle;
	
	private String memo;
	
	private String actUserName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getActUserId() {
		return actUserId;
	}

	public void setActUserId(String actUserId) {
		this.actUserId = actUserId;
	}

	public String getActLogContect() {
		return actLogContect;
	}

	public void setActLogContect(String actLogContect) {
		this.actLogContect = actLogContect;
	}

	public String getActLogTitle() {
		return actLogTitle;
	}

	public void setActLogTitle(String actLogTitle) {
		this.actLogTitle = actLogTitle;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getActUserName() {
		return actUserName;
	}

	public void setActUserName(String actUserName) {
		this.actUserName = actUserName;
	}
	
	
}

