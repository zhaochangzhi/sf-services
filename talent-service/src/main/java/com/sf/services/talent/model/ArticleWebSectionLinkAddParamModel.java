package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@ApiModel(value = "栏目关联-添加")
@Data
public class ArticleWebSectionLinkAddParamModel {


    @ApiModelProperty(value = "逗号分隔所属栏目id", example = "1002,2001,3001")
    private String webSectionLinkId;



}