package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * @author guchangliang
 * @date 2021/9/22
 */
@ApiModel(value = "JobFairUserListParamModel", description = "招聘会详情-参与个人用户列表")
@Data
public class JobFairUserListParamModel extends PageParamModel {

    @ApiModelProperty(value = "主键id（t_job_fair表id）", example = "56b2f8f7e6d1494481050904aeeb75db")
    private String jobFairId;


}
