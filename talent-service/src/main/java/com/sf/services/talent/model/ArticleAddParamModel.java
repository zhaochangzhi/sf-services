package com.sf.services.talent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 文章管理
 * @author guchangliang
 * @date 2021/8/1
 */
@ApiModel(value = "文章管理-添加文章")
@Data
public class ArticleAddParamModel {

    @ApiModelProperty(value = "所属网站key（t_dictionary表type=Website数据key）", example = "1")
    private String websiteKey;

    @ApiModelProperty(value = "所属栏目key（t_web_section表id）", example = "1001")
    private String webSectionId;

    @ApiModelProperty(value = "类型标识（1：文章，2：图片，3：文件）", example = "1")
    private Integer typeStatus;

    @ApiModelProperty(value = "标题", example = "xxxx标题")
    private String title;

    @ApiModelProperty(value = "作者", example = "王xx")
    private String author;

    @ApiModelProperty(value = "关键词（逗号拼接）", example = "关键字1,关键字2")
    private String keywords;

    @ApiModelProperty(value = "来源", example = "xxx新闻网")
    private String datafrom;

    @ApiModelProperty(value = "导言", example = "导言xxx")
    private String introduction;

    @ApiModelProperty(value = "描述", example = "描述xxx")
    private String describetion;

    @ApiModelProperty(value = "内容", example = "内容xxx")
    private String content;

    @ApiModelProperty(value = "缩略图id", example = "0096ff5444014df48c6301751d1f0830")
    private String smallPhotoId;

    @ApiModelProperty(value = "大图id", example = "0096ff5444014df48c6301751d1f0830")
    private String bigPhotoId;

    @ApiModelProperty(value = "文件id", example = "0096ff5444014df48c6301751d1f0830")
    private String fileId;

    @ApiModelProperty(value = "链接", example = "www.xxx.com")
    private String link;

    @ApiModelProperty(value = "排序", example = "1")
    private Integer sort;

    @ApiModelProperty(value = "文件列表（文件id逗号分隔）")
    private String articleFileList;

    @ApiModelProperty(value = "是否为悬浮banner", example = "1")
    private Integer isArticleFloatBanner;
    @ApiModelProperty(value = "悬浮banner图片id", example = "0096ff5444014df48c6301751d1f0830")
    private String articleFloatBannerPhotoId;

    @ApiModelProperty(value = "更新时间/发布时间", example = "2021-07-10")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date updateTime;

}