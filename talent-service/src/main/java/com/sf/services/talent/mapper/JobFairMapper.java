package com.sf.services.talent.mapper;

import com.sf.services.talent.model.JobFairParamModel;
import com.sf.services.talent.model.dto.JobFairDto;
import com.sf.services.talent.model.po.JobFair;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * JobFairMapper
 *
 * @author zhaochangzhi
 * @date 2021/7/23
 */
@Mapper
public interface JobFairMapper {
    /**
     * 查询列表
     *
     * @param params 参数
     * @return 列表数据
     */
    List<JobFairDto> selectList(JobFairParamModel params);

    /**
     * 根据id查询详情
     *
     * @param id id
     * @return 详情
     */
    JobFairDto selectOneById(@Param("id") String id);

    /**
     * 新增
     *
     * @param params 参数
     * @return 结果
     */
    Integer insertOne(JobFair params);

    /**
     * 修改
     *
     * @param params 参数
     * @return 结果
     */
    Integer updateOne(JobFair params);
}
