package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 逗号分隔memberId字符串
 * @author guchangliang
 * @date 2021/8/1
 */
@ApiModel(value = "逗号分隔memberId字符串")
@Data
public class MemberIdsParamModel {

    @ApiModelProperty(value = "逗号分隔memberId字符串", example = "835a3e5fb9c842c594c2e7372bd60d26,165a3e5fb9c842c594c2e7372bd60d54")
    private String memberIds;


}