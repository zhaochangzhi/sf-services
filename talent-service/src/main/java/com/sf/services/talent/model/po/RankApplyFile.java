package com.sf.services.talent.model.po;

import lombok.Data;

/**
 * RankApplyFile
 *
 * @author zhaochangzhi
 * @date 2021/7/20
 */
@Data
public class RankApplyFile {

    private String id;

    private String rankApplyId;

    private String fileId;
    private String typeStatus;
}
