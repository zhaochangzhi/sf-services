package com.sf.services.talent.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.DictionaryModel;
import com.sf.services.talent.model.po.Dictionary;
import com.sf.services.talent.service.IDictionaryService;
import com.sf.services.talent.util.ResultUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: DictionaryController 
 * @Description: 数据字典
 * @author: heyang
 * @date: 2021年7月12日 下午9:07:47
 */
@Slf4j
@Api(value = "数据字典", tags = "数据字典")
@RestController
@RequestMapping("/dictionary")
public class DictionaryController {
	
	@Autowired
	private IDictionaryService dictionaryService;
	
	@ApiOperation(notes = "字典删除", value="字典删除")
	@ApiImplicitParam(name = "params", value = "字典模型", required = false, dataType = "DictionaryModel")
	@PostMapping("/delete")
	public ResultUtil delete(@RequestBody DictionaryModel record) {
		return ResultUtil.success(dictionaryService.delete(record.getId()));
	}

	@ApiOperation(notes = "字典插入", value="字典插入")
	@ApiImplicitParam(name = "params", value = "字典插入", required = false, dataType = "DictionaryModel")
	@PostMapping("/insert")
	public ResultUtil insert(@RequestBody DictionaryModel record) {
		return ResultUtil.success(dictionaryService.insert(record));
	}
	
	@ApiOperation(notes = "字典更新", value="字典更新")
	@ApiImplicitParam(name = "params", value = "字典更新", required = false, dataType = "DictionaryModel")
	@PostMapping("/update")
	public ResultUtil update(@RequestBody DictionaryModel record) {
		return ResultUtil.success(dictionaryService.update(record));
	}
	 
	@ApiOperation(notes = "字典列表", value="字典列表")
	@ApiImplicitParam(name = "params", value = "字典模型", required = false, dataType = "DictionaryModel")
	@PostMapping("/list")
	@IgnoreSecurity
	public ResultUtil selectAll(@RequestBody DictionaryModel record) {
		return ResultUtil.success(dictionaryService.selectAll(record));
	}
}

