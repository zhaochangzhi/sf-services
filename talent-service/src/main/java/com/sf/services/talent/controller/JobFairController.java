package com.sf.services.talent.controller;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.annotations.CurrentUser;
import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.common.MemberTypeEnum;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.dto.JobFairDto;
import com.sf.services.talent.model.dto.UserDto;
import com.sf.services.talent.model.vo.JobFairEmploymentVO;
import com.sf.services.talent.model.vo.JobFairUserVO;
import com.sf.services.talent.service.IJobFairService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * JobFairController
 *
 * @author zhaochangzhi
 * @date 2021/7/23
 */
@Api(value = "招聘会", tags = "招聘会")
@RestController
@RequestMapping("/jobfair")
public class JobFairController {

    private final IJobFairService jobFairService;

    @Autowired
    public JobFairController(IJobFairService jobFairService) {
        this.jobFairService = jobFairService;
    }

    @ApiOperation(value = "招聘会列表", notes = "招聘会列表")
    @PostMapping("/list")
    @IgnoreSecurity
    public BaseResultModel list(@RequestBody JobFairParamModel params) {

        PageInfo<JobFairDto> pageInfo = jobFairService.getList(params);
        return BaseResultModel.success(pageInfo);
    }

    @ApiOperation(value = "招聘会详情", notes = "招聘会详情")
    @PostMapping("/detail")
    @IgnoreSecurity
    public BaseResultModel detail(@RequestBody GetOneParamModel params) {
        if (StringUtils.isBlank(params.getId())) {
            return BaseResultModel.badParam("招聘会id不能为空");
        }
        return BaseResultModel.success(jobFairService.getOneById(params.getId()));
    }

    @ApiOperation(value = "删除招聘会", notes = "删除招聘会")
    @PostMapping("/delete")
    public BaseResultModel delete(@RequestBody JobFairParamModel params, @CurrentUser UserDto currentUser) {
        //校验用户类型 是否为企业用户
        BaseResultModel validateAdmin = validateAdmin(currentUser);
        if (validateAdmin.isBadParam()) {
            return validateAdmin;
        }
        if (StringUtils.isBlank(params.getId())) {
            return BaseResultModel.badParam("删除所需主键不能为空");
        }
        JobFairDto jobFairDto = new JobFairDto();
        jobFairDto.setId(params.getId());
        //设置deleted为1
        jobFairDto.setDeleted(1);
        jobFairDto.setUpdateUser(currentUser.getUserId());
        jobFairDto.setUpdateTime(new Date());
        return BaseResultModel.success(jobFairService.deleteOne(jobFairDto));
    }

    @ApiOperation(value = "新增招聘会", notes = "新增招聘会")
    @PostMapping("/add")
    public BaseResultModel add(@RequestBody JobFairParamModel params, @CurrentUser UserDto currentUser) {
        //校验用户类型 是否为企业用户
        BaseResultModel validateAdmin = validateAdmin(currentUser);
        if (validateAdmin.isBadParam()) {
            return validateAdmin;
        }
        BaseResultModel validateResult = validateParams(params);
        if (validateResult.isBadParam()) {
            return validateResult;
        }
        JobFairDto jobFairDto = new JobFairDto();
        BeanUtils.copyProperties(params, jobFairDto);
        jobFairDto.setCreateUser(currentUser.getUserId());
        jobFairDto.setUpdateUser(currentUser.getUserId());
        Date nowDate = new Date();
        jobFairDto.setCreateTime(nowDate);
        jobFairDto.setUpdateTime(nowDate);
        //设置可用状态
        jobFairDto.setStatus(1);
        return BaseResultModel.success(jobFairService.addOne(jobFairDto));
    }

    @ApiOperation(value = "修改招聘会保存", notes = "修改招聘会保存")
    @PostMapping("/update")
    public BaseResultModel update(@RequestBody JobFairParamModel params, @CurrentUser UserDto currentUser) {
        //校验用户类型 是否为企业用户
        BaseResultModel validateAdmin = validateAdmin(currentUser);
        if (validateAdmin.isBadParam()) {
            return validateAdmin;
        }
        BaseResultModel validateResult = validateParams(params);
        if (validateResult.isBadParam()) {
            return validateResult;
        }
        if (StringUtils.isBlank(params.getId())) {
            return BaseResultModel.badParam("修改所需主键不能为空");
        }
        JobFairDto jobFairDto = new JobFairDto();
        BeanUtils.copyProperties(params, jobFairDto);
        jobFairDto.setUpdateUser(currentUser.getUserId());
        jobFairDto.setUpdateTime(new Date());
        //设置可用状态
        jobFairDto.setStatus(1);
        return BaseResultModel.success(jobFairService.updateOne(jobFairDto));
    }

    /**
     * 校验用户是否为管理员用户
     *
     * @param currentUser 当前登录人信息
     * @return 结果
     */
    private BaseResultModel validateAdmin(UserDto currentUser) {
        //校验用户类型 是否为管理员用户
        if (StringUtils.isBlank(currentUser.getType())
                || !MemberTypeEnum.ADMIN.getType().equals(currentUser.getType())) {
            return BaseResultModel.forbidden("不是管理员用户，无权限操作");
        }
        return BaseResultModel.noData();
    }

    /**
     * 参数校验
     *
     * @param params 参数
     * @return 结果
     */
    private BaseResultModel validateParams(JobFairParamModel params) {
        if (StringUtils.isBlank(params.getTitle())) {
            return BaseResultModel.badParam("标题不能为空");
        }
        if (StringUtils.isBlank(params.getSponsor())) {
            return BaseResultModel.badParam("主办方不能为空");
        }
        if (StringUtils.isBlank(params.getAddress())) {
            return BaseResultModel.badParam("举办地址不能为空");
        }
        if (StringUtils.isBlank(params.getContacts())) {
            return BaseResultModel.badParam("联系人不能为空");
        }
        if (StringUtils.isBlank(params.getContactsPhone())) {
            return BaseResultModel.badParam("联系方式不能为空");
        }
        if (null == params.getStartTime()) {
            return BaseResultModel.badParam("开始时间不能为空");
        }
        if (null == params.getEndTime()) {
            return BaseResultModel.badParam("结束时间不能为空");
        }
        if (StringUtils.isBlank(params.getImageId())) {
            return BaseResultModel.badParam("推广图片不能为空");
        }
        return BaseResultModel.noData();
    }


    @IgnoreSecurity
    @ApiOperation(value = "招聘会详情-参与企业列表", notes = "招聘会详情-参与企业列表")
    @PostMapping("/enterprise/list")
    public BaseResultModel enterpriseList(@RequestBody JobFairEnterpriseParamModel params) {

        PageInfo<JobFairEmploymentVO> pageInfo = jobFairService.getEnterpriseList(params);
        return BaseResultModel.success(pageInfo);
    }

    @IgnoreSecurity
    @ApiOperation(value = "招聘会详情-参与企业详情", notes = "招聘会详情-参与企业详情")
    @PostMapping("/enterprise/get")
    public BaseResultModel enterpriseGet(@RequestBody JobFairEnterpriseGetParamModel params) {

        return jobFairService.getEnterpriseDetail(params);
    }

    @ApiOperation(value = "招聘会-企业报名参会", notes = "招聘会-企业报名参会")
    @PostMapping("/enterprise/add")
    public BaseResultModel enterpriseAdd(@RequestBody JobFairEnterpriseAddParamModel params, @CurrentUser UserDto currentUser) {

        return jobFairService.jobFairEnterprisePositionAdd(params, currentUser.getUserId());
    }

    @ApiOperation(notes = "招聘会-企业报名参会-审核", value="招聘会-企业报名参会-审核")
    @PostMapping("/enterprise/audit")
    public BaseResultModel audit(@RequestBody JobFairEnterpriseAuditParamModel param
            , @CurrentUser UserDto currentUser) {

        return jobFairService.audit(param, currentUser.getUserId());
    }


    @ApiOperation(value = "招聘会-个人用户报名参会", notes = "招聘会-个人用户报名参会")
    @PostMapping("/user/add")
    public BaseResultModel userAdd(@RequestBody JobUserAddParamModel params, @CurrentUser UserDto currentUser) {

        return jobFairService.jobFairUserAdd(params, currentUser.getUserId());
    }

    @IgnoreSecurity
    @ApiOperation(value = "招聘会详情-参与个人用户列表", notes = "招聘会详情-参与个人用户列表")
    @PostMapping("/user/list")
    public BaseResultModel userList(@RequestBody JobFairUserListParamModel params) {
        if (params == null) {
            return BaseResultModel.badParam("参数不能为空");
        }
        if(StringUtils.isBlank(params.getJobFairId())){
            return BaseResultModel.badParam("招聘会id不能为空");
        }

        PageInfo<JobFairUserVO> pageInfo = jobFairService.getUserList(params);
        return BaseResultModel.success(pageInfo);
    }

}
