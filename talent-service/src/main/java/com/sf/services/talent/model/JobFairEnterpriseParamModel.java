package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * @author guchangliang
 * @date 2021/8/3
 */
@ApiModel(value = "JobFairEnterpriseParamModel", description = "招聘会参数实体")
@Data
public class JobFairEnterpriseParamModel extends PageParamModel {

    @ApiModelProperty(value = "主键id（t_job_fair表id）", example = "56b2f8f7e6d1494481050904aeeb75db")
    private String id;


    @ApiModelProperty(value = "审核状态：1 审核中，2 通过，3 不通过", example = "2")
    private Integer auditStatus;


}
