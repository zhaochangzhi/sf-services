package com.sf.services.talent.management.controller;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.annotations.CurrentUser;
import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.mapper.DictionaryMapper;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.RankFileParamModel;
import com.sf.services.talent.model.RankParamModel;
import com.sf.services.talent.model.dto.RankDto;
import com.sf.services.talent.model.dto.RankFileDto;
import com.sf.services.talent.model.dto.UserDto;
import com.sf.services.talent.model.po.Dictionary;
import com.sf.services.talent.model.vo.RankFileVO;
import com.sf.services.talent.model.vo.RankVO;
import com.sf.services.talent.service.IRankService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;

/**
 * RankController
 *
 * @author zhaochangzhi
 * @date 2021/7/10
 */
@Api(value = "管理后台-职称管理", tags = "后台管理-职称管理")
@RestController(value = "ManageRankController")
@RequestMapping("/manage/rank")
public class RankController {

    private final IRankService rankService;

    @Autowired
    public RankController(IRankService rankService) {
        this.rankService = rankService;
    }
    @Resource
    private DictionaryMapper dictionaryMapper;

    @ApiOperation(value = "职称列表", notes = "职称列表")
    @PostMapping("/list")
    public BaseResultModel list(@RequestBody RankParamModel params) {
        PageInfo<RankDto> pageInfo = rankService.getList(params);
        return BaseResultModel.success(pageInfo);
    }

    @ApiOperation(value = "职称附件列表", notes = "职称附件列表")
    @PostMapping("/attachment/list")
    @IgnoreSecurity
    public BaseResultModel listWithAttachment(@RequestBody RankFileParamModel params) {
        PageInfo<RankFileVO> pageInfo = rankService.getListWithAttachment(params);
        return BaseResultModel.success(pageInfo);
    }

    @ApiOperation(value = "删除附件", notes = "删除附件")
    @PostMapping("/attachment/delete")
    public BaseResultModel deleteAttachment(@RequestBody RankFileVO params, @CurrentUser UserDto currentUser) {
        if (StringUtils.isBlank(params.getId())) {
            return BaseResultModel.badParam("id不能为空");
        }
        return BaseResultModel.success(rankService.deleteOneRankFile(params.getId()));
    }

    @ApiOperation(value = "职称详情", notes = "职称详情")
    @PostMapping("/detail")
    public BaseResultModel<RankVO> detail(@RequestBody RankVO params) {
        if (params.getId() == null || params.getId() == 0) {
            return BaseResultModel.badParam("id不能为空");
        }
        return BaseResultModel.success(rankService.getOneById(params.getId()));
    }

    @ApiOperation(value = "新增职称", notes = "新增职称")
    @PostMapping("/add")
    public BaseResultModel<Integer> add(@RequestBody RankVO params, @CurrentUser UserDto currentUser) {
        RankDto rankDto = new RankDto();
        BeanUtils.copyProperties(params, rankDto);
        rankDto.setCreateUser(currentUser.getUserId());
        rankDto.setCreateTime(new Date());
        return BaseResultModel.success(rankService.addOne(rankDto));
    }

    @ApiOperation(value = "删除职称", notes = "删除职称")
    @PostMapping("/delete")
    public BaseResultModel<Integer> delete(@RequestBody RankVO params, @CurrentUser UserDto currentUser) {
        if (params.getId() == null || params.getId() == 0) {
            return BaseResultModel.badParam("id不能为空");
        }
        return BaseResultModel.success(rankService.deleteOneById(params.getId()));
    }

    @ApiOperation(value = "修改职称", notes = "修改职称")
    @PostMapping("/update")
    public BaseResultModel<Integer> update(@RequestBody RankVO params, @CurrentUser UserDto currentUser) {
        RankDto rankDto = new RankDto();
        if (params.getId() == null || params.getId() == 0) {
            return BaseResultModel.badParam("id不能为空");
        }
        BeanUtils.copyProperties(params, rankDto);
        rankDto.setUpdateUser(currentUser.getUserId());
        rankDto.setUpdateTime(new Date());
        return BaseResultModel.success(rankService.updateOneById(rankDto));
    }

    @ApiOperation(value = "上传职称文件", notes = "上传职称文件")
    @PostMapping("/upload")
    public BaseResultModel upload(@RequestBody RankFileParamModel params, @CurrentUser UserDto currentUser) {

        if (StringUtils.isBlank(params.getRankCode()) || null == params.getFileId() || params.getFileId().length == 0 || StringUtils.isBlank(params.getRankApplyTypeKey()) ) {
            return BaseResultModel.badParam("请选择上传文件或者职称类别或职称申报方式不能为空");
        }

        //查询职称申报方式t_dictionary表type=RankApplyType
        Dictionary salaryRequirement = dictionaryMapper.selectOneByTypeAndDickey("RankApplyType",params.getRankApplyTypeKey());
        if (salaryRequirement == null) {
            return BaseResultModel.badParam("职称申报方式不存在");
        }

        RankFileDto rankFileDto = new RankFileDto();
        rankFileDto.setRankCode(params.getRankCode());
        rankFileDto.setFileId(params.getFileId());
        rankFileDto.setRankApplyTypeKey(params.getRankApplyTypeKey());
        rankFileDto.setRankApplyType(salaryRequirement.getValue());
        return BaseResultModel.success(rankService.uploadRankFile(rankFileDto));
    }
}
