package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 报名课程
 * @author guchangliang
 * @date 2021/8/13
 */
@Data
@ApiModel(description = "报名课程")
public class CourseApplyAddParamModel {

    @ApiModelProperty(value = "课程id（t_course表id）", example = "ae2d6aae9fa4443fa86a23cd3a0a9f68")
    private String courseId;

    @ApiModelProperty(value = "报名人员信息列表")
    private List<CourseApplyDetailAddParamModel> curseApplyDetailList;

}
