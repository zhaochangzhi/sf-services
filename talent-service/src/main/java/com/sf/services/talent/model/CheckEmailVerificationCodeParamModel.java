package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: guchangliang
 * @date: 2021年8月19日 下午9:43:50
 */
@Data
@ApiModel(value = "校验邮件验证码", description = "校验邮件验证码")
public class CheckEmailVerificationCodeParamModel {

	@ApiModelProperty(value = "用户名", example = "用户名xxx")
	private String userName;

	@ApiModelProperty(value = "验证码", example = "验证码")
	private String verificationCode;

}

