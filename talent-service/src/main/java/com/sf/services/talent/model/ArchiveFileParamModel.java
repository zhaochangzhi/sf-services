package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author guchangliang
 * @date 2021/8/9
 */
@ApiModel(value = "档案文件搜索")
@Data
public class ArchiveFileParamModel extends PageParamModel{





}