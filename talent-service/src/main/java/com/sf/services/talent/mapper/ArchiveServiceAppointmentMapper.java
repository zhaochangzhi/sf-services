package com.sf.services.talent.mapper;

import java.util.List;
import java.util.Map;

import com.sf.services.talent.model.po.ArchiveServiceAppointment;

public interface ArchiveServiceAppointmentMapper {
    int deleteByPrimaryKey(String id);

    int insert(ArchiveServiceAppointment record);

    int insertSelective(ArchiveServiceAppointment record);

    ArchiveServiceAppointment selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ArchiveServiceAppointment record);

    int updateByPrimaryKey(ArchiveServiceAppointment record);
    
    List<ArchiveServiceAppointment> selectAll(ArchiveServiceAppointment record);
    
    /**
     * @Title: getNewestDetail 
     * @Description: 获取最新一次详情
     * @return
     * @return: Map<String, Object>
     */
    Map<String, Object> getNewestDetail(String memberId);
}