package com.sf.services.talent.model.po;

import lombok.Data;

/**
 * RankMajor
 *
 * @author zhaochangzhi
 * @date 2021/7/20
 */
@Data
public class RankMajor {

    private String id;

    private String parentId;

    private String name;

    private Integer level;

    private Integer sort;
}
