package com.sf.services.talent.enums;

public enum InterfaceEnum {
	FIND_PERSION("找人才", "/resume/getList"),FIND_JOB("找工作","/enterprisePositionResume/positionList"),JOBFAIL("招聘会","/jobfair/list"),
	ARCHIVE("档案查询","/archiveServiceAppointment/getNewestDetail"),ARCHIVE_APPLY("档案预约","/archiveServiceAppointment/appointment"),
	RANK_APPLY("职称申报","/rankapply/add"),POLICY_APPLY("政策申报","/policyApply/apply");

	private InterfaceEnum(String name, String url) {
		this.name = name;
		this.url = url;
	}

	private String name;
	private String url;

	 public static String getName(String url) {  
	        for (InterfaceEnum c : InterfaceEnum.values()) {  
	            if (c.getUrl().equals(url)) {  
	                return c.name;  
	            }  
	        }  
	        return null;  
	    } 
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}
