package com.sf.services.talent.controller;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.annotations.CurrentUser;
import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.*;
import com.sf.services.talent.model.dto.UserDto;
import com.sf.services.talent.model.vo.CourseApplyVO;
import com.sf.services.talent.model.vo.CourseTypeVO;
import com.sf.services.talent.model.vo.CourseVO;
import com.sf.services.talent.service.ICourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@Slf4j
@Api(value = "课程", tags = "课程")
@RestController
@RequestMapping("/course")
public class CourseController {
	
	@Resource
	private ICourseService courseService;

	@ApiOperation(value = "查询课程列表", notes = "查询课程列表")
	@PostMapping("/list")
	@IgnoreSecurity
	public BaseResultModel list(@RequestBody CourseListParamModel params) {


		PageInfo<CourseVO> pageInfo = courseService.getList(params);
		return BaseResultModel.success(pageInfo);
	}

	@ApiOperation(value = "查询课程类型列表", notes = "查询类型列表")
	@PostMapping("/type/list")
	@IgnoreSecurity
	public BaseResultModel typeList(@RequestBody CourseTypeListParamModel param) {


		PageInfo<CourseTypeVO> pageInfo = courseService.getTypeList(param);
		return BaseResultModel.success(pageInfo);
	}

	@ApiOperation(value = "创建课程类型", notes = "创建课程类型")
	@PostMapping("/type/add")
	public BaseResultModel typeAdd(@RequestBody CourseTypeAddParamModel param, @CurrentUser UserDto currentUser) {


		return courseService.typeAdd(param, currentUser.getUserId());
	}

	@ApiOperation(notes = "更新课程类型", value="更新课程类型")
	@PostMapping("/type/update")
	public BaseResultModel typeUpdate(@RequestBody CourseTypeUpdateParamModel param
			, @CurrentUser UserDto currentUser) {

		return courseService.typeUpdate(param, currentUser.getUserId());
	}

	@ApiOperation(notes = "删除课程类型", value="删除课程类型")
	@PostMapping("/type/delete")
	public BaseResultModel typeDelete(@RequestBody GetOneParamModel param
			, @CurrentUser UserDto currentUser) {

		if(param == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if(StringUtils.isBlank(param.getId())){
			return BaseResultModel.badParam("id不能为空");
		}
		if (currentUser == null || StringUtils.isBlank(currentUser.getUserId())) {
			return BaseResultModel.badParam("请登陆");
		}

		return courseService.typeDelete(param.getId(), currentUser.getUserId());
	}

	@ApiOperation(notes = "创建课程", value="创建课程")
	@PostMapping("/add")
	public BaseResultModel add(@RequestBody CourseAddParamModel param
			, @CurrentUser UserDto currentUser) {

		return courseService.add(param, currentUser.getUserId());
	}

	@ApiOperation(notes = "更新课程", value="更新课程")
	@PostMapping("/update")
	public BaseResultModel update(@RequestBody CourseUpdateParamModel param
			, @CurrentUser UserDto currentUser) {

		return courseService.update(param, currentUser.getUserId());
	}


	@ApiOperation(value = "查询申请课程记录列表", notes = "查询申请课程记录列表")
	@PostMapping("/courseApply/list")
	public BaseResultModel courseApplyList(@RequestBody CourseApplyListParamModel params) {

		PageInfo<CourseApplyVO> pageInfo = courseService.getCourseApplyList(params);
		return BaseResultModel.success(pageInfo);
	}

	@ApiOperation(value = "查询申请课程记录详情", notes = "查询申请课程记录详情")
	@PostMapping("/courseApply/get")
	public BaseResultModel courseApplyGet(@RequestBody GetOneParamModel params) {

		if (params == null) {
			return BaseResultModel.badParam("传入参数不能为空");
		}
		if (StringUtils.isBlank(params.getId())) {
			return BaseResultModel.badParam("id不能为空");
		}

		return courseService.getCourseApply(params.getId());
	}

	@ApiOperation(value = "课程报名", notes = "课程报名")
	@PostMapping("/courseApply/add")
	public BaseResultModel courseApplyAdd(@RequestBody CourseApplyAddParamModel params, @CurrentUser UserDto currentUser) {


		return courseService.courseApplyAdd(params, currentUser.getUserId());
	}

	@ApiOperation(value = "课程报名-审核", notes = "课程报名-审核")
	@PostMapping("/courseApply/audit")
	public BaseResultModel courseApplyAudit(@RequestBody CourseApplyAuditParamModel params, @CurrentUser UserDto currentUser) {


		return courseService.courseApplyAudit(params, currentUser.getUserId());
	}


	@ApiOperation(value = "获取课程报名Excel内容", notes = "获取课程报名Excel内容")
	@PostMapping("/getCourseApplyExcelMessage")
	public BaseResultModel getCourseApplyExcelMessage(@RequestParam("file") MultipartFile file) {

		return courseService.getCourseApplyExcelMessage(file);
	}

	@ApiOperation(value = "查询课程报名最后一条数据", notes = "查询课程报名最后一条数据")
	@PostMapping("/getLast")
	public BaseResultModel getLast(@CurrentUser UserDto currentUser) {

		return courseService.getLast(currentUser.getUserId());
	}

	@ApiOperation(notes = "删除课程", value="删除课程")
	@PostMapping("/delete")
	public BaseResultModel delete(@RequestBody GetOneParamModel param
			, @CurrentUser UserDto currentUser) {

		if(param == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if(StringUtils.isBlank(param.getId())){
			return BaseResultModel.badParam("id不能为空");
		}
		if (currentUser == null || StringUtils.isBlank(currentUser.getUserId())) {
			return BaseResultModel.badParam("请登陆");
		}

		return courseService.delete(param.getId(), currentUser.getUserId());
	}

}

