package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 找工作-查询
 * @author guchangliang
 * @date 2021/7/26
 */
@ApiModel(value = "找工作-查询")
@Data
public class EmploymentPositionResumeParamModel extends PageParamModel{

    @ApiModelProperty(value = "会员id（t_member表id）", example = "ae2d6aae9fa4443fa86a23cd3a0a9f68")
    private String memberId;

    @ApiModelProperty(value = "企业职位名称", example = "企业职位名称xxx")
    private String enterprisePositionName;

    @ApiModelProperty(value = "行业一级id（z_trade表id）", example = "35265196130441b08a390400f1cbe3b0")
    private String tradeOneId;
    @ApiModelProperty(value = "行业二级id（z_trade表id）", example = "1a86896b8cda4eeaad437dae818b6b41")
    private String tradeTwoId;

    @ApiModelProperty(value = "职位一级id（z_position表id）", example = "04639e4e94da4b3792baf97d619243bb")
    private String positionOneId;
    @ApiModelProperty(value = "职位二级id（z_position表id）", example = "2829a908755d4cc5a039eb4800017ab5")
    private String positionTwoId;
    @ApiModelProperty(value = "职位三级id（z_position表id）", example = "1b3e616ceac941d59781254d8a6cb8f4")
    private String positionThreeId;

    @ApiModelProperty(value = "工作地点省id（t_area表id", example = "2")
    private Integer workAddressProvinceId;
    @ApiModelProperty(value = "工作地点市id（t_area表id）", example = "52")
    private Integer workAddressCityId;

    @ApiModelProperty(value = "薪资要求key（t_dictionary表type=SalaryRequirement数据dickey）", example = "2")
    private String salaryRequirementKey;

    @ApiModelProperty(value = "工作经验key（t_dictionary表type=WorkExp数据dickey）", example = "1")
    private String workExpKey;

    @ApiModelProperty(value = "学历要求key（t_dictionary表type=Education数据dickey", example = "1")
    private String educationKey;

    @ApiModelProperty(value = "福利待遇key（逗号分隔）（t_dictionary表type=Welfare数据dickey）", example = "01,02")
    private String welfareKey;

    @ApiModelProperty(value = "企业名称", example = "xxx有限公司")
    private String enterpriseName;
    @ApiModelProperty(value = "信用代码", example = "xxx")
    private String enterpriseLcreditCode;

    @ApiModelProperty(value = "企业性质key（字典t_dictionary表type=EnterpriseNature数据dickey", example = "1")
    private String enterpriseNatureKey;
    @ApiModelProperty(value = "企业规模key（字典t_dictionary表type=EnterpriseScale数据dickey））", example = "1")
    private String enterpriseScaleKey;

    @ApiModelProperty(value = "排序关键字（t_enterprise_position表字段名）（creat_time/update_time）", example = "update_time")
    private String sortKey;

    @ApiModelProperty(value = "是否为热点职位（0：否，1：是）", example = "1")
    private Integer isHot;
}