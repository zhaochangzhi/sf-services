package com.sf.services.talent.controller;

import com.sf.services.talent.annotations.CurrentUser;
import com.sf.services.talent.config.ThreadLocalCache;
import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.MemberIdsParamModel;
import com.sf.services.talent.model.UserParamModel;
import com.sf.services.talent.model.dto.UserDto;
import com.sf.services.talent.model.vo.MemberVO;
import com.sf.services.talent.service.IMerberService;
import com.sf.services.talent.service.impl.UserService;
import com.sf.services.talent.util.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api(value = "用户", tags = "用户")
@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private IMerberService merberService;
	
	@ApiOperation(notes = "用户信息", value="用户信息")
	@PostMapping("/get")
	public ResultUtil get() {
		
		return ResultUtil.success(userService.getUser());
		
	}
	
	@ApiOperation(notes = "用户更新", value="用户更新")
	@PostMapping("/update")
	public ResultUtil update(@RequestBody UserParamModel userParamModel) {
		
		return ResultUtil.success(userService.update(userParamModel));
		
	}

	@ApiOperation(value = "获取用户街道信息", notes = "获取用户街道信息")
	@PostMapping("/getUserStreet")
	public BaseResultModel getUserStreet() {
		String memberId = ThreadLocalCache.getUser();//memberId
		if (StringUtils.isBlank(memberId)) {
			return BaseResultModel.badParam("当前登陆账户memberId为空");
		}
		return BaseResultModel.success(userService.getUserStreet(memberId));
	}
	
	@ApiOperation(notes = "用户是否存在", value="用户是否存在")
	@PostMapping("/existUserName")
	public ResultUtil existUserName(@RequestBody UserParamModel userParamModel) {
		
		return ResultUtil.success(merberService.existUserName(userParamModel.getUserName()));
		
	}
	
	@ApiOperation(notes = "用户列表", value="用户列表")
	@PostMapping("/userList")
	public ResultUtil userList(@RequestBody UserParamModel userParamModel) {
		return ResultUtil.success(userService.list(userParamModel));
	}
	
	@ApiOperation(notes = "用户详情", value="用户详情")
	@PostMapping("/userDetail")
	public ResultUtil userDetail(@RequestBody UserParamModel userParamModel) {
		return ResultUtil.success(userService.getUserDetail(userParamModel.getId()));
	}
	
	@ApiOperation(notes = "用户注册", value="用户注册")
	@PostMapping("/registUser")
	public ResultUtil registUser(@RequestBody UserParamModel userParamModel) {
		MemberVO memberVO = new MemberVO();
		memberVO.setUserName(userParamModel.getLoginName());
		memberVO.setPassword(userParamModel.getPassword());
		memberVO.setType(userParamModel.getType());
		String memberId = merberService.register(memberVO);
		userParamModel.setMemberId(memberId);
		userService.registUser(userParamModel);
		return ResultUtil.success(memberId);
	}
	
	@ApiOperation(notes = "用户删除", value="用户删除")
	@PostMapping("/delUser")
	public ResultUtil delUser(@RequestBody UserParamModel userParamModel) {
		
		return ResultUtil.success(userService.delUser(userParamModel.getMemberId()));
	}


	@ApiOperation(notes = "批量删除用户", value="批量删除用户")
	@PostMapping("/batchDelete")
	public BaseResultModel batchDelete(@RequestBody MemberIdsParamModel param, @CurrentUser UserDto currentUser) {

		if(param == null){
			return BaseResultModel.badParam("参数不能为空");
		}
		if(StringUtils.isBlank(param.getMemberIds())){
			return BaseResultModel.badParam("会员id不能为空");
		}
		if (currentUser == null || StringUtils.isBlank(currentUser.getUserId())) {
			return BaseResultModel.badParam("请登陆");
		}

		return userService.batchDelete(param.getMemberIds(), currentUser.getUserId());
	}

}

