package com.sf.services.talent.model.vo;

import java.io.Serializable;

import lombok.Data;

@Data
public class PageVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8530224750690074554L;
	

	private int pageNum;
	
	private int pageSize;

}
