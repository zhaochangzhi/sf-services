package com.sf.services.talent.service;

import com.sf.services.talent.model.BaseResultModel;
import com.sf.services.talent.model.CheckEmailVerificationCodeParamModel;
import com.sf.services.talent.model.SendEmailVerificationCodeParamModel;
import com.sf.services.talent.model.SendMailParamModel;

/**
 * 发送邮件
 */
public interface IMailService {

    /**
     * 发送邮件
     * @param params
     * @return
     */
    public BaseResultModel send(SendMailParamModel params);

    /**
     * 发送邮件验证码
     * @param params
     * @return
     */
    public BaseResultModel sendEmailVerificationCode(SendEmailVerificationCodeParamModel params);

    /**
     * 校验邮件验证码
     * @param params
     * @return
     */
    public BaseResultModel checkEmailVerificationCode(CheckEmailVerificationCodeParamModel params);


}