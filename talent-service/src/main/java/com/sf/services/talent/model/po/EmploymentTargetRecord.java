package com.sf.services.talent.model.po;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class EmploymentTargetRecord {
    private String id;

    private String targetId;

    private Integer targetYear;

    private Integer targetMonth;

    private String target;

    private String targetOne;

    private String targetTwo;

    private String actual;

    private String actualOne;

    private String actualTwo;

    private String different;

    private String differentOne;

    private String differentTwo;

    private String streetKey;

    private String streetName;

    private String reportUser;

    private String remark;

    private Date creatTime;

    private String createUser;

    private Date updateTime;

    private String updateUser;

    private Integer isdelete;

    private String flag;

    @ApiModelProperty(value = "就业指标记录-附件列表")
    List<EmploymentTargetRecordFile> employmentTargetRecordFileList;

}