package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 找工作-申请
 * @author guchangliang
 * @date 2021/7/26
 */
@ApiModel(value = "找工作-申请")
@Data
public class EmploymentPositionResumeAddParamModel {

    @ApiModelProperty(value = "企业-职位id（t_enterprise_position表id）", example = "691bd23ba5174f7c8229f6e9b1168776")
    private String enterprisePositionId;

    @ApiModelProperty(value = "简历id（t_resume表id）", example = "56a44a6f4a62489cbbdd3fb6c9ff77f3")
    private String resumeId;

    @ApiModelProperty(value = "会员id（t_member表id）", example = "ae2d6aae9fa4443fa86a23cd3a0a9f68")
    private String memberId;

    @ApiModelProperty(value = "招聘会id（t_job_fair表id）", example = "2c5c35a39160484bb93454173ba34948")
    private String jobFairId;

}