package com.sf.services.talent.model.po;

import java.util.Date;

import lombok.Data;

@Data
public class Dictionary {
    private String id;

    private String type;

    private String dickey;

    private String value;

    private String memo;

    private Integer sort;

    private Date createDate;

    private String createUser;

    private Date updateDate;

    private String updateUser;

}