package com.sf.services.talent.model.po;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class ArchiveServiceCategory{
    private String id;

    private String name;

    private String processAddress;

    private String createUser;

    private Date createDate;

    private String updateUser;

    private Date updateDate;
    
    private String childName;
    
    private String handlingElements;
    
    List<Map<String,Object>> children;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getProcessAddress() {
        return processAddress;
    }

    public void setProcessAddress(String processAddress) {
        this.processAddress = processAddress == null ? null : processAddress.trim();
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

	public String getChildName() {
		return childName;
	}

	public void setChildName(String childName) {
		this.childName = childName;
	}

	public String getHandlingElements() {
		return handlingElements;
	}

	public void setHandlingElements(String handlingElements) {
		this.handlingElements = handlingElements;
	}

	public List<Map<String, Object>> getChildren() {
		return children;
	}

	public void setChildren(List<Map<String, Object>> children) {
		this.children = children;
	}
    
    
}