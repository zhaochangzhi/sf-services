package com.sf.services.talent.controller;

import com.sf.services.talent.annotations.IgnoreSecurity;
import com.sf.services.talent.model.PolicyFileModel;
import com.sf.services.talent.service.IPolicyFileService;
import com.sf.services.talent.util.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "政策文件", tags = "政策文件")
@RestController
@RequestMapping("/policyFile")
public class PolicyFileController {
	
	@Autowired
	private IPolicyFileService policyFileService;
	
	/**
	 * @Title: list 
	 * @Description:政策文件列表
	 * @param PolicyFileModel
	 * @return
	 * @return: ResultUtil
	 */
	@ApiOperation(notes = "政策文件列表", value="政策文件列表")
	@ApiImplicitParam(name = "params", value = "政策文件模型", required = false, dataType = "PolicyFileModel")
	@PostMapping("/list")
	@IgnoreSecurity
	public ResultUtil list(@RequestBody PolicyFileModel PolicyFileModel) {
		return ResultUtil.success(policyFileService.list(PolicyFileModel));
	}
	
	/**
	 * @Title: appointment 
	 * @Description:政策文件添加
	 * @param PolicyFileModel
	 * @return
	 * @return: ResultUtil
	 */
	@ApiOperation(notes = "政策文件添加", value="政策文件添加")
	@ApiImplicitParam(name = "params", value = "政策文件模型", required = false, dataType = "PolicyFileModel")
	@PostMapping("/add")
	public ResultUtil appointment(@RequestBody PolicyFileModel PolicyFileModel) {
		
		return ResultUtil.success(policyFileService.add(PolicyFileModel));
	}
	
	/**
	 * 政策文件删除
	 * @param PolicyFileModel
	 * @return
	 */
	@ApiOperation(notes = "政策文件删除", value="政策文件删除")
	@ApiImplicitParam(name = "params", value = "政策文件模型", required = false, dataType = "PolicyFileModel")
	@PostMapping("/delete")
	public ResultUtil delete(@RequestBody PolicyFileModel PolicyFileModel) {
		return ResultUtil.success(policyFileService.delete(PolicyFileModel));
		
	}
	
	/**
	 * @Title: updateStatus 
	 * @Description: 政策文件状态
	 * @param PolicyFileModel
	 * @return
	 * @return: ResultUtil
	 */
	@ApiOperation(notes = "政策文件列表", value="政策文件列表")
	@ApiImplicitParam(name = "params", value = "政策文件模型", required = false, dataType = "PolicyFileModel")
	@PostMapping("/updateStatus")
	public ResultUtil updateStatus(@RequestBody PolicyFileModel PolicyFileModel) {
		return ResultUtil.success(policyFileService.updateStatus(PolicyFileModel));
	}
}

