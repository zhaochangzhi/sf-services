package com.sf.services.talent.service;

import com.github.pagehelper.PageInfo;
import com.sf.services.talent.model.PositionModel;
import com.sf.services.talent.model.TradeModel;
import com.sf.services.talent.model.po.Position;
import com.sf.services.talent.model.po.Trade;

/**
 * @ClassName: ITradeService
 * @Description: 行业数据字典接口
 * @author: guchangliang
 * @date: 2021年7月15日 下午8:45:27
 */
public interface ITradeService {
	

	 
	 PageInfo<Trade> selectAll(TradeModel record);


	/**
	 * 查询全部数据列表（二级数据）
	 * @param record
	 * @return
	 */
	PageInfo<Trade> selectAllList(TradeModel record);
	
}

