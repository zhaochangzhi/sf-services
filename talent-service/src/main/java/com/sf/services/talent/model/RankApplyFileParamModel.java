package com.sf.services.talent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 职称申请文件参数实体
 *
 * @author zhaochangzhi
 * @date 2021/7/120
 */
@Data
@ApiModel(value = "RankApplyFileParamModel", description = "职称申请文件参数实体")
public class RankApplyFileParamModel {

    @ApiModelProperty(value = "文件id")
    private String fileId;
    @ApiModelProperty(value = "附件类型标识（1：第一学历材料，2：第二学历材料，3：第三学历材料，4：评审类型已取得证书附件，5：其它，6：论文证明附件）")
    private String typeStatus;
}
